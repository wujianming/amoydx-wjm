#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   MC3.Filter.WES_StopGained.Panel_nofilter.py
@Time    :   2019/08/30 09:59:46
@Author  :   Wu JianMing 
@Version :   1.0
@Contact :   wujm@amoydx.com
'''

from __future__ import division
import argparse
import pandas as pd
import os
from scipy import stats
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split  #交叉验证
from sklearn.linear_model import LinearRegression


def run(workdir):
  #默认的输入文件为/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/MC3.Filter.maf。 该文件除了未过滤肿瘤纯度低、功能区之外，其他都按照FOCR条件过滤。 
  ##过滤肿瘤纯度低的样本。
  os.chdir(workdir)
#   Low_purity = []
#   with open('/amoydx/USER/wujianming/project/Jupyter/DATA/low_purity_tumor_CPE','r') as FA:
#       for line in FA:
#           line = line.strip()
#           Low_purity.append(line)
#   f = open('MC3.Filter.purity.maf','w')
#   with open('/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/MC3.Filter.maf','r') as FB:
#     for lines in FB:
#         if 'Hugo_Symbol' in lines:
#             f.write(lines)
#             continue
#         line = lines.strip().split('\t')
#         Name = line[15].split('-')[:4]
#         linkName = "-".join(str(i) for i in Name)
#         if linkName in Low_purity:
#             continue
#         f.write(lines)
#   f.close()


  ##过滤功能区，分两类，一类用于计算WES TMB，另一类计算Panel TMB
  data = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/MC3.Filter.maf",sep='\t')
  retain_WES_fuction_list = ['inframe_deletion','inframe_insertion','missense_variant','frameshift_variant','stop_gained']
  retain_Syn_fuction_list = ['inframe_deletion','inframe_insertion','synonymous_variant','missense_variant','splice_acceptor_variant','splice_donor_variant','splice_region_variant','frameshift_variant']
  mafData_filter_variant_Syn_function = data[data['Consequence'].isin(retain_Syn_fuction_list)]
  mafData_filter_variant_noSyn_function = data[data['Consequence'].isin(retain_WES_fuction_list)]
  mafData_filter_variant_noSyn_function.to_csv("TCGA.WES.noSyn.maf",sep='\t',index=False)
  mafData_filter_variant_Syn_function.to_csv("TCGA.WES.Syn.maf",sep='\t',index=False)

  os.system(" cat /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/MC3.Filter.maf|awk '{print \"chr\"$5\"\t\"$6\"\t\"$7\"\t\"$12\"\t\"$13\"\t\"$16}' > TCGA.WES.Syn.bed ")
  os.system(" sed -i '/chrChromosome/d' TCGA.WES.Syn.bed ")
  os.system(" cat TCGA.WES.noSyn.maf|awk '{print \"chr\"$5\"\t\"$6\"\t\"$7\"\t\"$12\"\t\"$13\"\t\"$16}' > TCGA.WES.noSyn.bed ")
  os.system(" sed -i '/chrChromosome/d' TCGA.WES.noSyn.bed ")
  os.system(" bedtools intersect -a TCGA.WES.Syn.bed -b /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/ADXREG.TMB448V2.CDS20.GRCh37_v1.0.1.bed > TCGA.TMB.Panel.HotSpot.txt ")
  os.system(" bedtools intersect -a TCGA.TMB.Panel.HotSpot.txt -b /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/lung_mela.hotspot.txt -v > TCGA.TMB.Panel.noHotSpot.txt ")

  ###计算WES 的TMB值
  WES_data = pd.read_table("TCGA.WES.noSyn.bed",sep="\t",names=['chrom','start','end','ref','alt','Tumor_Sample_Barcode'])
  count = WES_data.groupby('Tumor_Sample_Barcode',as_index=None).count()
  count['TCGA_TMB_value'] = round(count['end'] / 30,2)
  TCGA_data = count[['Tumor_Sample_Barcode','end','TCGA_TMB_value']]
  TCGA_data.to_csv("TCGA.WES.TMB.txt",sep='\t',index=False,header =['Tumor_Sample_Barcode','WES_SNV_num','WES_TMB'])


  ###计算Panel 的TMB值
  data2 = pd.read_csv("TCGA.TMB.Panel.noHotSpot.txt",sep='\t',header=None,names=['chrom','start','end','ref','alt','Tumor_Sample_Barcode'])
  group_data =data2.groupby('Tumor_Sample_Barcode',as_index=None).count()
  group_data['Panel_TMB'] = group_data['chrom'] 
  TMB_data = group_data[['Tumor_Sample_Barcode','Panel_TMB']]
  TMB_data['Panel_TMB'] = TMB_data['Panel_TMB'] / 1.46

  ###merge,得到spearman和r2系数
  WES_TMB_value = pd.read_table("TCGA.WES.TMB.txt",sep='\t')
  merge_data = pd.merge(TMB_data,WES_TMB_value,on='Tumor_Sample_Barcode',how='inner')
  sp1,p1 = stats.spearmanr(merge_data.WES_TMB,merge_data.Panel_TMB)
  r = r2_score(merge_data.Panel_TMB,merge_data.WES_TMB)

  ###区分癌种来计算，只列举10个癌种，LUAD、LUSC、BLCA、BRCA、CODA、ESCA、HNSC、SKCM、LIHC、STAD
  with open('/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/title.txt', 'r') as TITLE:
    for line in TITLE:
      LUAD_FILE = open('LUAD.maf','w')
      LUAD_FILE.write(line)
      LUSC_FILE = open('LUSC.maf','w')
      LUSC_FILE.write(line)
      BLCA_FILE = open('BLCA.maf','w')
      BLCA_FILE.write(line)
      BRCA_FILE = open('BRCA.maf','w')
      BRCA_FILE.write(line)
      COAD_FILE = open('COAD.maf','w')
      COAD_FILE.write(line)
      ESCA_FILE = open('ESCA.maf','w')
      ESCA_FILE.write(line)
      HNSC_FILE = open('HNSC.maf','w')
      HNSC_FILE.write(line)
      SKCM_FILE = open('SKCM.maf','w')
      SKCM_FILE.write(line)
      LIHC_FILE = open('LIHC.maf','w')
      LIHC_FILE.write(line)
      STAD_FILE = open('STAD.maf','w')
      STAD_FILE.write(line)

  code_Cancer_dict = {}
  with open('/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/code_CancerType.txt', 'r') as CC:
      for line in CC:
          line = line.strip().split('\t')
          code_Cancer_dict[line[0]] = line[1]
  with open('/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/MC3.Filter.maf','r') as CANCER:
      for cons in CANCER:
          if 'Entrez_Gene_Id' in cons:
              continue
          con = cons.strip().split('\t')
          code = con[15].split('-')[1]
          if code_Cancer_dict[code] == 'LUAD' :
              LUAD_FILE.write("\t".join(str(i) for i in con)+'\n')
          elif code_Cancer_dict[code] == 'LUSC' :
              LUSC_FILE.write("\t".join(str(i) for i in con)+'\n')       
          elif code_Cancer_dict[code] == 'BLCA' :
              BLCA_FILE.write("\t".join(str(i) for i in con)+'\n')        
          elif code_Cancer_dict[code] == 'BRCA' :
              BRCA_FILE.write("\t".join(str(i) for i in con)+'\n')    
          elif code_Cancer_dict[code] == 'COAD' :
              COAD_FILE.write("\t".join(str(i) for i in con)+'\n')    
          elif code_Cancer_dict[code] == 'ESCA' :
              ESCA_FILE.write("\t".join(str(i) for i in con)+'\n')    
          elif code_Cancer_dict[code] == 'HNSC' :
              HNSC_FILE.write("\t".join(str(i) for i in con)+'\n')
          elif code_Cancer_dict[code] == 'SKCM' :
              SKCM_FILE.write("\t".join(str(i) for i in con)+'\n')
          elif code_Cancer_dict[code] == 'LIHC' :
              LIHC_FILE.write("\t".join(str(i) for i in con)+'\n')
          elif code_Cancer_dict[code] == 'STAD' :
              STAD_FILE.write("\t".join(str(i) for i in con)+'\n')

  LUAD_FILE.close()
  LUSC_FILE.close()
  BLCA_FILE.close()
  BRCA_FILE.close()
  COAD_FILE.close()
  ESCA_FILE.close()
  HNSC_FILE.close()
  SKCM_FILE.close()
  LIHC_FILE.close()
  STAD_FILE.close()


  os.system("for i in $(cat /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/allcancerlist);do cat $i.maf|awk '{print \"chr\"$5\"\t\"$6\"\t\"$7\"\t\"$12\"\t\"$13\"\t\"$16}' > $i.Syn.bed;done")
  os.system("for i in $(cat /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/allcancerlist );do sed -i '/chrChromosome/d' $i.Syn.bed;done ")
  os.system("for i in $(cat /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/allcancerlist);do bedtools intersect -a $i.Syn.bed -b /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/ADXREG.TMB448V2.CDS20.GRCh37_v1.0.1.bed  > $i.Syn.Panel.txt ;done")
  os.system("for i in $(cat /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/allcancerlist );do bedtools intersect -a $i.Syn.Panel.txt -b /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/lung_mela.hotspot.txt -v > $i.Syn.Panel.noHotSpot.txt;done")
  os.system("for i in $(cat /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/allcancerlist );do python /amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/TMB_distri_argv.py mutations.$i.noHotSpot.csv $i.Syn.Panel.txt ;done ")

  f2 = open("All.Cancer.spearman.r.txt",'w')
  f2.write('CANCER'+'\t'+'Spearman'+'\t'+'noCorr'+'\t'+'Corr'+'\n')
  with open('/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter_method1/allcancerlist','r') as FA:
      for line in FA:
          cancerType = line.strip()
          data = pd.read_csv("mutations."+cancerType+".noHotSpot.csv",sep='\t',header=None,names=['chrom','start','end','num'])
          #先直接计算LUAD的panel TMB值，不区分high low region
          CANCER_data = pd.read_table(cancerType+'.Syn.Panel.noHotSpot.txt',sep='\t',names=['chrom','start','end','ref','alt','Tumor_Sample_Barcode'])
          CANCER_group_data =CANCER_data.groupby('Tumor_Sample_Barcode',as_index=None).count()
          CANCER_group_data['Panel_TMB'] = CANCER_group_data['chrom'] 
          CANCER_TMB_data = CANCER_group_data[['Tumor_Sample_Barcode','Panel_TMB']]
          CANCER_TMB_data['Panel_TMB'] = CANCER_TMB_data['Panel_TMB'] / 1.46

          WES_TMB_value = pd.read_table("TCGA.WES.TMB.txt",sep='\t')
          merge_data = pd.merge(CANCER_TMB_data,WES_TMB_value,on='Tumor_Sample_Barcode',how='inner')

          spear,p1 = stats.spearmanr(merge_data.WES_TMB,merge_data.Panel_TMB)
          r1 = r2_score(merge_data.Panel_TMB,merge_data.WES_TMB)
          X = merge_data[['Panel_TMB']]
          y = merge_data['WES_TMB']
          X_train,X_test, y_train, y_test = train_test_split(X, y, random_state=2,test_size=0.8)
          linreg = LinearRegression()
          model=linreg.fit(X_train, y_train)
          corr = linreg.coef_[0]
          intercept = linreg.intercept_
          merge_data['new_Panel_TMB'] = merge_data['Panel_TMB'] * corr  + intercept
          r2 = r2_score(merge_data.new_Panel_TMB,merge_data.WES_TMB)
          f2.write(cancerType+'\t'+str(spear)+'\t'+str(r1)+'\t'+str(r2)+'\n')
  f2.write("ALL_Spearman: %s \t ALL_R2: %s " % (str(sp1),str(r)))
  f2.close()


if __name__ == '__main__':
  parser = argparse.ArgumentParser()     #建立解析器
  parser.add_argument("-d", "--workdir",  help="work dir", required=True)
  args = parser.parse_args()
  run(args.workdir)
