#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd
import os
from scipy import stats
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split  
from sklearn.linear_model import LinearRegression


# In[67]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
os.chdir("/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain")
CANCER_data = pd.read_table('/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain/NSCLC.Syn.Panel.noHotSpot.txt',sep='\t',names=['chrom','start','end','ref','alt','Tumor_Sample_Barcode'])
CANCER_group_data =CANCER_data.groupby('Tumor_Sample_Barcode',as_index=None).count()
CANCER_group_data['Panel_TMB_Syn'] = CANCER_group_data['chrom'] 
CANCER_TMB_data = CANCER_group_data[['Tumor_Sample_Barcode','Panel_TMB_Syn']]
CANCER_TMB_data['Panel_TMB_Syn'] = CANCER_TMB_data['Panel_TMB_Syn'] / 1.16
WES_TMB_value = pd.read_table("TCGA.WES.TMB.txt",sep='\t')
merge_data2 = pd.merge(CANCER_TMB_data,WES_TMB_value,on='Tumor_Sample_Barcode',how='inner')
sp1,pp1 = stats.spearmanr(merge_data2.TCGA_TMB_30_value,merge_data2.Panel_TMB_Syn)
sp1 = round(sp1,4)
plt.scatter(merge_data2.TCGA_TMB_30_value,merge_data2.Panel_TMB_Syn,s=4)
plt.xlabel("WES TMB")
plt.ylabel("Panel TMB")
plt.title('Syn noHotSpot')
plt.text(0,80,"Spearman: "+str(sp1))
plt.figure(dpi=80)


# In[65]:


os.chdir("/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain")
CANCER_data = pd.read_table('/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain/NSCLC.noSyn.Panel.noHotSpot.txt',sep='\t',names=['chrom','start','end','ref','alt','Tumor_Sample_Barcode'])
CANCER_group_data =CANCER_data.groupby('Tumor_Sample_Barcode',as_index=None).count()
CANCER_group_data['Panel_TMB_noSyn'] = CANCER_group_data['chrom'] 
CANCER_TMB_data = CANCER_group_data[['Tumor_Sample_Barcode','Panel_TMB_noSyn']]
CANCER_TMB_data['Panel_TMB_noSyn'] = CANCER_TMB_data['Panel_TMB_noSyn'] / 1.16
WES_TMB_value = pd.read_table("TCGA.WES.TMB.txt",sep='\t')
merge_data2 = pd.merge(CANCER_TMB_data,WES_TMB_value,on='Tumor_Sample_Barcode',how='inner')
sp4,pp4 = stats.spearmanr(merge_data2.TCGA_TMB_30_value,merge_data2.Panel_TMB_noSyn)

sp4 = round(sp4,4)
plt.scatter(merge_data2.TCGA_TMB_30_value,merge_data2.Panel_TMB_noSyn,s=4)
plt.xlabel("WES TMB")
plt.ylabel("Panel TMB")
plt.title('noSyn noHotSpot')
plt.text(0,50,"Spearman: "+str(sp4))
plt.figure(dpi=80)


# In[62]:


os.chdir("/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain")
CANCER_data = pd.read_table('/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain/NSCLC.Syn.Panel.HotSpot.txt',sep='\t',names=['chrom','start','end','ref','alt','Tumor_Sample_Barcode'])
CANCER_group_data =CANCER_data.groupby('Tumor_Sample_Barcode',as_index=None).count()
CANCER_group_data['Panel_TMB_Syn'] = CANCER_group_data['chrom'] 
CANCER_TMB_data = CANCER_group_data[['Tumor_Sample_Barcode','Panel_TMB_Syn']]
CANCER_TMB_data['Panel_TMB_Syn'] = CANCER_TMB_data['Panel_TMB_Syn'] / 1.16
WES_TMB_value = pd.read_table("TCGA.WES.TMB.txt",sep='\t')
merge_data2 = pd.merge(CANCER_TMB_data,WES_TMB_value,on='Tumor_Sample_Barcode',how='inner')
sp2, pp2 = stats.spearmanr(merge_data2.TCGA_TMB_30_value,merge_data2.Panel_TMB_Syn)

sp2 = round(sp2,4)
plt.scatter(merge_data2.TCGA_TMB_30_value,merge_data2.Panel_TMB_Syn,s=3)
plt.xlabel("WES TMB")
plt.ylabel("Panel TMB")
plt.title('Syn HotSpot')
plt.text(0,80,"Spearman: "+str(sp2))
plt.figure(dpi=80)


# In[66]:


os.chdir("/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain")
CANCER_data = pd.read_table('/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain/NSCLC.noSyn.Panel.HotSpot.txt',sep='\t',names=['chrom','start','end','ref','alt','Tumor_Sample_Barcode'])
CANCER_group_data =CANCER_data.groupby('Tumor_Sample_Barcode',as_index=None).count()
CANCER_group_data['Panel_TMB_noSyn'] = CANCER_group_data['chrom'] 
CANCER_TMB_data = CANCER_group_data[['Tumor_Sample_Barcode','Panel_TMB_noSyn']]
CANCER_TMB_data['Panel_TMB_noSyn'] = CANCER_TMB_data['Panel_TMB_noSyn'] / 1.16
WES_TMB_value = pd.read_table("TCGA.WES.TMB.txt",sep='\t')
merge_data2 = pd.merge(CANCER_TMB_data,WES_TMB_value,on='Tumor_Sample_Barcode',how='inner')
sp3,pp3 = stats.spearmanr(merge_data2.TCGA_TMB_30_value,merge_data2.Panel_TMB_noSyn)

sp3 = round(sp3,4)
plt.scatter(merge_data2.TCGA_TMB_30_value,merge_data2.Panel_TMB_noSyn,s=4)
plt.xlabel("WES TMB")
plt.ylabel("Panel TMB")
plt.title('noSyn HotSpot')
plt.text(0,50,"Spearman: "+str(sp3))
plt.figure(dpi=80)


# In[29]:


os.chdir("/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain")
High_Mut_cutoff = 5
data = pd.read_table('/amoydx/USER/wujianming/project/Jupyter/DATA/MC3_Filter_artifacts_splicing-stopgain/NSCLC.Syn.Panel.noHotSpot.txt',sep='\t',names=['chrom','start','end','ref','alt','Tumor_Sample_Barcode'])
group_data = data.groupby('Tumor_Sample_Barcode',as_index=None).count()
high_mutData = group_data[group_data['chrom'] >= High_Mut_cutoff]
high_mutData['TMB_high_value']  = high_mutData['chrom'] / 1.46
high_mutData2 = high_mutData[['Tumor_Sample_Barcode','TMB_high_value']]

low_mutData = group_data[group_data['chrom'] < High_Mut_cutoff]
low_mutData['region_len'] = low_mutData['end'] - low_mutData['start']
low_mutData['TMB_low_value']  = low_mutData['chrom'] / 1.46
low_mutData2 =low_mutData[['Tumor_Sample_Barcode','TMB_low_value']]

#MERGE HiGH LOW
Panel_Merge = pd.merge(high_mutData2,low_mutData2,on='Tumor_Sample_Barcode',how='outer')
Panel_Merge = Panel_Merge.fillna(0)
WES_TMB_value = pd.read_table("TCGA.WES.TMB.txt",sep='\t')
mergeData = pd.merge(Panel_Merge,WES_TMB_value,on = 'Tumor_Sample_Barcode')

tranData = mergeData.sample(frac=0.8,replace=False,random_state = None,axis=0)
#纵向合并之后，去除所有重复，剩下的作为测试集
testData = pd.concat([tranData,mergeData])
testData = testData.drop_duplicates(subset=['Tumor_Sample_Barcode'],keep=False)
tranList = []
for index, row in tranData.iterrows():
    tranList.append([row['TMB_high_value'],row['TMB_low_value']])
tranRes = tranData['TCGA_TMB_30_value'].tolist()
#
from sklearn import linear_model
reg = linear_model.LinearRegression()
reg.fit(tranList,tranRes)
high_value, low_value = reg.coef_
#compute Panel TMB value
mergeData['new_Panel_TMB'] = mergeData['TMB_high_value'] * high_value + mergeData['TMB_low_value'] * low_value
sp,p1 = stats.spearmanr(mergeData.TCGA_TMB_30_value,mergeData.new_Panel_TMB)
per,p2 = stats.pearsonr(mergeData.TCGA_TMB_30_value,mergeData.new_Panel_TMB)
r = r2_score(mergeData.TCGA_TMB_30_value,mergeData.new_Panel_TMB)
print(str(sp)+'\t'+str(per)+'\t'+str(r))


# In[ ]:





# In[ ]:




