#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
pd.set_option('mode.chained_assignment',None)


# In[2]:


mafData = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/mc3.v0.2.8.PUBLIC.maf",sep="\t")


# In[3]:


all_maf = mafData.groupby('Tumor_Sample_Barcode',as_index=None).count()
pass_maf = mafData[mafData['FILTER']=='PASS']
pass_data = pass_maf.groupby('Tumor_Sample_Barcode',as_index=None).count()


# In[4]:


all_maf_data = all_maf[['Tumor_Sample_Barcode','FILTER']]
pass_maf_data = pass_data[['Tumor_Sample_Barcode','FILTER']]


# In[5]:


merge_data = pd.merge(all_maf_data,pass_maf_data,on='Tumor_Sample_Barcode',how='outer')


# In[6]:


#all_maf.shape[0]


# In[7]:


#第一个是删除某个样本没有PASS
#第二是删除PASS少于50%的样本，FILTER_x是全部tag，FILTER_y是有PASS的样本位点数
merge_data['per'] = merge_data['FILTER_y'] / merge_data['FILTER_x']


# In[8]:


merge_data_fill = merge_data.fillna('KONG')
FILTER1 = merge_data_fill[merge_data_fill['per'] == 'KONG']['Tumor_Sample_Barcode'].tolist()
FILTER2 = merge_data.dropna()
FILTER2 = FILTER2[FILTER2['per'] <= 0.5]['Tumor_Sample_Barcode'].tolist()


# In[9]:


FILTER_SAMPLE = FILTER1+FILTER2              ##此为Remove samples with greater than 50% of the variants identified as artifacts
#10295-1753=10142

# In[11]:

mafData = mafData[~mafData['Tumor_Sample_Barcode'].isin(FILTER_SAMPLE)]


# In[12]:

mafData_filter_artifacts = mafData[mafData['FILTER'] == 'PASS']  #Remove variant artifacts


# In[17]:


mafData_filter_variant_count = mafData_filter_artifacts[mafData_filter_artifacts['t_alt_count'] >= 3] #Remove variant with variant count < 3



# In[19]:


mafData_filter_variant_count['Freq'] = mafData_filter_variant_count['t_alt_count'] / mafData_filter_variant_count['t_depth']
mafData_filter_freq = mafData_filter_variant_count[mafData_filter_variant_count['Freq'] >= 0.05]#Remove variant with allele frequency < 5%


# In[24]:


mafData_filter_total_depth = mafData_filter_freq[mafData_filter_freq['t_depth'] >= 25]

# In[21]:


#mafData_filter_total_depth.groupby('Variant_Classification',as_index=None).count()
#filter cosmic
#mafData_filter_cosmic = mafData_filter_total_depth[mafData_filter_total_depth['COSMIC'] == "NONE"]



# In[25]:


dbsnp_list=[]
with open('/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/dbsnp_138.hg19.vcf','r') as FA:
    for line in FA:
        if '#' in line:
            continue
        dbsnp_list.append(line[2])

mafData_filter_dbsnp = mafData_filter_total_depth[~mafData_filter_total_depth['dbSNP_RS'].isin(dbsnp_list)]


# In[27]:


mafData_filter_dbsnp.to_csv("/amoydx/USER/wujianming/project/Jupyter/DATA/TCGA_Filter/MC3.Filter.maf",sep='\t',index=False)
