#!/usr/bin/env nextflow
params.tumor_fq1 = '/mnt/md0/wjm/raw_datas/ranshi/NGS/RS1711368TIS_S49_R1_001.fastq.gz'
params.tumor_fq2 = '/mnt/md0/wjm/raw_datas/ranshi/NGS/RS1711368TIS_S49_R2_001.fastq.gz'
params.tumor_name = 'RS1711368'
params.normal_fq1 = '/mnt/md0/wjm/raw_datas/Fusion_Samples/1805M21W_S17_R1_001.fastq.gz'
params.normal_fq2 = '/mnt/md0/wjm/raw_datas/Fusion_Samples/1805M21W_S17_R2_001.fastq.gz'
params.normal_name = '1805M21W'
params.input_type = 'fastq'
params.skip = false
params.ref = '/amoydx/USER/wangjianqing/database/reference/DNA/ucsc.hg19.fasta'
params.tumorBam = '/amoydx/USER/wujianming/config/White_Cell_Lib/sample5_wes_normal.bam'
params.normalBam = '/amoydx/USER/wujianming/config/White_Cell_Lib/sample5_wes_normal.bam'
params.annotate = "yes"
params.bed = '/mnt/md0/wjm/config/10Gene/database/10gene.bed.gz'
params.thread = 5
params.cpus = 48
params.annoDB = '/amoydx/USER/wujianming/database/annovar'
params.dir = '/amoydx/USER/wujianming/project/WYL_analysis'


process qualityControl {
    validExitStatus 0,1      //设置异常退出，1
    maxForks 1           //设置线程数
    beforeScript 'source /home/wujianming/.bashrc'
    // beforeScript "rm -fr ${params.dir}/${params.tumor_name} && mkdir -p ${params.dir}/test && chmod 777 -R ${params.dir}"  //在执行process里的脚本之前进行的初始化操作
    publishDir "${params.dir}/${params.tumor_name}", mode:'copy'    //将生成的结果move至指定目录，否则存储于work内

    output:
    // file "${params.tumor_name}_fastp.json" into json
    file "${params.tumor_name}_fastp.html" 
    file "${params.tumor_name}_R1.clean.fastq.gz" into clean_R1_data
    file "${params.tumor_name}_R2.clean.fastq.gz" into clean_R2_data
    file "${params.normal_name}_fastp.html" 
    file "${params.normal_name}_R1.clean.fastq.gz" into normal_clean_R1_data
    file "${params.normal_name}_R2.clean.fastq.gz" into normal_clean_R2_data

    when:
    !params.skip

    shell:
    '''
    fastp -i !{params.normal_fq1} -o !{params.normal_name}_R1.clean.fastq.gz -I !{params.normal_fq2} -O !{params.normal_name}_R2.clean.fastq.gz --thread=5 --length_required=30 --n_base_limit=6 --compression=6 -j !{params.normal_name}_fastp.json -h !{params.normal_name}_fastp.html
    fastp -i !{params.tumor_fq1} -o !{params.tumor_name}_R1.clean.fastq.gz -I !{params.tumor_fq2} -O !{params.tumor_name}_R2.clean.fastq.gz --thread=5 --length_required=30 --n_base_limit=6 --compression=6 -j !{params.tumor_name}_fastp.json -h !{params.tumor_name}_fastp.html
    '''

}

process bwa {
    validExitStatus 0,1        
    publishDir "${params.dir}/${params.tumor_name}", mode:"copy"
    beforeScript 'source /home/wujianming/.bashrc'

    input:
    file tumor_fastq1 from clean_R1_data
    file tumor_fastq2 from clean_R2_data
    file normal_fastq1 from normal_clean_R1_data
    file normal_fastq2 from normal_clean_R2_data

    output:
    file "${params.tumor_name}.sort.bam" into tumor_sortbam    //将生成的文件名传至变量，供nextflow后面调用
    file "${params.tumor_name}.sort.bam.bai" 
    file "${params.normal_name}.sort.bam" into normal_sortbam    
    file "${params.normal_name}.sort.bam.bai"

    when:
    !params.skip

    shell:

    """
    bwa mem -M -R '@RG\\tID:'${params.normal_name}'\\tSM:'${params.normal_name}'\\tPL:illumina\\tPU:illumina\\tLB:illumina' -t 12 ${params.ref} ${fastq1} ${fastq2}|samtools view -@ ${params.thread} -Sb - |samtools sort -@ ${params.thread} - > ${params.normal_name}.sort.bam
    samtools index ${params.normal_name}.sort.bam

    bwa mem -M -R '@RG\\tID:'${params.tumor_name}'\\tSM:'${params.tumor_name}'\\tPL:illumina\\tPU:illumina\\tLB:illumina' -t 12 ${params.ref} ${fastq1} ${fastq2}|samtools view -@ ${params.thread} -Sb - |samtools sort -@ ${params.thread} - > ${params.tumor_name}.sort.bam
    samtools index ${params.tumor_name}.sort.bam
    """


}

process markdup{
    validExitStatus 0,1        
    publishDir "${params.dir}/${params.tumor_name}", mode:"copy"
    beforeScript 'source /home/wujianming/.bashrc'

    input:
    file tbam from tumor_sortbam
    file nbam from normal_sortbam


    output:
    file "${params.tumor_name}.markdup.bam" into t_markdupbam
    file "${params.tumor_name}.markdup.bam.bai" into t_markdupbambai
    file "${params.normal_name}.markdup.bam" into n_markdupbam
    file "${params.normal_name}.markdup.bam.bai" into n_markdupbambai

    when:
    !params.skip

    shell:
    """
    java -Xmx10G  -jar /amoydx/USER/wujianming/software/picard.jar MarkDuplicates REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=LENIENT INPUT=${nbam} OUTPUT=${params.normal_name}.markdup.bam METRICS_FILE=${params.normal_name}.bam.md.metrics
    samtools index ${params.normal_name}.markdup.bam

    java -Xmx10G  -jar /amoydx/USER/wujianming/software/picard.jar MarkDuplicates REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=LENIENT INPUT=${tbam} OUTPUT=${params.tumor_name}.markdup.bam METRICS_FILE=${params.tumor_name}.bam.md.metrics
    samtools index ${params.tumor_name}.markdup.bam
    """

}

process callvar_single{        //strelka
    validExitStatus 0,1        
    publishDir "${params.dir}/${params.tumor_name}", mode:"copy"
    beforeScript 'source /home/wujianming/.bashrc'

    input:
    file t_dupbam from t_markdupbam
    file t_dupbambai from t_markdupbambai
    file n_dupbam from n_markdupbam
    file n_dupbambai from n_markdupbambai

    output:
    file "${params.tumor_name}_variant/results/variants/somatic.snvs.vcf.gz"  into snv
    file "${params.tumor_name}_variant/results/variants/somatic.indels.vcf.gz"  into indel
    file "${params.tumor_name}_variant/results/variants/somatic.snvs.vcf.gz.tbi"
    file "${params.tumor_name}_variant/results/variants/somatic.indels.vcf.gz.tbi"

    when:
    !params.skip

    shell:
        """
        python2 /amoydx/USER/wujianming/software/strelka-2.9.10.centos6_x86_64/bin/configureStrelkaSomaticWorkflow.py --exome --referenceFasta ${params.ref} --tumorBam ${t_dupbam} --normalBam ${n_dupbam} --runDir ${params.tumor_name}_variant
        cd ${params.tumor_name}_variant
        ./runWorkflow.py -m local -j 7
        """
}

process callvar_paired{        //strelka
    validExitStatus 0,1        
    publishDir "${params.dir}/${params.tumor_name}", mode:"copy"
    beforeScript 'source /home/wujianming/.bashrc'

    output:
    file "${params.tumor_name}_variant/results/variants/somatic.snvs.vcf.gz"  into snv_pair
    file "${params.tumor_name}_variant/results/variants/somatic.indels.vcf.gz"  into indel_pair
    file "${params.tumor_name}_variant/results/variants/somatic.snvs.vcf.gz.tbi"
    file "${params.tumor_name}_variant/results/variants/somatic.indels.vcf.gz.tbi"

    when:
    params.skip

    shell:
    """
    python2 /amoydx/USER/wujianming/software/strelka-2.9.10.centos6_x86_64/bin/configureStrelkaSomaticWorkflow.py --exome --referenceFasta ${params.ref} --tumorBam ${params.tumorBam} --normalBam ${params.normalBam} --runDir ${params.tumor_name}_variant
    cd ${params.tumor_name}_variant
    ./runWorkflow.py -m local -j 7
    """
}

process germline_filter{
    validExitStatus 0,1        
    publishDir "${params.dir}/${params.tumor_name}", mode:"copy"
    beforeScript 'source /home/wujianming/.bashrc'

    input:
    file snv_file from snv.mix(snv_pair)
    file indel_file from indel.mix(indel_pair)

    output:
    file "${snv_file}_filter.vcf" into snv_filter
    file "${indel_file}_filter.vcf" into indel_filter

    shell:
    """
    python /amoydx/USER/wujianming/config/White_Cell_Lib/strelka_filter_germline.py -s ${snv_file} -i ${indel_file}
    """
    
}

process annotateVCF {
    validExitStatus 0,1
    publishDir "${params.dir}/${params.tumor_name}", mode: 'copy'
    beforeScript 'source /home/wujianming/.bashrc'

    input:
    file snvs from snv_filter
    file indels from indel_filter

    output:
    file "${params.name}.hg19_multianno.vcf" 
    file "${params.name}.hg19_multianno.txt" 

    when:
    params.annotate == "yes"

    shell:
    '''
    /amoydx/USER/wangjianqing/software/annovar/table_annovar.pl !{snvs} !{params.annoDB} -buildver hg19 -out !{params.name} -remove -protocol refGene,EAS.sites.2015_08 -operation g,f -nastring . -vcfinput
    /amoydx/USER/wangjianqing/software/annovar/table_annovar.pl !{indels} !{params.annoDB} -buildver hg19 -out !{params.name} -remove -protocol refGene,EAS.sites.2015_08 -operation g,f -nastring . -vcfinput
    '''
}
