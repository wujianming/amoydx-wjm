#!/bin/env python
#coding=utf-8
from __future__ import division
import argparse
import datetime
import xlrd
import re
import os

#report_gene_file = "/Users/jianming.wu/work/config/10gene_auto_report/raw_database/report_replace_genelist"
#config_path = "/Users/jianming.wu/work/config/10gene_auto_report/report_template_database/"
#pic_dir = "/Users/jianming.wu/work/config/10gene_auto_report/report_templet/pic"
#css_path = "/Users/jianming.wu/work/config/10gene_auto_report/report_templet/mystyle.css"
change_page = '<div style="page-break-after:always;"></div>'
report_gene_file = "/amoydx/appclub/10gene_automatic_report_version1.1_20190428/database/report_replace_genelist"
config_path = "/amoydx/appclub/10gene_automatic_report_version1.1_20190428/report_template_module_database/"
pic_dir = "/amoydx/appclub/10gene_automatic_report_version1.1_20190428/report_templet_20190428/pic"
css_path = "/amoydx/appclub/10gene_automatic_report_version1.1_20190428/report_templet_20190428/mystyle.css"


def get_negative_id(negative_file):
	#先确定哪些是需要出阳性报告的，哪些是要出阴性报告的
	negative_id_list = []
	with open(negative_file,'r') as NE:
		for con in NE:
			con = con.strip().split('\t')		
			negative_sample_id = re.sub("[A-Z]$","",con[0])
			if negative_sample_id in negative_id_list:
				pass
			else:
				negative_id_list.append(con[0])
	return(negative_id_list)


def get_negative_report(sample_info_file,negative_file,control_file):
	#出全阴性报告
	now_time = datetime.datetime.now()
	report_date = now_time.strftime('%Y-%m-%d')	#默认报告日期为当天日期
	#print(report_time)
	negative_id_list = get_negative_id(negative_file)
	#print(sample_info_file)
	#print(negative_id_list)
	gene_10_list = ["EGFR","ALK","ROS1","RET","KRAS","BRAF","MET","HER2","NRAS","PIK3CA"]
	#根据sample_id获取客户信息
	for sample_id in negative_id_list:		
		with open(sample_info_file,'r') as SAMPLE:
			for line in SAMPLE:
				line = line.strip().split('\t')				
				if line[0] in sample_id:
					hospital_name = line[1]
					patient_name = line[2]
					patient_name = patient_name.strip()
					if line[3] != "男" and line[3] != "女":		#性别除了这两个选项外，为空
						patient_sex = ""
					else:
						patient_sex = line[3]
					if str(line[4]) == "0.0":		#如果年龄为0，为空
						patient_age = ""
					else:
						patient_age = int(float(line[4]))
					
					if str(line[5]) == "0":
						patient_dignose = ""
					else:
						patient_dignose = line[5]	#病理诊断
					patient_dignose_num = line[6]	#病理编号
					accept_date = line[7]	#接收日期
					sample_type1 = line[8] #样本类型1
					sample_type2 = line[9]		#样本类型2
					if sample_type2 == "NA":
						sample_type2 = ""
					sample_num1 = line[10]
					sample_num2 = line[11]		#样本数量2
					if sample_num2 == "NA":
						sample_num2 = ""				
					sample_quality = line[12]
					dignose_result = line[13]
					if dignose_result == "NA":
						dignose_result = ""
					detect_project = line[14]
					with open(control_file,'r') as CON:
						for con in CON:
							con = con.strip().split('\t')
							if con[0] == sample_id:
								pdl1_stat = con[-1]
					#通过检测项目括号里的内容，确定深度:普通深度2000X,高深度5000X
					if "（" in detect_project:
						depth_info = re.split("[（）]",detect_project)[1]
						#print(depth_info)
					else:
						depth_info = "普通深度"
					
					report_path = str(sample_id)+'-'+patient_name+'-'+detect_project
					if os.path.exists(report_path) == False:	#新建目录，存在样本报告
						os.mkdir(report_path)
					else:
						pass
					os.system('cp -r %s %s %s' % (pic_dir,css_path,report_path))
					#print(hospital_name,patient_name,patient_sex,patient_age,patient_dignose,patient_dignose_num,accept_date,sample_type1,sample_num1,sample_type2,sample_num2,sample_quality,dignose_result,detect_project)
					f = open(report_path+'/'+report_path+'.tmp.html','w')
					read_1 = open(config_path+'1_html_header.html')
					content_1 = read_1.readlines()
					f.write("".join(str(i) for i in content_1))	#加入html的title
					read_2 = open(config_path+'2_first_page.html')	
					content_2 = read_2.readlines()				
					f.write("".join(str(i) for i in content_2))		#加入首页
					f.write("\n"+change_page+'\n\n\n')				#换页
					if patient_sex == "男":				#根据性别，删减内容
						with open(config_path+'3_second_page.html','r') as SEC:
							for con in SEC:
								if "女士" in con:
									continue
								if "先生" in con:
									con = con.replace("replace",patient_name)
								f.write(con)
					elif patient_sex == "女":
						with open(config_path+'3_second_page.html','r') as SEC:
							for con in SEC:
								if "先生" in con:
									continue
								if "女士" in con:
									con = con.replace("replace",patient_name)
								f.write(con)
					else:
						with open(config_path+'3_second_page.html','r') as SEC:
							for con in SEC:
								if "先生" in con:
									continue
								if "女士" in con:
									con = con.replace("replace",patient_name)
									con = re.sub("女士","",con)
								f.write(con)
					f.write("\n"+change_page+'\n')				#换页

					####正式编辑第一页
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)


					##读取送检信息
					with open(config_path+'4_guest_info.html','r') as FB:
						for con in FB:
							if "Inspection_hospital" in con:
								con = con.replace("replace",hospital_name)
							if "patient_name" in con:
								con = con.replace("replace",patient_name)
							if "sex" in con:
								con = con.replace("replace",patient_sex)
							if "age" in con:
								con = con.replace("replace",str(patient_age))
							if "pathological_number" in con:
								con = con.replace("replace",patient_dignose_num)
							if "pathologic_diagnosis" in con:
								con = con.replace("replace",patient_dignose)
							f.write(con)

					##读取样本信息
					with open(config_path+'5_sample_info.html','r') as FC:
						for con in FC:
							if "sample_number" in con:
								con = con.replace("replace",line[0])
							if "sample_type1" in con:
								con = con.replace("replace",sample_type1)
							if "sample_quantity1" in con:
								con = con.replace("replace",sample_num1)
							if "sample_type2" in con:
								con = con.replace("replace",sample_type2)
							if "sample_quantity2" in con:
								con = con.replace("replace",sample_num2)
							if "sample_quality" in con:
								con = con.replace("replace",sample_quality)
							if "control_result" in con:
								con = con.replace("replace",dignose_result)
							if "accept_date" in con:
								con = con.replace("replace",accept_date)
							if "report_date" in con:
								con = con.replace("replace",report_date)
							f.write(con)
					
					##读取目录和检测结果
					with open(config_path+'6_negative_dir_result.html','r') as FD:
						for con in FD:
							if "page3_detect_result" in con:
								con = con.replace("replace","未检测到突变")
							if "page3_pdl1" in con:
								if pdl1_stat == "阴性":
									con = con.replace("replace","阴性")
								elif pdl1_stat.isdigit() == True:
									pdl1_result = "阳性，TPS为"+str(pdl1_stat)+'%'
									con = con.replace("replace",pdl1_result)
								else:
									continue
							if "lung_col" in con:
								if patient_dignose == "结直肠癌":
									pass
								else:
									continue
							f.write(con)
					##添加页脚
					read_footer = open(config_path+'footer.html')
					content_footer = read_footer.readlines()
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页


					#读取用药信息
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)
					f.write('\n')
					##读取该页的主title
					read_title_detect = open(config_path+'title_detect.html')
					content_title_detect = read_title_detect.readlines()
					f.write("".join(str(i) for i in content_title_detect))
					f.write('\n')
					##插入深度信息
					#if depth_info == "高深度":
					#	read_depth = open(config_path+'depth_5000.html')
					#	content_depth = read_depth.readlines()
					#	f.write("".join(str(i) for i in content_depth))
					#elif depth_info == "超高深度":
					#	read_depth = open(config_path+'depth_20000.html')
					#	content_depth = read_depth.readlines()
					#	f.write("".join(str(i) for i in content_depth))
					#else:
					#	depth_des = "depth_2000X"
					#	read_depth = open(config_path+'depth_2000.html')
					#	content_depth = read_depth.readlines()
					#	f.write("".join(str(i) for i in content_depth))
					##读取表头
					read_negative_FDA_start = open(config_path+'7_lung_FDA_result_start.html')
					content_negative_FDA_start = read_negative_FDA_start.readlines()
					f.write("".join(str(i) for i in content_negative_FDA_start))
					f.write('\n')
					##读取表内容
					read_negative_FDA_content = open(config_path+'7_lung_negative.html')
					content_negative_FDA_content = read_negative_FDA_content.readlines()
					f.write("".join(str(i) for i in content_negative_FDA_content))
					f.write('\n')
					##读取表尾
					read_negative_FDA_end = open(config_path+'7_lung_FDA_result_end.html')
					content_negative_FDA_end = read_negative_FDA_end.readlines()
					f.write("".join(str(i) for i in content_negative_FDA_end))
					f.write('\n')
					##读取其他的用药信息
					read_other_result = open(config_path+'8_other_result.html')
					content_other_result = read_other_result.readlines()
					f.write("".join(str(i) for i in content_other_result))
					f.write('\n')
					##读取备注信息
					#读取签名页
					read_sign_name = open(config_path+'12_sign_name.html')
					content_sign_name = read_sign_name.readlines()
					f.write("".join(str(i) for i in content_sign_name))
					f.write('\n')

					#添加页脚
					# read_footer = open(config_path+'footer.html')
					# content_footer = read_footer.readlines()
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页


					#添加质量控制
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)
					f.write('\n')
					##添加控制内容
					read_quality_control = open(config_path+'13_quality_control.html')
					content_quality_control = read_quality_control.readlines()
					f.write("".join(str(i) for i in content_quality_control))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页


					#添加样品质量评级
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)
					f.write('\n')
					##添加评级内容
					read_sample_quality_rate = open(config_path+'14_sample_quality_rate.html')
					content_sample_quality_rate = read_sample_quality_rate.readlines()
					f.write("".join(str(i) for i in content_sample_quality_rate))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页


					#添加数据质控结果
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)
					f.write('\n')
					##添加质控内容，打开control_file，根据sample_id获取数据质控信息
					with open(control_file,'r') as CONTROL:
						for zhikong in CONTROL:
							zhikong = zhikong.strip().split('\t')
							if sample_id in zhikong[0]:
								zhikong_Q20 = '%.2f%%' % (float(zhikong[1]) * 100)
								zhikong_bidui = '%.2f%%' % (float(zhikong[2]) * 100)
								zhikong_fugai = '%.2f%%' % (float(zhikong[3]) * 100)
								zhikong_junyi = '%.2f%%' % (float(zhikong[4]) * 100)
								zhikong_shendu = str(zhikong[5])
					with open(config_path+'data_control_result.html','r') as CON_HTML:
						for con in CON_HTML:
							if "sample_id" in con:
								con = con.replace("replace",sample_id)
							if "Q20_data" in con:
								con = con.replace("replace",zhikong_Q20)
							if "bidui_data" in con:
								con = con.replace("replace",zhikong_bidui)
							if "fugai_data" in con:
								con = con.replace("replace",zhikong_fugai)
							if "junyi_data" in con:
								con = con.replace("replace",zhikong_junyi)
							if "shendu_data" in con:
								con = con.replace("replace",zhikong_shendu)
							f.write(con)
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页

					#循环添加附录1
					for gene_name in gene_10_list:
						##先添加页眉
						with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",line[0])
									continue
								f.write(con)
						f.write('\n')
						##再添加内容
						read_gene = open(config_path+'fulu1_'+gene_name+'.html')
						read_content = read_gene.readlines()
						f.write("".join(str(i) for i in read_content))
						f.write('\n')
						##再添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n"+change_page+'\n\n\n')				#换页

					#添加药物证据等级
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",line[0])
								continue
							f.write(con)
					f.write('\n')
					##添加内容
					read_drug_grade = open(config_path+'25_drug_grade.html')
					content_drug_grade = read_drug_grade.readlines()
					f.write("".join(str(i) for i in content_drug_grade))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页

					#添加慈善援助药物信息
					for help_page in ["first",'second','third']:
						##添加页眉
						with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",line[0])
									continue
								f.write(con)
						read_help_drug = open(config_path+'help_drug_info_'+help_page+'.html')
						content_help_drug = read_help_drug.readlines()
						f.write("".join(str(i) for i in content_help_drug))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n"+change_page+'\n\n\n')				#换页

					#添加参考文献,先循环前4页，因为最后页不需要换页，所以单独拿出来
					for wenxian_page in ["first",'second','third','fourth']:
						##添加页眉
						with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",line[0])
									continue
								f.write(con)
						read_wenxian = open(config_path+'wenxian_'+wenxian_page+'.html')
						content_wenxian = read_wenxian.readlines()
						f.write("".join(str(i) for i in content_wenxian))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n"+change_page+'\n\n\n')				#换页

					#添加最后一页页眉
					with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",line[0])
									continue
								f.write(con)
					read_last_wenxian = open(config_path+'wenxian_fifth.html')
					content_last_wenxian = read_last_wenxian.readlines()
					f.write("".join(str(i) for i in content_last_wenxian))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.close()

					#获取需要替换的所有基因名称
					report_gene_list = []
					with open(report_gene_file,'r') as GE:
						for k in GE:
							k = k.strip()
							if k not in report_gene_list:
								report_gene_list.append(k)
							else:
								pass

					#先替换页码
					count = 0
					tmp2_report = open(report_path+'/'+report_path+'.tmp2.html','w')
					with open(report_path+'/'+report_path+'.tmp.html','r') as HTML:
						for each in HTML:
							if "footer_end" in each:
								count += 1
								each = each.replace("replace",str(count))
							tmp2_report.write(each)
					tmp2_report.close()
					#再将所有的基因改成斜体
					final_report = open(report_path+'/'+report_path+'.html','w')
					with open(report_path+'/'+report_path+'.tmp2.html','r') as TMP2:
						for each in TMP2:
							if "charset" in each:
								final_report.write(each)
								continue
							for GENE in report_gene_list:
								if GENE in each:
									new_gene = '<i>'+GENE+' </i>'
									#print(new_gene)
									each = each.replace(GENE,new_gene)
							final_report.write(each)
					final_report.close()
					os.system("rm %s/%s.tmp.html %s/%s.tmp2.html" % (report_path,report_path,report_path,report_path))
					os.system("/amoydx/USER/wujianming/software/miniconda-python3/bin/wkhtmltopdf --encoding utf8 --quiet %s/%s.html %s/%s.pdf" %(report_path,report_path,report_path,report_path))
					

def get_positive_id(somatic_file,fusion_file,sample_info_file):
	#循环somatic和fusion文件，获取阳性样本id信息
	somatic_sample_list = []
	for file in somatic_file,fusion_file:
		with open(file,'r') as SOMA:
			for line in SOMA:
				line = line.strip().split('\t')
				if "Sample" in line:
					continue
				if "F" in line[0][:2]:
					if line[0][0] == "S":
						sample_id = line[0][:8]
					else:
						sample_id = line[0][:7]
				if "F" not in line[0][:2]:
					if line[0][0] == "S":
						sample_id = line[0][:9]
					else:
						sample_id = line[0][:8]
				if sample_id not in somatic_sample_list:
					somatic_sample_list.append(sample_id)
	return(somatic_sample_list)


def get_positive_report(somatic_file,fusion_file,sample_info_file,control_file):
	#出阳性报告
	positive_sample_list = get_positive_id(somatic_file,fusion_file,sample_info_file) 
	#print(positive_sample_list)
	now_time = datetime.datetime.now()
	report_date = now_time.strftime('%Y-%m-%d')	#默认报告日期为当天日期
	gene_10_list = ["EGFR","ALK","ROS1","RET","KRAS","BRAF","MET","HER2","NRAS","PIK3CA"]
	#根据sample_id获取客户信息
	for sample_id in positive_sample_list:		
		with open(sample_info_file,'r') as SAMPLE:
			for line in SAMPLE:
				line = line.strip().split('\t')
				if line[0] in sample_id: 
					hospital_name = line[1]
					patient_name = line[2]
					patient_name = patient_name.strip()
					if line[3] != "男" and line[3] != "女":		#性别除了这两个选项外，为空
						patient_sex = ""
					else:
						patient_sex = line[3]
					if str(line[4]) == "0.0":		#如果年龄为0，为空
						patient_age = ""
					else:
						patient_age = int(float(line[4]))
				
					if str(line[5]) == "0":
						patient_dignose = ""
					else:
						patient_dignose = line[5]	#病理诊断
					patient_dignose_num = line[6]	#病理编号
					accept_date = line[7]	#接收日期
					sample_type1 = line[8] #样本类型1
					sample_type2 = line[9]		#样本类型2
					if sample_type2 == "NA":
						sample_type2 = ""
					sample_num1 = line[10]
					sample_num2 = line[11]		#样本数量2
					if sample_num2 == "NA":
						sample_num2 = ""				
					sample_quality = line[12]
					#print(sample_quality)
					dignose_result = line[13]
					if dignose_result == "NA":
						dignose_result = ""
					detect_project = line[14]
					with open(control_file,'r') as CON:
						for con in CON:
							con = con.strip().split('\t')
							if con[0] == sample_id:
								pdl1_stat = con[-1]
					#通过检测项目括号里的内容，确定深度:普通深度2000X,高深度5000X
					if "（" in detect_project:
						depth_info = re.split("[（）]",detect_project)[1]
						#print(depth_info)
					else:
						depth_info = "普通深度"
					
					report_path = str(sample_id)+'-'+patient_name+'-'+detect_project
					if os.path.exists(report_path) == False:	#新建目录，存在样本报告
						os.mkdir(report_path)
					else:
						pass
					os.system('cp -r %s %s %s' % (pic_dir,css_path,report_path))
					#print(hospital_name,patient_name,patient_sex,patient_age,patient_dignose,patient_dignose_num,accept_date,sample_type1,sample_num1,sample_type2,sample_num2,sample_quality,dignose_result,detect_project)
					f = open(report_path+'/'+report_path+'.tmp.html','w')
					read_1 = open(config_path+'1_html_header.html')
					content_1 = read_1.readlines()
					f.write("".join(str(i) for i in content_1))	#加入html的title
					f.write('\n')
					read_2 = open(config_path+'2_first_page.html')	
					content_2 = read_2.readlines()				
					f.write("".join(str(i) for i in content_2))		#加入首页
					f.write('\n')
					f.write("\n"+change_page+'\n\n\n')				#换页
					if patient_sex == "男":				#根据性别，删减内容
						with open(config_path+'3_second_page.html','r') as SEC:
							for con in SEC:
								if "女士" in con:
									continue
								if "先生" in con:
									con = con.replace("replace",patient_name)
								f.write(con)
					elif patient_sex == "女":
						with open(config_path+'3_second_page.html','r') as SEC:
							for con in SEC:
								if "先生" in con:
									continue
								if "女士" in con:
									con = con.replace("replace",patient_name)
								f.write(con)
					else:
						with open(config_path+'3_second_page.html','r') as SEC:
							for con in SEC:
								if "先生" in con:
									continue
								if "女士" in con:
									con = con.replace("replace",patient_name)
									con = re.sub("女士","",con)
								f.write(con)
					f.write("\n"+change_page+'\n')				#换页

					#################<--------------   添加第一页   ----------------->#################
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)
					f.write('\n')

					##读取送检信息
					with open(config_path+'4_guest_info.html','r') as FB:
						for con in FB:
							if "Inspection_hospital" in con:
								con = con.replace("replace",hospital_name)
							if "patient_name" in con:
								con = con.replace("replace",patient_name)
							if "sex" in con:
								con = con.replace("replace",patient_sex)
							if "age" in con:
								con = con.replace("replace",str(patient_age))
							if "pathological_number" in con:
								con = con.replace("replace",patient_dignose_num)
							if "pathologic_diagnosis" in con:
								con = con.replace("replace",patient_dignose)
							f.write(con)
					f.write('\n')
					##读取样本信息
					with open(config_path+'5_sample_info.html','r') as FC:
						for con in FC:
							if "sample_number" in con:
								con = con.replace("replace",line[0])
							if "sample_type1" in con:
								con = con.replace("replace",sample_type1)
							if "sample_quantity1" in con:
								con = con.replace("replace",sample_num1)
							if "sample_type2" in con:
								con = con.replace("replace",sample_type2)
							if "sample_quantity2" in con:
								con = con.replace("replace",sample_num2)
							if "sample_quality" in con:
								con = con.replace("replace",sample_quality)
							if "control_result" in con:
								con = con.replace("replace",dignose_result)
							if "accept_date" in con:
								con = con.replace("replace",accept_date)
							if "report_date" in con:
								con = con.replace("replace",report_date)
							f.write(con)
					f.write('\n')
					##读取目录和检测结果,输出gene  蛋白突变
					###这里需要读取两个文件，somatic和fusion文件
					###先判断文件是否为空
					detect_result = []		##检测结果汇总使用，提供体细胞突变数,基因、碱基突变、蛋白突变
					dir_result=[]
					detect_result_usefule_list= []  ##检测结果使用，提供有意义突变
					detect_result_nousefule_list= []  ##检测结果使用，提供无意义突变
					detect_FDA_result = []		##检测结果汇总使用，提供有意义突变
					detect_freq = {}
					detect_gene = []
					if os.path.getsize(sample_id+'_somatic.xls') != 0:
						with open(sample_id+'_somatic.xls','r') as KNOW:
							for know_info in KNOW:
								know_info  = know_info.strip().split('\t')
								exon_info = know_info[14].split(":")[1].capitalize()
								cds_mut = know_info[14].split(":")[-2]
								if 'del' in cds_mut and 'delins' not in cds_mut:
									cds_mut = cds_mut.split('del')[0]+'del'+str(len(cds_mut.split('del')[-1]))
								if 'delins' in cds_mut:
									cds_mut = cds_mut.split('delins')[0]+'delins'+str(len(cds_mut.split('delins')[-1]))
								pro_mut = know_info[14].split(":")[-1].split('.')[-1]
								if pro_mut == "?":			#如果蛋白是？，就输出CDS变化
									dir_value = know_info[12]+' &nbsp;'+cds_mut
								else:
									dir_value = know_info[12]+' &nbsp;'+pro_mut
								detect_freq[pro_mut] = str(know_info[10])
								in_value = know_info[12]+'<br>'+exon_info+'<br>'+cds_mut+'<br>'+know_info[14].split(":")[-1]
								detect_freq[pro_mut] = '%.2f%%' % (float(know_info[10]) * 100)
								if know_info[12] not in detect_gene:
									detect_gene.append(know_info[12])
								if in_value not in detect_result:
									detect_result.append(in_value)
								if dir_value not in dir_result:
									dir_result.append(dir_value)
								else:
									pass
					else:
						pass
					if os.path.getsize(sample_id+'_fusion.xls') != 0:
						with open(sample_id+'_fusion.xls','r') as FUS:
							for fus in FUS:
								fus = fus.strip().split('\t')
								gene1 = fus[5].split(':')[0]
								gene2 = fus[9].split(':')[0]
								gene_gene = gene1+'-'+gene2+'融合'
								detect_freq[gene_gene] = '%.2f%%' % (float(fus[14]) * 100)
								if gene1 not in detect_gene:
									detect_gene.append(gene1)
								if gene2 not in detect_gene:
									detect_gene.append(gene2)
								if gene_gene not in detect_result:
									detect_result.append(gene_gene)
								if gene_gene not in dir_result:
									dir_result.append(gene_gene)
					if os.path.getsize(sample_id+'_know_annot.xls') != 0:
						with open(sample_id+'_know_annot.xls','r') as K:
							for ke in K:
								ke = ke.strip().split('\t')
#								print (ke)
								exon_info = ke[9]
								cds_mut = ke[10]
								pro_mut = ke[11]
								gene_pro = ke[6]+'<br>'+exon_info+'<br>'+cds_mut+'<br>'+pro_mut
								if pro_mut == "?":
									FDA_in_value = ke[6] +' &nbsp;'+cds_mut
								else:
									FDA_in_value = ke[6]+' &nbsp;'+pro_mut
								if FDA_in_value not in detect_FDA_result:
									detect_FDA_result.append(FDA_in_value)
								if gene_pro not in detect_result_usefule_list:
									detect_result_usefule_list.append(gene_pro)
								else:
									pass
					else:
						pass
					if os.path.getsize(sample_id+'_fusion_annot.xls') != 0:
						with open(sample_id+'_fusion_annot.xls','r') as N:
							for L in N:
								L = L.strip().split('\t')
								result = L[0]+'融合'
								if result not in detect_FDA_result:
									detect_FDA_result.append(result)
								if result not in detect_result_usefule_list:
									detect_result_usefule_list.append(result)
								else:
									pass 
					
					detect_result_str = "<br><br>".join(str(i) for i in dir_result)
					#if os.path.getsize(sample_id+'_fusion_annot.xls') == 0 and os.path.getsize(sample_id+'_know_annot.xls') == 0: #如果都没有FDA用药，更换目录模板
					if len(detect_FDA_result) == 0 and len(detect_result) == 1: #如果检测的突变或者fusion有一个/多个， 模板是不一样的
						with open(config_path+'6_postive_insign_single_dir_result.html','r') as FD:
							for con in FD:
								if "page3_detect_result" in con:
									con = con.replace("replace",detect_result_str)
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat.isdigit() == True:
										pdl1_result = "阳性，TPS为"+str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
									else:
										continue
								if "detect_number" in con:
									con = con.replace("replace",str(len(detect_result)))
								f.write(con)
					elif len(detect_FDA_result) == 0 and len(detect_result) > 1:
						with open(config_path+'6_postive_insign_dir_result.html','r') as FD1:
							for con in FD1:
								if "page3_detect_result" in con:
									con = con.replace("replace",detect_result_str)
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat.isdigit() == True:
										pdl1_result = "阳性，TPS为"+str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
									else:
										continue
								if "detect_number" in con:
									con = con.replace("replace",str(len(detect_result)))
								f.write(con)
					else:
						with open(config_path+'6_postive_sign_dir_result.html','r') as FD:
							for con in FD:
								if len(detect_result) > len(detect_FDA_result) :	#突变部分有意义，部分无意义
									if "page3_detect_result" in con:
										con = con.replace("replace",detect_result_str)
									if "page3_pdl1" in con:
										if pdl1_stat == "阴性":
											con = con.replace("replace","阴性")
										elif pdl1_stat.isdigit() == True:
											pdl1_result = "阳性，TPS为"+str(pdl1_stat)+'%'
											con = con.replace("replace",pdl1_result)
										else:
											continue
									if "detect_number" in con:
										con = con.replace("all_detect",str(len(detect_result)))
										FDA_gene_out = "、".join(i.replace("p.","") for i in detect_FDA_result)
										con = con.replace("FDA_detect",FDA_gene_out)
									f.write(con)
								else:	#全部都有意义
									if "page3_detect_result" in con:
										con = con.replace("replace",detect_result_str)
									if "page3_pdl1" in con:
										if pdl1_stat == "阴性":
											con = con.replace("replace","阴性")
										elif pdl1_stat.isdigit() == True:
											pdl1_result = "阳性，TPS为"+str(pdl1_stat)+'%'
											con = con.replace("replace",pdl1_result)
										else:
											continue
									if "detect_number" in con:
										continue
									f.write(con)
					f.write('\n')
					##添加页脚
					read_footer = open(config_path+'footer.html')
					content_footer = read_footer.readlines()
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页


					#################<--------------   添加检测结果   ----------------->#################
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)
					f.write('\n')
					##读取该页的主title
					read_title_detect = open(config_path+'title_detect.html')
					content_title_detect = read_title_detect.readlines()
					f.write("".join(str(i) for i in content_title_detect))
					f.write('\n')
					##插入深度信息
					#if depth_info == "高深度":
					#	read_depth = open(config_path+'depth_5000.html')
					#	content_depth = read_depth.readlines()
					#	f.write("".join(str(i) for i in content_depth))
					#elif depth_info == "超高深度":
					#	read_depth = open(config_path+'depth_20000.html')
					#	content_depth = read_depth.readlines()
					#	f.write("".join(str(i) for i in content_depth))
					#else:
					#	depth_des = "depth_2000X"
					#	read_depth = open(config_path+'depth_2000.html')
					#	content_depth = read_depth.readlines()
					#	f.write("".join(str(i) for i in content_depth))
					##读取表头
					read_negative_FDA_start = open(config_path+'7_lung_FDA_result_start.html')
					content_negative_FDA_start = read_negative_FDA_start.readlines()
					f.write("".join(str(i) for i in content_negative_FDA_start))
					f.write('\n')
					##读取表内容
					###这个也要读取两个文件，somatic_annot和fusion_annot,如果这两个都为空，则无FDA用药
					FDA_result = []
					FDA_gene_list = []
					FDA_explain_list = []
					##如果这两个文件都为空，代表突变没有FDA用药，直接沿用阴性报告的模块
					if os.path.getsize(sample_id+'_somatic.xls') == 0 and  os.path.getsize(sample_id+'_fusion.xls') == 0:
						read_negative_FDA_content = open(config_path+'7_lung_negative.html')
						content_negative_FDA_content = read_negative_FDA_content.readlines()
						f.write("".join(str(i) for i in content_negative_FDA_content))
						f.write('\n')
						#读取表尾
						read_negative_FDA_end = open(config_path+'7_lung_FDA_result_end.html')
						content_negative_FDA_end = read_negative_FDA_end.readlines()
						f.write("".join(str(i) for i in content_negative_FDA_end))
						f.write('\n')
						##读取其他的用药信息
					# 	read_other_result = open(config_path+'8_other_result.html')
					# 	content_other_result = read_other_result.readlines()
					# 	f.write("".join(str(i) for i in content_other_result))

					if os.path.getsize(sample_id+'_know_annot.xls') != 0:
						with open(sample_id+'_know_annot.xls','r') as KNOW:
							for know_info in KNOW:
								know_info  = know_info.strip().split('\t')
								exon_info = know_info[9]
								cds = know_info[10]
								mut = know_info[11].split(".")[-1]
								FDA_gene = know_info[6]+'<br>'+exon_info+'<br>'+cds+'<br>'+know_info[11]
								# if mut == "?":
								# 	FDA_gene = know_info[6]+'  &nbsp;'+cds
								# else:
								# 	FDA_gene = know_info[6]+' &nbsp;'+mut
								if know_info[6] not in FDA_gene_list:			#获取基因名称，作后面删除"靶向用药其他基因结果"
									FDA_gene_list.append(know_info[6])	
								FDA_freq = detect_freq[mut]	#蛋白突变作为key
								FDA_sensitive = know_info[13]
								clinical_sensitive = know_info[14]
								insensitive = know_info[15]
								FDA_connect = FDA_gene+'\t'+FDA_freq+'\t'+FDA_sensitive+'\t'+clinical_sensitive+'\t'+insensitive
								if FDA_connect not in FDA_result:
									FDA_result.append(FDA_connect)
								#分别是基因名称、蛋白突变、基因介绍、	变异介绍、治疗策略、药物信息
								if mut == "?":
									FDA_explain_content = know_info[6]+'\t'+cds+'\t'+know_info[16]+'\t'+know_info[17]+'\t'+know_info[18]+'\t'+know_info[13]
								else:
									FDA_explain_content = know_info[6]+'\t'+mut+'\t'+know_info[16]+'\t'+know_info[17]+'\t'+know_info[18]+'\t'+know_info[13]
								if FDA_explain_content not in FDA_explain_list:
									FDA_explain_list.append(FDA_explain_content)


					if os.path.getsize(sample_id+'_fusion_annot.xls') != 0:
						with open(sample_id+'_fusion_annot.xls','r') as FUS:
							for fus in FUS:
								fus = fus.strip().split('\t')
								print (fus)
								gene1 = fus[0].split('-')[0]								
								gene2 = fus[0].split('-')[1]
								if gene1 not in FDA_gene_list:
									FDA_gene_list.append(gene1)
								if gene2 not in FDA_gene_list:
									FDA_gene_list.append(gene2)
								gene_gene1 = gene1+'-'+gene2+'融合'
								if gene_gene1 in detect_freq:
									FREQ = detect_freq[gene_gene1]
								FDA_sensitive = fus[10]
								clinical_sensitive = fus[12]
								insensitive = fus[13]
								fusion_content = fus[0]+'融合\t'+FREQ+'\t'+FDA_sensitive+'\t'+clinical_sensitive+'\t'+insensitive
								if fusion_content not in FDA_result:
									FDA_result.append(fusion_content)
								#分别是融合名称、基因介绍、	变异介绍、治疗策略、药物信息、再加GENE:exon-GENE:exon
								FDA_explain_content2 = fus[0]+'\t'+fus[14]+'\t'+fus[15]+'\t'+fus[16]+'\t'+fus[10]+'\t'+fus[-1]
								if FDA_explain_content2 not in FDA_explain_list:
									FDA_explain_list.append(FDA_explain_content2)
		##			print("---------------------")
		#			print(detect_result)
		#			print(FDA_result)
		#			print(detect_result_usefule_list)
		#			print("---------------------")
					for i in detect_result:
						if '-' in i and '<' not in i and '>' not in i:
							if i not in detect_result_usefule_list:
								detect_result_nousefule_list.append(i+'\t'+str(detect_freq[i]))
						else: #不是融合
							#all_gene_pro = i.split('<')[0]+'<br>'+i.split('>')[-1]
							if i not in detect_result_usefule_list:
								nouse_freq = detect_freq[i.split('.')[-1]]
								detect_result_nousefule_list.append(i+'\t'+str(nouse_freq))
					# print("---------------------")
					# print(detect_result)
					# print(FDA_result)
					# print(detect_result_usefule_list)
					# print(detect_result_nousefule_list)
					# print("---------------------")							
					##获取有FDA用药信息
					for i in range(len(FDA_result)):
						#print(FDA_result[i].split('\t'))
						with open(config_path+'7_lung_FDA_content.html') as FDA_content:
							for con in FDA_content:
								if 'FDA_result' in con:
									con = con.replace("replace",FDA_result[i].split('\t')[0])
								if 'FDA_freq' in con:
									con = con.replace("replace",FDA_result[i].split('\t')[1])
								if 'FDA_sensitive' in con:
									FDA_sen = FDA_result[i].split('\t')[2]
									if '；' in FDA_sen or ';' in FDA_sen:
										FDA_sen = FDA_sen.replace("；","<br>")
										FDA_sen = FDA_sen.replace(";","<br>")
									con = con.replace("replace",FDA_sen)
								if 'clinical_sensitive' in con:
									clinical_sen = FDA_result[i].split('\t')[3]
									if '；' in clinical_sen or ';' in clinical_sen:
										clinical_sen = clinical_sen.replace("；","<br>")
										clinical_sen = clinical_sen.replace(";","<br>")
									con = con.replace("replace",clinical_sen)
								if 'resistance' in con:
									resis = FDA_result[i].split('\t')[4]
									if '；' in resis or ';' in resis:
										resis = resis.replace("；","<br>")
										resis = resis.replace(";","<br>")
									con = con.replace("replace",resis)
								f.write(con)
					f.write('\n')
					#print(detect_result_nousefule_list)
					##此时再加上无意义位点
					if len(detect_result_nousefule_list) != 0:
						for k in range(len(detect_result_nousefule_list)):
							with open(config_path+'7_lung_FDA_content.html') as FDA_content:
								for con in FDA_content:
									if 'FDA_result' in con:
										con = con.replace("replace",detect_result_nousefule_list[k].split('\t')[0])
									if 'FDA_freq' in con:
										con = con.replace("replace",detect_result_nousefule_list[k].split('\t')[1])
									if 'FDA_sensitive' in con:
										con = con.replace("replace","NA")
									if 'clinical_sensitive' in con:
										con = con.replace("replace","NA")
									if 'resistance' in con:
										con = con.replace("replace","NA")
									f.write(con)
						f.write('\n')
					#读取表尾
					read_negative_FDA_end = open(config_path+'7_lung_FDA_result_end.html')
					content_negative_FDA_end = read_negative_FDA_end.readlines()
					f.write("".join(str(i) for i in content_negative_FDA_end))
					f.write('\n')
					#print(FDA_gene_list)
					##读取其他的用药信息,如果在FDA里面有基因出现，跳过;暂时想不到好的方法，只能一一列举
					with open(config_path+'8_other_result.html') as OTHER:
						for cont in OTHER:	
							if len(detect_gene) == 1:
								if detect_gene[0] in cont:
									continue
								f.write(cont)
							if len(detect_gene) == 2:
								if detect_gene[0] in cont or detect_gene[1] in cont:
									continue
								f.write(cont)
							if len(detect_gene) == 3:
								if detect_gene[0] in cont or detect_gene[1] in cont or detect_gene[2] in cont:
									continue
								f.write(cont)
							if len(detect_gene) == 4:
								if detect_gene[0] in cont or detect_gene[1] in cont or detect_gene[2] in cont or detect_gene[3] in cont :
									continue
								f.write(cont)
							if len(detect_gene) == 5:
								if detect_gene[0] in cont or detect_gene[1] in cont or detect_gene[2] in cont or detect_gene[3] in cont or detect_gene[4] in cont:
									continue
								f.write(cont)
							if len(detect_gene) == 6:
								if detect_gene[0] in cont or detect_gene[1] in cont or detect_gene[2] in cont or detect_gene[3] in cont or detect_gene[4] in cont or detect_gene[5] in cont:
									continue
								f.write(cont)
							if len(detect_gene) == 7:
								if detect_gene[0] in cont or detect_gene[1] in cont or detect_gene[2] in cont or detect_gene[3] in cont or detect_gene[4] in cont or detect_gene[5] in cont or detect_gene[6] in cont:
									continue
								f.write(cont)
							if len(detect_gene) == 8:
								if detect_gene[0] in cont or detect_gene[1] in cont or detect_gene[2] in cont or detect_gene[3] in cont or detect_gene[4] in cont or detect_gene[5] in cont or detect_gene[6] in cont or detect_gene[7] in cont:
									continue
								f.write(cont)
					f.write('\n')
					# if len(FDA_result) == 0:
					# 	pass
					# else:
					read_mark  = open(config_path+'9_beizhu.html')
					content_mark = read_mark.readlines()
					f.write("".join(str(i) for i in content_mark))
					f.write('\n')

					#添加页脚
					#read_footer = open(config_path+'footer.html')
					#content_footer = read_footer.readlines()
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页


					#################<--------------   添加检测结果解释及用药信息   ----------------->#################


					if os.path.getsize(sample_id+'_know_annot.xls') == 0 and  os.path.getsize(sample_id+'_fusion_annot.xls') == 0:
						pass  			#如果全部没有FDA用药信息，则不显示"检测结果解释及用药信息"
					else:
						##添加页眉
						with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									con = con.replace("replace",patient_name)
								if "title_sample_number" in con:
									con = con.replace("replace",line[0])
								f.write(con)
						f.write('\n')
						##添加检测结果解释及用药信息的title信息
						read_detect_title = open(config_path+'title_detect_drug_info.html')
						content_detect_title = read_detect_title.readlines()
						f.write("".join(str(i) for i in content_detect_title))
						f.write('\n')
						##插入深度信息
						# if depth_info == "高深度":
						# 	read_depth = open(config_path+'depth_5000.html')
						# 	content_depth = read_depth.readlines()
						# 	f.write("".join(str(i) for i in content_depth))
						# elif depth_info == "超高深度":
						# 	read_depth = open(config_path+'depth_20000.html')
						# 	content_depth = read_depth.readlines()
						# 	f.write("".join(str(i) for i in content_depth))
						# else:
						# 	depth_des = "depth_2000X"
						# 	read_depth = open(config_path+'depth_2000.html')
						# 	content_depth = read_depth.readlines()
						# 	f.write("".join(str(i) for i in content_depth))
						##添加突变解释表格
						#print(len(FDA_explain_list))
						#SNP分别是基因名称、蛋白突变、基因介绍、	变异介绍、治疗策略、药物信息
						#fusion分别是融合名称、基因介绍、	变异介绍、治疗策略、药物信息
						FDA_explain_list.sort()		#对list进行排序，让相同基因不同突变靠在一起
						geneList = []
						run_num = 0
						fusion_num = 0 		#这个是用于 结果如果大于一个，后面是需要加页眉的
						snp_num = 0
						for i in range(len(FDA_explain_list)):
							detect_info = FDA_explain_list[i].strip().split('\t')
							#print(detect_info)
							if '-' in detect_info[0]:	#代表是融合基因
								if fusion_num == 0 and run_num == 0:	#第一个是fusion,不需要添加页眉，因为上面已经加了
									with open(config_path+'10_var_detect_title.html','r') as DETE:
										for EACH in DETE:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+'融合')
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[1])
											f.write(EACH)
									with open(config_path+'10_var_detect_drug.html','r') as DR:
										for EACH in DR:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+'融合')
											if 'var_description' in EACH:
												if "ExonX-ExonY" in detect_info[2]:	#替换
													connect_exon = detect_info[2].replace("ExonX-ExonY",detect_info[-1])
												else:
													connect_exon = detect_info[2]
												EACH = EACH.replace("replace",connect_exon)
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[4] or ';' in detect_info[4]:		#添加药物，将；改成换行符
													beni_drug = detect_info[4].replace("；","\t")
													beni_drug = detect_info[4].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														Drug = re.sub(u"\\(.*?\\)","",each_drug)
														EACH_copy = EACH_copy.replace("drug_replace",Drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[4]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
													EACH = EACH.replace("other_replace","---")
												else:
													Drug = re.sub(u"\\(.*?\\)","",detect_info[4])
													EACH = EACH.replace("drug_replace",Drug)
													if '1' in detect_info[4] or '2（A）' in detect_info[4] or '2(A)' in detect_info[4]:		#本癌种，根据药物中是否有（1），有打钩，无---
														EACH = EACH.replace("this_replace","&#10003")
													else:
														EACH = EACH.replace("this_replace","---")
													if '2（B）' in detect_info[4] or '2(B)' in detect_info[4]:
														EACH = EACH.replace("other_replace","&#10003")
													else:
														EACH = EACH.replace("other_replace","---")
											if "drug_replace" in EACH:
												continue
											# 	elif "NA" in detect_info[4]:
											# 		EACH = EACH.replace("replace","NA")
											# 	else:
											# 		EACH = EACH.replace("replace",detect_info[4])
											# if 'this_cancer' in EACH:
											# 	if '1' in detect_info[4] or '2（A）' in detect_info[4] or '2(A)' in detect_info[4]:		#本癌种，根据药物中是否有（1），有打钩，无---
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											# if 'other_cancer' in EACH:
											# 	if '2（B）' in detect_info[4] or '2(B)' in detect_info[4]:
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											f.write(EACH)
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n"+change_page+'\n\n\n')				#换页
									fusion_num += 1

								elif fusion_num == 0 and run_num > 0:	#第一个不是fusion,但是后面有fusion,那第一次出现的时候就要加页眉了
									##添加页眉
									with open(config_path+'header.html','r') as FA:
										for con in FA:
											if "title_name" in con:
												con = con.replace("replace",patient_name)
											if "title_sample_number" in con:
												con = con.replace("replace",line[0])
											f.write(con)
									with open(config_path+'10_var_detect_title.html','r') as DETE:
										for EACH in DETE:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+'融合')
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[1])
											f.write(EACH)
									with open(config_path+'10_var_detect_drug.html','r') as DR:
										for EACH in DR:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+'融合')
											if 'var_description' in EACH:
												if "ExonX-ExonY" in detect_info[2]:	#替换
													connect_exon = detect_info[2].replace("ExonX-ExonY",detect_info[-1])
												else:
													connect_exon = detect_info[2]
												EACH = EACH.replace("replace",connect_exon)
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[4] or ';' in detect_info[4]:		#添加药物，将；改成换行符
													beni_drug = detect_info[4].replace("；","\t")
													beni_drug = detect_info[4].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														Drug = re.sub(u"\\(.*?\\)","",each_drug)
														EACH_copy = EACH_copy.replace("drug_replace",Drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[4]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
													EACH = EACH.replace("other_replace","---")
												else:
													Drug = re.sub(u"\\(.*?\\)","",detect_info[4])
													EACH = EACH.replace("drug_replace",Drug)
													if '1' in detect_info[4] or '2（A）' in detect_info[4] or '2(A)' in detect_info[4]:		#本癌种，根据药物中是否有（1），有打钩，无---
														EACH = EACH.replace("this_replace","&#10003")
													else:
														EACH = EACH.replace("this_replace","---")
													if '2（B）' in detect_info[4] or '2(B)' in detect_info[4]:
														EACH = EACH.replace("other_replace","&#10003")
													else:
														EACH = EACH.replace("other_replace","---")
											if "drug_replace" in EACH:
												continue
											# if 'get_drug' in EACH:
											# 	if '；' in detect_info[4] or ';' in detect_info[4]:		#添加药物，将；改成换行符
											# 		beni_drug = detect_info[4].replace("；","<br>")
											# 		beni_drug = detect_info[4].replace(";","<br>")
											# 		EACH = EACH.replace("replace",beni_drug)											
											# 	elif "NA" in detect_info[4]:
											# 		EACH = EACH.replace("replace","NA")
											# 	else:
											# 		EACH = EACH.replace("replace",detect_info[4])
											# if 'this_cancer' in EACH:
											# 	if '1' in detect_info[4] or '2（A）' in detect_info[4] or '2(A)' in detect_info[4]:		#本癌种，根据药物中是否有（1），有打钩，无---
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											# if 'other_cancer' in EACH:
											# 	if '2（B）' in detect_info[4] or '2(B)' in detect_info[4]:
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											f.write(EACH)
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n"+change_page+'\n\n\n')				#换页
									fusion_num += 1
								elif fusion_num > 0:
									#此时就需要添加页眉了
									with open(config_path+'header.html','r') as FA:
										for con in FA:
											if "title_name" in con:
												con = con.replace("replace",patient_name)
											if "title_sample_number" in con:
												con = con.replace("replace",line[0])
											f.write(con)

									with open(config_path+'10_var_detect_title.html','r') as DETE:
										for EACH in DETE:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+'融合')
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[1])
											f.write(EACH)
									with open(config_path+'10_var_detect_drug.html','r') as DR:
										for EACH in DR:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+'融合')
											if 'var_description' in EACH:
												if "ExonX-ExonY" in detect_info[2]:	#替换
													connect_exon = detect_info[2].replace("ExonX-ExonY",detect_info[-1])
												else:
													connect_exon = detect_info[2]
												EACH = EACH.replace("replace",connect_exon)
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[4] or ';' in detect_info[4]:		#添加药物，将；改成换行符
													beni_drug = detect_info[4].replace("；","\t")
													beni_drug = detect_info[4].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														Drug = re.sub(u"\\(.*?\\)","",each_drug)
														EACH_copy = EACH_copy.replace("drug_replace",Drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[4]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
													EACH = EACH.replace("other_replace","---")
												else:
													Drug = re.sub(u"\\(.*?\\)","",detect_info[4])
													EACH = EACH.replace("drug_replace",Drug)
													if '1' in detect_info[4] or '2（A）' in detect_info[4] or '2(A)' in detect_info[4]:		#本癌种，根据药物中是否有（1），有打钩，无---
														EACH = EACH.replace("this_replace","&#10003")
													else:
														EACH = EACH.replace("this_replace","---")
													if '2（B）' in detect_info[4] or '2(B)' in detect_info[4]:
														EACH = EACH.replace("other_replace","&#10003")
													else:
														EACH = EACH.replace("other_replace","---")
											if "drug_replace" in EACH:
												continue
											# if 'get_drug' in EACH:
											# 	if '；' in detect_info[4] or ';' in detect_info[4]:		#添加药物，将；改成换行符
											# 		beni_drug = detect_info[4].replace("；","<br>")
											# 		beni_drug = detect_info[4].replace(";","<br>")
											# 		EACH = EACH.replace("replace",beni_drug)
											# 	elif "NA" in detect_info[4]:
											# 		EACH = EACH.replace("replace","NA")
											# 	else:
											# 		EACH = EACH.replace("replace",detect_info[4])
											# if 'this_cancer' in EACH:
											# 	if '1' in detect_info[4] or '2（A）' in detect_info[4] or '2(A)' in detect_info[4]:		#本癌种，根据药物中是否有（1），有打钩，无---
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											# if 'other_cancer' in EACH:
											# 	if '2（B）' in detect_info[4] or '2(B)' in detect_info[4]:
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											f.write(EACH)
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n"+change_page+'\n\n\n')				#换页		
									fusion_num += 1
							if '-' not in detect_info[0]:		#代表的是单个突变点,此时需要判断基因是否在
								if snp_num == 0 and run_num == 0:	#第一次运行，同时第一个出现的是snp，就不需要添加页眉
									with open(config_path+'10_var_detect_title.html','r') as DETE2:
										for EACH in DETE2:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[2])
											f.write(EACH)
									with open(config_path+'10_var_detect_drug.html','r') as DR2:
										for EACH in DR2:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+" "+detect_info[1])
											if 'var_description' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[4])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
													beni_drug = detect_info[5].replace("；","\t")
													beni_drug = detect_info[5].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														Drug = re.sub(u"\\(.*?\\)","",each_drug)
														EACH_copy = EACH_copy.replace("drug_replace",Drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[5]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
													EACH = EACH.replace("other_replace","---")
												else:
													Drug = re.sub(u"\\(.*?\\)","",detect_info[5])
													EACH = EACH.replace("drug_replace",Drug)
													if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
														EACH = EACH.replace("this_replace","&#10003")
													else:
														EACH = EACH.replace("this_replace","---")
													if '2（B）' in detect_info[5]  or '2(B)' in detect_info[5]:
														EACH = EACH.replace("other_replace","&#10003")
													else:
														EACH = EACH.replace("other_replace","---")
											if "drug_replace" in EACH:
												continue
											# if 'get_drug' in EACH:
											# 	if "NA" in detect_info[5]:		#如果药物为NA
											# 		EACH = EACH.replace("replace","NA")
											# 	elif '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
											# 		beni_drug = detect_info[5].replace("；","<br>")
											# 		beni_drug = detect_info[5].replace(";","<br>")
											# 		EACH = EACH.replace("replace",beni_drug)
											# 	else:
											# 		EACH = EACH.replace("replace",detect_info[5])
											# if 'this_cancer' in EACH:
											# 	if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											# if 'other_cancer' in EACH:
											# 	if '2（B）' in detect_info[5]  or '2(B)' in detect_info[5]:
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											f.write(EACH)				
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n"+change_page+'\n\n\n')				#换页
									snp_num += 1
									geneList.append(detect_info[0])
								elif snp_num == 0 and run_num > 0:	#第一次运行，但是第一个出现的是fusion，这时候就需要添加页眉
									with open(config_path+'header.html','r') as FA:
										for con in FA:
											if "title_name" in con:
												con = con.replace("replace",patient_name)
											if "title_sample_number" in con:
												con = con.replace("replace",line[0])
											f.write(con)
									with open(config_path+'10_var_detect_title.html','r') as DETE2:
										for EACH in DETE2:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[2])
											f.write(EACH)
									with open(config_path+'10_var_detect_drug.html','r') as DR2:
										for EACH in DR2:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+" "+detect_info[1])
											if 'var_description' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[4])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
													beni_drug = detect_info[5].replace("；","\t")
													beni_drug = detect_info[5].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														Drug = re.sub(u"\\(.*?\\)","",each_drug)
														EACH_copy = EACH_copy.replace("drug_replace",Drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[5]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
													EACH = EACH.replace("other_replace","---")
												else:
													Drug = re.sub(u"\\(.*?\\)","",detect_info[5])
													EACH = EACH.replace("drug_replace",Drug)
													if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
														EACH = EACH.replace("this_replace","&#10003")
													else:
														EACH = EACH.replace("this_replace","---")
													if '2（B）' in detect_info[5]  or '2(B)' in detect_info[5]:
														EACH = EACH.replace("other_replace","&#10003")
													else:
														EACH = EACH.replace("other_replace","---")
											if "drug_replace" in EACH:
												continue
											# if 'get_drug' in EACH:
											# 	if "NA" in detect_info[5]:		#如果药物为NA
											# 		EACH = EACH.replace("replace","NA")
											# 	elif '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
											# 		beni_drug = detect_info[5].replace("；","<br>")
											# 		beni_drug = detect_info[5].replace(";","<br>")
											# 		EACH = EACH.replace("replace",beni_drug)
											# 	else:
											# 		EACH = EACH.replace("replace",detect_info[5])
											# if 'this_cancer' in EACH:
											# 	if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											# if 'other_cancer' in EACH:
											# 	if '2（B）' in detect_info[5]  or '2(B)' in detect_info[5]:
											# 		EACH = EACH.replace("replace","&#10003")
											# 	else:
											# 		EACH = EACH.replace("replace","---")
											f.write(EACH)				
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n"+change_page+'\n\n\n')				#换页
									snp_num += 1
									geneList.append(detect_info[0])		
								elif snp_num > 0:
									#print(detect_info[0],detect_info[1])
									#运行第二次，这个时候，需要检测下这次的基因是不是和上次的一样，如果是一样的， 就不要添加基因和基因介绍了
									if detect_info[0] in geneList:	
										#需要添加页眉了
										with open(config_path+'header.html','r') as FA:
											for con in FA:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",line[0])
												f.write(con)
										# with open(config_path+'10_var_detect_title.html','r') as DETE2:
										# 	for EACH in DETE2:
										# 		if 'gene_name' in EACH:
										# 			EACH = EACH.replace("replace",detect_info[0])
										# 		if 'gene_des' in EACH:
										# 			EACH = EACH.replace("replace",detect_info[2])
										# 		f.write(EACH)
										with open(config_path+'10_var_detect_drug.html','r') as DR2:
											for EACH in DR2:
												if 'gene_var' in EACH:
													EACH = EACH.replace("replace",detect_info[0]+" "+detect_info[1])
												if 'var_description' in EACH:
													EACH = EACH.replace("replace",detect_info[3])
												if 'treat_strategy' in EACH:
													EACH = EACH.replace("replace",detect_info[4])
												if 'drug_cancer' in EACH:
													if '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
														beni_drug = detect_info[5].replace("；","\t")
														beni_drug = detect_info[5].replace(";","\t")
														drugs = beni_drug.split('\t')
														for each_drug in drugs:
															EACH_copy = EACH
															Drug = re.sub(u"\\(.*?\\)","",each_drug)
															EACH_copy = EACH_copy.replace("drug_replace",Drug)
															if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
																EACH_copy = EACH_copy.replace("this_replace","&#10003")
															else:
																EACH_copy = EACH_copy.replace("this_replace","---")
															if '2（B）' in each_drug or '2(B)' in each_drug:
																EACH_copy = EACH_copy.replace("other_replace","&#10003")
															else:
																EACH_copy = EACH_copy.replace("other_replace","---")
															f.write(EACH_copy)
													elif "NA" in detect_info[5]:
														EACH = EACH.replace("drug_replace","NA")
														EACH = EACH.replace("this_replace","---")
														EACH = EACH.replace("other_replace","---")
													else:
														Drug = re.sub(u"\\(.*?\\)","",detect_info[5])
														EACH = EACH.replace("drug_replace",Drug)
														if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH = EACH.replace("this_replace","&#10003")
														else:
															EACH = EACH.replace("this_replace","---")
														if '2（B）' in detect_info[5]  or '2(B)' in detect_info[5]:
															EACH = EACH.replace("other_replace","&#10003")
														else:
															EACH = EACH.replace("other_replace","---")
												if "drug_replace" in EACH:
													continue
												# if 'get_drug' in EACH:
												# 	if "NA" in detect_info[5]:		#如果药物为NA
												# 		EACH = EACH.replace("replace","NA")
												# 	elif '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
												# 		beni_drug = detect_info[5].replace("；","<br>")
												# 		beni_drug = detect_info[5].replace(";","<br>")
												# 		EACH = EACH.replace("replace",beni_drug)
												# 	else:
												# 		EACH = EACH.replace("replace",detect_info[5])
												# if 'this_cancer' in EACH:
												# 	if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
												# 		EACH = EACH.replace("replace","&#10003")
												# 	else:
												# 		EACH = EACH.replace("replace","---")
												# if 'other_cancer' in EACH:
												# 	if '2（B）' in detect_info[5]  or '2(B)' in detect_info[5]:
												# 		EACH = EACH.replace("replace","&#10003")
												# 	else:
												# 		EACH = EACH.replace("replace","---")
												f.write(EACH)				
										#再加上页脚
										#添加页脚
										f.write("".join(str(i) for i in content_footer))
										f.write("\n"+change_page+'\n\n\n')				#换页
										snp_num += 1
									else:		#基因不重复，正常添加
										#此时就需要添加页眉了
										#print(detect_info[0])
										with open(config_path+'header.html','r') as FA:
											for con in FA:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",line[0])
												f.write(con)

										with open(config_path+'10_var_detect_title.html','r') as DETE2:
											for EACH in DETE2:
												if 'gene_name' in EACH:
													EACH = EACH.replace("replace",detect_info[0])
												if 'gene_des' in EACH:
													EACH = EACH.replace("replace",detect_info[2])
												f.write(EACH)
										with open(config_path+'10_var_detect_drug.html','r') as DR2:
											for EACH in DR2:
												if 'gene_var' in EACH:
													EACH = EACH.replace("replace",detect_info[0]+" "+detect_info[1])
												if 'var_description' in EACH:
													EACH = EACH.replace("replace",detect_info[3])
												if 'treat_strategy' in EACH:
													EACH = EACH.replace("replace",detect_info[4])
												if 'drug_cancer' in EACH:
													if '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
														beni_drug = detect_info[5].replace("；","\t")
														beni_drug = detect_info[5].replace(";","\t")
														drugs = beni_drug.split('\t')
														for each_drug in drugs:
															EACH_copy = EACH
															Drug = re.sub(u"\\(.*?\\)","",each_drug)
															EACH_copy = EACH_copy.replace("drug_replace",Drug)
															if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
																EACH_copy = EACH_copy.replace("this_replace","&#10003")
															else:
																EACH_copy = EACH_copy.replace("this_replace","---")
															if '2（B）' in each_drug or '2(B)' in each_drug:
																EACH_copy = EACH_copy.replace("other_replace","&#10003")
															else:
																EACH_copy = EACH_copy.replace("other_replace","---")
															f.write(EACH_copy)
													elif "NA" in detect_info[5]:
														EACH = EACH.replace("drug_replace","NA")
														EACH = EACH.replace("this_replace","---")
														EACH = EACH.replace("other_replace","---")
													else:
														Drug = re.sub(u"\\(.*?\\)","",detect_info[5])
														EACH = EACH.replace("drug_replace",Drug)
														if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH = EACH.replace("this_replace","&#10003")
														else:
															EACH = EACH.replace("this_replace","---")
														if '2（B）' in detect_info[5]  or '2(B)' in detect_info[5]:
															EACH = EACH.replace("other_replace","&#10003")
														else:
															EACH = EACH.replace("other_replace","---")
												if "drug_replace" in EACH:
													continue
												# if 'get_drug' in EACH:
												# 	if "NA" in detect_info[5]:		#如果药物为NA
												# 		EACH = EACH.replace("replace","NA")
												# 	elif '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
												# 		beni_drug = detect_info[5].replace("；","<br>")
												# 		beni_drug = detect_info[5].replace(";","<br>")
												# 		EACH = EACH.replace("replace",beni_drug)
												# 	else:
												# 		EACH = EACH.replace("replace",detect_info[5])
												# if 'this_cancer' in EACH:
												# 	if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
												# 		EACH = EACH.replace("replace","&#10003")
												# 	else:
												# 		EACH = EACH.replace("replace","---")
												# if 'other_cancer' in EACH:
												# 	if '2（B）' in detect_info[5]  or '2(B)' in detect_info[5]:
												# 		EACH = EACH.replace("replace","&#10003")
												# 	else:
												# 		EACH = EACH.replace("replace","---")
												f.write(EACH)				
										#再加上页脚
										#添加页脚
										f.write("".join(str(i) for i in content_footer))
										f.write("\n"+change_page+'\n\n\n')				#换页
										geneList.append(detect_info[0])
										snp_num += 1
							run_num += 1

					#################<--------------   添加临床试验信息   ----------------->#################			
					#设置一个上限，规定每页输出几个条目的clinical信息
					limit = 3
					#先获取clinical表格条目数，可认为是行数
					clinical_num =  len(open(sample_id+'_clinical.xls','r').readlines())
					#print(clinical_num)
					##如果clinical_num为0 ，无临床试验信息
					if clinical_num == 0:
						###添加页眉
						with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									con = con.replace("replace",patient_name)
								if "title_sample_number" in con:
									con = con.replace("replace",line[0])
								f.write(con)
						f.write('\n')
						###添加clinical_info的title信息
						read_clinical_info = open(config_path+'title_clincal_info.html')
						content_clinical_info = read_clinical_info.readlines()
						f.write("".join(str(i) for i in content_clinical_info))
						f.write('\n')
						###添加clinical表头信息
						read_clinical_sheet_title = open(config_path+'11_clinical_sheet_title.html')
						content_clinical_sheet_title = read_clinical_sheet_title.readlines()
						f.write("".join(str(i) for i in content_clinical_sheet_title))
						f.write('\n')
						###clinical的内容全部都是---
						read_neg_clinical_title = open(config_path+'11_neg_clinical_sheet_content.html')
						content_neg_clinical_title = read_neg_clinical_title.readlines()
						f.write("".join(str(i) for i in content_neg_clinical_title))
						f.write('\n')
						#读取签名页
						read_sign_name = open(config_path+'12_sign_name.html')
						content_sign_name = read_sign_name.readlines()
						f.write("".join(str(i) for i in content_sign_name))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))	#添加页脚
						f.write("\n"+change_page+'\n\n\n')				#换页

					pipei_list = ["Drug","Biological","Procedure","Other","Device","Behavioral"]
					if clinical_num > 0:
						clinical_list = []
						with open(sample_id+'_clinical.xls','r') as CLIN:
							for rows in CLIN:
								if rows not in clinical_list:
									clinical_list.append(rows)
						#print(len(clinical_list))
						##先确定所需要添加页数
						pages = (clinical_num-1) // limit + 1
						#<-----------如果只有一页临床试验------------>#
						if pages == 1:
							with open(config_path+'header.html','r') as FA:
								for con in FA:
									if "title_name" in con:
										con = con.replace("replace",patient_name)
									if "title_sample_number" in con:
										con = con.replace("replace",line[0])
									f.write(con)
							f.write('\n')
							###添加clinical_info的title信息
							read_clinical_info = open(config_path+'title_clincal_info.html')
							content_clinical_info = read_clinical_info.readlines()
							f.write("".join(str(i) for i in content_clinical_info))
							f.write('\n')
							read_clinical_sheet_title = open(config_path+'11_clinical_sheet_title.html')
							content_clinical_sheet_title = read_clinical_sheet_title.readlines()
							f.write("".join(str(i) for i in content_clinical_sheet_title))
							f.write('\n')
							#clinical格式是，基因，癌种，敏感/不敏感，药物名称，阶段，证据类型，试验名称，ID
							n = 0
							for i in range(len(clinical_list)):
								if n < limit:		#保证每页最多三个条目
									clinical_info = clinical_list[0].split('\t')
									with open(config_path+'11_clinical_sheet_content.html','r') as CI:
										for row in CI:
											if "clinical_info_gene" in row:
												row = row.replace("replace",clinical_info[0])
											if "clinical_info_name" in row:
												row = row.replace("replace",clinical_info[2])
											if "clinical_info_drug" in row:
												for key in pipei_list:
													if key in clinical_info[5]:
														clinical_info[5] = clinical_info[5].replace(key,"<br>"+key)
												row = row.replace("replace",clinical_info[5])
											if "clinical_info_step" in row:
												row = row.replace("replace",clinical_info[6])
											if "clinical_info_NCI" in row:
												row = row.replace("replace",clinical_info[1])
											f.write(row)
									del clinical_list[0]	#删除第一项
									n += 1
							#读取签名页
							read_sign_name = open(config_path+'12_sign_name.html')
							content_sign_name = read_sign_name.readlines()
							f.write("".join(str(i) for i in content_sign_name))
							f.write('\n')
							#还要给每页添加页脚和换行
							f.write("".join(str(i) for i in content_footer))
							f.write("\n"+change_page+'\n\n\n')				#换页
						#<---------如果临床试验大于一页---------->#
						if pages > 1:
							for page in range(pages):
								#给每页添加页眉 以及表头
								if page == 0 :  #第一页，无签名
									with open(config_path+'header.html','r') as FA:
										for con in FA:
											if "title_name" in con:
												con = con.replace("replace",patient_name)
											if "title_sample_number" in con:
												con = con.replace("replace",line[0])
											f.write(con)
									###添加clinical_info的title信息
									read_clinical_info = open(config_path+'title_clincal_info.html')
									content_clinical_info = read_clinical_info.readlines()
									f.write("".join(str(i) for i in content_clinical_info))
									f.write('\n')
									read_clinical_sheet_title = open(config_path+'11_clinical_sheet_title.html')
									content_clinical_sheet_title = read_clinical_sheet_title.readlines()
									f.write("".join(str(i) for i in content_clinical_sheet_title))
									f.write('\n')
									#clinical格式是，基因，癌种，敏感/不敏感，药物名称，阶段，证据类型，试验名称，ID
									n = 0
									for i in range(len(clinical_list)):
										if n < limit:		#保证每页最多三个条目
											clinical_info = clinical_list[0].split('\t')
											with open(config_path+'11_clinical_sheet_content.html','r') as CI:
												for row in CI:
													if "clinical_info_gene" in row:
														row = row.replace("replace",clinical_info[0])
													if "clinical_info_name" in row:
														row = row.replace("replace",clinical_info[2])
													if "clinical_info_drug" in row:
														for key in pipei_list:
															if key in clinical_info[5]:
																clinical_info[5] = clinical_info[5].replace(key,"<br>"+key)
														row = row.replace("replace",clinical_info[5])
													if "clinical_info_step" in row:
														row = row.replace("replace",clinical_info[6])
													if "clinical_info_NCI" in row:
														row = row.replace("replace",clinical_info[1])
													f.write(row)
											del clinical_list[0]	#删除第一项
											n += 1
									#还要给每页添加页脚和换行
									f.write("".join(str(i) for i in content_footer))
									f.write("\n"+change_page+'\n\n\n')				#换页

								if 0 < page < pages -1:
									with open(config_path+'header.html','r') as FA:
										for con in FA:
											if "title_name" in con:
												con = con.replace("replace",patient_name)
											if "title_sample_number" in con:
												con = con.replace("replace",line[0])
											f.write(con)
									f.write('\n')
									###添加clinical_info_sheet的title信息
									read_clinical_sheet_title = open(config_path+'11_clinical_sheet_title.html')
									content_clinical_sheet_title = read_clinical_sheet_title.readlines()
									f.write("".join(str(i) for i in content_clinical_sheet_title))
									f.write('\n')
									n = 0
									for i in range(len(clinical_list)):
										if n < limit:		#保证每页最多三个条目
											clinical_info = clinical_list[0].split('\t')
											with open(config_path+'11_clinical_sheet_content.html','r') as CI:
												for row in CI:
													if "clinical_info_gene" in row:
														row = row.replace("replace",clinical_info[0])
													if "clinical_info_name" in row:
														row = row.replace("replace",clinical_info[2])
													if "clinical_info_drug" in row:
														for key in pipei_list:
															if key in clinical_info[5]:
																clinical_info[5] = clinical_info[5].replace(key,"<br>"+key)
														row = row.replace("replace",clinical_info[5])
													if "clinical_info_step" in row:
														row = row.replace("replace",clinical_info[6])
													if "clinical_info_NCI" in row:
														row = row.replace("replace",clinical_info[1])
													f.write(row)
											del clinical_list[0]	#删除第一项
											n += 1
									#还要给每页添加页脚和换行
									f.write("".join(str(i) for i in content_footer))
									f.write("\n"+change_page+'\n\n\n')				#换页
								if page == pages -1 and page != 0:	#确定是最后一页，因为要在最后一页添加签名档
									with open(config_path+'header.html','r') as FA:
										for con in FA:
											if "title_name" in con:
												con = con.replace("replace",patient_name)
											if "title_sample_number" in con:
												con = con.replace("replace",line[0])
											f.write(con)
									f.write('\n')
									###添加clinical_info_sheet的title信息
									read_clinical_sheet_title = open(config_path+'11_clinical_sheet_title.html')
									content_clinical_sheet_title = read_clinical_sheet_title.readlines()
									f.write("".join(str(i) for i in content_clinical_sheet_title))
									f.write('\n')
									n = 0
									for i in range(len(clinical_list)):
										if n < limit:		#保证每页最多三个条目
											clinical_info = clinical_list[0].split('\t')
											with open(config_path+'11_clinical_sheet_content.html','r') as CI:
												for row in CI:
													if "clinical_info_gene" in row:
														row = row.replace("replace",clinical_info[0])
													if "clinical_info_name" in row:
														row = row.replace("replace",clinical_info[2])
													if "clinical_info_drug" in row:
														for key in pipei_list:
															if key in clinical_info[5]:
																clinical_info[5] = clinical_info[5].replace(key,"<br>"+key)
														row = row.replace("replace",clinical_info[5])
													if "clinical_info_step" in row:
														row = row.replace("replace",clinical_info[6])
													if "clinical_info_NCI" in row:
														row = row.replace("replace",clinical_info[1])
													f.write(row)
											del clinical_list[0]	#删除第一项
											n += 1
									#读取签名页
									read_sign_name = open(config_path+'12_sign_name.html')
									content_sign_name = read_sign_name.readlines()
									f.write("".join(str(i) for i in content_sign_name))
									f.write('\n')
									#还要给每页添加页脚和换行
									f.write("".join(str(i) for i in content_footer))
									f.write("\n"+change_page+'\n\n\n')				#换页


					#读取签名页
					# read_sign_name = open(config_path+'12_sign_name.html')
					# content_sign_name = read_sign_name.readlines()
					# f.write("".join(str(i) for i in content_sign_name))
					# f.write("".join(str(i) for i in content_footer))	#添加页脚
					# f.write("\n"+change_page+'\n\n\n')				#换页

					#添加质量控制
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)
					f.write('\n')
					##添加控制内容
					read_quality_control = open(config_path+'13_quality_control.html')
					content_quality_control = read_quality_control.readlines()
					f.write("".join(str(i) for i in content_quality_control))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页


					#添加样品质量评级
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)
					f.write('\n')
					##添加评级内容
					read_sample_quality_rate = open(config_path+'14_sample_quality_rate.html')
					content_sample_quality_rate = read_sample_quality_rate.readlines()
					f.write("".join(str(i) for i in content_sample_quality_rate))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页


					#添加数据质控结果
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",line[0])
							f.write(con)
					f.write('\n')
					##添加质控内容，打开control_file，根据sample_id获取数据质控信息
					with open(control_file,'r') as CONTROL:
						for zhikong in CONTROL:
							zhikong = zhikong.strip().split('\t')
							if sample_id in zhikong[0]:
								zhikong_Q20 = '%.2f%%' % (float(zhikong[1]) * 100)
								zhikong_bidui = '%.2f%%' % (float(zhikong[2]) * 100)
								zhikong_fugai = '%.2f%%' % (float(zhikong[3]) * 100)
								zhikong_junyi = '%.2f%%' % (float(zhikong[4]) * 100)
								zhikong_shendu = str(zhikong[5])
					with open(config_path+'data_control_result.html','r') as CON_HTML:
						for con in CON_HTML:
							if "sample_id" in con:
								con = con.replace("replace",sample_id)
							if "Q20_data" in con:
								con = con.replace("replace",zhikong_Q20)
							if "bidui_data" in con:
								con = con.replace("replace",zhikong_bidui)
							if "fugai_data" in con:
								con = con.replace("replace",zhikong_fugai)
							if "junyi_data" in con:
								con = con.replace("replace",zhikong_junyi)
							if "shendu_data" in con:
								con = con.replace("replace",zhikong_shendu)
							f.write(con)
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页





					#循环添加附录1
					for gene_name in gene_10_list:
						##先添加页眉
						with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",line[0])
									continue
								f.write(con)
						f.write('\n')
						##再添加内容
						read_gene = open(config_path+'fulu1_'+gene_name+'.html')
						read_content = read_gene.readlines()
						f.write("".join(str(i) for i in read_content))
						f.write('\n')
						##再添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n"+change_page+'\n\n\n')				#换页

					#添加药物证据等级
					##读取页眉
					with open(config_path+'header.html','r') as FA:
						for con in FA:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",line[0])
								continue
							f.write(con)
					f.write('\n')
					##添加内容
					read_drug_grade = open(config_path+'25_drug_grade.html')
					content_drug_grade = read_drug_grade.readlines()
					f.write("".join(str(i) for i in content_drug_grade))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n"+change_page+'\n\n\n')				#换页

					#添加慈善援助药物信息
					for help_page in ["first",'second','third']:
						##添加页眉
						with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",line[0])
									continue
								f.write(con)
						f.write('\n')
						read_help_drug = open(config_path+'help_drug_info_'+help_page+'.html')
						content_help_drug = read_help_drug.readlines()
						f.write("".join(str(i) for i in content_help_drug))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n"+change_page+'\n\n\n')				#换页

					#添加参考文献,先循环前4页，因为最后页不需要换页，所以单独拿出来
					for wenxian_page in ["first",'second','third','fourth']:
						##添加页眉
						with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",line[0])
									continue
								f.write(con)
						f.write('\n')
						read_wenxian = open(config_path+'wenxian_'+wenxian_page+'.html')
						content_wenxian = read_wenxian.readlines()
						f.write("".join(str(i) for i in content_wenxian))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n"+change_page+'\n\n\n')				#换页

					#添加最后一页页眉
					with open(config_path+'header.html','r') as FA:
							for con in FA:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",line[0])
									continue
								f.write(con)
					f.write('\n')
					read_last_wenxian = open(config_path+'wenxian_fifth.html')
					content_last_wenxian = read_last_wenxian.readlines()
					f.write("".join(str(i) for i in content_last_wenxian))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.close()

					#获取需要替换的所有基因名称
					report_gene_list = []
					with open(report_gene_file,'r') as GE:
						for k in GE:
							k = k.strip()
							if k not in report_gene_list:
								report_gene_list.append(k)
							else:
								pass

					#先替换页码
					count = 0
					tmp2_report = open(report_path+'/'+report_path+'.tmp2.html','w')
					with open(report_path+'/'+report_path+'.tmp.html','r') as HTML:
						for each in HTML:
							if "footer_end" in each:
								count += 1
								each = each.replace("replace",str(count))
							tmp2_report.write(each)
					tmp2_report.close()
					#再将所有的基因改成斜体
					final_report = open(report_path+'/'+report_path+'.html','w')
					with open(report_path+'/'+report_path+'.tmp2.html','r') as TMP2:
						for each in TMP2:
							if "charset" in each:
								final_report.write(each)
								continue
							if 'ERBB2' in each:
								each = each.replace('ERBB2','HER2')
							for GENE in report_gene_list:
								if GENE in each:
									new_gene = '<i>'+GENE+' </i>'
									#print(new_gene)
									each = each.replace(GENE,new_gene)
							final_report.write(each)
					final_report.close()
					os.system("rm %s/%s.tmp.html %s/%s.tmp2.html" % (report_path,report_path,report_path,report_path))
					os.system("/amoydx/USER/wujianming/software/miniconda-python3/bin/wkhtmltopdf --encoding utf8 --quiet %s/%s.html %s/%s.pdf" %(report_path,report_path,report_path,report_path))





if __name__ == '__main__':
	parser = argparse.ArgumentParser()     #建立解析器
	parser.add_argument("--somatic", "-S", help="somatic file", required=False)
	parser.add_argument("--fusion", "-F", help="fusion file", required=False)
	parser.add_argument("--samples_file", "-sampleInfo", help="samples info file", required=False)
	parser.add_argument("--negative", "-N", help="give the negative file", required=False)
	parser.add_argument("--control", "-C", help="give the samples data control file", required=False)
	args = parser.parse_args()       #从外部传递参数到此
	#get_sample_id(args.samples_file,args.negative)
	get_negative_report(args.samples_file,args.negative,args.control)
	#get_positive_id(args.somatic,args.fusion,args.samples_file)
	get_positive_report(args.somatic,args.fusion,args.samples_file,args.control)
