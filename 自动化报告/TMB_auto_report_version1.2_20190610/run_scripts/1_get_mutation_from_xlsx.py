#!/bin/env python
#coding=utf-8
from __future__ import division
import argparse
import xlrd
import sys
import re
import os

def get_quality_dict(quality_file):
	#读取质量分级文件，将里面的内容存于dict中
	#生成质量质控dict，以及病理质控dict
	quality_dict = {}
	pathology_dict = {}
	qualitybook = xlrd.open_workbook(quality_file)
	quality_sheet_name = qualitybook.sheet_names()	#循环读取xlsx里的标签页
	for each in quality_sheet_name:
		count = 0
		quality_info_sheet = qualitybook.sheet_by_name(each)
		for each_row in range(quality_info_sheet.nrows):	#按行读取
			rows = quality_info_sheet.row_values(each_row)
			#content = "\t".join(str(test) for test in rows) 
			if count == 0:
				count += 1
				continue
			pattern = re.compile(r'\d+')
			result = pattern.findall(rows[4])		#获取癌种百分比数值,衡量样本质量分级（ABCD）
			if len(result) == 0:
				result_type = "C"
				pathology_final_result = ""
			else:
				result = int(result[0])
				if result >= 20:
					result_type = "A"
				if result < 20:
					result_type = "B"
			quality_dict[rows[0]] = result_type 		#键值为sample_id : A/B/C
			#目前10基因的病理结果，都是以肿瘤细胞占比XX%
			pathology_final_result = "肿瘤细胞占比"+str(result)+"%"+'\t'+str(result)
			pathology_dict[rows[0]] = pathology_final_result	#键值为sample_id 
	return(quality_dict,pathology_dict)
			

def get_sample_info(guest_info_file,quality_file):		#输入文件为客户信息汇总表，如2017_info_12.31.xls
	info_dict = {}
	quality_dict,pathology_dict = get_quality_dict(quality_file)
	infobook = xlrd.open_workbook(guest_info_file)
	info_sheet_name = infobook.sheet_names()
	for each in info_sheet_name:
		each = each.strip()
		count = 0
		sample_info_sheet = infobook.sheet_by_name(each)
		for each_row in range(sample_info_sheet.nrows):
			rows = sample_info_sheet.row_values(each_row)
			if count == 0:
				count += 1
				continue
			sample_id = rows[2]
			hospital = rows[5]
			if '-' in hospital:
				hospital = rows[5].split('-')[0]
			else:
				hospital = rows[5]
			patient_name = rows[6]
			patient_sex = rows[7]
			patient_age = rows[8]
			patient_dignose = rows[11]	#病理诊断
			patient_dignose_num = rows[12]
			sample_date = rows[4]	#格式类似43098.62190972222
			sample_type_split = re.split('[,，]',rows[14])	#可能有多少送样类型
			sample_type_split = [i.replace("全血对照","全血") for i in sample_type_split]
			if len(rows[15]) != 0:
				if rows[15][-1] == '、':
					sample_type_num = rows[15][:-1].split("、")
				else:
					sample_type_num = rows[15].split('、')
			detect_project = rows[18]	#检验项目
			#print(sample_id,sample_type_split,sample_type_num)
			sample_date = xlrd.xldate.xldate_as_datetime(sample_date,0)
			sample_date = sample_date.strftime("%Y-%m-%d")
			if sample_id in quality_dict.keys():
				quality_result = quality_dict[sample_id]
			else:
				quality_result = "C"
			quality_result = "{}.II".format(quality_result) ###暂时，所有的结果都是II
			if sample_id in pathology_dict.keys():
				pathology_result = pathology_dict[sample_id].split('\t')[0]
				pathology_value = pathology_dict[sample_id].split('\t')[1]
			else:
				pathology_result = "NA"
				pathology_value = "NA"
			
			if "其他（" in patient_dignose:
				patient_dignose = re.sub("其他（","",patient_dignose)
				patient_dignose = re.sub("）","",patient_dignose)
			
			if len(sample_type_split) == 0:
				sample_type1 = "NA"
				sample_num1 = "NA"
				sample_type2 = "NA"
				sample_num2 = "NA"
			elif len(sample_type_split) == 1:
				sample_type1 = sample_type_split[0]
				sample_num1 = sample_type_num[0]
				sample_type2 = "NA"
				sample_num2 = "NA"
			elif len(sample_type_split) > 1:
				if len(sample_type_num) == len(sample_type_split) :
					if "全血" in sample_type_split and "全血" not in sample_type_split[0]:
						n = 0
						for each in sample_type_split:
							if "全血" in each:
								break
							else:
								n += 1
						sample_type_split.remove("全血")
						sample_type_split.insert(0,"全血")
						num = sample_type_num[n]
						del sample_type_num[n]
						sample_type_num.insert(0,num)
					else:
						pass
					sample_type1 = sample_type_split[0]
					sample_num1 = sample_type_num[0]
					sample_type2 = sample_type_split[1]
					sample_num2 = sample_type_num[1]
				else:
				  print("ERROR: %s 样本类型数目 和 样本数量数目不一致---> %s VS %s" % (str(sample_id),",".join(i for i in sample_type_split),",".join(i for i in sample_type_num)))
				  sys.exit(1)
			new_rows = [sample_id,hospital,patient_name,patient_sex,patient_age,patient_dignose,patient_dignose_num,sample_date,sample_type1,sample_type2,sample_num1,sample_num2,quality_result,pathology_result,pathology_value,detect_project]
			new_rows = "\t".join(str(test) for test in new_rows)
			info_dict[sample_id] = new_rows
	return(info_dict)


def get_somatic_fusion_info(samples_file,guest_info_file,quality_file,PDL1_file,outdir): #输入文件为样本测序信息xlsx
	
	#创建输出目录，判断是否存在
	if not os.path.exists(outdir):
		os.mkdir(outdir)
	else:
		pass
	info_dict = get_sample_info(guest_info_file,quality_file)
	DataSample = []
	samples_dict = {}
	control_dict = {}
	samples_book = xlrd.open_workbook(samples_file)
	somatic_sheet_name = samples_book.sheet_names()
#	somatic_sheet_name = somatic_sheet_name.strip()	
	somatic_sheet_dict = {}
	for each in somatic_sheet_name:
		each = each.strip()	#去除可能存在的空格
		somatic_sheet_dict[each] = 1

	#确定somatic和fusion的sheet名称,现在使用Final和FinalFusion这两个人工审核后的内容，只要这里面有内容，直接报，by wjm at 20181115
	Dataproduction_sheet_by_name = "DataProduction"
	somatic_sheet_by_name = "Final"
	fusion_sheet_by_name = "FinalFusion"
	###计算本次数据所有的样本
	dataproduction_sheet = samples_book.sheet_by_name(Dataproduction_sheet_by_name)
	for each in range(dataproduction_sheet.nrows):
		rows =  dataproduction_sheet.row_values(each)
		sample_id = rows[0]
		if '.' in str(sample_id) or 'Sample' in sample_id or rows[0][-1] == "W":
			continue
		#sample_id = re.sub("[A-Z]$","",sample_id)   edit by wujianming at 20181112
		if "F" in rows[0][:2]:
			if rows[0][0] == "S":
				sample_id = rows[0][:8]
			else:
				sample_id = rows[0][:7]
		if "F" not in rows[0][:2]:
			if rows[0][0] == "S":
				sample_id = rows[0][:9]
			else:
				sample_id = rows[0][:8]
		if sample_id in info_dict.keys():
			if sample_id not in DataSample:
				DataSample.append(sample_id)  #如果sample_id在 客户信息表(接收表)中存在，计入DataSamplezhong 
			else:
				pass
		##判断样本类型(血液还是组织，用于后面区分数据质控模板)
		if "F" in rows[0][3:] or "T" in rows[0][3:]:
			sample_type = "tissue"
		elif "P" in rows[0][3:]:
			sample_type = "blood"
		else:
			print("%s has no 'T' or 'F' or 'P' key word! EXIT!" % str(sample_id))
			sys.exit(1)
		#样本的质控信息： sample_id , Q20 , 比对率, 覆盖度, dsbc均一性, 平均深度，样本类型
		#control_dict[rows[0]] = rows[0]+'\t'+str(rows[9])+'\t'+str(rows[11])+'\t'+str(rows[21])+'\t'+ str(rows[27])+'\t'+str(rows[22])
		control_dict[sample_id] = str(sample_id)+'\t'+str(rows[9])+'\t'+str(rows[11])+'\t'+str(rows[21])+'\t'+ str(rows[27])+'\t'+str(rows[22])
	#计算somatic sheet
	excel_name = (samples_file.split('/'))[-1]
	#print(excel_name)
	negative_out_file = open("{}/{}_negative.xls".format(outdir,excel_name),'w')
	somatic_out_file = open("{}/{}_somatic.xls".format(outdir,excel_name),'w')
	somatic_sheet = samples_book.sheet_by_name(somatic_sheet_by_name)
	all_somatic = []
	all_fusion = []
	for each in range(somatic_sheet.nrows):
		rows = somatic_sheet.row_values(each)
		#只要里面有内容，就报
		content = "\t".join(str(i) for i in rows)
		somatic_out_file.write(content+"\n")
#		if rows[0] not in all_somatic:
#			all_somatic.append(rows[0])
#		else:
#			pass
		if "F" in rows[0][:2]:
			if rows[0][0] == "S":
				sample_id = rows[0][:8]
			else:
				sample_id = rows[0][:7]
		if "F" not in rows[0][:2]:
			if rows[0][0] == "S":
				sample_id = rows[0][:9]
			else:
				sample_id = rows[0][:8]

		###判断，如果样本ID不在sample_info中，则跳过
		if sample_id not in info_dict.keys():
			continue
		samples_dict[sample_id] = 1 ###加入samples_dict中
	for i in samples_dict.keys():
		if i not in DataSample:
			print('somatic'+i)
			negative_out_file.write(i+'\n')
		else:
			pass

	####计算fusion sheet,同时输出sample_info（即需要出报告的样本），negative（阴性样本）。
	fusion_out_file = open("{}/{}_fusion.xls".format(outdir,excel_name),'w')
	sample_info_out_file = open("{}/{}_sample_info.xls".format(outdir,excel_name),'w')
	control_out_file = open("{}/{}_control.xls".format(outdir,excel_name),'w')
	control_out_file.write("Sample"+'\t'+'TotalQ20'+'\t'+"Alignment"+'\t'+"Coverage"+'\t'+"DSBCUni-20%"+'\t'+"MeanDepth"+'\t'+"sample_type"+'\t'+"MSI"+'\t'+"PDL1"+'\n')
	MSI_dict = {}
	
	# for value in control_dict.values():
	# 	control_out_file.write(value+'\tMSS'+'\n')
	# control_out_file.close()
	#添加MSI信息
	# MSI_file = ".".join(str(i) for i in samples_file.split('.')[:-1]) + '_MSI_result.xlsx'
	# msi_book = xlrd.open_workbook(MSI_file)
	# msi_sheet = msi_book.sheet_by_name("Sheet1")
	# for con in range(msi_sheet.nrows):
	# 	lines = msi_sheet.row_values(con)
	# 	if "F" in lines[0][:2]:
	# 		if lines[0][0] == "S":
	# 			sample_id = lines[0][:8]
	# 		else:
	# 			sample_id = lines[0][:7]
	# 	if "F" not in lines[0][:2]:
	# 		if lines[0][0] == "S":
	# 			sample_id = lines[0][:9]
	# 		else:
	# 			sample_id = lines[0][:8]
	# 	MSI_dict[sample_id] = lines[-1]

	# for sample in DataSample:
	# 	control_out_file.write(control_dict[sample]+'\t'+MSI_dict[sample]+'\n')
	# control_out_file.close()

	##添加PDL1信息
	PDL1_dict = {}
	PDL1_book = xlrd.open_workbook(PDL1_file)
	PDL1_sheet = PDL1_book.sheet_by_name("Sheet1")
	for con in range(PDL1_sheet.nrows):
		lines = PDL1_sheet.row_values(con)
		PDL1_dict[lines[0]] = lines[1]

	for sample in DataSample:
		if sample in PDL1_dict:
			control_out_file.write(control_dict[sample]+'\t'+"MSS\t"+PDL1_dict[sample]+'\n')
		else:
			control_out_file.write(control_dict[sample]+'\t'+"MSS\tNA"+'\n')

	control_out_file.close()




	fusion_sheet = samples_book.sheet_by_name(fusion_sheet_by_name)
	for line in range(fusion_sheet.nrows):
		rows = fusion_sheet.row_values(line)
		out = "\t".join(str(test) for test in rows)
		fusion_out_file.write(out+'\n')
#		if rows[0] not in all_fusion:
#			all_fusion.append(rows[0])
#		else:
#			pass
#		print(all_fusion)
		if "F" in rows[0][:2]:
			if rows[0][0] == "S":
				sample_id = rows[0][:8]
			else:
				sample_id = rows[0][:7]
		if "F" not in rows[0][:2]:
			if rows[0][0] == "S":
				sample_id = rows[0][:9]
			else:
				sample_id = rows[0][:8]
		if sample_id not in info_dict.keys(): 
			continue
		samples_dict[sample_id] = 1

	
	for i in samples_dict.keys():
		if i not in DataSample:
			print('fusion'+i)
			negative_out_file.write(i+'\n')
		else:
			pass
#	print(samples_dict)
	for each in DataSample:
		if each in info_dict.keys():
			sample_info_out_file.write(info_dict[each]+"\n")
		else:
			print ("{}\tdo not have sample info!pay attention!\n".format(each))

		if each not in samples_dict.keys():
			negative_out_file.write(each + "\n")
	sample_info_out_file.close()
	negative_out_file.close()
	fusion_out_file.close()
	



if __name__ == '__main__':
	parser = argparse.ArgumentParser()     #建立解析器
	parser.add_argument("--quality_file", "-Q", help="samples quality file", required=False)
	parser.add_argument("--guest_info_file", "-I", help="guest info file", required=False)
	parser.add_argument("--samples_file", "-S", help="sampels info file", required=False)
	parser.add_argument("--out_dir", "-O", help="the dir of output", required=False)
	parser.add_argument("--PDL1_file","-P", help="the file of PD-L1", required=False)
	args = parser.parse_args()       #从外部传递参数到此
#	get_quality_dict(args.quality_file)
#	get_sample_info(args.guest_info_file,args.quality_file)
	get_somatic_fusion_info(args.samples_file,args.guest_info_file,args.quality_file,args.PDL1_file,args.out_dir)
