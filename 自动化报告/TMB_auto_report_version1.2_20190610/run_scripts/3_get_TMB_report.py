
#!/bin/env python
#coding=utf-8
from __future__ import division
import pandas as pd
import argparse
import datetime
import xlrd
import re
import os

change_page = '<div style="page-break-after:always;"></div>'
#mac
#report_gene_file = "/Users/jianming.wu/work/program/auto_report/TMB_auto_report/database/report_replace_genelist"
#config_path = "/Users/jianming.wu/work/program/auto_report/TMB_auto_report/TMB_template_database/"
#pic_dir = "/Users/jianming.wu/work/program/auto_report/TMB_auto_report/TMB_report_templet_20181213/genepic"
#css_path = "/Users/jianming.wu/work/program/auto_report/TMB_auto_report/TMB_report_templet_20181213/TMB_new_style.css"

#amoydx
TMB_MSI_clinical  = "/amoydx/appclub/TMB_auto_report_version1.0_20181219/database/TMB_MSI_clinical.txt"
report_gene_file = "/amoydx/appclub/TMB_auto_report_version1.0_20181219/database/report_replace_genelist"
config_path = "/amoydx/appclub/TMB_auto_report_version1.0_20181219/TMB_template_database/"
pic_dir = "/amoydx/appclub/TMB_auto_report_version1.0_20181219/TMB_report_templet_20181213/genepic"
css_path = "/amoydx/appclub/TMB_auto_report_version1.0_20181219/TMB_report_templet_20181213/TMB_new_style.css"

def get_neg_id(negative_file):
	neg_id = []
	with open(negative_file,'r') as NEG:
		for line in NEG:
			line = line.strip()
			if str(line)  not in neg_id:
				neg_id.append(str(line))
	return(neg_id)

def get_pos_id(negative_file,control_file):
	pos_id = []
	neg_id = get_neg_id(negative_file)
	dat = pd.read_csv(control_file,sep='\t')
	sampleList = dat['Sample'].tolist()
	for i in sampleList:
		if str(i) not in neg_id:
			if str(i) not in pos_id:
				pos_id.append(str(i))
			else:
				pass
		else:
			pass
	print(neg_id)
	#print(pos_id)
	return(pos_id)

def get_neg_report(sample_info_file,control_file,negative_file):
	now_time = datetime.datetime.now()
	report_date = now_time.strftime('%Y-%m-%d')	#默认报告日期为当天日期
	neg_sampleList = get_neg_id(negative_file)
	for sample in neg_sampleList:
		sample = str(sample)		#有时候sample是一串数字，需要把他们转成字符串
		with open(sample_info_file,'r') as SAMPLE:
			for line in SAMPLE:
				line = line.strip().split('\t')
				if line[0] == sample: 
					hospital_name = line[1]
					patient_name = line[2]
					patient_name = patient_name.strip()
					if line[3] != "男" and line[3] != "女":		#性别除了这两个选项外，为空
						patient_sex = ""
					else:
						patient_sex = line[3]
					if str(line[4]) == "0.0":		#如果年龄为0，为空
						patient_age = ""
					else:
						patient_age = int(float(line[4]))
					if str(line[5]) == "0":
						patient_dignose = ""
					else:
						patient_dignose = line[5]	#病理诊断
					patient_dignose_num = line[6]	#病理编号
					accept_date = line[7]	#接收日期
					sample_type1 = line[8] 	#样本类型1
					sample_type2 = line[9]		#样本类型2
					if sample_type2 == "NA":
						sample_type2 = ""
					sample_num1 = line[10]
					sample_num2 = line[11]		#样本数量2
					if sample_num2 == "NA":
						sample_num2 = ""				
					sample_quality = line[12]
					dignose_result = line[13]
					if dignose_result == "NA":
						dignose_result = ""
					detect_project = line[-1]
					tumor_num = line[-2]
					with open(control_file,'r') as CON:
						for con in CON:
							con = con.strip().split('\t')
							if con[0] == sample:
								TMB_num = con[-1]
								if con[-3] == "MSS":
									MSI_stat = "MSS"
								elif con[-3] == "MSI":
									MSI_stat = "MSI-H"
								pdl1_stat = con[-2]
								sample_type = con[-4]
							
					report_path = str(sample)+'-'+patient_name+'-'+detect_project
					if os.path.exists(report_path) == False:	#新建目录，存在样本报告
						os.mkdir(report_path)
					else:
						pass
					os.system('cp -r %s %s %s' % (pic_dir,css_path,report_path))
					f = open(report_path+'/'+report_path+'.tmp.html','w')
					read_1 = open(config_path+'config_html_header.html')
					content_1 = read_1.readlines()
					f.write("".join(str(i) for i in content_1))	#加入html的title
					f.write('\n')
					read_2 = open(config_path+'1_first_page.html')	
					content_2 = read_2.readlines()
					f.write("".join(str(i) for i in content_2))		#加入首页
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页	
					if patient_sex == "男":				#根据性别，删减内容,无性别则留空
						with open(config_path+'2_second_page.html','r') as SEC:
							for con in SEC:
								if "女士" in con:
									continue
								if "先生" in con:
									con = con.replace("replace",patient_name)
								f.write(con)
					elif patient_sex == "女":
						with open(config_path+'2_second_page.html','r') as SEC:
							for con in SEC:
								if "先生" in con:
									continue
								if "女士" in con:
									con = con.replace("replace",patient_name)
								f.write(con)
					else:
						with open(config_path+'2_second_page.html','r') as SEC:
							for con in SEC:
								if "先生" in con:
									continue
								if "女士" in con:
									con = con.replace("replace",patient_name)
									con = re.sub("女士","",con)
								f.write(con)
					f.write("\n\t\t"+change_page+'\n')				#换页

					#########<---------------  添加样本信息，客户信息页  ----------------->#########
					##添加页面
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')
					#添加送检信息
					with open(config_path+'3_guest_info.html','r') as SONGJIAN:
						for con in SONGJIAN:
							if "Inspection_hospital" in con:
								con = con.replace("replace",hospital_name)
							if "patient_name" in con:
								con = con.replace("replace",patient_name)
							if "sex" in con:
								con = con.replace("replace",patient_sex)
							if "age" in con:
								con = con.replace("replace",str(patient_age))
							if "pathological_number" in con:
								con = con.replace("replace",patient_dignose_num)
							if "pathologic_diagnosis" in con:
								con = con.replace("replace",patient_dignose)
							f.write(con)
					f.write('\n')
					##读取样本信息
					with open(config_path+'4_sample_info.html','r') as SAMPLE_INFO:
						for con in SAMPLE_INFO:
							if "sample_number" in con:
								con = con.replace("replace",sample)
							if "sample_type1" in con:
								con = con.replace("replace",sample_type1)
							if "sample_quantity1" in con:
								con = con.replace("replace",sample_num1)
							if "sample_type2" in con:
								con = con.replace("replace",sample_type2)
							if "sample_quantity2" in con:
								con = con.replace("replace",sample_num2)
							if "sample_quality" in con:
								con = con.replace("replace",sample_quality)
							if "control_result" in con:
								con = con.replace("replace",dignose_result)
							if "accept_date" in con:
								con = con.replace("replace",accept_date)
							if "report_date" in con:
								con = con.replace("replace",report_date)
							f.write(con)
					f.write('\n')
					###阴性报告里只有MSS 和MSI的区别
					##如果MSS，就5_all_neg_MSS
					###血浆样本无MSI，组织有MSI
					with open(config_path+'5_all_neg_MSS.html','r') as DIR4:
						for con in DIR4:
							if "page3_MSI" in con:
								if sample_type == "tissue":
									con = con.replace("replace",MSI_stat)
								else:
									continue
							if "page3_pdl1" in con:
								if pdl1_stat == "阴性":
									con = con.replace("replace","阴性")
								elif pdl1_stat == "NA":
									continue
								else:
									pdl1_result = str(pdl1_stat)+'%'
									con = con.replace("replace",pdl1_result)
							f.write(con)
					##添加页脚
					read_footer = open(config_path+'config_footer.html')
					content_footer = read_footer.readlines()
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					###检测结果
					#再添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')

					##读取该页的主title
					read_title_detect = open(config_path+'6_detect_result.html')
					content_title_detect = read_title_detect.readlines()
					f.write("".join(str(i) for i in content_title_detect))
					f.write("\n")

					#读取TMB title
					read_TMB_title = open(config_path+'6-1_TMB_title.html')
					content_TMB_title = read_TMB_title.readlines()
					f.write("".join(str(i) for i in content_TMB_title))
					#读取TMB内容
					with open(config_path+'6_TMB_content.html') as tmb_content:
						for con in tmb_content:
							if "TMB_num" in con:
								con = con.replace("replace","0")
							if "TMB_result" in con:
								con = con.replace("replace","低")
							if "TMB_drug" in con:
								con = con.replace("replace","NA")
							f.write(con)
					f.write('\n')

					##如果是tissue，才会有MSI信息
					#读取MSI title
					if sample_type == "tissue":
						read_MSI_title = open(config_path+'6-2_MSI_title.html')
						content_MSI_title = read_MSI_title.readlines()
						f.write("".join(str(i) for i in content_MSI_title))
						f.write("\n")
						#读取MSI内容
						with open(config_path+'6_MSI_content.html') as msi_content:
							for con in msi_content:
								if "MSI_result" in con:
									con = con.replace("replace",MSI_stat)
								if MSI_stat == "MSS" or MSI_stat == "MSI-L":
									if "MSI_drug" in con:
										con = con.replace("replace","NA")
								else:
									if "MSI_drug" in con:
										con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
								f.write(con)
						f.write('\n')
						#添加备注信息
						mark = open(config_path+'6_mark_TMB_low.html')
						content_mark = mark.readlines()
						f.write("".join(str(i) for i in content_mark))
						f.write("\n")
						###如果MSI状态是MSS，则在此处加上签名页.
						if MSI_stat == "MSS" or MSI_stat == "MSI-L":
							read_sign_name = open(config_path+'9_sign_name.html')
							content_sign_name = read_sign_name.readlines()
							f.write("".join(str(i) for i in content_sign_name))
							f.write('\n')
					else:
						#添加备注信息
						mark = open(config_path+'6_mark_TMB_low.html')
						content_mark = mark.readlines()
						f.write("".join(str(i) for i in content_mark))
						f.write("\n")
						read_sign_name = open(config_path+'9_sign_name.html')
						content_sign_name = read_sign_name.readlines()
						f.write("".join(str(i) for i in content_sign_name))
						f.write('\n')
					##添加页脚
					read_footer = open(config_path+'config_footer.html')
					content_footer = read_footer.readlines()
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					##添加检测结果解析，只判断MSI
					if MSI_stat == "MSS" or MSI_stat == "MSI-L":
						pass
					elif MSI_stat == "MSI-H":
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									con = con.replace("replace",patient_name)
								if "title_sample_number" in con:
									con = con.replace("replace",sample)
								f.write(con)
						f.write('\n')
						##读取content

						read_MSI_explain = open(config_path+'7_MSI_explain.html')
						content_MSI_explain = read_MSI_explain.readlines()
						f.write("".join(str(i) for i in content_MSI_explain))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#########<---------------  添加临床试验信息  ----------------->#########
						#如果是MSI-H，是有临床试验信息的，同时将签名页加在临床试验信息后面
						#设置一个上限，规定每页输出几个条目的clinical信息
						limit = 3
						#先获取clinical表格条目数，可认为是行数
						clinical_num =  len(open(TMB_MSI_clinical,'r').readlines())
						pipei_list = ["Drug","Biological","Procedure","Other","Device","Behavioral"]
						if clinical_num == 0:
							pass
						if clinical_num > 0:
							clinical_list = []
							with open(TMB_MSI_clinical,'r') as CLIN:
								for rows in CLIN:
									if rows not in clinical_list:
										clinical_list.append(rows)
							#print(len(clinical_list))
							##先确定所需要添加页数
							pages = (clinical_num-1) // limit + 1
							#<-----------如果只有一页临床试验------------>#
							if pages == 1:
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')
								###添加clinical_info的title信息
								read_clinical_info = open(config_path+'8_clinical_tilte_info.html')
								content_clinical_info = read_clinical_info.readlines()
								f.write("".join(str(i) for i in content_clinical_info))
								f.write('\n')
								read_clinical_sheet_title = open(config_path+'8_clinical_sheet_title.html')
								content_clinical_sheet_title = read_clinical_sheet_title.readlines()
								f.write("".join(str(i) for i in content_clinical_sheet_title))
								f.write('\n')
								#clinical格式是，基因，癌种，敏感/不敏感，药物名称，阶段，证据类型，试验名称，ID
								n = 0
								for i in range(len(clinical_list)):
									if n < limit:		#保证每页最多三个条目
										clinical_info = clinical_list[0].split('\t')
										#print(clinical_info[5])
										with open(config_path+'8_clinical_sheet_content.html','r') as CI:
											for row in CI:
												if "clinical_info_gene" in row:
													row = row.replace("replace",clinical_info[0])
												if "clinical_info_name" in row:
													row = row.replace("replace",clinical_info[2])
												if "clinical_info_drug" in row:
													for key in pipei_list:
														if key in clinical_info[3]:
															clinical_info[3] = clinical_info[3].replace(key,"<br>"+key)
													row = row.replace("replace",clinical_info[3])
												if "clinical_info_step" in row:
													row = row.replace("replace",clinical_info[4])
												if "clinical_info_NCI" in row:
													row = row.replace("replace",clinical_info[1])
												f.write(row)
										del clinical_list[0]	#删除第一项
										n += 1
										f.write('\n')
								#读取签名页
								read_sign_name = open(config_path+'9_sign_name.html')
								content_sign_name = read_sign_name.readlines()
								f.write("".join(str(i) for i in content_sign_name))
								f.write('\n')
								#还要给每页添加页脚和换行
								f.write("".join(str(i) for i in content_footer))
								f.write("\n"+change_page+'\n\n\n')				#换页
							#<---------如果临床试验大于一页---------->#
							if pages > 1:
								for page in range(pages):
									#给每页添加页眉 以及表头
									if page == 0 :  #第一页，无签名
										#添加页眉
										with open(config_path+'config_header.html','r') as HEADER:
											for con in HEADER:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",sample)
												f.write(con)
										f.write('\n')
										###添加clinical_info的title信息
										read_clinical_info = open(config_path+'8_clinical_tilte_info.html')
										content_clinical_info = read_clinical_info.readlines()
										f.write("".join(str(i) for i in content_clinical_info))
										f.write('\n')
										read_clinical_sheet_title = open(config_path+'8_clinical_sheet_title.html')
										content_clinical_sheet_title = read_clinical_sheet_title.readlines()
										f.write("".join(str(i) for i in content_clinical_sheet_title))
										f.write('\n')
										#clinical格式是，基因，癌种，敏感/不敏感，药物名称，阶段，证据类型，试验名称，ID
										n = 0
										for i in range(len(clinical_list)):
											if n < limit:		#保证每页最多三个条目
												clinical_info = clinical_list[0].split('\t')
												with open(config_path+'8_clinical_sheet_content.html','r') as CI:
													for row in CI:
														if "clinical_info_gene" in row:
															row = row.replace("replace",clinical_info[0])
														if "clinical_info_name" in row:
															row = row.replace("replace",clinical_info[2])
														if "clinical_info_drug" in row:
															for key in pipei_list:
																if key in clinical_info[3]:
																	clinical_info[3] = clinical_info[3].replace(key,"<br>"+key)
															row = row.replace("replace",clinical_info[3])
														if "clinical_info_step" in row:
															row = row.replace("replace",clinical_info[4])
														if "clinical_info_NCI" in row:
															row = row.replace("replace",clinical_info[1])
														f.write(row)
												del clinical_list[0]	#删除第一项
												n += 1
												f.write('\n')
										#还要给每页添加页脚和换行
										f.write("".join(str(i) for i in content_footer))
										f.write("\n"+change_page+'\n\n\n')				#换页

									if 0 < page < pages -1:
										#添加页眉
										with open(config_path+'config_header.html','r') as HEADER:
											for con in HEADER:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",sample)
												f.write(con)
										f.write('\n')
										###添加clinical_info_sheet的title信息
										read_clinical_sheet_title = open(config_path+'8_clinical_sheet_title.html')
										content_clinical_sheet_title = read_clinical_sheet_title.readlines()
										f.write("".join(str(i) for i in content_clinical_sheet_title))
										n = 0
										for i in range(len(clinical_list)):
											if n < limit:		#保证每页最多三个条目
												clinical_info = clinical_list[0].split('\t')
												with open(config_path+'8_clinical_sheet_content.html','r') as CI:
													for row in CI:
														if "clinical_info_gene" in row:
															row = row.replace("replace",clinical_info[0])
														if "clinical_info_name" in row:
															row = row.replace("replace",clinical_info[2])
														if "clinical_info_drug" in row:
															for key in pipei_list:
																if key in clinical_info[3]:
																	clinical_info[3] = clinical_info[3].replace(key,"<br>"+key)
															row = row.replace("replace",clinical_info[3])
														if "clinical_info_step" in row:
															row = row.replace("replace",clinical_info[4])
														if "clinical_info_NCI" in row:
															row = row.replace("replace",clinical_info[1])
														f.write(row)
												del clinical_list[0]	#删除第一项
												n += 1
												f.write('\n')
										#还要给每页添加页脚和换行
										f.write("".join(str(i) for i in content_footer))
										f.write("\n"+change_page+'\n\n\n')				#换页
									if page == pages -1 and page != 0:	#确定是最后一页，因为要在最后一页添加签名档
										#添加页眉
										with open(config_path+'config_header.html','r') as HEADER:
											for con in HEADER:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",sample)
												f.write(con)
										f.write('\n')
										###添加clinical_info_sheet的title信息
										read_clinical_sheet_title = open(config_path+'8_clinical_sheet_title.html')
										content_clinical_sheet_title = read_clinical_sheet_title.readlines()
										f.write("".join(str(i) for i in content_clinical_sheet_title))
										f.write('\n')
										n = 0
										for i in range(len(clinical_list)):
											if n < limit:		#保证每页最多三个条目
												clinical_info = clinical_list[0].split('\t')
												with open(config_path+'8_clinical_sheet_content.html','r') as CI:
													for row in CI:
														if "clinical_info_gene" in row:
															row = row.replace("replace",clinical_info[0])
														if "clinical_info_name" in row:
															row = row.replace("replace",clinical_info[2])
														if "clinical_info_drug" in row:
															for key in pipei_list:
																if key in clinical_info[3]:
																	clinical_info[3] = clinical_info[3].replace(key,"<br>"+key)
															row = row.replace("replace",clinical_info[3])
														if "clinical_info_step" in row:
															row = row.replace("replace",clinical_info[4])
														if "clinical_info_NCI" in row:
															row = row.replace("replace",clinical_info[1])
														f.write(row)
												del clinical_list[0]	#删除第一项
												n += 1
												f.write('\n')
										#读取签名页
										read_sign_name = open(config_path+'9_sign_name.html')
										content_sign_name = read_sign_name.readlines()
										f.write("".join(str(i) for i in content_sign_name))
										f.write('\n')
										#还要给每页添加页脚和换行
										f.write("".join(str(i) for i in content_footer))
										f.write("\n"+change_page+'\n\n\n')				#换页


					#########<---------------  添加全程多节点质控  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')

					read_quality_control = open(config_path+'10_quality_control.html')
					content_quality_control = read_quality_control.readlines()
					f.write("".join(str(i) for i in content_quality_control))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					#########<---------------  添加样本质量评级  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')

					read_quality_control = open(config_path+'11_sample_quality_rate.html')
					content_quality_control = read_quality_control.readlines()
					f.write("".join(str(i) for i in content_quality_control))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					#########<---------------  添加数据质控  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')

					##添加质控内容，打开control_file，根据sample_id获取数据质控信息
					
					with open(control_file,'r') as CONTROL:
						for zhikong in CONTROL:
							zhikong = zhikong.strip().split('\t')
							if sample in zhikong[0]:
								zhikong_Q20 = '%.2f%%' % (float(zhikong[1]) * 100)
								zhikong_bidui = '%.2f%%' % (float(zhikong[2]) * 100)
								zhikong_fugai = '%.2f%%' % (float(zhikong[3]) * 100)
								zhikong_junyi = '%.2f%%' % (float(zhikong[4]) * 100)
								zhikong_shendu = str(zhikong[5])
					##根据sample_type是血液还是组织，来使用不同的模板
					if sample_type == "blood":
						with open(config_path+'blood_data_control_result.html','r') as CON_HTML:
							for con in CON_HTML:
								if "sample_id" in con:
									con = con.replace("replace",sample)
								if "Q20_data" in con:
									con = con.replace("replace",zhikong_Q20)
								if "bidui_data" in con:
									con = con.replace("replace",zhikong_bidui)
								if "fugai_data" in con:
									con = con.replace("replace",zhikong_fugai)
								if "junyi_data" in con:
									con = con.replace("replace",zhikong_junyi)
								if "shendu_data" in con:
									con = con.replace("replace",zhikong_shendu)
								f.write(con)
						f.write('\n')
					elif sample_type == "tissue":# 如果是组织的话，需要判断肿瘤含量
						with open(config_path+'tissue_data_control_result.html','r') as CON_HTML:
							for con in CON_HTML:
								if "sample_id" in con:
									con = con.replace("replace",sample)
								if "tumor_num" in con:
									if tumor_num == "NA" or int(tumor_num) < 5:
										con = con.replace('replace1',"/")
										con = con.replace('replace2',"/")
									elif int(tumor_num) >= 5:
										con = con.replace('replace1',"&#10003")
										con = con.replace('replace2',"&#10003")
								if "Q20_data" in con:
									con = con.replace("replace",zhikong_Q20)
								if "bidui_data" in con:
									con = con.replace("replace",zhikong_bidui)
								if "fugai_data" in con:
									con = con.replace("replace",zhikong_fugai)
								if "junyi_data" in con:
									con = con.replace("replace",zhikong_junyi)
								if "shendu_data" in con:
									con = con.replace("replace",zhikong_shendu)
								f.write(con)
						f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					#########<---------------  添加附录1'TMB检测意义'  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_fulu1_TMB = open(config_path+'fulu1_TMB_yiyi.html')
					content_fulu1_TMB = read_fulu1_TMB.readlines()
					f.write("".join(str(i) for i in content_fulu1_TMB))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					#########<---------------  添加附录2'基因检测列表'  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_fulu2_gene1 = open(config_path+'fulu2-1_detect_genelist_1.html')
					content_fulu2_gene1 = read_fulu2_gene1.readlines()
					f.write("".join(str(i) for i in content_fulu2_gene1))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_fulu2_gene2 = open(config_path+'fulu4-2_detect_genelist_2.html')
					content_fulu2_gene2 = read_fulu2_gene2.readlines()
					f.write("".join(str(i) for i in content_fulu2_gene2))
					f.write('\n')
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					#########<---------------  添加附录3'药物证据等级'  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')

					read_fulu3_grade = open(config_path+'fulu3_drug_grade.html')
					content_fulu3_grade = read_fulu3_grade.readlines()
					f.write("".join(str(i) for i in content_fulu3_grade))

					f.write('\n')
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页


					#########<---------------  添加附录4'慈善药物'  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_fulu4_help1 = open(config_path+'fulu4_help_drug_info_first.html')
					content_fulu4_help1 = read_fulu4_help1.readlines()
					f.write("".join(str(i) for i in content_fulu4_help1))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_fulu4_help1 = open(config_path+'help_drug_info_second.html')
					content_fulu4_help1 = read_fulu4_help1.readlines()
					f.write("".join(str(i) for i in content_fulu4_help1))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_fulu4_help1 = open(config_path+'help_drug_info_third.html')
					content_fulu4_help1 = read_fulu4_help1.readlines()
					f.write("".join(str(i) for i in content_fulu4_help1))
					f.write('\n')
					f.write("".join(str(i) for i in content_footer))
				

					#########<---------------  添加附录5'已上市靶向药物汇总'  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					##添加title
					read_fulu5_public = open(config_path+'fulu5_public_drug_title.html')
					content_fulu5_public = read_fulu5_public.readlines()
					f.write("".join(str(i) for i in content_fulu5_public))
					f.write('\n')

					read_fulu7_page1 = open(config_path+'fulu7_drug_page1.html')
					content_fulu7_page1 = read_fulu7_page1.readlines()
					f.write("".join(str(i) for i in content_fulu7_page1))
					f.write('\n')

					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_fulu7_page2 = open(config_path+'fulu7_drug_page2.html')
					content_fulu7_page2 = read_fulu7_page2.readlines()
					f.write("".join(str(i) for i in content_fulu7_page2))
					f.write('\n')

					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_fulu7_page3 = open(config_path+'fulu7_drug_page3.html')
					content_fulu7_page3 = read_fulu7_page3.readlines()
					f.write("".join(str(i) for i in content_fulu7_page3))
					f.write('\n')

					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_fulu7_page4 = open(config_path+'fulu7_drug_page4.html')
					content_fulu7_page4 = read_fulu7_page4.readlines()
					f.write("".join(str(i) for i in content_fulu7_page4))
					f.write('\n')
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					######最后添加文献######
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_wenxian = open(config_path+'wenxian.html')
					content_wenxian = read_wenxian.readlines()
					f.write("".join(str(i) for i in content_wenxian))
					f.write('\n')
					f.write("".join(str(i) for i in content_footer))
					read_end = open(config_path+'END.html')
					content_end = read_end.readlines()
					f.write("".join(str(i) for i in content_end))
					f.close()

					#获取需要替换的所有基因名称
					report_gene_list = []
					with open(report_gene_file,'r') as GE:
						for k in GE:
							k = k.strip()
							if k not in report_gene_list:
								report_gene_list.append(k)
							else:
								pass

					#先替换页码
					count = 0
					tmp2_report = open(report_path+'/'+report_path+'.tmp2.html','w')
					with open(report_path+'/'+report_path+'.tmp.html','r') as HTML:
						for each in HTML:
							if "footer_end" in each:
								count += 1
								each = each.replace("replace",str(count))
							tmp2_report.write(each)
					tmp2_report.close()
					#再将所有的基因改成斜体
					final_report = open(report_path+'/'+report_path+'.html','w')
					with open(report_path+'/'+report_path+'.tmp2.html','r') as TMP2:
						for each in TMP2:
							if "charset" in each:
								final_report.write(each)
								continue
							if 'ERBB2' in each:
								each = each.replace('ERBB2','HER2')
							if 'pass_gene' in each:
								final_report.write(each)
								continue
							for GENE in report_gene_list:
								new_gene = "<i>"+GENE+" </i>"
									#print(new_gene)
								each = each.replace(GENE,new_gene)

							final_report.write(each)
					final_report.close()
					os.system("rm %s/%s.tmp.html %s/%s.tmp2.html " % (report_path,report_path,report_path,report_path))
					os.system("/amoydx/USER/wujianming/software/miniconda-python3/bin/wkhtmltopdf --encoding utf8 --quiet %s/%s.html %s/%s.pdf" %(report_path,report_path,report_path,report_path))




def get_pos_report(sample_info_file,control_file,negative_file):
	now_time = datetime.datetime.now()
	report_date = now_time.strftime('%Y-%m-%d')	#默认报告日期为当天日期

	Lung_dict = {"肺癌":"Lung","小细胞癌":"Lung","小细胞肺癌":"Lung","肺小细胞癌":"Lung","非小细胞肺癌":"Lung","腺鳞癌":"Lung","腺癌":"Lung","肺腺癌":"Lung","肺鳞癌":"Lung"}

	pos_sampleList = get_pos_id(negative_file,control_file)
	for sample in pos_sampleList:
		sample = str(sample)		#有时候sample是一串数字，需要把他们转成字符串
		with open(sample_info_file,'r') as SAMPLE:
			for line in SAMPLE:
				line = line.strip().split('\t')
				if line[0] == sample: 
					hospital_name = line[1]
					patient_name = line[2]
					patient_name = patient_name.strip()
					if line[3] != "男" and line[3] != "女":		#性别除了这两个选项外，为空
						patient_sex = ""
					else:
						patient_sex = line[3]
					if str(line[4]) == "0.0":		#如果年龄为0，为空
						patient_age = ""
					else:
						patient_age = int(float(line[4]))
					if str(line[5]) == "0":
						patient_dignose = ""
					else:
						patient_dignose = line[5]	#病理诊断
					patient_dignose_num = line[6]	#病理编号
					accept_date = line[7]	#接收日期
					sample_type1 = line[8] 	#样本类型1
					sample_type2 = line[9]		#样本类型2
					if sample_type2 == "NA":
						sample_type2 = ""
					sample_num1 = line[10]
					sample_num2 = line[11]		#样本数量2
					if sample_num2 == "NA":
						sample_num2 = ""				
					sample_quality = line[12]
					dignose_result = line[13]
					if dignose_result == "NA":
						dignose_result = ""
					detect_project = line[-1]
					tumor_num = line[-2]
					with open(control_file,'r') as CON:
						for con in CON:
							con = con.strip().split('\t')
							if con[0] == sample:
								TMB_num = con[-1]
								if con[-3] == "MSS":
									MSI_stat = "MSS"
								elif  con[-3] == "MSI":
									MSI_stat = "MSI-H"
								pdl1_stat = con[-2]
								sample_type = con[-4]
							
					report_path = str(sample)+'-'+patient_name+'-'+detect_project
					if os.path.exists(report_path) == False:	#新建目录，存在样本报告
						os.mkdir(report_path)
					else:
						pass
					os.system('cp -r %s %s %s' % (pic_dir,css_path,report_path))
					f = open(report_path+'/'+report_path+'.tmp.html','w')
					read_1 = open(config_path+'config_html_header.html')
					content_1 = read_1.readlines()
					f.write("".join(str(i) for i in content_1))	#加入html的title
					f.write('\n')
					read_2 = open(config_path+'1_first_page.html')	
					content_2 = read_2.readlines()
					f.write("".join(str(i) for i in content_2))		#加入首页
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页	
					if patient_sex == "男":				#根据性别，删减内容,无性别则留空
						with open(config_path+'2_second_page.html','r') as SEC:
							for con in SEC:
								if "女士" in con:
									continue
								if "先生" in con:
									con = con.replace("replace",patient_name)
								f.write(con)
					elif patient_sex == "女":
						with open(config_path+'2_second_page.html','r') as SEC:
							for con in SEC:
								if "先生" in con:
									continue
								if "女士" in con:
									con = con.replace("replace",patient_name)
								f.write(con)
					else:
						with open(config_path+'2_second_page.html','r') as SEC:
							for con in SEC:
								if "先生" in con:
									continue
								if "女士" in con:
									con = con.replace("replace",patient_name)
									con = re.sub("女士","",con)
								f.write(con)
					f.write("\n\t\t"+change_page+'\n')				#换页

					#########<---------------  添加样本信息，客户信息页  ----------------->#########
					##添加页面
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')
					#添加送检信息
					with open(config_path+'3_guest_info.html','r') as SONGJIAN:
						for con in SONGJIAN:
							if "Inspection_hospital" in con:
								con = con.replace("replace",hospital_name)
							if "patient_name" in con:
								con = con.replace("replace",patient_name)
							if "sex" in con:
								con = con.replace("replace",patient_sex)
							if "age" in con:
								con = con.replace("replace",str(patient_age))
							if "pathological_number" in con:
								con = con.replace("replace",patient_dignose_num)
							if "pathologic_diagnosis" in con:
								con = con.replace("replace",patient_dignose)
							f.write(con)
					f.write('\n')
					##读取样本信息
					with open(config_path+'4_sample_info.html','r') as SAMPLE_INFO:
						for con in SAMPLE_INFO:
							if "sample_number" in con:
								con = con.replace("replace",sample)
							if "sample_type1" in con:
								con = con.replace("replace",sample_type1)
							if "sample_quantity1" in con:
								con = con.replace("replace",sample_num1)
							if "sample_type2" in con:
								con = con.replace("replace",sample_type2)
							if "sample_quantity2" in con:
								con = con.replace("replace",sample_num2)
							if "sample_quality" in con:
								con = con.replace("replace",sample_quality)
							if "control_result" in con:
								con = con.replace("replace",dignose_result)
							if "accept_date" in con:
								con = con.replace("replace",accept_date)
							if "report_date" in con:
								con = con.replace("replace",report_date)
							f.write(con)
					f.write('\n')
					##读取目录，分5种可能性
					###1、TMB high + 突变有意义 ---- 目录全部输出
					###2、TMB low + 突变有意义 ---- 目录4没有第一点（无意义基因列表）
					###3、TMB high + 突变全部无意义 ---- 目录2没有'临床试验信息',目录4没有第二点
					###4、TMB low + MSI + 全部无意义 ---- 目录2没有'临床试验信息',目录4没有第一点和第二点
					###5、TMB low + MSS + 全部无意义 ----	 目录2没有'检测结果解释及用药信息'和'临床试验信息',目录4没有第一点和第二点
					##综上送需要列举出来的信息包括：
					###TMB high/low：这里0为low，1为high
					###MSS信息：先以MSI举例
					###突变是否都有意义，这里将列出三个列表，全部突变列表，有意义突变列表，无意义突变列表。0为有意义，1为全部无意义
					TMB_value = 0
					if float(TMB_num) > 10.0:
						TMB_value = 1			#确定TMB是high还是low

					nouseful_list = []		#储存无意义突变,有基因 碱基突变 蛋白突变
					useful_list = []		#储存有意义突变，有基因 碱基突变 蛋白突变
					gp_list = []   #储存无意义突变，只有碱基和蛋白突变

					all_mutation_list = []	#储存所有突变	
					detect_freq = {}		#储存gene+cds+protein : freq
					
					##确定all_mutation_list
					if os.path.getsize(sample+'_somatic.xls') != 0:
						with open(sample+'_somatic.xls','r') as KNOW:
							for know_info in KNOW:
								know_info  = know_info.strip().split('\t')
								cds_mut = know_info[14].split(":")[-2]
								if 'del' in cds_mut and 'delins' not in cds_mut:
									cds_mut = cds_mut.split('del')[0]+'del'+str(len(cds_mut.split('del')[-1]))
								pro_mut = know_info[14].split(":")[-1].split('.')[-1]
								in_value= know_info[12]+' '+cds_mut+' '+know_info[14].split(":")[-1]
								if pro_mut == "?":			#如果蛋白是？，就输出CDS变化
									value = know_info[12]+' &nbsp;'+cds_mut
								else:
									value = know_info[12]+' &nbsp;'+pro_mut
								#detect_freq[pro_mut] = str(know_info[10])
								detect_freq[value] = '%.2f%%' % (float(know_info[10]) * 100)
								if in_value not in all_mutation_list:
									all_mutation_list.append(in_value)		#gene+cds+protein
								else:
									pass
					else:
						pass

					if os.path.getsize(sample+'_fusion.xls') != 0:
						with open(sample+'_fusion.xls','r') as FUS:
							for fus in FUS:
								fus = fus.strip().split('\t')
								gene1 = fus[5].split(':')[0]
								gene2 = fus[9].split(':')[0]
								gene_gene = gene1+'-'+gene2+' 融合'
								detect_freq[gene_gene] = '%.2f%%' % (float(fus[14]) * 100)
								if gene_gene not in all_mutation_list:
									all_mutation_list.append(gene_gene)
								else:
									pass

					#确定useful_list
					#同时确定FDA_result以及FDA_explain_list
					FDA_result = []			#gene+cds+protein
					FDA_gene_list = []		
					FDA_explain_list = []
					#print(detect_freq)
					if os.path.getsize(sample+'_know_annot.xls') != 0:
						with open(sample+'_know_annot.xls','r') as K:
							for ke in K:
								ke = ke.strip().split('\t')
								cds_mut = ke[9]
								if 'del' in cds_mut and 'delins' not in cds_mut:
									cds_mut = cds_mut.split('del')[0]+'del'+str(len(cds_mut.split('del')[-1]))
								pro_mut = ke[10].split('.')[-1]
								FDA_in_value = ke[6]+' '+cds_mut+' '+ke[10]
								if pro_mut == "?":
									value = ke[6] +' &nbsp;'+cds_mut
								else:
									value = ke[6]+' &nbsp;'+pro_mut
								if value not in gp_list:
									gp_list.append(value)
								if FDA_in_value not in useful_list:
									useful_list.append(FDA_in_value)
								if ke[6] not in FDA_gene_list:
									FDA_gene_list.append(ke[6])
								FDA_freq = detect_freq[value]	#蛋白突变作为key
								FDA_sensitive = ke[12]
								clinical_sensitive = ke[13]
								insensitive = ke[14]
								FDA_connect = FDA_in_value+'\t'+FDA_freq+'\t'+FDA_sensitive+'\t'+clinical_sensitive+'\t'+insensitive
								if FDA_connect not in FDA_result:
									FDA_result.append(FDA_connect)
								#分别是基因名称、蛋白突变、基因介绍、	变异介绍、治疗策略、药物信息
								if pro_mut.split('.')[-1] == "?":
									FDA_explain_content = ke[6]+'\tc.'+cds_mut+'\t'+ke[15]+'\t'+ke[16]+'\t'+ke[17]+'\t'+ke[12]
								else:
									FDA_explain_content = ke[6]+'\t'+pro_mut+'\t'+ke[15]+'\t'+ke[16]+'\t'+ke[17]+'\t'+ke[12]
								if FDA_explain_content not in FDA_explain_list:
									FDA_explain_list.append(FDA_explain_content)


					if os.path.getsize(sample+'_fusion_annot.xls') != 0:
						with open(sample+'_fusion_annot.xls','r') as N:
							for fus in N:
								fus = fus.strip().split('\t')
								if fus[0] not in useful_list:
									useful_list.append(fus[0]+' 融合')
								gene1 = fus[0].split('-')[0]								
								gene2 = fus[0].split('-')[1]
								gene_gene1 = gene1+'-'+gene2+' 融合'
								if gene2 not in FDA_gene_list:
									FDA_gene_list.append(gene2)
								if gene_gene1 in detect_freq:
									FREQ = detect_freq[gene_gene1]
								if gene_gene1 not in gp_list:
									gp_list.append(gene_gene1)
								FDA_sensitive = fus[3]
								clinical_sensitive = fus[5]
								insensitive = fus[6]
								fusion_content = fus[0]+' 融合\t'+FREQ+'\t'+FDA_sensitive+'\t'+clinical_sensitive+'\t'+insensitive
								if fusion_content not in FDA_result:
									FDA_result.append(fusion_content)
								#分别是融合名称、基因介绍、	变异介绍、治疗策略、药物信息、再加GENE:exon-GENE:exon
								FDA_explain_content2 = fus[0]+' 融合\t'+fus[7]+'\t'+fus[8]+'\t'+fus[9]+'\t'+fus[3]+'\t'+fus[-1]
								if FDA_explain_content2 not in FDA_explain_list:
									FDA_explain_list.append(FDA_explain_content2)


					#输出突变检测结果，只输出有意义的位点
					detect_result_str = "<br><br>".join(str(i) for i in gp_list)
					detect_result_content = "、".join(str(i) for i in gp_list)
					##如果useful_list没有内容，则是全部无意义,所有的突变都是all_mutation_list
					###TMB high + 突变有意义 + clinical文件不为空---- 目录全部输出
					if TMB_value == 1 and len(useful_list) != 0 and os.path.getsize(sample+'_clinical.xls') != 0:
						with open(config_path+'5_pos_TMB_high.html','r') as DIR1:
							for con in DIR1:
								if "page3_detect_result" in con:
									con = con.replace("replace",detect_result_str)
								if "page3_tmb_num" in con:
									con = con.replace("replace",TMB_num)
								if "page3_MSI" in con:
									if sample_type == "tissue":
										con = con.replace("replace",MSI_stat)
									else:
										continue
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat == "NA":
										continue
									else:
										pdl1_result = str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
								if "detect_num" in con:
									con = con.replace("replace1",str(len(all_mutation_list)))
									con = con.replace("replace2",detect_result_content)
								f.write(con)
					###TMB low + 突变有意义 + clinical文件不为空---- 目录4没有第一点（无意义基因列表）
					elif TMB_value == 0 and len(useful_list) != 0 and os.path.getsize(sample+'_clinical.xls') != 0 and len(useful_list) != len(all_mutation_list):
						with open(config_path+'5_pos_TMB_low.html','r') as DIR2:
							for con in DIR2:
								if "page3_detect_result" in con:
									con = con.replace("replace",detect_result_str)
								if "page3_tmb_num" in con:
									con = con.replace("replace",TMB_num)
								if "page3_MSI" in con:
									if sample_type == "tissue":
										con = con.replace("replace",MSI_stat)
									else:
										continue
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat == "NA":
										continue
									else:
										pdl1_result = str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
								if "detect_num" in con:
									con = con.replace("replace1",str(len(all_mutation_list)))
									con = con.replace("replace2",detect_result_content)
								f.write(con)
					elif TMB_value == 0 and len(useful_list) != 0 and os.path.getsize(sample+'_clinical.xls') != 0 and len(useful_list) == len(all_mutation_list):
						with open(config_path+'5_pos_TMB_alluse.html','r') as DIR2:
							for con in DIR2:
								if "page3_detect_result" in con:
									con = con.replace("replace",detect_result_str)
								if "page3_tmb_num" in con:
									con = con.replace("replace",TMB_num)
								if "page3_MSI" in con:
									if sample_type == "tissue":
										con = con.replace("replace",MSI_stat)
									else:
										continue
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat == "NA":
										continue
									else:
										pdl1_result = str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
								if "detect_num" in con:
									con = con.replace("replace1",str(len(all_mutation_list)))
								f.write(con)
					###TMB high + 突变有意义 + clinical文件为空---- 没有临床试验信息
					elif TMB_value == 1 and len(useful_list) != 0 and os.path.getsize(sample+'_clinical.xls') == 0:
						with open(config_path+'5_high_TMB_clinical_neg.html','r') as DIR1:
							for con in DIR1:
								if "page3_detect_result" in con:
									con = con.replace("replace",detect_result_str)
								if "page3_tmb_num" in con:
									con = con.replace("replace",TMB_num)
								if "page3_MSI" in con:
									if sample_type == "tissue":
										con = con.replace("replace",MSI_stat)
									else:
										continue
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat == "NA":
										continue
									else:
										pdl1_result = str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
								if "detect_num" in con:
									con = con.replace("replace1",str(len(all_mutation_list)))
									con = con.replace("replace2",detect_result_content)
								f.write(con)
					###TMB low + 突变有意义 + clinical文件为空---- 目录2没有'临床试验信息',目录4没有第一点（无意义基因列表）
					elif TMB_value == 0 and len(useful_list) != 0 and os.path.getsize(sample+'_clinical.xls') == 0:
						with open(config_path+'5_low_TMB_clinical_neg.html','r') as DIR2:
							for con in DIR2:
								if "page3_detect_result" in con:
									con = con.replace("replace",detect_result_str)
								if "page3_tmb_num" in con:
									con = con.replace("replace",TMB_num)
								if "page3_MSI" in con:
									if sample_type == "tissue":
										con = con.replace("replace",MSI_stat)
									else:
										continue
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat == "NA":
										continue
									else:
										pdl1_result = str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
								if "detect_num" in con:
									con = con.replace("replace1",str(len(all_mutation_list)))
									con = con.replace("replace2",detect_result_content)
								f.write(con)

					###TMB high + 突变全部无意义 ---- 目录2没有'临床试验信息',目录4没有第二点
					elif TMB_value == 1 and len(useful_list) == 0:
						with open(config_path+'5_neg_TMB_high.html','r') as DIR3:
							for con in DIR3:
								if "page3_detect_result" in con:
									con = con.replace("replace","")
								if "page3_tmb_num" in con:
									con = con.replace("replace",TMB_num)
								if "page3_MSI" in con:
									if sample_type == "tissue":
										con = con.replace("replace",MSI_stat)
									else:
										continue
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat == "NA":
										continue
									else:
										pdl1_result = str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
								if "detect_num" in con:
									con = con.replace("replace1",str(len(all_mutation_list)))
								f.write(con)
					###TMB low + MSI + 全部无意义 ---- 目录2没有'临床试验信息',目录4没有第一点和第二点
					elif TMB_value == 0 and len(useful_list) == 0 and MSI_stat == "MSI-H":
						with open(config_path+'5_neg_TMB_low_MSI.html','r') as DIR4:
							for con in DIR4:
								if "page3_detect_result" in con:
									con = con.replace("replace","")
								if "page3_tmb_num" in con:
									con = con.replace("replace",TMB_num)
								if "page3_MSI" in con:
									if sample_type == "tissue":
										con = con.replace("replace",MSI_stat)
									else:
										continue
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat == "NA":
										continue
									else:
										pdl1_result = str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
								if "detect_num" in con:
									if len(all_mutation_list) == 1:
										con = con.replace("replace1",str(len(all_mutation_list)))
										con = con.replace("这些突变均","该突变")
									else:
										con = con.replace("replace1",str(len(all_mutation_list)))
								f.write(con)

					###5、TMB low + MSS + 全部无意义 ----	 目录2没有'检测结果解释及用药信息'和'临床试验信息',目录4没有第一点和第二点
					elif TMB_value == 0 and len(useful_list) == 0 and (MSI_stat == "MSS" or MSI_stat == "MSI-L"):
						with open(config_path+'5_neg_TMB_low_MSS.html','r') as DIR5:
							for con in DIR5:
								if "page3_detect_result" in con:
									con = con.replace("replace","")
								if "page3_tmb_num" in con:
									con = con.replace("replace",TMB_num)
								if "page3_MSI" in con:
									if sample_type == "tissue":
										con = con.replace("replace",MSI_stat)
									else:
										continue
								if "page3_pdl1" in con:
									if pdl1_stat == "阴性":
										con = con.replace("replace","阴性")
									elif pdl1_stat == "NA":
										continue
									else:
										pdl1_result = str(pdl1_stat)+'%'
										con = con.replace("replace",pdl1_result)
								if "detect_num" in con:
									if len(all_mutation_list) == 1:
										con = con.replace("replace1",str(len(all_mutation_list)))
										con = con.replace("这些突变均","该突变")
									else:
										con = con.replace("replace1",str(len(all_mutation_list)))
								f.write(con)

					f.write('\n')
					##添加页脚
					read_footer = open(config_path+'config_footer.html')
					content_footer = read_footer.readlines()
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页


					#########<---------------  添加检测结果页  ----------------->#########
					#思路：点突变--TMB high，只显示突变有意义的位点；low，显示所有突变位点，无意义的NA
					##添加页面
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')

					##读取该页的主title
					read_title_detect = open(config_path+'6_detect_result.html')
					content_title_detect = read_title_detect.readlines()
					f.write("".join(str(i) for i in content_title_detect))
					f.write("\n")

					##确定无意义的位点
					#print(useful_list)
					#print(all_mutation_list)
					for i in all_mutation_list:
						#先判断是否由融合在里面
						if "融合" in i:
							if i not in useful_list:
								nouseful_list.append(i+'\t'+detect_freq[i])
						elif i.split(' ')[-1].split('.')[-1] == "？" or i.split(' ')[-1].split('.')[-1] == "?":
							if i.split(' ')[-2] not in " ".join(k for k in useful_list)  or i not in useful_list:
								key = i.split(' ')[0]+' &nbsp;'+i.split(' ')[1]
								nouseful_list.append(i+'\t'+detect_freq[key])
						else:
							alllist = " ".join(k for k in useful_list)
							if alllist.find(i.split(' ')[-1]) < 0 and i not in useful_list:
							#if i.split(' ')[-1] not in " ".join(k for k in useful_list)  or i not in useful_list:
								#print(i.split(' ')[-1]+'+++++'+" ".join(k for k in useful_list))
								key = i.split(' ')[0]+' &nbsp;'+i.split(' ')[-1].split('.')[-1]
								nouseful_list.append(i+'\t'+detect_freq[key])
					# for key in detect_freq.keys():
					# 	co = ','.join(i.split(' ')[0] for i in useful_list)
					# 	if key not in useful_list and key not in co:
					# 		nouseful_list.append(key+'\t'+detect_freq[key])
					#print(nouseful_list)
					#读取点突变，出现点突变检测结果有三种情况
					##TMB high，有意义位点，只取有意义位点
					##TMB low，有意义位点，有意义和无意义都取
					##TMB low，全部无意义位点，全部放无意义位点，NA
					if TMB_value == 1 and len(useful_list) != 0:
						##读取表头
						snp_title = open(config_path+'6_snp_title.html')
						content_snp = snp_title.readlines()
						f.write("".join(str(i) for i in content_snp))
						f.write('\n')
						read_FDA_start = open(config_path+'6_snp_detect_result_start.html')
						content_FDA_start = read_FDA_start.readlines()
						f.write("".join(str(i) for i in content_FDA_start))
						f.write("\n")
						##如果是blood，cut= 4 , tissue,cut =2
						###如果有意义的点 < 2，一页； >=2 ，两页显示
						if sample_type == "blood":
							cut = 4
							if len(useful_list) <= cut:
								for i in range(len(FDA_result)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = FDA_result[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",FDA_result[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												FDA_sen = FDA_result[i].split('\t')[2]
												if '；' in FDA_sen or ';' in FDA_sen:
													FDA_sen = FDA_sen.replace("；","<br>")
													FDA_sen = FDA_sen.replace(";","<br>")
												con = con.replace("replace",FDA_sen)
											if 'clinical_sensitive' in con:
												clinical_sen = FDA_result[i].split('\t')[3]
												if '；' in clinical_sen or ';' in clinical_sen:
													clinical_sen = clinical_sen.replace("；","<br>")
													clinical_sen = clinical_sen.replace(";","<br>")
												con = con.replace("replace",clinical_sen)
											if 'resistance' in con:
												resis = FDA_result[i].split('\t')[4]
												if '；' in resis or ';' in resis:
													resis = resis.replace("；","<br>")
													resis = resis.replace(";","<br>")
												con = con.replace("replace",resis)
											f.write(con)
								f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##后直接跟TMB信息，因为没有MSI信息
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","高")
										if "TMB_drug" in con:
											if patient_dignose in Lung_dict or "肺" in patient_dignose:	#判断该癌种是否为肺癌，肺癌为2A，其他为2B
												con = con.replace("replace","Pembrolizumab（2A）<br>Ipilimumab（2A）<br>Atezolizumab（2A）<br>Nivolumab（2A）<br>Nivolumab+Ipilimumab（2A）")
											else:
												con = con.replace("replace","Pembrolizumab（2B）<br>Ipilimumab（2B）<br>Atezolizumab（2B）<br>Nivolumab（2B）<br>Nivolumab+Ipilimumab（2B）")
										f.write(con)
								f.write('\n')

							elif len(useful_list) > cut:
								#两页，换页，再加页眉
								for i in range(len(FDA_result)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = FDA_result[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",FDA_result[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												FDA_sen = FDA_result[i].split('\t')[2]
												if '；' in FDA_sen or ';' in FDA_sen:
													FDA_sen = FDA_sen.replace("；","<br>")
													FDA_sen = FDA_sen.replace(";","<br>")
												con = con.replace("replace",FDA_sen)
											if 'clinical_sensitive' in con:
												clinical_sen = FDA_result[i].split('\t')[3]
												if '；' in clinical_sen or ';' in clinical_sen:
													clinical_sen = clinical_sen.replace("；","<br>")
													clinical_sen = clinical_sen.replace(";","<br>")
												con = con.replace("replace",clinical_sen)
											if 'resistance' in con:
												resis = FDA_result[i].split('\t')[4]
												if '；' in resis or ';' in resis:
													resis = resis.replace("；","<br>")
													resis = resis.replace(";","<br>")
												con = con.replace("replace",resis)
											f.write(con)
								f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								#再添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')
								#读取TMB title

								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","高")
										if "TMB_drug" in con:
											if patient_dignose in Lung_dict or "肺" in patient_dignose:	#判断该癌种是否为肺癌，肺癌为2A，其他为2B
												con = con.replace("replace","Pembrolizumab（2A）<br>Ipilimumab（2A）<br>Atezolizumab（2A）<br>Nivolumab（2A）<br>Nivolumab+Ipilimumab（2A）")
											else:
												con = con.replace("replace","Pembrolizumab（2B）<br>Ipilimumab（2B）<br>Atezolizumab（2B）<br>Nivolumab（2B）<br>Nivolumab+Ipilimumab（2B）")
										f.write(con)
								f.write('\n')
								#添加备注信息
							mark = open(config_path+'6_mark_TMB_high.html')
							content_mark = mark.readlines()
							f.write("".join(str(i) for i in content_mark))
							f.write("\n")
							# ##添加页脚
							# read_footer = open(config_path+'config_footer.html')
							# content_footer = read_footer.readlines()
							# f.write("".join(str(i) for i in content_footer))
							# f.write("\n\t\t"+change_page+'\n\n\n')				#换页



						elif sample_type == "tissue":
							cut = 2
							if len(useful_list) <= cut:
								for i in range(len(FDA_result)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = FDA_result[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",FDA_result[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												FDA_sen = FDA_result[i].split('\t')[2]
												if '；' in FDA_sen or ';' in FDA_sen:
													FDA_sen = FDA_sen.replace("；","<br>")
													FDA_sen = FDA_sen.replace(";","<br>")
												con = con.replace("replace",FDA_sen)
											if 'clinical_sensitive' in con:
												clinical_sen = FDA_result[i].split('\t')[3]
												if '；' in clinical_sen or ';' in clinical_sen:
													clinical_sen = clinical_sen.replace("；","<br>")
													clinical_sen = clinical_sen.replace(";","<br>")
												con = con.replace("replace",clinical_sen)
											if 'resistance' in con:
												resis = FDA_result[i].split('\t')[4]
												if '；' in resis or ';' in resis:
													resis = resis.replace("；","<br>")
													resis = resis.replace(";","<br>")
												con = con.replace("replace",resis)
											f.write(con)
								f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##后直接跟TMB信息，和MSI信息
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","高")
										if "TMB_drug" in con:
											if patient_dignose in Lung_dict or "肺" in patient_dignose:	#判断该癌种是否为肺癌，肺癌为2A，其他为2B
												con = con.replace("replace","Pembrolizumab（2A）<br>Ipilimumab（2A）<br>Atezolizumab（2A）<br>Nivolumab（2A）<br>Nivolumab+Ipilimumab（2A）")
											else:
												con = con.replace("replace","Pembrolizumab（2B）<br>Ipilimumab（2B）<br>Atezolizumab（2B）<br>Nivolumab（2B）<br>Nivolumab+Ipilimumab（2B）")
										f.write(con)
								f.write('\n')
								#读取MSI信息
								read_MSI_title = open(config_path+'6_MSI_title.html')
								content_MSI_title = read_MSI_title.readlines()
								f.write("".join(str(i) for i in content_MSI_title))
								f.write("\n")
								#读取MSI内容
								with open(config_path+'6_MSI_content.html') as msi_content:
									for con in msi_content:
										if "MSI_result" in con:
											con = con.replace("replace",MSI_stat)
										if MSI_stat == "MSS" or MSI_stat == "MSI-L":
											if "MSI_drug" in con:
												con = con.replace("replace","NA")
										else:
											if "MSI_drug" in con:
												con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
										f.write(con)
								f.write('\n')

							elif len(useful_list) > cut:
								#两页，换页，再加页眉
								for i in range(len(FDA_result)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = FDA_result[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",FDA_result[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												FDA_sen = FDA_result[i].split('\t')[2]
												if '；' in FDA_sen or ';' in FDA_sen:
													FDA_sen = FDA_sen.replace("；","<br>")
													FDA_sen = FDA_sen.replace(";","<br>")
												con = con.replace("replace",FDA_sen)
											if 'clinical_sensitive' in con:
												clinical_sen = FDA_result[i].split('\t')[3]
												if '；' in clinical_sen or ';' in clinical_sen:
													clinical_sen = clinical_sen.replace("；","<br>")
													clinical_sen = clinical_sen.replace(";","<br>")
												con = con.replace("replace",clinical_sen)
											if 'resistance' in con:
												resis = FDA_result[i].split('\t')[4]
												if '；' in resis or ';' in resis:
													resis = resis.replace("；","<br>")
													resis = resis.replace(";","<br>")
												con = con.replace("replace",resis)
											f.write(con)
								f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								#再添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')
								#读取TMB title

								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","高")
										if "TMB_drug" in con:
											if patient_dignose in Lung_dict or "肺" in patient_dignose:	#判断该癌种是否为肺癌，肺癌为2A，其他为2B
												con = con.replace("replace","Pembrolizumab（2A）<br>Ipilimumab（2A）<br>Atezolizumab（2A）<br>Nivolumab（2A）<br>Nivolumab+Ipilimumab（2A）")
											else:
												con = con.replace("replace","Pembrolizumab（2B）<br>Ipilimumab（2B）<br>Atezolizumab（2B）<br>Nivolumab（2B）<br>Nivolumab+Ipilimumab（2B）")
										f.write(con)
								f.write('\n')
								#读取MSI title
								read_MSI_title = open(config_path+'6_MSI_title.html')
								content_MSI_title = read_MSI_title.readlines()
								f.write("".join(str(i) for i in content_MSI_title))
								f.write("\n")
								#读取MSI内容
								with open(config_path+'6_MSI_content.html') as msi_content:
									for con in msi_content:
										if "MSI_result" in con:
											con = con.replace("replace",MSI_stat)
										if MSI_stat == "MSS" or MSI_stat == "MSI-L":
											if "MSI_drug" in con:
												con = con.replace("replace","NA")
										else:
											if "MSI_drug" in con:
												con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
										f.write(con)
								f.write('\n')

							#添加备注信息
							mark = open(config_path+'6_mark_TMB_high.html')
							content_mark = mark.readlines()
							f.write("".join(str(i) for i in content_mark))
							f.write("\n")
							# ##添加页脚
							# read_footer = open(config_path+'config_footer.html')
							# content_footer = read_footer.readlines()
							# f.write("".join(str(i) for i in content_footer))
							# f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					##TMB low，有意义位点，有意义和无意义都取	,但是点突变单独一页。但是如果无意义的点接近13个，得判断情况了
					if TMB_value == 0 and len(useful_list) != 0:
						##读取表头
						snp_title = open(config_path+'6_snp_title.html')
						content_snp = snp_title.readlines()
						f.write("".join(str(i) for i in content_snp))
						f.write('\n')
						read_FDA_start = open(config_path+'6_snp_detect_result_start.html')
						content_FDA_start = read_FDA_start.readlines()
						f.write("".join(str(i) for i in content_FDA_start))
						f.write("\n")
						#循环有意义的点
						for i in range(len(FDA_result)):
							with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
								for con in FDA_content:
									if 'SNP_result' in con:
										snp_result = FDA_result[i].split('\t')[0].replace(" ","<br>")
										con = con.replace("replace",snp_result)
									if 'SNP_freq' in con:
										con = con.replace("replace",FDA_result[i].split('\t')[1])
									if 'SNP_sensitive' in con:
										FDA_sen = FDA_result[i].split('\t')[2]
										if '；' in FDA_sen or ';' in FDA_sen:
											FDA_sen = FDA_sen.replace("；","<br>")
											FDA_sen = FDA_sen.replace(";","<br>")
										con = con.replace("replace",FDA_sen)
									if 'clinical_sensitive' in con:
										clinical_sen = FDA_result[i].split('\t')[3]
										if '；' in clinical_sen or ';' in clinical_sen:
											clinical_sen = clinical_sen.replace("；","<br>")
											clinical_sen = clinical_sen.replace(";","<br>")
										con = con.replace("replace",clinical_sen)
									if 'resistance' in con:
										resis = FDA_result[i].split('\t')[4]
										if '；' in resis or ';' in resis:
											resis = resis.replace("；","<br>")
											resis = resis.replace(";","<br>")
										con = con.replace("replace",resis)
									f.write(con)
							f.write('\n')
						#判断是否将snp和TMB是否放一起，blood cut=5,单页突变过多，定cut=8
						if sample_type == "blood":
							cut1 = 3-len(useful_list)
							cut2 = 8-len(useful_list)
							if len(nouseful_list)  <= cut1:
								for i in range(len(nouseful_list)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')

							elif cut1 < len(nouseful_list) <=  cut2:
								#print(nouseful_list)
								for i in range(len(nouseful_list)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								##添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')

							else:
								for i in range(cut2):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								##添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')

								##读取表头
								read_FDA_start = open(config_path+'6_snp_detect_result_start.html')
								content_FDA_start = read_FDA_start.readlines()
								f.write("".join(str(i) for i in content_FDA_start))
								f.write("\n")

								for i in range(len(nouseful_list)-cut2):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[cut2+i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[cut2+i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')

								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
						
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')
							#添加备注信息
							mark = open(config_path+'6_mark_TMB_low.html')
							content_mark = mark.readlines()
							f.write("".join(str(i) for i in content_mark))
							f.write("\n")
							# ##添加页脚
							# read_footer = open(config_path+'config_footer.html')
							# content_footer = read_footer.readlines()
							# f.write("".join(str(i) for i in content_footer))
							# f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						
						elif sample_type == "tissue":
							cut1 = 2-len(useful_list)
							cut2 = 9-len(useful_list)
							if len(nouseful_list)  <= cut1:
								for i in range(len(nouseful_list)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')
								read_MSI_title = open(config_path+'6_MSI_title.html')
								content_MSI_title = read_MSI_title.readlines()
								f.write("".join(str(i) for i in content_MSI_title))
								f.write("\n")
								#读取MSI内容
								with open(config_path+'6_MSI_content.html') as msi_content:
									for con in msi_content:
										if "MSI_result" in con:
											con = con.replace("replace",MSI_stat)
										if MSI_stat == "MSS" or MSI_stat == "MSI-L":
											if "MSI_drug" in con:
												con = con.replace("replace","NA")
										else:
											if "MSI_drug" in con:
												con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
										f.write(con)
								f.write('\n')

							elif cut1 < len(nouseful_list) <=  cut2:
								#print(nouseful_list)
								for i in range(len(nouseful_list)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								##添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')
								read_MSI_title = open(config_path+'6_MSI_title.html')
								content_MSI_title = read_MSI_title.readlines()
								f.write("".join(str(i) for i in content_MSI_title))
								f.write("\n")
								#读取MSI内容
								with open(config_path+'6_MSI_content.html') as msi_content:
									for con in msi_content:
										if "MSI_result" in con:
											con = con.replace("replace",MSI_stat)
										if MSI_stat == "MSS" or MSI_stat == "MSI-L":
											if "MSI_drug" in con:
												con = con.replace("replace","NA")
										else:
											if "MSI_drug" in con:
												con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
										f.write(con)
								f.write('\n')

							else:
								for i in range(cut2):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								##添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')

								##读取表头
								read_FDA_start = open(config_path+'6_snp_detect_result_start.html')
								content_FDA_start = read_FDA_start.readlines()
								f.write("".join(str(i) for i in content_FDA_start))
								f.write("\n")

								for i in range(len(nouseful_list)-cut2):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[cut2+i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[cut2+i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')

								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
						
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')
								read_MSI_title = open(config_path+'6_MSI_title.html')
								content_MSI_title = read_MSI_title.readlines()
								f.write("".join(str(i) for i in content_MSI_title))
								f.write("\n")
								#读取MSI内容
								with open(config_path+'6_MSI_content.html') as msi_content:
									for con in msi_content:
										if "MSI_result" in con:
											con = con.replace("replace",MSI_stat)
										if MSI_stat == "MSS" or MSI_stat == "MSI-L":
											if "MSI_drug" in con:
												con = con.replace("replace","NA")
										else:
											if "MSI_drug" in con:
												con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
										f.write(con)
								f.write('\n')

							#添加备注信息
							mark = open(config_path+'6_mark_TMB_low.html')
							content_mark = mark.readlines()
							f.write("".join(str(i) for i in content_mark))
							f.write("\n")
							# ##添加页脚
							# read_footer = open(config_path+'config_footer.html')
							# content_footer = read_footer.readlines()
							# f.write("".join(str(i) for i in content_footer))
							# f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					##TMB low，全部无意义位点，全部放无意义位点，NA
					if TMB_value == 0 and len(useful_list) == 0:
						#如果无意义的点<5个，一页，其余两页
						##读取表头
						snp_title = open(config_path+'6_snp_title.html')
						content_snp = snp_title.readlines()
						f.write("".join(str(i) for i in content_snp))
						f.write('\n')
						read_FDA_start = open(config_path+'6_snp_detect_result_start.html')
						content_FDA_start = read_FDA_start.readlines()
						f.write("".join(str(i) for i in content_FDA_start))
						f.write("\n")
						if sample_type == "blood":
							cut1 = 2  
							cut2 = 8
							if len(nouseful_list)  <= cut1:
								for i in range(len(nouseful_list)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')

							elif cut1 < len(nouseful_list) <=  cut2:
								#print(nouseful_list)
								for i in range(len(nouseful_list)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								##添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')

							else:
								for i in range(cut2):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								##添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')

								##读取表头
								read_FDA_start = open(config_path+'6_snp_detect_result_start.html')
								content_FDA_start = read_FDA_start.readlines()
								f.write("".join(str(i) for i in content_FDA_start))
								f.write("\n")

								for i in range(len(nouseful_list)-cut2):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[cut2+i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[cut2+i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')

								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
						
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')
							#添加备注信息
							mark = open(config_path+'6_mark_TMB_low.html')
							content_mark = mark.readlines()
							f.write("".join(str(i) for i in content_mark))
							f.write("\n")
							# ##添加页脚
							# read_footer = open(config_path+'config_footer.html')
							# content_footer = read_footer.readlines()
							# f.write("".join(str(i) for i in content_footer))
							# f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						
						elif sample_type == "tissue":
							cut1 = 2 
							cut2 = 5
							if len(nouseful_list)  < cut1:
								for i in range(len(nouseful_list)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')
								read_MSI_title = open(config_path+'6_MSI_title.html')
								content_MSI_title = read_MSI_title.readlines()
								f.write("".join(str(i) for i in content_MSI_title))
								f.write("\n")
								#读取MSI内容
								with open(config_path+'6_MSI_content.html') as msi_content:
									for con in msi_content:
										if "MSI_result" in con:
											con = con.replace("replace",MSI_stat)
										if MSI_stat == "MSS" or MSI_stat == "MSI-L":
											if "MSI_drug" in con:
												con = con.replace("replace","NA")
										else:
											if "MSI_drug" in con:
												con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
										f.write(con)
								f.write('\n')

							elif cut1 <= len(nouseful_list) <=  cut2:
								#print(nouseful_list)
								for i in range(len(nouseful_list)):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								##添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')
								
								read_MSI_title = open(config_path+'6_MSI_title.html')
								content_MSI_title = read_MSI_title.readlines()
								f.write("".join(str(i) for i in content_MSI_title))
								f.write("\n")
								#读取MSI内容
								with open(config_path+'6_MSI_content.html') as msi_content:
									for con in msi_content:
										if "MSI_result" in con:
											con = con.replace("replace",MSI_stat)
										if MSI_stat == "MSS" or MSI_stat == "MSI-L":
											if "MSI_drug" in con:
												con = con.replace("replace","NA")
										else:
											if "MSI_drug" in con:
												con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
										f.write(con)
								f.write('\n')

							else:
								for i in range(cut2):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')
								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
								##添加页脚
								read_footer = open(config_path+'config_footer.html')
								content_footer = read_footer.readlines()
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页
								##添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')

								##读取表头
								read_FDA_start = open(config_path+'6_snp_detect_result_start.html')
								content_FDA_start = read_FDA_start.readlines()
								f.write("".join(str(i) for i in content_FDA_start))
								f.write("\n")

								for i in range(len(nouseful_list)-cut2):
									with open(config_path+'6_snp_detect_result_content.html') as FDA_content:
										for con in FDA_content:
											if 'SNP_result' in con:
												snp_result = nouseful_list[cut2+i].split('\t')[0].replace(" ","<br>")
												con = con.replace("replace",snp_result)
											if 'SNP_freq' in con:
												con = con.replace("replace",nouseful_list[cut2+i].split('\t')[1])
											if 'SNP_sensitive' in con:
												con = con.replace("replace","NA")
											if 'clinical_sensitive' in con:
												con = con.replace("replace","NA")
											if 'resistance' in con:
												con = con.replace("replace","NA")
											f.write(con)
									f.write('\n')

								#读取表尾
								read_FDA_end = open(config_path+'6_snp_detect_result_end.html')
								content_FDA_end = read_FDA_end.readlines()
								f.write("".join(str(i) for i in content_FDA_end))
								f.write("\n")
						
								#读取TMB title
								read_TMB_title = open(config_path+'6_TMB_title.html')
								content_TMB_title = read_TMB_title.readlines()
								f.write("".join(str(i) for i in content_TMB_title))
								#读取TMB内容
								with open(config_path+'6_TMB_content.html') as tmb_content:
									for con in tmb_content:
										if "TMB_num" in con:
											con = con.replace("replace",TMB_num)
										if "TMB_result" in con:
											con = con.replace("replace","低")
										if "TMB_drug" in con:
											con = con.replace("replace","NA")
										f.write(con)
								f.write('\n')
								read_MSI_title = open(config_path+'6_MSI_title.html')
								content_MSI_title = read_MSI_title.readlines()
								f.write("".join(str(i) for i in content_MSI_title))
								f.write("\n")
								#读取MSI内容
								with open(config_path+'6_MSI_content.html') as msi_content:
									for con in msi_content:
										if "MSI_result" in con:
											con = con.replace("replace",MSI_stat)
										if MSI_stat == "MSS" or MSI_stat == "MSI-L":
											if "MSI_drug" in con:
												con = con.replace("replace","NA")
										else:
											if "MSI_drug" in con:
												con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
										f.write(con)
								f.write('\n')

							#添加备注信息
							mark = open(config_path+'6_mark_TMB_low.html')
							content_mark = mark.readlines()
							f.write("".join(str(i) for i in content_mark))
							f.write("\n")
							# ##添加页脚
							# read_footer = open(config_path+'config_footer.html')
							# content_footer = read_footer.readlines()
							# f.write("".join(str(i) for i in content_footer))
							# f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						

					#判断，如果TMB high+全阴性样本，点突变没有内容；其他情况，点突变、TMB、MSI结果都有
					if TMB_value == 1 and len(useful_list) == 0:
						#直接显示TMB、MSI
						#读取TMB title
						read_TMB_title = open(config_path+'6-1_TMB_title.html')
						content_TMB_title = read_TMB_title.readlines()
						f.write("".join(str(i) for i in content_TMB_title))
						#读取TMB内容
						with open(config_path+'6_TMB_content.html') as tmb_content:
							for con in tmb_content:
								if "TMB_num" in con:
									con = con.replace("replace",TMB_num)
								if "TMB_result" in con:
									con = con.replace("replace","高")
								if "TMB_drug" in con:
									if patient_dignose in Lung_dict or "肺" in patient_dignose:	#判断该癌种是否为肺癌，肺癌为2A，其他为2B
										con = con.replace("replace","Pembrolizumab（2A）<br>Ipilimumab（2A）<br>Atezolizumab（2A）<br>Nivolumab（2A）<br>Nivolumab+Ipilimumab（2A）")
									else:
										con = con.replace("replace","Pembrolizumab（2B）<br>Ipilimumab（2B）<br>Atezolizumab（2B）<br>Nivolumab（2B）<br>Nivolumab+Ipilimumab（2B）")

								f.write(con)
						f.write('\n')

						#读取MSI title
						if sample_type == "tissue":
							read_MSI_title = open(config_path+'6-2_MSI_title.html')
							content_MSI_title = read_MSI_title.readlines()
							f.write("".join(str(i) for i in content_MSI_title))
							f.write("\n")
							#读取MSI内容
							with open(config_path+'6_MSI_content.html') as msi_content:
								for con in msi_content:
									if "MSI_result" in con:
										con = con.replace("replace",MSI_stat)
									if MSI_stat == "MSS" or MSI_stat == "MSI-L":
										if "MSI_drug" in con:
											con = con.replace("replace","NA")
									else:
										if "MSI_drug" in con:
											con = con.replace("replace","Pembrolizumab（1）<br>Ipilimumab（1）<br>nivolumab（1）<br>irinotecan（3A）<br>mitomycin C（3A）")
									f.write(con)
							f.write('\n')
						else:
							pass
						#添加备注信息
						mark = open(config_path+'6_mark_TMB_high.html')
						content_mark = mark.readlines()
						f.write("".join(str(i) for i in content_mark))
						f.write("\n")

					##如果突变全部无意义，TMB是low，MSI是low，在此处需要加上签名页。
					if TMB_value == 0 and len(useful_list) == 0 and (MSI_stat == "MSS" or MSI_stat == "MSI_L"):
						read_sign_name = open(config_path+'9_sign_name.html')
						content_sign_name = read_sign_name.readlines()
						f.write("".join(str(i) for i in content_sign_name))
						f.write('\n')

					##添加页脚
					read_footer = open(config_path+'config_footer.html')
					content_footer = read_footer.readlines()
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页


				




					#########<---------------  添加检测结果解析及用药信息  ----------------->#########
					#这部分
					##全无意义位点，点突变没有
					##TMB low， TMB没有
					##MSS，   MSI没有

					#####------点突变------####
					if len(useful_list) == 0: 		#全部无意义
						pass
					if len(useful_list) != 0:
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									con = con.replace("replace",patient_name)
								if "title_sample_number" in con:
									con = con.replace("replace",sample)
								f.write(con)
						f.write('\n')
						##添加检测结果解析及用药信息的title信息
						read_detect_title = open(config_path+'7_explain_title.html')
						content_detect_title = read_detect_title.readlines()
						f.write("".join(str(i) for i in content_detect_title))
						f.write('\n')
						###添加点突变的title
						read_snp_title = open(config_path+'7_snp_explain_title.html')
						content_snp_title = read_snp_title.readlines()
						f.write("".join(str(i) for i in content_snp_title))
						f.write('\n')

						FDA_explain_list.sort()		#对list进行排序，让相同基因不同突变靠在一起
						geneList = []
						run_num = 0
						fusion_num = 0 		#这个是用于 结果如果大于一个，后面是需要加页眉的
						snp_num = 0
						for i in range(len(FDA_explain_list)):
							detect_info = FDA_explain_list[i].strip().split('\t')
							#print(detect_info)
							if '-' in detect_info[0]:	#代表是融合基因
								if fusion_num == 0 and run_num == 0:	#第一个是fusion,不需要添加页眉，因为上面已经加了
									with open(config_path+'7_snp_explain_gene_des.html','r') as DETE:
										for EACH in DETE:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[1])
											f.write(EACH)
									with open(config_path+'7_snp_explain_content.html','r') as DR:
										for EACH in DR:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'var_description' in EACH:
												if "ExonX-ExonY" in detect_info[2]:	#替换
													connect_exon = detect_info[2].replace("ExonX-ExonY",detect_info[-1])
												else:
													connect_exon = detect_info[2]
												EACH = EACH.replace("replace",connect_exon)
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[4] or ';' in detect_info[4]:		#添加药物，将；改成换行符
													beni_drug = detect_info[4].replace("；","\t")
													beni_drug = detect_info[4].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														EACH_copy = EACH_copy.replace("drug_replace",each_drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[4]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
													EACH = EACH.replace("other_replace","---")
												else:
													EACH = EACH.replace("drug_replace",detect_info[4])
													if '1' in detect_info[4] or '2（A）' in detect_info[4] or '2(A)' in detect_info[4]:		#本癌种，根据药物中是否有（1），有打钩，无---
														EACH = EACH.replace("this_replace","&#10003")
													else:
														EACH = EACH.replace("this_replace","---")
													if '2（B）' in detect_info[4] or '2(B)' in detect_info[4]:
														EACH = EACH.replace("other_replace","&#10003")
													else:
														EACH = EACH.replace("other_replace","---")
											if "drug_replace" in EACH:
												continue
											f.write(EACH)
									f.write('\n')
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n\t\t"+change_page+'\n\n\n')				#换页
									fusion_num += 1

								elif fusion_num == 0 and run_num > 0:	#第一个不是fusion,但是后面有fusion,那第一次出现的时候就要加页眉了
									##添加页眉
									with open(config_path+'config_header.html','r') as HEADER:
										for con in HEADER:
											if "title_name" in con:
												con = con.replace("replace",patient_name)
											if "title_sample_number" in con:
												con = con.replace("replace",sample)
											f.write(con)
									with open(config_path+'7_snp_explain_gene_des.html','r') as DETE:
										for EACH in DETE:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[1])
											f.write(EACH)
									with open(config_path+'7_snp_explain_content.html','r') as DR:
										for EACH in DR:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'var_description' in EACH:
												if "ExonX-ExonY" in detect_info[2]:	#替换
													connect_exon = detect_info[2].replace("ExonX-ExonY",detect_info[-1])
												else:
													connect_exon = detect_info[2]
												EACH = EACH.replace("replace",connect_exon)
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[4] or ';' in detect_info[4]:		#添加药物，将；改成换行符
													beni_drug = detect_info[4].replace("；","\t")
													beni_drug = detect_info[4].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														EACH_copy = EACH_copy.replace("drug_replace",each_drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[4]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
													EACH = EACH.replace("other_replace","---")
												else:
													EACH = EACH.replace("drug_replace",detect_info[4])
													if '1' in detect_info[4] or '2（A）' in detect_info[4] or '2(A)' in detect_info[4]:		#本癌种，根据药物中是否有（1），有打钩，无---
														EACH = EACH.replace("this_replace","&#10003")
													else:
														EACH = EACH.replace("this_replace","---")
													if '2（B）' in detect_info[4] or '2(B)' in detect_info[4]:
														EACH = EACH.replace("other_replace","&#10003")
													else:
														EACH = EACH.replace("other_replace","---")
											if "drug_replace" in EACH:
												continue
											f.write(EACH)
									f.write('\n')
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n\t\t"+change_page+'\n\n\n')				#换页
									fusion_num += 1
								elif fusion_num > 0:
									#此时就需要添加页眉了
									with open(config_path+'config_header.html','r') as HEADER:
										for con in HEADER:
											if "title_name" in con:
												con = con.replace("replace",patient_name)
											if "title_sample_number" in con:
												con = con.replace("replace",sample)
											f.write(con)

									with open(config_path+'7_snp_explain_gene_des.html','r') as DETE:
										for EACH in DETE:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[1])
											f.write(EACH)
									with open(config_path+'7_snp_explain_content.html','r') as DR:
										for EACH in DR:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'var_description' in EACH:
												if "ExonX-ExonY" in detect_info[2]:	#替换
													connect_exon = detect_info[2].replace("ExonX-ExonY",detect_info[-1])
												else:
													connect_exon = detect_info[2]
												EACH = EACH.replace("replace",connect_exon)
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[4] or ';' in detect_info[4]:		#添加药物，将；改成换行符
													beni_drug = detect_info[4].replace("；","\t")
													beni_drug = detect_info[4].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														EACH_copy = EACH_copy.replace("drug_replace",each_drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[4]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
												EACH = EACH.replace("other_replace","---")
											else:
												EACH = EACH.replace("drug_replace",detect_info[4])
												if '1' in detect_info[4] or '2（A）' in detect_info[4] or '2(A)' in detect_info[4]:		#本癌种，根据药物中是否有（1），有打钩，无---
													EACH = EACH.replace("this_replace","&#10003")
												else:
													EACH = EACH.replace("this_replace","---")
												if '2（B）' in detect_info[4] or '2(B)' in detect_info[4]:
													EACH = EACH.replace("other_replace","&#10003")
												else:
													EACH = EACH.replace("other_replace","---")
										if "drug_replace" in EACH:
											continue
										f.write(EACH)
										f.write('\n')
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n\t\t"+change_page+'\n\n\n')				#换页		
									fusion_num += 1
							if '-' not in detect_info[0]:		#代表的是单个突变点,此时需要判断基因是否在
								if snp_num == 0 and run_num == 0:	#第一次运行，同时第一个出现的是snp，就不需要添加页眉
									with open(config_path+'7_snp_explain_gene_des.html','r') as DETE2:
										for EACH in DETE2:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[2])
											f.write(EACH)
									with open(config_path+'7_snp_explain_content.html','r') as DR2:
										for EACH in DR2:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+" "+detect_info[1])
											if 'var_description' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[4])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
													beni_drug = detect_info[5].replace("；","\t")
													beni_drug = detect_info[5].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														EACH_copy = EACH_copy.replace("drug_replace",each_drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[5]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
													EACH = EACH.replace("other_replace","---")
												else:
													EACH = EACH.replace("drug_replace",detect_info[5])
													if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
														EACH = EACH.replace("this_replace","&#10003")
													else:
														EACH = EACH.replace("this_replace","---")
													if '2（B）' in detect_info[5] or '2(B)' in detect_info[5]:
														EACH = EACH.replace("other_replace","&#10003")
													else:
														EACH = EACH.replace("other_replace","---")
											if "drug_replace" in EACH:
												continue
											f.write(EACH)
									f.write('\n')				
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n\t\t"+change_page+'\n\n\n')				#换页
									snp_num += 1
									geneList.append(detect_info[0])
								elif snp_num == 0 and run_num > 0:	#第一次运行，但是第一个出现的是fusion，这时候就需要添加页眉
									with open(config_path+'config_header.html','r') as HEADER:
										for con in HEADER:
											if "title_name" in con:
												con = con.replace("replace",patient_name)
											if "title_sample_number" in con:
												con = con.replace("replace",sample)
											f.write(con)
									with open(config_path+'7_snp_explain_gene_des.html','r') as DETE2:
										for EACH in DETE2:
											if 'gene_name' in EACH:
												EACH = EACH.replace("replace",detect_info[0])
											if 'gene_des' in EACH:
												EACH = EACH.replace("replace",detect_info[2])
											f.write(EACH)
									with open(config_path+'7_snp_explain_content.html','r') as DR2:
										for EACH in DR2:
											if 'gene_var' in EACH:
												EACH = EACH.replace("replace",detect_info[0]+" "+detect_info[1])
											if 'var_description' in EACH:
												EACH = EACH.replace("replace",detect_info[3])
											if 'treat_strategy' in EACH:
												EACH = EACH.replace("replace",detect_info[4])
											if 'drug_cancer' in EACH:
												if '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
													beni_drug = detect_info[5].replace("；","\t")
													beni_drug = detect_info[5].replace(";","\t")
													drugs = beni_drug.split('\t')
													for each_drug in drugs:
														EACH_copy = EACH
														EACH_copy = EACH_copy.replace("drug_replace",each_drug)
														if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH_copy = EACH_copy.replace("this_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("this_replace","---")
														if '2（B）' in each_drug or '2(B)' in each_drug:
															EACH_copy = EACH_copy.replace("other_replace","&#10003")
														else:
															EACH_copy = EACH_copy.replace("other_replace","---")
														f.write(EACH_copy)
												elif "NA" in detect_info[5]:
													EACH = EACH.replace("drug_replace","NA")
													EACH = EACH.replace("this_replace","---")
													EACH = EACH.replace("other_replace","---")
												else:
													EACH = EACH.replace("drug_replace",detect_info[5])
													if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
														EACH = EACH.replace("this_replace","&#10003")
													else:
														EACH = EACH.replace("this_replace","---")
													if '2（B）' in detect_info[5] or '2(B)' in detect_info[5]:
														EACH = EACH.replace("other_replace","&#10003")
													else:
														EACH = EACH.replace("other_replace","---")
											if "drug_replace" in EACH:
												continue
											f.write(EACH)	
									f.write('\n')			
									#再加上页脚
									#添加页脚
									f.write("".join(str(i) for i in content_footer))
									f.write("\n\t\t"+change_page+'\n\n\n')				#换页
									snp_num += 1
									geneList.append(detect_info[0])		
								elif snp_num > 0:
									#print(detect_info[0],detect_info[1])
									#运行第二次，这个时候，需要检测下这次的基因是不是和上次的一样，如果是一样的， 就不要添加基因和基因介绍了
									if detect_info[0] in geneList:	
										#需要添加页眉了
										with open(config_path+'config_header.html','r') as HEADER:
											for con in HEADER:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",sample)
												f.write(con)
						
										with open(config_path+'7_snp_explain_content.html','r') as DR2:
											for EACH in DR2:
												if 'gene_var' in EACH:
													EACH = EACH.replace("replace",detect_info[0]+" "+detect_info[1])
												if 'var_description' in EACH:
													EACH = EACH.replace("replace",detect_info[3])
												if 'treat_strategy' in EACH:
													EACH = EACH.replace("replace",detect_info[4])
												if 'drug_cancer' in EACH:
													if '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
														beni_drug = detect_info[5].replace("；","\t")
														beni_drug = detect_info[5].replace(";","\t")
														drugs = beni_drug.split('\t')
														for each_drug in drugs:
															EACH_copy = EACH
															EACH_copy = EACH_copy.replace("drug_replace",each_drug)
															if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
																EACH_copy = EACH_copy.replace("this_replace","&#10003")
															else:
																EACH_copy = EACH_copy.replace("this_replace","---")
															if '2（B）' in each_drug or '2(B)' in each_drug:
																EACH_copy = EACH_copy.replace("other_replace","&#10003")
															else:
																EACH_copy = EACH_copy.replace("other_replace","---")
															f.write(EACH_copy)
													elif "NA" in detect_info[5]:
														EACH = EACH.replace("drug_replace","NA")
														EACH = EACH.replace("this_replace","---")
														EACH = EACH.replace("other_replace","---")
													else:
														EACH = EACH.replace("drug_replace",detect_info[5])
														if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH = EACH.replace("this_replace","&#10003")
														else:
															EACH = EACH.replace("this_replace","---")
														if '2（B）' in detect_info[5] or '2(B)' in detect_info[5]:
															EACH = EACH.replace("other_replace","&#10003")
														else:
															EACH = EACH.replace("other_replace","---")
												if "drug_replace" in EACH:
													continue
												f.write(EACH)
										f.write('\n')				
										#再加上页脚
										#添加页脚
										f.write("".join(str(i) for i in content_footer))
										f.write("\n\t\t"+change_page+'\n\n\n')				#换页
										snp_num += 1
									else:		#基因不重复，正常添加
										#此时就需要添加页眉了
										#print(detect_info[0])
										with open(config_path+'config_header.html','r') as HEADER:
											for con in HEADER:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",sample)
												f.write(con)

										with open(config_path+'7_snp_explain_gene_des.html','r') as DETE2:
											for EACH in DETE2:
												if 'gene_name' in EACH:
													EACH = EACH.replace("replace",detect_info[0])
												if 'gene_des' in EACH:
													EACH = EACH.replace("replace",detect_info[2])
												f.write(EACH)
										with open(config_path+'7_snp_explain_content.html','r') as DR2:
											for EACH in DR2:
												if 'gene_var' in EACH:
													EACH = EACH.replace("replace",detect_info[0]+" "+detect_info[1])
												if 'var_description' in EACH:
													EACH = EACH.replace("replace",detect_info[3])
												if 'treat_strategy' in EACH:
													EACH = EACH.replace("replace",detect_info[4])
												if 'drug_cancer' in EACH:
													if '；' in detect_info[5] or ';' in detect_info[5]:		#添加药物，将；改成换行符
														beni_drug = detect_info[5].replace("；","\t")
														beni_drug = detect_info[5].replace(";","\t")
														drugs = beni_drug.split('\t')
														for each_drug in drugs:
															EACH_copy = EACH
															EACH_copy = EACH_copy.replace("drug_replace",each_drug)
															if '1' in each_drug or '2（A）' in each_drug or '2(A)' in each_drug:		#本癌种，根据药物中是否有（1），有打钩，无---
																EACH_copy = EACH_copy.replace("this_replace","&#10003")
															else:
																EACH_copy = EACH_copy.replace("this_replace","---")
															if '2（B）' in each_drug or '2(B)' in each_drug:
																EACH_copy = EACH_copy.replace("other_replace","&#10003")
															else:
																EACH_copy = EACH_copy.replace("other_replace","---")
															f.write(EACH_copy)
													elif "NA" in detect_info[5]:
														EACH = EACH.replace("drug_replace","NA")
														EACH = EACH.replace("this_replace","---")
														EACH = EACH.replace("other_replace","---")
													else:
														EACH = EACH.replace("drug_replace",detect_info[5])
														if '1' in detect_info[5] or '2（A）' in detect_info[5] or '2(A)' in detect_info[5]:		#本癌种，根据药物中是否有（1），有打钩，无---
															EACH = EACH.replace("this_replace","&#10003")
														else:
															EACH = EACH.replace("this_replace","---")
														if '2（B）' in detect_info[5] or '2(B)' in detect_info[5]:
															EACH = EACH.replace("other_replace","&#10003")
														else:
															EACH = EACH.replace("other_replace","---")
												if "drug_replace" in EACH:
													continue
												f.write(EACH)	
										f.write('\n')			
										#再加上页脚
										#添加页脚
										f.write("".join(str(i) for i in content_footer))
										f.write("\n\t\t"+change_page+'\n\n\n')				#换页
										geneList.append(detect_info[0])
										snp_num += 1
							run_num += 1

					#####------TMB------####
					##TMB low,不显示
					if TMB_value == 0:
						pass
					else:
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									con = con.replace("replace",patient_name)
								if "title_sample_number" in con:
									con = con.replace("replace",sample)
								f.write(con)

						##读取content,区分肺癌与其他癌
						if '肺' in patient_dignose:
							with open(config_path+'7_TMB_lung_explain.html','r') as TMB_EX:
								for con in TMB_EX:
									if "TMB_num" in con:
										con = con.replace("replace",TMB_num)
									f.write(con)
							f.write('\n')
						else:
							with open(config_path+'7_TMB_nolung_explain.html','r') as TMB_EX:
								for con in TMB_EX:
									if "TMB_num" in con:
										con = con.replace("replace",TMB_num)
									f.write(con)
							f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					###MSS，不显示或者blood样本，不显示
					if MSI_stat == "MSS" or MSI_stat == "MSI-L" or sample_type == "blood":
						pass
					elif MSI_stat == "MSI-H":
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									con = con.replace("replace",patient_name)
								if "title_sample_number" in con:
									con = con.replace("replace",sample)
								f.write(con)
						f.write('\n')
						##读取content

						read_MSI_explain = open(config_path+'7_MSI_explain.html')
						content_MSI_explain = read_MSI_explain.readlines()
						f.write("".join(str(i) for i in content_MSI_explain))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页


					#########<---------------  添加临床试验信息  ----------------->#########
					if TMB_value == 0 and len(useful_list) == 0 and (MSI_stat == "MSS" or MSI_stat == "MSI_L"):
						pass

					else:
						#设置一个上限，规定每页输出几个条目的clinical信息
						limit = 3
						#先获取clinical表格条目数，可认为是行数
						clinical_num =  len(open(sample+'_clinical.xls','r').readlines())
						##如果clinical_num为0 ，无临床试验信息,写入信息：	暂未找到相关临床试验
						pipei_list = ["Drug","Biological","Procedure","Other","Device","Behavioral"]
						if clinical_num == 0:
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										con = con.replace("replace",patient_name)
									if "title_sample_number" in con:
										con = con.replace("replace",sample)
									f.write(con)
							f.write('\n')
							###添加clinical_info的title信息
							read_clinical_info = open(config_path+'8_clinical_tilte_info.html')
							content_clinical_info = read_clinical_info.readlines()
							f.write("".join(str(i) for i in content_clinical_info))
							f.write('\n')
							read_clinical_sheet_title = open(config_path+'8_clinical_sheet_title.html')
							content_clinical_sheet_title = read_clinical_sheet_title.readlines()
							f.write("".join(str(i) for i in content_clinical_sheet_title))
							f.write('\n')
							read_neg_clinical_content = open(config_path+'8_clinical_neg.html')
							content_neg_clinical = read_neg_clinical_content.readlines()
							f.write("".join(str(i) for i in content_neg_clinical))
							f.write('\n')
							#添加签名页
							read_sign_name = open(config_path+'9_sign_name.html')
							content_sign_name = read_sign_name.readlines()
							f.write("".join(str(i) for i in content_sign_name))
							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页


						if clinical_num > 0:	##对于突变有临床信息的，如果TMB high 或者 MSI_stat 是MSI的，需要先添加到临床信息里面。
							if TMB_value == 1:	#MSI-H,就添加MSI-H信息
								clinical_add_TMB = open(sample+'_clinical.xls','a')
								with open(TMB_MSI_clinical,'r') as TM:
									for line in TM:
										line = line.strip().split('\t')
										if line[0] == "TMB-H":
											clinical_add_TMB.write(line[0]+'\t'+line[1]+'\t'+line[2]+'\t\t\t'+line[3]+'\t'+line[4]+'\n')
								clinical_add_TMB.close()

							if MSI_stat == "MSI" or MSI_stat == "MSI-H":
								clinical_add_MSI = open(sample+'_clinical.xls','a')
								with open(TMB_MSI_clinical,'r') as MS:
									for line in MS:
										line = line.strip().split('\t')
										if line[0] == "MSI-H":
											clinical_add_MSI.write(line[0]+'\t'+line[1]+'\t'+line[2]+'\t\t\t'+line[3]+'\t'+line[4]+'\n')
								clinical_add_MSI.close()

							clinical_list = []
							with open(sample+'_clinical.xls','r') as CLIN:
								for rows in CLIN:
									if rows not in clinical_list:
										clinical_list.append(rows)
							#print(len(clinical_list))
							##先确定所需要添加页数
							pages = (clinical_num-1) // limit + 1
							#<-----------如果只有一页临床试验------------>#
							if pages == 1:
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											con = con.replace("replace",patient_name)
										if "title_sample_number" in con:
											con = con.replace("replace",sample)
										f.write(con)
								f.write('\n')
								###添加clinical_info的title信息
								read_clinical_info = open(config_path+'8_clinical_tilte_info.html')
								content_clinical_info = read_clinical_info.readlines()
								f.write("".join(str(i) for i in content_clinical_info))
								f.write('\n')
								read_clinical_sheet_title = open(config_path+'8_clinical_sheet_title.html')
								content_clinical_sheet_title = read_clinical_sheet_title.readlines()
								f.write("".join(str(i) for i in content_clinical_sheet_title))
								f.write('\n')
								#clinical格式是，基因，癌种，敏感/不敏感，药物名称，阶段，证据类型，试验名称，ID
								n = 0
								for i in range(len(clinical_list)):
									if n < limit:		#保证每页最多三个条目
										clinical_info = clinical_list[0].split('\t')
										#print(clinical_info[5])
										with open(config_path+'8_clinical_sheet_content.html','r') as CI:
											for row in CI:
												if "clinical_info_gene" in row:
													row = row.replace("replace",clinical_info[0])
												if "clinical_info_name" in row:
													row = row.replace("replace",clinical_info[2])
												if "clinical_info_drug" in row:
													for key in pipei_list:
														if key in clinical_info[5]:
															clinical_info[5] = clinical_info[5].replace(key,"<br>"+key)
													row = row.replace("replace",clinical_info[5])
												if "clinical_info_step" in row:
													row = row.replace("replace",clinical_info[6])
												if "clinical_info_NCI" in row:
													row = row.replace("replace",clinical_info[1])
												f.write(row)
										del clinical_list[0]	#删除第一项
										n += 1
										f.write('\n')
								#读取签名页
								read_sign_name = open(config_path+'9_sign_name.html')
								content_sign_name = read_sign_name.readlines()
								f.write("".join(str(i) for i in content_sign_name))
								f.write('\n')
								#还要给每页添加页脚和换行
								f.write("".join(str(i) for i in content_footer))
								f.write("\n"+change_page+'\n\n\n')				#换页
							#<---------如果临床试验大于一页---------->#
							if pages > 1:
								for page in range(pages):
									#给每页添加页眉 以及表头
									if page == 0 :  #第一页，无签名
										#添加页眉
										with open(config_path+'config_header.html','r') as HEADER:
											for con in HEADER:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",sample)
												f.write(con)
										f.write('\n')
										###添加clinical_info的title信息
										read_clinical_info = open(config_path+'8_clinical_tilte_info.html')
										content_clinical_info = read_clinical_info.readlines()
										f.write("".join(str(i) for i in content_clinical_info))
										f.write('\n')
										read_clinical_sheet_title = open(config_path+'8_clinical_sheet_title.html')
										content_clinical_sheet_title = read_clinical_sheet_title.readlines()
										f.write("".join(str(i) for i in content_clinical_sheet_title))
										f.write('\n')
										#clinical格式是，基因，癌种，敏感/不敏感，药物名称，阶段，证据类型，试验名称，ID
										n = 0
										for i in range(len(clinical_list)):
											if n < limit:		#保证每页最多三个条目
												clinical_info = clinical_list[0].split('\t')
												with open(config_path+'8_clinical_sheet_content.html','r') as CI:
													for row in CI:
														if "clinical_info_gene" in row:
															row = row.replace("replace",clinical_info[0])
														if "clinical_info_name" in row:
															row = row.replace("replace",clinical_info[2])
														if "clinical_info_drug" in row:
															for key in pipei_list:
																if key in clinical_info[5]:
																	clinical_info[5] = clinical_info[5].replace(key,"<br>"+key)
															row = row.replace("replace",clinical_info[5])
														if "clinical_info_step" in row:
															row = row.replace("replace",clinical_info[6])
														if "clinical_info_NCI" in row:
															row = row.replace("replace",clinical_info[1])
														f.write(row)
												del clinical_list[0]	#删除第一项
												n += 1
												f.write('\n')
										#还要给每页添加页脚和换行
										f.write("".join(str(i) for i in content_footer))
										f.write("\n"+change_page+'\n\n\n')				#换页

									if 0 < page < pages -1:
										#添加页眉
										with open(config_path+'config_header.html','r') as HEADER:
											for con in HEADER:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",sample)
												f.write(con)
										f.write('\n')
										###添加clinical_info_sheet的title信息
										read_clinical_sheet_title = open(config_path+'8_clinical_sheet_title.html')
										content_clinical_sheet_title = read_clinical_sheet_title.readlines()
										f.write("".join(str(i) for i in content_clinical_sheet_title))
										n = 0
										for i in range(len(clinical_list)):
											if n < limit:		#保证每页最多三个条目
												clinical_info = clinical_list[0].split('\t')
												with open(config_path+'8_clinical_sheet_content.html','r') as CI:
													for row in CI:
														if "clinical_info_gene" in row:
															row = row.replace("replace",clinical_info[0])
														if "clinical_info_name" in row:
															row = row.replace("replace",clinical_info[2])
														if "clinical_info_drug" in row:
															for key in pipei_list:
																if key in clinical_info[5]:
																	clinical_info[5] = clinical_info[5].replace(key,"<br>"+key)
															row = row.replace("replace",clinical_info[5])
														if "clinical_info_step" in row:
															row = row.replace("replace",clinical_info[6])
														if "clinical_info_NCI" in row:
															row = row.replace("replace",clinical_info[1])
														f.write(row)
												del clinical_list[0]	#删除第一项
												n += 1
												f.write('\n')
										#还要给每页添加页脚和换行
										f.write("".join(str(i) for i in content_footer))
										f.write("\n"+change_page+'\n\n\n')				#换页
									if page == pages -1 and page != 0:	#确定是最后一页，因为要在最后一页添加签名档
										#添加页眉
										with open(config_path+'config_header.html','r') as HEADER:
											for con in HEADER:
												if "title_name" in con:
													con = con.replace("replace",patient_name)
												if "title_sample_number" in con:
													con = con.replace("replace",sample)
												f.write(con)
										f.write('\n')
										###添加clinical_info_sheet的title信息
										read_clinical_sheet_title = open(config_path+'8_clinical_sheet_title.html')
										content_clinical_sheet_title = read_clinical_sheet_title.readlines()
										f.write("".join(str(i) for i in content_clinical_sheet_title))
										f.write('\n')
										n = 0
										for i in range(len(clinical_list)):
											if n < limit:		#保证每页最多三个条目
												clinical_info = clinical_list[0].split('\t')
												with open(config_path+'8_clinical_sheet_content.html','r') as CI:
													for row in CI:
														if "clinical_info_gene" in row:
															row = row.replace("replace",clinical_info[0])
														if "clinical_info_name" in row:
															row = row.replace("replace",clinical_info[2])
														if "clinical_info_drug" in row:
															for key in pipei_list:
																if key in clinical_info[5]:
																	clinical_info[5] = clinical_info[5].replace(key,"<br>"+key)
															row = row.replace("replace",clinical_info[5])
														if "clinical_info_step" in row:
															row = row.replace("replace",clinical_info[6])
														if "clinical_info_NCI" in row:
															row = row.replace("replace",clinical_info[1])
														f.write(row)
												del clinical_list[0]	#删除第一项
												n += 1
												f.write('\n')
										#读取签名页
										read_sign_name = open(config_path+'9_sign_name.html')
										content_sign_name = read_sign_name.readlines()
										f.write("".join(str(i) for i in content_sign_name))
										f.write('\n')
										#还要给每页添加页脚和换行
										f.write("".join(str(i) for i in content_footer))
										f.write("\n"+change_page+'\n\n\n')				#换页



					#########<---------------  添加全程多节点质控  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')

					read_quality_control = open(config_path+'10_quality_control.html')
					content_quality_control = read_quality_control.readlines()
					f.write("".join(str(i) for i in content_quality_control))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					#########<---------------  添加样本质量评级  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')

					read_quality_control = open(config_path+'11_sample_quality_rate.html')
					content_quality_control = read_quality_control.readlines()
					f.write("".join(str(i) for i in content_quality_control))
					f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					#########<---------------  添加数据质控  ----------------->#########
					#添加页眉
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								con = con.replace("replace",patient_name)
							if "title_sample_number" in con:
								con = con.replace("replace",sample)
							f.write(con)
					f.write('\n')

					##添加质控内容，打开control_file，根据sample_id获取数据质控信息
					with open(control_file,'r') as CONTROL:
						for zhikong in CONTROL:
							zhikong = zhikong.strip().split('\t')
							if sample in zhikong[0]:
								zhikong_Q20 = '%.2f%%' % (float(zhikong[1]) * 100)
								zhikong_bidui = '%.2f%%' % (float(zhikong[2]) * 100)
								zhikong_fugai = '%.2f%%' % (float(zhikong[3]) * 100)
								zhikong_junyi = '%.2f%%' % (float(zhikong[4]) * 100)
								zhikong_shendu = str(zhikong[5])
					##根据sample_type是血液还是组织，来使用不同的模板
					if sample_type == "blood":
						with open(config_path+'blood_data_control_result.html','r') as CON_HTML:
							for con in CON_HTML:
								if "sample_id" in con:
									con = con.replace("replace",sample)
								if "Q20_data" in con:
									con = con.replace("replace",zhikong_Q20)
								if "bidui_data" in con:
									con = con.replace("replace",zhikong_bidui)
								if "fugai_data" in con:
									con = con.replace("replace",zhikong_fugai)
								if "junyi_data" in con:
									con = con.replace("replace",zhikong_junyi)
								if "shendu_data" in con:
									con = con.replace("replace",zhikong_shendu)
								f.write(con)
						f.write('\n')
					elif sample_type == "tissue":# 如果是组织的话，需要判断肿瘤含量
						with open(config_path+'tissue_data_control_result.html','r') as CON_HTML:
							for con in CON_HTML:
								if "sample_id" in con:
									con = con.replace("replace",sample)
								if "tumor_num" in con:
									if tumor_num == "NA" or int(tumor_num) < 5:
										con = con.replace('replace1',"/")
										con = con.replace('replace2',"/")
									elif int(tumor_num) >= 5:
										con = con.replace('replace1',"&#10003")
										con = con.replace('replace2',"&#10003")
								if "Q20_data" in con:
									con = con.replace("replace",zhikong_Q20)
								if "bidui_data" in con:
									con = con.replace("replace",zhikong_bidui)
								if "fugai_data" in con:
									con = con.replace("replace",zhikong_fugai)
								if "junyi_data" in con:
									con = con.replace("replace",zhikong_junyi)
								if "shendu_data" in con:
									con = con.replace("replace",zhikong_shendu)
								f.write(con)
						f.write('\n')
					#添加页脚
					f.write("".join(str(i) for i in content_footer))
					f.write("\n\t\t"+change_page+'\n\n\n')				#换页
					#########<---------------  添加附录  ----------------->#########
					
					if TMB_value == 1 and len(useful_list) != 0:
						#########<---------------  添加附录1'意义不明确的检测位点'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')

						#求无意义突变总数与3的商和余数，商为行数，余数为最后一行的列数
						#用divmod(a,b),获得元组，前后分别是商和余数
						if len(nouseful_list) <= 81:
							div,mod = divmod(len(nouseful_list),3)
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 0
										while n < div:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if mod == 0:
											if 'gene1' in con:
												continue
										elif mod == 1:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2","")
											con = con.replace("gene3","")
										elif mod == 2:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2",nouseful_list[-2])
											con = con.replace("gene3","")
									f.write(con)
							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						elif 81 < len(nouseful_list) <= 171:		#突变如果在81~171之间，就要两页
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 0
										while n < 27:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if 'gene1' in con:
											continue
									f.write(con)
							f.write('\n')
							##一整页，添加页脚
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页
							##再添加页眉
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n<br>')
							div,mod = divmod(len(nouseful_list[81:]),3)
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if 'title1' in con or 'title2' in con:
										continue
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 81
										while n < div:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if mod == 0:
											if 'gene1' in con:
												continue
										elif mod == 1:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2","")
											con = con.replace("gene3","")
										elif mod == 2:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2",nouseful_list[-2])
											con = con.replace("gene3","")

									f.write(con)
							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						elif len(nouseful_list) > 171:		#突变如果超过之间，就要三页。前两页满页 27行+30行   57*3=171
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 0
										while n < 27:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if 'gene1' in con:
											continue
									f.write(con)
							f.write('\n')
							##一整页，添加页脚
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页
							##再添加页眉
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n<br>')
							#下面添加第二页
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if 'title1' in con or 'title2' in con:
										continue
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 81
										while n < 30:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if 'gene1' in con:
											continue
									f.write(con)
							f.write('\n')
							##一整页，添加页脚
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页
							#接下来添加第三页，先添加页面
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n<br>')
							div,mod = divmod(len(nouseful_list[171:]),3)
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if 'title1' in con or 'title2' in con:
										continue
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 171
										while n < div:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if mod == 0:
											if 'gene1' in con:
												continue
										elif mod == 1:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2","")
											con = con.replace("gene3","")
										elif mod == 2:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2",nouseful_list[-2])
											con = con.replace("gene3","")

									f.write(con)
							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						
						#########<---------------  添加附录2'基因描述'  ----------------->#########
						###如果是融合，描述第二个基因，FDA_gene_list里已汇总好
						if len(FDA_gene_list) == 1:
							#添加页眉
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n')

							read_fulu2_des = open(config_path+'fulu2_gene_description_title.html')
							content_fulu2_des = read_fulu2_des.readlines()
							f.write("".join(str(i) for i in content_fulu2_des))
							f.write('\n')
							module_name = "fulu_%s.html" % FDA_gene_list[0]
							read_fulu = open(config_path+module_name)
							content_fulu = read_fulu.readlines()
							f.write("".join(str(i) for i in content_fulu))

							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						if len(FDA_gene_list) > 1:	#第一个基因有附录title，后面的没有
							#添加页眉
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n')

							read_fulu2_des = open(config_path+'fulu2_gene_description_title.html')
							content_fulu2_des = read_fulu2_des.readlines()
							f.write("".join(str(i) for i in content_fulu2_des))
							f.write('\n')
							module_name = "fulu_%s.html" % FDA_gene_list[0]
							read_fulu = open(config_path+module_name)
							content_fulu = read_fulu.readlines()
							f.write("".join(str(i) for i in content_fulu))

							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页

							for each_gene in FDA_gene_list[1:]: #从第二个基因开始，每一个基因调用一个小模块
								#添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											#con = con.replace("replace",patient_name)
											continue
										if "title_sample_number" in con:
											#con = con.replace("replace",sample)
											continue
										f.write(con)
								f.write('\n')

								module_name = "fulu_%s.html" % each_gene
								read_fulu = open(config_path+module_name)
								content_fulu = read_fulu.readlines()
								f.write("".join(str(i) for i in content_fulu))

								f.write('\n')
								#添加页脚
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#########<---------------  添加附录3'TMB检测意义'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu3_TMB = open(config_path+'fulu3_TMB_yiyi.html')
						content_fulu3_TMB = read_fulu3_TMB.readlines()
						f.write("".join(str(i) for i in content_fulu3_TMB))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#########<---------------  添加附录4'基因检测列表'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu4_gene1 = open(config_path+'fulu4-1_detect_genelist_1.html')
						content_fulu4_gene1 = read_fulu4_gene1.readlines()
						f.write("".join(str(i) for i in content_fulu4_gene1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu4_gene2 = open(config_path+'fulu4-2_detect_genelist_2.html')
						content_fulu4_gene2 = read_fulu4_gene2.readlines()
						f.write("".join(str(i) for i in content_fulu4_gene2))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#########<---------------  添加附录5'药物证据等级'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')

						read_fulu5_grade = open(config_path+'fulu5_drug_grade.html')
						content_fulu5_grade = read_fulu5_grade.readlines()
						f.write("".join(str(i) for i in content_fulu5_grade))

						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页


						#########<---------------  添加附录6'慈善药物'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu6_help1 = open(config_path+'fulu6_help_drug_info_first.html')
						content_fulu6_help1 = read_fulu6_help1.readlines()
						f.write("".join(str(i) for i in content_fulu6_help1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu6_help1 = open(config_path+'help_drug_info_second.html')
						content_fulu6_help1 = read_fulu6_help1.readlines()
						f.write("".join(str(i) for i in content_fulu6_help1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu6_help1 = open(config_path+'help_drug_info_third.html')
						content_fulu6_help1 = read_fulu6_help1.readlines()
						f.write("".join(str(i) for i in content_fulu6_help1))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#########<---------------  添加附录7'已上市靶向药物汇总'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						##添加title
						read_fulu7_public = open(config_path+'fulu7_public_drug_title.html')
						content_fulu7_public = read_fulu7_public.readlines()
						f.write("".join(str(i) for i in content_fulu7_public))
						f.write('\n')
						read_fulu7_page1 = open(config_path+'fulu7_drug_page1.html')
						content_fulu7_page1 = read_fulu7_page1.readlines()
						f.write("".join(str(i) for i in content_fulu7_page1))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page2 = open(config_path+'fulu7_drug_page2.html')
						content_fulu7_page2 = read_fulu7_page2.readlines()
						f.write("".join(str(i) for i in content_fulu7_page2))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page3 = open(config_path+'fulu7_drug_page3.html')
						content_fulu7_page3 = read_fulu7_page3.readlines()
						f.write("".join(str(i) for i in content_fulu7_page3))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page4 = open(config_path+'fulu7_drug_page4.html')
						content_fulu7_page4 = read_fulu7_page4.readlines()
						f.write("".join(str(i) for i in content_fulu7_page4))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页




					#####<<<<<<<<<< TMB low + 突变有意义 2 3 4 5 6 7 >>>>>>>>#####
					if TMB_value == 0 and len(useful_list) != 0:	
						#########<---------------  添加附录1'基因描述'  ----------------->#########
						###如果是融合，描述第二个基因，FDA_gene_list里已汇总好
						if len(FDA_gene_list) == 1:
							#添加页眉
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n')

							read_fulu1_des = open(config_path+'fulu1_gene_description_title.html')
							content_fulu1_des = read_fulu1_des.readlines()
							f.write("".join(str(i) for i in content_fulu1_des))
							f.write('\n')
							module_name = "fulu_%s.html" % FDA_gene_list[0]
							read_fulu = open(config_path+module_name)
							content_fulu = read_fulu.readlines()
							f.write("".join(str(i) for i in content_fulu))

							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						if len(FDA_gene_list) > 1:	#第一个基因有附录title，后面的没有
							#添加页眉
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n')

							read_fulu1_des = open(config_path+'fulu1_gene_description_title.html')
							content_fulu1_des = read_fulu1_des.readlines()
							f.write("".join(str(i) for i in content_fulu1_des))
							f.write('\n')
							module_name = "fulu_%s.html" % FDA_gene_list[0]
							read_fulu = open(config_path+module_name)
							content_fulu = read_fulu.readlines()
							f.write("".join(str(i) for i in content_fulu))

							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页

							for each_gene in FDA_gene_list[1:]: #从第二个基因开始，每一个基因调用一个小模块
								#添加页眉
								with open(config_path+'config_header.html','r') as HEADER:
									for con in HEADER:
										if "title_name" in con:
											#con = con.replace("replace",patient_name)
											continue
										if "title_sample_number" in con:
											#con = con.replace("replace",sample)
											continue
										f.write(con)
								f.write('\n')

								module_name = "fulu_%s.html" % each_gene
								read_fulu = open(config_path+module_name)
								content_fulu = read_fulu.readlines()
								f.write("".join(str(i) for i in content_fulu))

								f.write('\n')
								#添加页脚
								f.write("".join(str(i) for i in content_footer))
								f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#########<---------------  添加附录2'TMB检测意义'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu2_TMB = open(config_path+'fulu2_TMB_yiyi.html')
						content_fulu2_TMB = read_fulu2_TMB.readlines()
						f.write("".join(str(i) for i in content_fulu2_TMB))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#########<---------------  添加附录3'基因检测列表'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu3_gene1 = open(config_path+'fulu3-1_detect_genelist_1.html')
						content_fulu3_gene1 = read_fulu3_gene1.readlines()
						f.write("".join(str(i) for i in content_fulu3_gene1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu3_gene2 = open(config_path+'fulu4-2_detect_genelist_2.html')
						content_fulu3_gene2 = read_fulu3_gene2.readlines()
						f.write("".join(str(i) for i in content_fulu3_gene2))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#########<---------------  添加附录4'药物证据等级'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')

						read_fulu4_grade = open(config_path+'fulu4_drug_grade.html')
						content_fulu4_grade = read_fulu4_grade.readlines()
						f.write("".join(str(i) for i in content_fulu4_grade))

						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页


						#########<---------------  添加附录5'慈善药物'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu5_help1 = open(config_path+'fulu5_help_drug_info_first.html')
						content_fulu5_help1 = read_fulu5_help1.readlines()
						f.write("".join(str(i) for i in content_fulu5_help1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu5_help1 = open(config_path+'help_drug_info_second.html')
						content_fulu5_help1 = read_fulu5_help1.readlines()
						f.write("".join(str(i) for i in content_fulu5_help1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu5_help1 = open(config_path+'help_drug_info_third.html')
						content_fulu5_help1 = read_fulu5_help1.readlines()
						f.write("".join(str(i) for i in content_fulu5_help1))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页


						#########<---------------  添加附录6'已上市靶向药物汇总'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						##添加title
						read_fulu6_public = open(config_path+'fulu6_public_drug_title.html')
						content_fulu6_public = read_fulu6_public.readlines()
						f.write("".join(str(i) for i in content_fulu6_public))
						f.write('\n')
						read_fulu7_page1 = open(config_path+'fulu7_drug_page1.html')
						content_fulu7_page1 = read_fulu7_page1.readlines()
						f.write("".join(str(i) for i in content_fulu7_page1))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page2 = open(config_path+'fulu7_drug_page2.html')
						content_fulu7_page2 = read_fulu7_page2.readlines()
						f.write("".join(str(i) for i in content_fulu7_page2))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page3 = open(config_path+'fulu7_drug_page3.html')
						content_fulu7_page3 = read_fulu7_page3.readlines()
						f.write("".join(str(i) for i in content_fulu7_page3))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page4 = open(config_path+'fulu7_drug_page4.html')
						content_fulu7_page4 = read_fulu7_page4.readlines()
						f.write("".join(str(i) for i in content_fulu7_page4))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					#####<<<<<<<<<< TMB high + 全部突变无意义 1 3 4 5 6 7 >>>>>>>>#####
					if TMB_value == 1 and len(useful_list) == 0:	
						#########<---------------  添加附录1'意义不明确的突变列表'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')

						#求无意义突变总数与4的商和余数，商为行数，余数为最后一行的列数
						#用divmod(a,b),获得元组，前后分别是商和余数
						if len(nouseful_list) <= 81:
							div,mod = divmod(len(nouseful_list),3)
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 0
										while n < div:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if mod == 0:
											if 'gene1' in con:
												continue
										elif mod == 1:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2","")
											con = con.replace("gene3","")
										elif mod == 2:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2",nouseful_list[-2])
											con = con.replace("gene3","")
									f.write(con)
							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						elif 81 < len(nouseful_list) <= 171:		#突变如果在81~171之间，就要两页
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 0
										while n < 27:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if 'gene1' in con:
											continue
									f.write(con)
							f.write('\n')
							##一整页，添加页脚
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页
							##再添加页眉
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n<br>')
							div,mod = divmod(len(nouseful_list[81:]),3)
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if 'title1' in con or 'title2' in con:
										continue
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 81
										while n < div:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if mod == 0:
											if 'gene1' in con:
												continue
										elif mod == 1:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2","")
											con = con.replace("gene3","")
										elif mod == 2:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2",nouseful_list[-2])
											con = con.replace("gene3","")

									f.write(con)
							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						elif len(nouseful_list) > 171:		#突变如果超过之间，就要三页。前两页满页 27行+30行   57*3=171
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 0
										while n < 27:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if 'gene1' in con:
											continue
									f.write(con)
							f.write('\n')
							##一整页，添加页脚
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页
							##再添加页眉
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n<br>')
							#下面添加第二页
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 81
										while n < 30:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if 'gene1' in con:
											continue
									f.write(con)
							f.write('\n')
							##一整页，添加页脚
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页
							#接下来添加第三页，先添加页面
							with open(config_path+'config_header.html','r') as HEADER:
								for con in HEADER:
									if "title_name" in con:
										#con = con.replace("replace",patient_name)
										continue
									if "title_sample_number" in con:
										#con = con.replace("replace",sample)
										continue
									f.write(con)
							f.write('\n<br>')
							div,mod = divmod(len(nouseful_list[171:]),3)
							with open(config_path+'fulu1_nouseful_genelist.html','r') as FU1:
								for con in FU1:
									if 'title1' in con or 'title2' in con:
										continue
									if "nouse_genes" in con:
										#如果余数为0，则为整行
										n = 0
										m = 171
										while n < div:
											tmp = con.replace("gene1",nouseful_list[m])
											tmp = tmp.replace("gene2",nouseful_list[m+1])
											tmp = tmp.replace("gene3",nouseful_list[m+2])
											n += 1
											m += 3
											f.write(tmp)
										if mod == 0:
											if 'gene1' in con:
												continue
										elif mod == 1:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2","")
											con = con.replace("gene3","")
										elif mod == 2:
											con = con.replace("gene1",nouseful_list[-1])
											con = con.replace("gene2",nouseful_list[-2])
											con = con.replace("gene3","")

									f.write(con)
							f.write('\n')
							#添加页脚
							f.write("".join(str(i) for i in content_footer))
							f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#########<---------------  添加附录2'TMB检测意义'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu2_TMB = open(config_path+'fulu2_TMB_yiyi.html')
						content_fulu2_TMB = read_fulu2_TMB.readlines()
						f.write("".join(str(i) for i in content_fulu2_TMB))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#########<---------------  添加附录3'基因检测列表'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu3_gene1 = open(config_path+'fulu3-1_detect_genelist_1.html')
						content_fulu3_gene1 = read_fulu3_gene1.readlines()
						f.write("".join(str(i) for i in content_fulu3_gene1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu3_gene2 = open(config_path+'fulu4-2_detect_genelist_2.html')
						content_fulu3_gene2 = read_fulu3_gene2.readlines()
						f.write("".join(str(i) for i in content_fulu3_gene2))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#########<---------------  添加附录4'药物证据等级'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')

						read_fulu4_grade = open(config_path+'fulu4_drug_grade.html')
						content_fulu4_grade = read_fulu4_grade.readlines()
						f.write("".join(str(i) for i in content_fulu4_grade))

						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页


						#########<---------------  添加附录5'慈善药物'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu5_help1 = open(config_path+'fulu5_help_drug_info_first.html')
						content_fulu5_help1 = read_fulu5_help1.readlines()
						f.write("".join(str(i) for i in content_fulu5_help1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu5_help1 = open(config_path+'help_drug_info_second.html')
						content_fulu5_help1 = read_fulu5_help1.readlines()
						f.write("".join(str(i) for i in content_fulu5_help1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu5_help1 = open(config_path+'help_drug_info_third.html')
						content_fulu5_help1 = read_fulu5_help1.readlines()
						f.write("".join(str(i) for i in content_fulu5_help1))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页


						#########<---------------  添加附录6'已上市靶向药物汇总'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						##添加title
						read_fulu6_public = open(config_path+'fulu6_public_drug_title.html')
						content_fulu6_public = read_fulu6_public.readlines()
						f.write("".join(str(i) for i in content_fulu6_public))
						f.write('\n')
						read_fulu7_page1 = open(config_path+'fulu7_drug_page1.html')
						content_fulu7_page1 = read_fulu7_page1.readlines()
						f.write("".join(str(i) for i in content_fulu7_page1))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page2 = open(config_path+'fulu7_drug_page2.html')
						content_fulu7_page2 = read_fulu7_page2.readlines()
						f.write("".join(str(i) for i in content_fulu7_page2))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page3 = open(config_path+'fulu7_drug_page3.html')
						content_fulu7_page3 = read_fulu7_page3.readlines()
						f.write("".join(str(i) for i in content_fulu7_page3))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page4 = open(config_path+'fulu7_drug_page4.html')
						content_fulu7_page4 = read_fulu7_page4.readlines()
						f.write("".join(str(i) for i in content_fulu7_page4))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页



							#####<<<<<<<<<< TMB high + 全部突变无意义 1 3 4 5 6 7 >>>>>>>>#####
					if TMB_value == 0 and len(useful_list) == 0:	
						#########<---------------  添加附录1'TMB检测意义'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu1_TMB = open(config_path+'fulu1_TMB_yiyi.html')
						content_fulu1_TMB = read_fulu1_TMB.readlines()
						f.write("".join(str(i) for i in content_fulu1_TMB))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#########<---------------  添加附录2'基因检测列表'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu2_gene1 = open(config_path+'fulu2-1_detect_genelist_1.html')
						content_fulu2_gene1 = read_fulu2_gene1.readlines()
						f.write("".join(str(i) for i in content_fulu2_gene1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu2_gene2 = open(config_path+'fulu4-2_detect_genelist_2.html')
						content_fulu2_gene2 = read_fulu2_gene2.readlines()
						f.write("".join(str(i) for i in content_fulu2_gene2))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#########<---------------  添加附录3'药物证据等级'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')

						read_fulu3_grade = open(config_path+'fulu3_drug_grade.html')
						content_fulu3_grade = read_fulu3_grade.readlines()
						f.write("".join(str(i) for i in content_fulu3_grade))

						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页


						#########<---------------  添加附录4'慈善药物'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu4_help1 = open(config_path+'fulu4_help_drug_info_first.html')
						content_fulu4_help1 = read_fulu4_help1.readlines()
						f.write("".join(str(i) for i in content_fulu4_help1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu4_help1 = open(config_path+'help_drug_info_second.html')
						content_fulu4_help1 = read_fulu4_help1.readlines()
						f.write("".join(str(i) for i in content_fulu4_help1))
						f.write('\n')
						#添加页脚
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu4_help1 = open(config_path+'help_drug_info_third.html')
						content_fulu4_help1 = read_fulu4_help1.readlines()
						f.write("".join(str(i) for i in content_fulu4_help1))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
					

						#########<---------------  添加附录5'已上市靶向药物汇总'  ----------------->#########
						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						##添加title
						read_fulu5_public = open(config_path+'fulu5_public_drug_title.html')
						content_fulu5_public = read_fulu5_public.readlines()
						f.write("".join(str(i) for i in content_fulu5_public))
						f.write('\n')

						read_fulu7_page1 = open(config_path+'fulu7_drug_page1.html')
						content_fulu7_page1 = read_fulu7_page1.readlines()
						f.write("".join(str(i) for i in content_fulu7_page1))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page2 = open(config_path+'fulu7_drug_page2.html')
						content_fulu7_page2 = read_fulu7_page2.readlines()
						f.write("".join(str(i) for i in content_fulu7_page2))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page3 = open(config_path+'fulu7_drug_page3.html')
						content_fulu7_page3 = read_fulu7_page3.readlines()
						f.write("".join(str(i) for i in content_fulu7_page3))
						f.write('\n')

						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

						#添加页眉
						with open(config_path+'config_header.html','r') as HEADER:
							for con in HEADER:
								if "title_name" in con:
									#con = con.replace("replace",patient_name)
									continue
								if "title_sample_number" in con:
									#con = con.replace("replace",sample)
									continue
								f.write(con)
						f.write('\n')
						read_fulu7_page4 = open(config_path+'fulu7_drug_page4.html')
						content_fulu7_page4 = read_fulu7_page4.readlines()
						f.write("".join(str(i) for i in content_fulu7_page4))
						f.write('\n')
						f.write("".join(str(i) for i in content_footer))
						f.write("\n\t\t"+change_page+'\n\n\n')				#换页

					######最后添加文献######
					with open(config_path+'config_header.html','r') as HEADER:
						for con in HEADER:
							if "title_name" in con:
								#con = con.replace("replace",patient_name)
								continue
							if "title_sample_number" in con:
								#con = con.replace("replace",sample)
								continue
							f.write(con)
					f.write('\n')
					read_wenxian = open(config_path+'wenxian.html')
					content_wenxian = read_wenxian.readlines()
					f.write("".join(str(i) for i in content_wenxian))
					f.write('\n')
					f.write("".join(str(i) for i in content_footer))
					read_end = open(config_path+'END.html')
					content_end = read_end.readlines()
					f.write("".join(str(i) for i in content_end))
					f.close()

					#获取需要替换的所有基因名称
					report_gene_list = []
					with open(report_gene_file,'r') as GE:
						for k in GE:
							k = k.strip()
							if k not in report_gene_list:
								report_gene_list.append(k)
							else:
								pass

					#先替换页码
					count = 0
					tmp2_report = open(report_path+'/'+report_path+'.tmp2.html','w')
					with open(report_path+'/'+report_path+'.tmp.html','r') as HTML:
						for each in HTML:
							if "footer_end" in each:
								count += 1
								each = each.replace("replace",str(count))
							tmp2_report.write(each)
					tmp2_report.close()
					#再将所有的基因改成斜体
					final_report = open(report_path+'/'+report_path+'.html','w')
					with open(report_path+'/'+report_path+'.tmp2.html','r') as TMP2:
						for each in TMP2:
							if "charset" in each:
								final_report.write(each)
								continue
							if 'ERBB2' in each:
								each = each.replace('ERBB2','HER2')
							if 'pass_gene' in each:
								final_report.write(each)
								continue
							for GENE in report_gene_list:
								new_gene = "<i>"+GENE+" </i>"
									#print(new_gene)
								each = each.replace(GENE,new_gene)

							final_report.write(each)
					final_report.close()
					os.system("rm %s/%s.tmp.html %s/%s.tmp2.html " % (report_path,report_path,report_path,report_path))
					os.system("/amoydx/USER/wujianming/software/miniconda-python3/bin/wkhtmltopdf --encoding utf8 --quiet %s/%s.html %s/%s.pdf" %(report_path,report_path,report_path,report_path))







if __name__ == '__main__':
	parser = argparse.ArgumentParser()     #建立解析器
	# parser.add_argument("--somatic", "-S", help="somatic file", required=False)
	# parser.add_argument("--fusion", "-F", help="fusion file", required=False)
	parser.add_argument("--samples_file", "-sampleInfo", help="samples info file", required=False)
	parser.add_argument("--negative", "-N", help="give the negative file", required=False)
	parser.add_argument("--control", "-C", help="give the samples data control file", required=False)
	args = parser.parse_args()       #从外部传递参数到此
	get_neg_report(args.samples_file,args.control,args.negative)
	get_pos_report(args.samples_file,args.control,args.negative)
