#!/bin/env python
#coding=utf-8
'''
该脚本的目的：1、读取somatic、fusion表格，获取信息
		    2、根据somatic信息，检索知识库、clinical库，将获取的信息存入know_annot文件里
		    3、根据fusion信息，检索知识库、clinical库，将获取的文件存入clinical_trials文件里
'''
from __future__ import division
import pandas as pd
import argparse
import xlrd
import re
import os
import sys

phase_order = ["Phase 4","Phase 3","Phase 2 Phase 3","Phase 2","Phase 1","Phase 1 Phase 2","Early Phase 1"]
#knowledge_databse = "/Users/jianming.wu/work/config/10gene_auto_report/raw_database/TumorMutationBase_V3.0.0.xls"
drug_database = "/amoydx/appclub/TMB_auto_report_version1.0_20181219/database/ClinicalTrails2.txt"
#drug_database = "/Users/jianming.wu/work/program/auto_report/TMB_auto_report/database/ClinicalTrails2.txt"
knowledge_databse = "/amoydx/appclub/TMB_auto_report_version1.0_20181219/database/TumorMutationBase_V4.0.6.xlsx"
#knowledge_databse = "/amoydx/appclub/TMB_auto_report_version1.0_20181219/database/TumorMutationBase_V4.0.5.xlsx"
#knowledge_databse = "/Users/jianming.wu/work/program/auto_report/TMB_auto_report/database/TumorMutationBase_V4.0.3.xlsx"
sample_type_dict = {"肺癌":"Lung","小细胞癌":"Lung","小细胞肺癌":"Lung","肺小细胞癌":"Lung","非小细胞肺癌":"Lung","腺鳞癌":"Lung","腺癌":"Lung","肺腺癌":"Lung","肺鳞癌":"Lung","结直肠癌":"Colonrectal","十二指肠肿物":"Colonrectal","乙状结肠管状腺瘤":"Colonrectal","乳腺癌":"Breast","胆总管细胞癌":"Cholangiocarcinoma","胆管癌":"Cholangiocarcinoma","胆囊癌":"Cholangiocarcinoma","食管癌":"Esophageal","胃癌":"Gastric","巨细胞瘤":"Giant Cell","胃肠道间质瘤":"GIST","脑胶质瘤":"Glioma","肝癌":"Hepatocellular","纵隔恶性肿瘤":"Mediastinal","黑色素瘤":"Melanoma","脉络膜恶性黑色素瘤":"Melanoma","卵巢癌":"Ovarian","甲状腺癌":"Thyroid","甲状腺结节":"Thyroid","宫颈癌":"Cervical","淋巴瘤":"Lymphoma","腹膜癌":"Peritoneal","胰腺癌":"Pancreatic","子宫内膜癌":"Endometrial","睾丸癌":"Testicular","前列腺癌":"Prostate","肾癌":"Kidney","滑膜肉瘤":"SynovialSarcoma","贲门癌":"CardiacCarcinoma","脂肪肉瘤":"Liposarcoma","平滑肌肉瘤":"Leiomyosarcoma","胸膜恶性肿瘤":"Pleural Malignancy","舌高分化鳞癌":"Tongue","骨肉瘤":"Osteosarcoma","颅内肿瘤":"Intracranial","纤维肉瘤":"Fibrosarcoma","肾外横纹肌瘤":"Extrarenal Rhabdoid","低分化癌":"Poorly Differentiated Carcinoma","皮脂腺癌":"Sebaceous Carcinoma","其他":"Lung"}

def get_positive_id(somatic_file,fusion_file,sample_info_file):
	#循环somatic和fusion文件，获取阳性样本id信息
	somatic_sample_list = []
	for file in somatic_file,fusion_file:
		with open(file,'r') as SOMA:
			for line in SOMA:
				line = line.strip().split('\t')
				if "Sample" in line:
					continue
				if "F" in line[0][:2]:
					if line[0][0] == "S":
						sample_id = line[0][:8]
					else:
						sample_id = line[0][:7]
				if "F" not in line[0][:2]:
					if line[0][0] == "S":
						sample_id = line[0][:9]
					else:
						sample_id = line[0][:8]
				if sample_id not in somatic_sample_list:
					somatic_sample_list.append(str(sample_id))
	return(somatic_sample_list)



def get_FDA_drug_info(somatic_file,fusion_file,sample_info_file,control_file):
	somatic_sample_list = get_positive_id(somatic_file,fusion_file,sample_info_file)
	suppressor_gene = ['TP53']	#抑癌基因列表
	#然后循环list，再循环somatic总表，再从知识库里提取用药信息
	for sample in somatic_sample_list:
		sample_somatic_sheet = open(sample+'_somatic.xls','w')
		sample_know_sheet = open(sample+'_know_annot.xls','w')
		sample_fusion_annot_sheet = open(sample+'_fusion_annot.xls','w')
		sample_fusion_sheet = open(sample+'_fusion.xls','w')

		with open(sample_info_file,'r') as INFO:
			for line in INFO:
				line = line.strip().split('\t')
				if line[0] in sample:
					sample_type_key = line[5].title()
					if sample_type_key in sample_type_dict:
						sample_type_value = sample_type_dict[sample_type_key]
					else:
						##如果没有对应的癌种，直接报错退出
						sample_type_value = "Lung"
						#print("%s has no %s cancer name !! Exit !!!" % (str(sample),sample_type_key))
						#sys.exit()
					# if '肺' in sample_type_key or '小细胞' in sample_type_key:
					# 	sample_type_value = sample_type_list['肺']
					# elif '肠' in sample_type_key:
					# 	sample_type_value = sample_type_list['肠']
					# else:
					# 	sample_type_value = ""

					with open(somatic_file,'r') as TMP:
						for lines in TMP:
							lines = lines.strip().split('\t')
							if sample in lines[0]:		#获取了每个样本所有的信息
								sample_somatic_sheet.write("\t".join(str(i) for i in lines)+'\n')
								chr_name = lines[3]
								start = lines[4]
								end = lines[5]
								ref = lines[6]
								alt = lines[7]
								gene = lines[12]
								aa_mut = lines[14].split(':')[2]
								pro_mut = lines[14].split(':')[3]
								#print(aa_mut,pro_mut)

								#打开知识库，从确定用药信息
								#里面的title为:chr  pos_start		pos_end		ref	alt	ID	Gene	Type	Exon	CDSChange	AAChange	肿瘤类型	NCCN/FDA/CFDA提示敏感	临床实验研究提示敏感	提示耐药	基因描述	突变描述	相关药物描述	小结	文献来源
								know_book = xlrd.open_workbook(knowledge_databse)
								know_sheet_name = know_book.sheet_names()
								SNP_sheet = know_book.sheet_by_name("ClinicalSNVBase")			#先获取SNP突变信息
								
								#首先判断gene是否是抑癌基因（目前ALL只出现在抑癌基因里）--20190114
									#是：再判断癌种是否是胃癌（Gastric）或者卵巢癌（Ovarian）
										##是：不需要判断ALL（all代表用药信息适合所有癌种）
										##否：需要判断ALL
									#否：正常癌种匹配
								if gene in  suppressor_gene:
									if sample_type_value == 'Gastric' or sample_type_value == 'Ovarian':
										for each in range(SNP_sheet.nrows):
											rows =  SNP_sheet.row_values(each)
											if chr_name == rows[0] and float(start) == float(rows[1]) and float(end) == float(rows[2]) and ref == rows[3] and alt == rows[4] and gene == rows[6]  and pro_mut == rows[10] and sample_type_value in rows[11].title():
												sample_know_sheet.write("\t".join(str(i) for i in rows)+'\n')
									else:
										for each in range(SNP_sheet.nrows):
											rows =  SNP_sheet.row_values(each)
											if chr_name == rows[0] and float(start) == float(rows[1]) and float(end) == float(rows[2]) and ref == rows[3] and alt == rows[4] and gene == rows[6]  and pro_mut == rows[10] and ( sample_type_value in rows[11].title() or rows[11]=='all'):
												sample_know_sheet.write("\t".join(str(i) for i in rows)+'\n')
								else:
									for each in range(SNP_sheet.nrows):
										rows =  SNP_sheet.row_values(each)
										if chr_name == rows[0] and float(start) == float(rows[1]) and float(end) == float(rows[2]) and ref == rows[3] and alt == rows[4] and gene == rows[6]  and pro_mut == rows[10] and sample_type_value in rows[11].title():
											sample_know_sheet.write("\t".join(str(i) for i in rows)+'\n')
										
					with open(fusion_file,'r') as FU:
						for content in FU:
							content = content.strip().split('\t')
							if sample in content[0]:
								sample_fusion_sheet.write("\t".join(str(i) for i in content)+'\n')
								gene1 = content[5].split(":")[0]
								gene2 = content[9].split(":")[0]
								pattern = re.compile(r'\d+')	#匹配所有的数字
								exon_info_1 = content[12].split('-')[0].split('_')[-1]
								exon_info_2 = content[12].split('-')[1].split('_')[-1]
								if 'exon' in exon_info_1:
									site1 = pattern.findall(exon_info_1)[0]  #EML4:NM_019063.4_exon20-ALK:NM_004304.4_exon20, '-'分隔，第一元素，匹配数字，取最后一个即为site1
								else:
									site1 = exon_info_1.upper()
								if 'exon' in exon_info_2:
									site2 = pattern.findall(exon_info_2)[0]
								else:
									site2 = exon_info_2.upper()
								#print(gene1,gene2)
								#打开知识库，确定用药信息
								#title为融合基因	肿瘤类型	临床意义	NCCN/FDA/CFDA提示敏感	NCCN/FDA/CFDA提示耐药	实验研究提示敏感	实验研究提示耐药	基因描述	突变描述	药物总结	注释内容	文献来源	备注
								know_book = xlrd.open_workbook(knowledge_databse)
								know_sheet_name = know_book.sheet_names()
								fusion_sheet = know_book.sheet_by_name("Fusion")			#获取fusion信息
								for each in range(fusion_sheet.nrows):
									rows =  fusion_sheet.row_values(each)
									#if gene1 in rows[0] and gene2 in rows[0]:
									if '.' in str(rows[3]):
											database_site1 = int(rows[3])
									else:
										database_site1 = rows[3]
									if '.' in str(rows[6]):
										database_site2 = int(rows[6])
									else:
										database_site2 = rows[6]
									if gene1 == rows[1] and gene2 == rows[4] and str(site1) == str(database_site1) and str(site2) == str(database_site2):
										sample_fusion_annot_sheet.write("\t".join(str(i) for i in rows)+'\n')
										break
				else:
					continue
		sample_somatic_sheet.close()
		sample_know_sheet.close()
		sample_fusion_sheet.close()
		sample_fusion_annot_sheet.close()

	TMB_num_dict = {}
	for sample in somatic_sample_list:
		###再统计各样本的TMB数值###
		num = 0
		if os.path.getsize(sample+'_somatic.xls') != 0:
			so_data = pd.read_table(sample+'_somatic.xls',sep='\t',header=None)
			num = so_data.shape[0] +num
#		if os.path.getsize(sample+'_fusion.xls') != 0:
#			fu_data = pd.read_table(sample+'_fusion.xls',sep='\t',header=None)
#			num = fu_data.shape[0] +num
		TMB_num = num / 1.46
		TMB_num_dict[sample] = '%.2f' % TMB_num



	new_control_name = ".".join(str(i) for i in control_file.split('.')[:-1]) + '_TMB.xls'
	f = open(new_control_name,'w')
	with open(control_file,'r') as CONT:
		for line in CONT:
			line = line.strip().split('\t')
			if "Sample" in  line:
				f.write("\t".join(i for i in line)+'\t'+'TMB'+'\n')
				continue
			if str(line[0]) not in TMB_num_dict:
				TMB_num_dict[line[0]] = 0
			f.write("\t".join(str(i) for i in line)+'\t'+str(TMB_num_dict[line[0]])+'\n')
	f.close()


	# dat = pd.read_table(control_file,sep='\t',index_col = 0)
	# dat['TMB'] = 0
	# for sam in somatic_sample_list:
	# 	print(sam+','+str(TMB_num_dict[sam]))
	# 	dat.loc[sam,'TMB'] = TMB_num_dict[sam]
	# new_name = ".".join(str(i) for i in control_file.split('.')[:-1]) + '_TMB.xls'
	# dat.to_csv(new_name,sep='\t')



def get_clinical_drug_info(somatic_file,fusion_file,sample_info_file):
	somatic_sample_list = get_positive_id(somatic_file,fusion_file,sample_info_file)
	#print(somatic_sample_list)
	#然后循环list，再循环somatic总表，再从clinical库里提取临床试验信息
	#clinical库title:第一列是基因名，第二列是NCI编号，第三列是临床试验名称，第四列是试验招募状态，第五列主要是涉及的疾病，第六列是药物和治疗方法，最后一列是该试验处在临床哪一期(使用中)
	#以基因作为key和癌种，癌种目前只选肺癌和结直肠癌（其他肠癌不算）。(使用中)
	#目前版本只选择正在招募的（Recruiting First Posted，招募受试者）、必须要有治疗药物、临床分期按4 3 2 1 早1期排序，其余阶段不要；如果有改，在下一个版本进行更改。(使用中)

	###最后输出的格式是，基因，癌种，敏感/不敏感，药物名称，阶段，证据类型，效果描述，ID（取消）

	for sample in somatic_sample_list:
		sample_clinical_sheet = open(sample+'_clinical.xls','w')
		with open(sample_info_file,'r') as INFO:
			for line in INFO:
				line = line.strip().split('\t')
				if line[0] in sample:
					sample_type_key = line[5]
					# if "肺" in sample_type_key or "小细胞" in sample_type_key:
					# 	cancer_name = sample_type_list['肺']
					# elif '肠' in sample_type_key:
					# 	cancer_name = sample_type_list['肠']
					# else:
					# 	cancer_name = ""
					#print(cancer_name)
					if sample_type_key in sample_type_dict:
						cancer_name = sample_type_dict[sample_type_key]
					else:
						##如果没有对应的癌种，直接报错退出
						cancer_name = "Lung"
						#print("%s has no %s cancer name !! Exit !!!" % (str(sample),sample_type_key))
						#sys.exit()

					GENE_LIST = []
					if os.path.getsize(sample+'_know_annot.xls') != 0:
						with open(sample+'_know_annot.xls','r') as TMP:
							for line in TMP:
								line = line.strip().split('\t')
								gene = line[6]
								#pro_mut = line[14].split(':')[3]
								#key = gene+'\t'+cancer_name				
								#循环clinical数据库里的内容，先过滤不必须要的信息，再进行排序
								if gene not in GENE_LIST:
									GENE_LIST.append(gene)

					FUSION_GENE_LIST = []
					if os.path.getsize(sample+'_fusion_annot.xls') != 0:
						with open(sample+'_fusion_annot.xls','r') as FU:
							for line in FU:
								line = line.strip().split('\t')
								gene1 = line[0].split("-")[0]
								gene2 = line[0].split("-")[1]
								if gene2 not in FUSION_GENE_LIST:
									FUSION_GENE_LIST.append(gene2)

				
					for gene in GENE_LIST:
						clinical_num = 0
						for order in phase_order:	
							with open(drug_database,'r') as CLINI:
								for line in CLINI:
									line = line.strip().split('\t')
									if len(line) <= 6:
										continue
									if 'Active' in line[3] or 'Terminated' in line[3] or 'invitation' in line[3] or line[5] == "" or  "Not" in line[6]:
										continue			#信息过滤
									n = 0 
									for i in line[5]:
										if i == ":":
											n += 1
									if n > 10 :
										continue
									#if gene in line[0] and cancer_name in line[4]:
									if	bool(re.search(gene,line[0],re.IGNORECASE)) == True and bool(re.search(cancer_name,line[4],re.IGNORECASE)) == True:
									 #首先过滤基因和癌种							
										if line[6] == order:
											if clinical_num < 3:
												sample_clinical_sheet.write("\t".join(str(i) for i in line)+'\n')
												clinical_num += 1
								# with open(drug_database,'r') as CLINI:
								# 	for con in CLINI:
								# 		con = con.strip().split('\t')
								# 		#if cancer_name in con[1] and ((gene in con[0] and pro_mut in con[0]) or gene in con[0]):
								# 		if pro_mut.split('.')[1] in con[0]:
								# 			for order in phase_order:
								# 				if order == con[4]:
								# 					if clinical_num < 3:
								# 						sample_clinical_sheet.write("\t".join(str(i) for i in con)+'\n')
								# 						clinical_num += 1

					#####<------融合只判断gene1-gene2里面的后一个基因以及癌种------>#####
					
					for FU_gene in FUSION_GENE_LIST:
						clinical_num = 0
						for order in phase_order:
							with open(drug_database,'r') as CLINI:
								for con in CLINI:
									con = con.strip().split('\t')
									if len(con) <= 6:
										continue
									if 'Active' in con[3] or 'Terminated' in con[3] or 'invitation' in con[3] or con[5] == "" or  "Not" in con[6]:
										continue			#信息过滤
									n = 0 
									for i in con[5]:
										if i == ":":
											n += 1
									if n > 10 :
										continue
									#if gene in con[0] and cancer_name in con[4]:
									if	bool(re.search(FU_gene,con[0],re.IGNORECASE)) == True and bool(re.search(cancer_name,con[4],re.IGNORECASE)) == True:
										if order == con[6]:
											if clinical_num < 3:
												sample_clinical_sheet.write("\t".join(str(i) for i in con)+'\n')
												clinical_num += 1


				else:
					continue
		sample_clinical_sheet.close()



if __name__ == '__main__':
	parser = argparse.ArgumentParser()     #建立解析器
	parser.add_argument("--somatic", "-S", help="somatic file", required=False)
	parser.add_argument("--fusion", "-F", help="fusion file", required=False)
	parser.add_argument("--sample_info", "-sampleInfo", help="sample info file", required=False)
	parser.add_argument("--control", "-C", help='control file')
	args = parser.parse_args()       #从外部传递参数到此
	#get_positive_id(args.somatic,args.fusion)
	get_FDA_drug_info(args.somatic,args.fusion,args.sample_info,args.control)
	get_clinical_drug_info(args.somatic,args.fusion,args.sample_info)

