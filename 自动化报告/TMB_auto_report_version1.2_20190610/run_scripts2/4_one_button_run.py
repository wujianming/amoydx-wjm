#!/bin/env python
#coding=utf-8
#version 1.0.0
#author : wujianming
#email: wujm@amoydx.com
#last edit date:2018-10-24

#该脚本的目的是为了把10基因自动化报告的前3个代码串起来，以便一键化运行。
#出报告日期没有设置参数，默认为当天
from __future__ import division
import argparse
import os

#mac
#First_script = "/Users/jianming.wu/work/program/auto_report/TMB_auto_report/run_scripts/1_get_mutation_from_xlsx.py"
#Second_script = "/Users/jianming.wu/work/program/auto_report/TMB_auto_report/run_scripts/2_get_database_info.py"
#Third_script = "/Users/jianming.wu/work/program/auto_report/TMB_auto_report/run_scripts/3_get_TMB_report.py"
#python = "/Users/jianming.wu/software/miniconda-python3/bin/python"
#amoydx
First_script = "/amoydx/appclub/TMB_auto_report_version1.2_20190610/run_scripts2/1_TMB_get_mutation_from_xlsx.py"
Second_script = "/amoydx/appclub/TMB_auto_report_version1.2_20190610/run_scripts2/2_TMB_get_database_info.py"
Third_script = "/amoydx/appclub/TMB_auto_report_version1.2_20190610/run_scripts2/3_get_TMB_report.py"
python = "/amoydx/USER/wujianming/software/miniconda-python3/bin/python"

def step_1(quality_file,guest_info_file,samples_file,PDL1_file,outdir):	
	'''
	-Q  质量文件，如quality_after_2016.09.12.xlsx
	-I  接收表，如2018_info_08.17.xlsx
	-S  样本结果信息表，如20180325_1696_ADX10Gene_v0.2.0.xlsx
	-O  输出的目录
	该步骤和10基因的一致
	'''

	os.system("%s %s -Q %s -I %s -S %s -P %s -O %s" % (python,First_script,quality_file,guest_info_file,samples_file,PDL1_file,outdir))

	'''
	该脚本输出以表格为单位的:*xlsx_somatic.xls  *xlsx_fusion.xls *xlsx_sample_info.xls *xlsx_negative.xls *xlsx_control.xls
	'''

def step_2(samples_file,outdir):
	'''
	-S 	somatic文件，如20180325_1696_ADX10Gene_v0.2.0.xlsx_somatic.xls（不是单个样本的somatic结果）
	-F  fusion文件，如20180325_1696_ADX10Gene_v0.2.0.xlsx_fusion.xls（不是单个样本的fusion结果）
	-sampleInfo  程序1运行处的样本信息表，如20180325_1696_ADX10Gene_v0.2.0.xlsx_sample_info.xls
	'''
	os.chdir(outdir)
	os.system("%s %s -S %s_somatic.xls -F %s_fusion.xls -sampleInfo %s_sample_info.xls -C %s_control.xls  -H %s_chemotherapy.xls -CNV %s_cnv.xls -N %s_negative.xls" % (python,Second_script,samples_file,samples_file,samples_file,samples_file,samples_file,samples_file,samples_file))

	'''
	输出：以样本单位的sample_somatic.xls, sample_fusion.xls, sample_know_annot.xls, sample_fusion_annot.xls
	'''

def step_3(samples_file):
	'''
	-S 	somatic文件，如20180325_1696_ADX10Gene_v0.2.0.xlsx_somatic.xls（不是单个样本的somatic结果）
	-F  fusion文件，如20180325_1696_ADX10Gene_v0.2.0.xlsx_fusion.xls（不是单个样本的fusion结果）
	-sampleInfo  程序1运行处的样本信息表，如20180325_1696_ADX10Gene_v0.2.0.xlsx_sample_info.xls
	-N negative文件,里面都是阴性样本
	'''
	os.system("%s %s  -sampleInfo %s_sample_info.xls  -C %s_control_TMB.xls -N %s_negative.xls" % (python,Third_script,samples_file,samples_file,samples_file))



if __name__ == '__main__':
	parser = argparse.ArgumentParser()     #建立解析器
	parser.add_argument("--quality_file", "-Q", help="samples quality file", required=False)
	parser.add_argument("--guest_info_file", "-I", help="guest info file", required=False)
	parser.add_argument("--samples_file", "-S", help="sampels info file, include somatic、fusion、DataProduction information", required=False)
	parser.add_argument("--PDL1_file","-P", help="the file of PD-L1",required=False)
	parser.add_argument("--out_dir", "-O", help="the dir of output", required=False)
	parser.add_argument("--chemotherapy","-H" ,help='chemotherapy file')
	args = parser.parse_args()       #从外部传递参数到此
	step_1(args.quality_file,args.guest_info_file,args.samples_file,args.PDL1_file,args.out_dir)
	step_2(args.samples_file,args.out_dir)
	step_3(args.samples_file)


