#!/usr/bin/env nextflow
params.tumorName = '30EP'
params.normalName = '30W'
params.ePfq1 = '/amoydx/USER/wujianming/project/NextFlow/WES/TEST/30EP_FDHE190633485-1a_1.fq.gz'
params.ePfq2 = '/amoydx/USER/wujianming/project/NextFlow/WES/TEST/30EP_FDHE190633485-1a_2.fq.gz'
params.wfq1 = '/amoydx/USER/wujianming/project/NextFlow/WES/TEST/30W_FDHE190633488-1a_1.fq.gz'
params.wfq2 = '/amoydx/USER/wujianming/project/NextFlow/WES/TEST/30W_FDHE190633488-1a_2.fq.gz'

// params.ePfq1 = '/amoydx/USER/wujianming/project/NextFlow/WES/TEST/30EP_R1.fastq.gz'
// params.ePfq2 = '/amoydx/USER/wujianming/project/NextFlow/WES/TEST/30EP_R2.fastq.gz'
// params.wfq1 = '/amoydx/USER/wujianming/project/NextFlow/WES/TEST/30W_R1.fastq.gz'
// params.wfq2 = '/amoydx/USER/wujianming/project/NextFlow/WES/TEST/30W_R2.fastq.gz'


params.python = '/amoydx/USER/wujianming/software/miniconda-python3/bin/python'
params.python2 = '/amoydx/USER/wujianming/software/miniconda-python3/envs/py2/bin/python2'
params.multiqc = '/amoydx/USER/wujianming/software/miniconda-python3/bin/multiqc'
params.bcftools = '/amoydx/USER/wujianming/software/miniconda-python3/bin/bcftools'
params.fastqc = '/amoydx/USER/wujianming/software/miniconda-python3/bin/fastqc'
params.bwa = '/amoydx/USER/wujianming/software/miniconda-python3/bin/bwa'
params.samtools = '/amoydx/USER/wujianming/software/miniconda-python3/bin/samtools'
params.perl = '/amoydx/USER/wujianming/software/miniconda-python3/bin/perl'
params.somaticSniper = '/amoydx/USER/wujianming/software/miniconda-python3/bin/bam-somaticsniper'
params.muSE = '/amoydx/USER/wujianming/software/miniconda-python3/bin/MuSE'
params.pindel = '/amoydx/USER/wujianming/software/miniconda-python3/bin/pindel'
params.pindel2vcf = '/amoydx/USER/wujianming/software/miniconda-python3/bin/pindel2vcf'
params.bgzip = '/amoydx/USER/wujianming/software/miniconda-python3/bin/bgzip'
params.vcf_sort = '/amoydx/USER/wujianming/software/miniconda-python3/bin/vcf-sort'

params.gatk = '/amoydx/USER/wangjianqing/software/gatk-4.0.11.0/gatk-package-4.0.11.0-local.jar'
params.gatk3 = '/amoydx/USER/wangjianqing/software/GenomeAnalysisTK-3.8-1-0-gf15c1c3ef/GenomeAnalysisTK.jar'
params.picard = '/amoydx/app/picard-tools-2.0.1/picard.jar'
params.java = '/amoydx/USER/wangjianqing/software/miniconda3/bin/java'
//params.known_sites = '/amoydx/USER/wangjianqing/database/reference/DNA/dbsnp_138.hg19.vcf'
params.known_sites = '/amoydx/USER/wujianming/database/VEP/.vep/ExAC_nonTCGA.r0.3.1.sites.vep.vcf.gz'
params.splitBedDir = '/amoydx/USER/wujianming/config/WES_nextflow_config/bed'
//params.ref = '/amoydx/USER/wangjianqing/database/reference/DNA/ucsc.hg19.fasta'
params.ref = '/amoydx/USER/wujianming/database/VEP/.vep/homo_sapiens/97_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa'
params.wes_bed = '/amoydx/USER/wujianming/project/MC3/RUN/S07604514_Covered.bed'
params.muSE_bed = '/amoydx/USER/wujianming/project/MC3/RUN/MuSE_beds/MuSE.bed'
params.thread = 5



beds = Channel.fromPath("${params.splitBedDir}/*.bed")
muse_beds = Channel.fromPath("/amoydx/USER/wujianming/project/MC3/RUN/MuSE_beds/*.txt")
pindel_beds = Channel.fromPath("/amoydx/USER/wujianming/project/MC3/RUN/Pindel/*.bed")
pindel_beds_twice = Channel.fromPath("/amoydx/USER/wujianming/project/MC3/RUN/Pindel/*.bed")
chrom_nums = Channel.from(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,"X","Y","MT")

Channel
  .fromFilePairs('./*_R{1,2}_001.fastq.gz')
  .set { fastq_G }

// process fastqc{
//   clusterOptions '-l p=3'
//   memory "20 GB"
//   queue "default.q"
//   executor "sge"

//   beforeScript "rm -rf ${params.tumorName}/qc/fastQC && mkdir -p ${params.tumorName}/qc/fastQC && mkdir -p ${params.tumorName}/mapped "
//   publishDir "./", mode:'copy'
  
//   input:
//   set name, fqs from fastq_G

//   output:
//   file "${params.tumorName}/qc/fastQC/*_fastqc.html"
//   file "${params.tumorName}/qc/fastQC/*_fastqc.zip"
//   //set "${params.tumorName}/qc/fastqc/${name}_R1_fastqc.html", "${params.tumorName}/qc/fastqc/${name}_R2_fastqc.html", "${params.tumorName}/qc/fastqc/${name}_R1_fastqc.zip", "${params.tumorName}/qc/fastqc/${name}_R2_fastqc.zip" into fastqc_group

//   shell:
//   """
//   ${params.fastqc} -t ${params.thread} -o ${params.tumorName}/qc/fastQC ${fqs[0]} ${fqs[1]}

//   """
// }

process bwa_tumor{
  clusterOptions '-l p=5'
  memory '70 GB'
  queue "default.q"
  executor 'sge'

  // beforeScript "rm -rf ${params.tumorName}/mapped && mkdir -p ${params.tumorName}/mapped"
  publishDir "${params.tumorName}/mapped/", mode:'copy'
  
  output:
  file "${params.tumorName}.bam" into tumor_bam
  file "${params.tumorName}.bam.bai" into tumor_bam_bai

  shell:
  """
  ${params.bwa} mem -Y -M -R '@RG\\tID:'${params.tumorName}'\\tLB:'${params.tumorName}'\\tSM:'${params.tumorName}'\\tPL:illumina\\tPU:illumina' -t ${params.thread} ${params.ref} ${params.ePfq1} ${params.ePfq2} | ${params.samtools} view -bS -@ ${params.thread} - > ${params.tumorName}.raw.bam
  ${params.samtools} sort -@ ${params.thread} -o ${params.tumorName}.bam ${params.tumorName}.raw.bam
  ${params.samtools} index ${params.tumorName}.bam
  """
}

process bwa_normal{
  clusterOptions '-l p=5'
  memory '70 GB'
  queue "default.q"
  executor 'sge'

  // beforeScript "rm -rf ${params.tumorName}/mapped && mkdir -p ${params.tumorName}/mapped"
  publishDir "${params.tumorName}/mapped/", mode:'copy'

  output:

  file "${params.normalName}.bam"  into normal_bam
  file "${params.normalName}.bam.bai"  into normal_bam_bai

  shell:
  """
  ${params.bwa} mem -Y -M -R '@RG\\tID:'${params.normalName}'\\tLB:'${params.normalName}'\\tSM:'${params.normalName}'\\tPL:illumina\\tPU:illumina' -t ${params.thread} ${params.ref} ${params.wfq1} ${params.wfq2} | ${params.samtools} view -bS -@ ${params.thread} - > ${params.normalName}.normal.bam
  ${params.samtools} sort -@ ${params.thread} -o ${params.normalName}.bam ${params.normalName}.normal.bam
  ${params.samtools} index  ${params.normalName}.bam
  """
}

// gatk
process mark_duplicated_tumor {
  clusterOptions '-l p=5'
  memory "70 GB"
  queue "default.q"
  executor "sge"

  beforeScript "rm -rf ${params.tumorName}/qc/picards  && mkdir -p ${params.tumorName}/qc/picards"
  publishDir "${params.tumorName}/mapped/", mode: 'copy'

  input:
  file mark_t_bam from tumor_bam
  file mark_t_bam_bai from tumor_bam_bai

  output:
  file "${params.tumorName}_md.bam" into mark_t_md_bam
  file "${params.tumorName}.markduplicated.matrix" into t_mark
 

  shell:
  """
  ${params.java} -Xmx10G -jar ${params.gatk} MarkDuplicates --REMOVE_DUPLICATES=true -I=${mark_t_bam} -M=${params.tumorName}.markduplicated.matrix -O=${params.tumorName}_md.bam
  cp *.markduplicated.matrix ${params.tumorName}/qc/picards/

  """
}

process mark_duplicated_normal {
  clusterOptions '-l p=5'
  memory "70 GB"
  queue "default.q"
  executor "sge"

  beforeScript "rm -rf ${params.tumorName}/qc/picards  && mkdir -p ${params.tumorName}/qc/picards"
  publishDir "${params.tumorName}/mapped/", mode: 'copy'

  input:
  file mark_n_bam from normal_bam
  file mark_n_bam_bai from normal_bam_bai

  output:

  file "${params.normalName}_md.bam" into mark_n_md_bam
  file "${params.normalName}.markduplicated.matrix" into n_mark

  shell:
  """
  ${params.java} -Xmx10G -jar ${params.gatk} MarkDuplicates --REMOVE_DUPLICATES=true -I=${mark_n_bam} -M=${params.normalName}.markduplicated.matrix -O=${params.normalName}_md.bam
  cp *.markduplicated.matrix ${params.tumorName}/qc/picards/

  """
}

process baseRecalibrator_tumor{
  clusterOptions '-l p=5'
  memory "70 GB"
  queue "default.q"
  executor "sge"

  publishDir "${params.tumorName}/mapped/", mode: 'copy'

  input:
  file t_md_bam from mark_t_md_bam
  
  output:
  file "${params.tumorName}_recal.bam" into t_recal_bam
  file "${params.tumorName}_recal.bam.bai" into t_recal_bam_bai
  file "${params.tumorName}.bqsr"

  shell:
  """
  ${params.java} -Xmx10G -jar ${params.gatk} BaseRecalibrator -I ${t_md_bam} -O ${params.tumorName}.bqsr -R ${params.ref} --known-sites ${params.known_sites}
  ${params.java} -Xmx10G -jar ${params.gatk} ApplyBQSR -I ${t_md_bam} -O ${params.tumorName}_recal.bam -bqsr ${params.tumorName}.bqsr -R ${params.ref}
  ${params.samtools} index ${params.tumorName}_recal.bam

  """
}

process baseRecalibrator_normal{
  clusterOptions '-l p=5'
  memory "70 GB"
  queue "default.q"
  executor "sge"

  publishDir "${params.tumorName}/mapped/", mode: 'copy'

  input:
  file n_md_bam from mark_n_md_bam
  
  output:
  file "${params.normalName}_recal.bam" into n_recal_bam
  file "${params.normalName}_recal.bam.bai" into n_recal_bam_bai
  file "${params.normalName}.bqsr"

  shell:
  """
  ${params.java} -Xmx10G -jar ${params.gatk} BaseRecalibrator -I ${n_md_bam} -O ${params.normalName}.bqsr -R ${params.ref} --known-sites ${params.known_sites}
  ${params.java} -Xmx10G -jar ${params.gatk} ApplyBQSR -I ${n_md_bam} -O ${params.normalName}_recal.bam -bqsr ${params.normalName}.bqsr -R ${params.ref}
  ${params.samtools} index  ${params.normalName}_recal.bam

  """
}


process alignment_metric_tumor {
  clusterOptions '-l p=5'
  memory "70 GB"
  queue "default.q"
  executor "sge"

  publishDir "${params.tumorName}/qc/picards/", mode: 'copy'

  input:
  file align_t_bam from t_recal_bam
  file align_t_bam_bai from t_recal_bam_bai


  output:
  file "${params.tumorName}.alignment.metrix" into t_align

  shell:
  """
  ${params.java} -Xmx10G -jar ${params.picard} CollectAlignmentSummaryMetrics I=${align_t_bam} O=${params.tumorName}.alignment.metrix R=${params.ref}
  """

}

process alignment_metric_normal {
  clusterOptions '-l p=5'
  memory "70 GB"
  queue "default.q"
  executor "sge"

  publishDir "${params.tumorName}/qc/picards/", mode: 'copy'

  input:
  file align_n_bam from n_recal_bam
  file align_n_bam_bai from n_recal_bam_bai

  output:
  file "${params.normalName}.alignment.metrix" into n_align

  shell:
  """
  ${params.java} -Xmx10G -jar ${params.picard} CollectAlignmentSummaryMetrics I=${align_n_bam} O=${params.normalName}.alignment.metrix R=${params.ref}
  """
}

/**
mutect2:
  1、mutect2
  2、mergeVCF
  3、mutect vcf filter
  output: ${params.tumorName}.filter.vcf
*/
process mutect2 {
  clusterOptions '-l p=5'
  memory '70 GB'
  queue "default.q"
  executor 'sge'

  beforeScript "rm -rf ${params.tumorName}/mutect  && mkdir -p ${params.tumorName}/mutect"
  publishDir "${params.tumorName}/mutect/", mode: 'copy'
  
  input:
  file beds
  file t_bam from t_recal_bam
  file n_bam from n_recal_bam

  output:
  file "*.vcf.temp" into vcf_temps

  shell:
  """
  ${params.java} -Xmx10G  -jar ${params.gatk} Mutect2 -R ${params.ref} -I ${t_bam} -I ${n_bam} -tumor ${params.tumorName} -normal ${params.normalName} -L ${beds} -O ${beds}.vcf.temp
  
  """
}

process mutect2_merge {
  clusterOptions '-l p=2'
  memory "20 GB"
  queue "default.q"
  executor "sge"

  publishDir "${params.tumorName}/mutect/", mode: 'copy'

  input:
  file "*" from vcf_temps.collect()

  output:
  file "${params.tumorName}.merge.temp.vcf" into mutect_merge_temp_vcf

  shell:
  """
  ${params.python} /amoydx/USER/wujianming/config/WES_nextflow_config/scripts/Merge_vcfTmp.py *.vcf.temp  ${params.tumorName}.merge.temp.vcf
  
  """

}

process mutect_filter {
  clusterOptions '-l p=7'
  memory "70 GB"
  queue "default.q"
  executor "sge"

  publishDir "${params.tumorName}/mutect/", mode: 'copy'
  
  input:
  file mutect_mergeTmpVcf from mutect_merge_temp_vcf
  output:
  file "${params.tumorName}.mutect.sort.vcf"
  file "${params.tumorName}.mutect.vcf2vcf.vcf" 
  file "${params.tumorName}.mutect.final.vcf" into mutect_filter_Vcf    //--------------> mutect Vcf <---------------

  shell:
  """
  ${params.java} -Xmx10G  -jar ${params.gatk} FilterMutectCalls -O ${params.tumorName}.mutect.filter.vcf -V ${mutect_mergeTmpVcf}
  ${params.bcftools} sort ${params.tumorName}.mutect.filter.vcf > ${params.tumorName}.mutect.sort.vcf
  ${params.perl} /amoydx/USER/wujianming/software/mskcc-vcf2maf-5453f80/vcf2vcf.pl --input-vcf ${params.tumorName}.mutect.sort.vcf --output-vcf ${params.tumorName}.mutect.vcf2vcf.vcf --new-tumor-id PRIMARY --new-normal-id NORMAL  --ref-fasta ${params.ref} --vcf-tumor-id ${params.tumorName} --vcf-normal-id ${params.normalName}
  ${params.python} /amoydx/USER/wujianming/config/MC3/bin/MC3_mutect_DP_Freq_Filter.py -i ${params.tumorName}.mutect.vcf2vcf.vcf -md 25 -mad 3 -mf 0.05 -o ${params.tumorName}.mutect.final.vcf
  """
}

/**
indelocator
*/
process indelocator {
  clusterOptions '-l p=5'
  memory '70 GB'
  queue "default.q"
  executor 'sge'

  beforeScript "rm -rf ${params.tumorName}/indelocator  && mkdir -p ${params.tumorName}/indelocator"
  publishDir "${params.tumorName}/indelocator/", mode: 'copy'
  
  input:
  file indelocator_t_bam from t_recal_bam
  file indelocator_n_bam from n_recal_bam
  file indelocator_t_bam_bai from t_recal_bam_bai
  file indelocator_n_bam_bai from n_recal_bam_bai

  output:
  file "${params.tumorName}.indelocator.reformat.vcf"
  file "${params.tumorName}.indelocator.vcf2vcf.vcf" 
  file "${params.tumorName}.indelocator.final.vcf" into indelocator_vcf      //--------------> indelocator Vcf <---------------
  file "${params.tumorName}.indelocator.sort.vcf" 

  shell:
  """
  /amoydx/USER/wujianming/software/miniconda-python3/bin/java -Xmx10G  -jar /amoydx/USER/wujianming/project/MC3/bin/IndelGenotyper.jar -T IndelGenotyperV2 --somatic -I:normal ${indelocator_n_bam} -I:tumor ${indelocator_t_bam} -bed ${params.wes_bed} -o ${params.tumorName}.indelocator.vcf -R ${params.ref} --minCoverage 30 --minNormalCoverage 30 -ws 300
  bash /amoydx/USER/wujianming/project/MC3/bin/reformat_vcf.sh -i ${params.tumorName}.indelocator.vcf -o ${params.tumorName}.indelocator.reformat.vcf
  cat ${params.tumorName}.indelocator.reformat.vcf | ${params.vcf_sort} -c > ${params.tumorName}.indelocator.sort.vcf
  ${params.perl} /amoydx/USER/wujianming/software/mskcc-vcf2maf-5453f80/vcf2vcf.pl --input-vcf ${params.tumorName}.indelocator.sort.vcf --output-vcf ${params.tumorName}.indelocator.vcf2vcf.vcf --new-tumor-id PRIMARY --new-normal-id NORMAL  --ref-fasta ${params.ref} --vcf-tumor-id ${params.tumorName} --vcf-normal-id ${params.normalName}
  ${params.python} /amoydx/USER/wujianming/config/MC3/bin/MC3_indelocator_DP_Freq_Filter.py -i ${params.tumorName}.indelocator.vcf2vcf.vcf -md 25 -mad 3 -mf 0.05 -o ${params.tumorName}.indelocator.final.vcf
  """
}

/**
  VarScan:
    1、samtools mpileup
    2、VarScan
    3、pfilter
    output: snpVCF   indelVCF
*/
process tumor_mpileup{
  clusterOptions '-l p=4'
  memory "50 GB"
  queue "default.q"
  executor "sge"

  beforeScript "rm -rf ${params.tumorName}/varScan  && mkdir -p ${params.tumorName}/varScan"
  publishDir "${params.tumorName}/varScan/", mode: 'copy'

  input:
  file sam_t_bam from t_recal_bam
  file sam_t_bam_bai from t_recal_bam_bai

  output:
  file "${params.tumorName}.tumor.mpileup" into t_mp

  shell:
  """
  ${params.samtools} mpileup -f ${params.ref} ${sam_t_bam} -B -o ${params.tumorName}.tumor.mpileup
  """
}

process normal_mpileup{
  clusterOptions '-l p=4'
  memory "50 GB"
  queue "default.q"
  executor "sge"

  beforeScript "rm -rf ${params.tumorName}/varScan  && mkdir -p ${params.tumorName}/varScan"
  publishDir "${params.tumorName}/varScan/", mode: 'copy'

  input:
  file sam_n_bam from n_recal_bam
  file sam_n_bam_bai from n_recal_bam_bai

  output:
  file "${params.normalName}.normal.mpileup" into n_mp

  shell:
  """
  ${params.samtools} mpileup -f ${params.ref} ${sam_n_bam} -B -o ${params.normalName}.normal.mpileup
  """
}

process varScan{
  clusterOptions '-l p=2'
  memory "30 GB"
  queue "default.q"
  executor "sge"
  publishDir "${params.tumorName}/varScan/", mode: 'copy'

  input:
  file tumor_mp from t_mp           //from process tumor_mpileup
  file normal_mp from n_mp          //from process normal_mpileup
  output:
  file "varscan_snp.vcf" into varscan_snp
  file "varscan_indel.vcf"  into varscan_indel

  shell:
  """
  ${params.python2} /amoydx/USER/wujianming/project/MC3/bin/VarScanSomaticVcf.py --min-coverage 3 --min-var-freq 0.08 --p-value 0.1 --jar-file /amoydx/USER/wujianming/project/MC3/bin/VarScan.v2.3.9.jar ${normal_mp} ${tumor_mp}
  """
}

process varScan_fpfilter{
  clusterOptions '-l p=2'
  memory "30 GB"
  queue "default.q"
  executor "sge"
  publishDir "${params.tumorName}/varScan/", mode: 'copy'

  input:
  file var_snp from varscan_snp   // from process varScan
  file var_indel from varscan_indel   // from process varScan
  file var_bam from t_recal_bam    //from process baseRecalibrator_tumor
  file var_bam_bai from t_recal_bam_bai   //from process baseRecalibrator_tumor

  output:
  file "${params.tumorName}.varscan.indel.fpfilter.vcf"
  file "${params.tumorName}.varscan.snp.fpfilter.vcf"
  file "${params.tumorName}.varscan.indel.vcf2vcf.vcf" 
  file "${params.tumorName}.varscan.indel.final.vcf" into varscan_indel_vcf       //--------------> VarScan indel Vcf <---------------
  file "${params.tumorName}.varscan.indel.sort.vcf" 
  file "${params.tumorName}.varscan.snp.vcf2vcf.vcf"   
  file "${params.tumorName}.varscan.snp.final.vcf" into varscan_snp_vcf       //--------------> VarScan snp Vcf <---------------
  file "${params.tumorName}.varscan.snp.sort.vcf" 

  shell:
  """
  ${params.perl} /amoydx/USER/wujianming/project/MC3/bin/fpfilter.pl --vcf-file ${var_indel} --bam-file ${var_bam} --sample TUMOR --reference ${params.ref} --output ${params.tumorName}.varscan.indel.fpfilter.vcf
  ${params.perl} /amoydx/USER/wujianming/project/MC3/bin/fpfilter.pl --vcf-file ${var_snp} --bam-file ${var_bam} --sample TUMOR --reference ${params.ref} --output ${params.tumorName}.varscan.snp.fpfilter.vcf
  cat ${params.tumorName}.varscan.indel.fpfilter.vcf | ${params.vcf_sort} -c > ${params.tumorName}.varscan.indel.sort.vcf
  cat ${params.tumorName}.varscan.snp.fpfilter.vcf | ${params.vcf_sort} -c >  ${params.tumorName}.varscan.snp.sort.vcf
  ${params.perl} /amoydx/USER/wujianming/software/mskcc-vcf2maf-5453f80/vcf2vcf.pl --input-vcf ${params.tumorName}.varscan.snp.sort.vcf --output-vcf ${params.tumorName}.varscan.snp.vcf2vcf.vcf --new-tumor-id PRIMARY --new-normal-id NORMAL  --ref-fasta ${params.ref} --vcf-tumor-id TUMOR --vcf-normal-id NORMAL
  ${params.perl} /amoydx/USER/wujianming/software/mskcc-vcf2maf-5453f80/vcf2vcf.pl --input-vcf ${params.tumorName}.varscan.indel.sort.vcf --output-vcf ${params.tumorName}.varscan.indel.vcf2vcf.vcf --new-tumor-id PRIMARY --new-normal-id NORMAL  --ref-fasta ${params.ref} --vcf-tumor-id TUMOR --vcf-normal-id NORMAL
  ${params.python} /amoydx/USER/wujianming/config/MC3/bin/MC3_varscan_DP_Freq_Filter.py -i ${params.tumorName}.varscan.snp.vcf2vcf.vcf -md 25 -mad 3 -mf 0.05 -o ${params.tumorName}.varscan.snp.final.vcf
  ${params.python} /amoydx/USER/wujianming/config/MC3/bin/MC3_varscan_DP_Freq_Filter.py -i ${params.tumorName}.varscan.indel.vcf2vcf.vcf -md 25 -mad 3 -mf 0.05 -o ${params.tumorName}.varscan.indel.final.vcf

  """
}

/**
  somaticSniper
  1、 call somatic
  2、 fpfilter
*/
process somaticSniper{
  clusterOptions '-l p=2'
  memory "30 GB"
  queue "default.q"
  executor "sge"
  beforeScript "rm -rf ${params.tumorName}/somaticSniper  && mkdir -p ${params.tumorName}/somaticSniper"
  publishDir "${params.tumorName}/somaticSniper/", mode: 'copy'

  input:
  file snip_t_bam from t_recal_bam    //from process baseRecalibrator_tumor
  file snip_t_bam_bai from t_recal_bam_bai   //from process baseRecalibrator_tumor
  file snip_n_bam from n_recal_bam    //from process baseRecalibrator_normal
  file snip_n_bam_bai from n_recal_bam_bai   //from process baseRecalibrator_normal

  output:
  file "${params.tumorName}.somaticSniper.vcf" into snip_vcf

  shell:
  """
  ${params.somaticSniper} -f ${params.ref} -q 1 -Q 40 -L -G -n ${params.normalName} -t ${params.tumorName} -F vcf ${snip_t_bam} ${snip_n_bam} ${params.tumorName}.somaticSniper.vcf
  """
}

process somaticSniper_fpfilter{
  clusterOptions '-l p=1'
  memory "10 GB"
  queue "default.q"
  executor "sge"
  publishDir "${params.tumorName}/somaticSniper/", mode: 'copy'

  input:
  file fp_snip from snip_vcf   // from process somaticSniper
  file fp_t_bam from t_recal_bam    //from process baseRecalibrator_tumor
  file fp_t_bam_bai from t_recal_bam_bai   //from process baseRecalibrator_tumor

  output:
  file "${params.tumorName}.somaticSniper.fpfilter.vcf"
  file "${params.tumorName}.somaticSniper.vcf2vcf.vcf" 
  file "${params.tumorName}.somaticSniper.final.vcf" into somaticSniper_Vcf      //--------------> somaticSniper Vcf <---------------
  file "${params.tumorName}.somaticSniper.sort.vcf" 

  shell:
  """
  ${params.perl}  /amoydx/USER/wujianming/project/MC3/bin/fpfilter.pl --vcf-file ${fp_snip} --bam-file ${fp_t_bam} --sample ${params.tumorName} --reference ${params.ref} --output ${params.tumorName}.somaticSniper.fpfilter.vcf
  cat ${params.tumorName}.somaticSniper.fpfilter.vcf | ${params.vcf_sort} -c > ${params.tumorName}.somaticSniper.sort.vcf
  ${params.perl} /amoydx/USER/wujianming/software/mskcc-vcf2maf-5453f80/vcf2vcf.pl --input-vcf ${params.tumorName}.somaticSniper.sort.vcf --output-vcf ${params.tumorName}.somaticSniper.vcf2vcf.vcf --new-tumor-id PRIMARY --new-normal-id NORMAL  --ref-fasta ${params.ref} --vcf-tumor-id ${params.tumorName} --vcf-normal-id ${params.normalName}
  ${params.python} /amoydx/USER/wujianming/config/MC3/bin/MC3_somaticSniper_DP_Freq_Filter.py -i ${params.tumorName}.somaticSniper.vcf2vcf.vcf -md 25 -mad 3 -mf 0.05 -o ${params.tumorName}.somaticSniper.final.vcf
  """
}

process muse_split{
  clusterOptions '-l p=2'
  memory "30 GB"
  queue "default.q"
  executor "sge"
  beforeScript "rm -rf ${params.tumorName}/muSE  && mkdir -p ${params.tumorName}/muSE"
  publishDir "${params.tumorName}/muSE/", mode: 'copy'

  input:
  file muse_beds
  file muse_t_bam from t_recal_bam    //from process baseRecalibrator_tumor
  file muse_t_bam_bai from t_recal_bam_bai   //from process baseRecalibrator_tumor
  file muse_n_bam from n_recal_bam    //from process baseRecalibrator_normal
  file muse_n_bam_bai from n_recal_bam_bai   //from process baseRecalibrator_normal

  output:
  file "*.MuSE.txt" into muse_files

  shell:
  """
  ${params.muSE} call -f ${params.ref} -l ${muse_beds} -O ${params.tumorName}_${muse_beds} ${muse_t_bam} ${muse_n_bam}
  """
}


process muse_merge{
  clusterOptions '-l p=2'
  memory "20 GB"
  queue "default.q"
  executor "sge"
  publishDir "${params.tumorName}/muSE/", mode: 'copy'

  input:
  file "*" from muse_files.collect()

  output:
  file "${params.tumorName}.merge.MuSE.vcf" into muse_merge_vcf

  shell:
  """
  ${params.python2} /amoydx/USER/wujianming/config/MC3/bin/Merge_MuSE_vcf.py ${params.tumorName} ${params.tumorName}.merge.MuSE.vcf
  """
}

process muse_sump{
  clusterOptions '-l p=4'
  memory "50 GB"
  queue "default.q"
  executor "sge"
  publishDir "${params.tumorName}/muSE/", mode: 'copy'

  input:
  file muse_sump from muse_merge_vcf

  output:
  file "${params.tumorName}.sump.MuSE.vcf"
  file "${params.tumorName}.MuSE.vcf2vcf.vcf" into muse_vcf    //--------------> MuSE Vcf <---------------
  file "${params.tumorName}.MuSE.sort.vcf" 

  shell:
  """
  ${params.muSE} sump -I ${muse_sump} -E -O ${params.tumorName}.sump.MuSE.vcf
  cat ${params.tumorName}.sump.MuSE.vcf | ${params.vcf_sort} -c > ${params.tumorName}.MuSE.sort.vcf
  ${params.perl} /amoydx/USER/wujianming/software/mskcc-vcf2maf-5453f80/vcf2vcf.pl --input-vcf ${params.tumorName}.MuSE.sort.vcf --output-vcf ${params.tumorName}.MuSE.vcf2vcf.vcf --new-tumor-id PRIMARY --new-normal-id NORMAL  --ref-fasta ${params.ref} --vcf-tumor-id TUMOR --vcf-normal-id NORMAL
  """
}

/**
Pindel
  1、split bed
  2、run tumor pindel (31)
  3、merge 31 sub tumor vcf
  4、run normal pindel (31)
  5、merge 31 sub normal vcf
  4、merge vcf  ==> bgzip -c  && bcftools index  && bcftools merge
*/

process tumor_pindel{
  clusterOptions '-l p=4'
  memory "50 GB"
  queue "default.q"
  executor "sge"
  beforeScript "rm -rf ${params.tumorName}/Pindel  && mkdir -p ${params.tumorName}/Pindel"
  publishDir "${params.tumorName}/Pindel/", mode: 'copy'

  input:
  file pindel_beds
  file pindel_t_bam from t_recal_bam    //from process baseRecalibrator_tumor
  file pindel_t_bam_bai from t_recal_bam_bai   //from process baseRecalibrator_tumor

  output:
  file "${params.tumorName}.*.pindel.indel.vcf"  into tumor_indel_31_vcfs

  shell:
  '''
  now=$(date +%Y%m%d)
  echo -e "!{pindel_t_bam}\t250\t!{params.tumorName}" > !{params.tumorName}.pindel.config
  !{params.pindel} -f !{params.ref} -i !{params.tumorName}.pindel.config -x 1 -w 0.1 -m 6 -T 10 --include !{pindel_beds} -o !{params.tumorName}.!{pindel_beds}.pindel
  !{params.pindel2vcf} -r !{params.ref} -R GRCh37 -P !{params.tumorName}.!{pindel_beds}.pindel -d ${now} -v !{params.tumorName}.!{pindel_beds}.pindel.indel.vcf
  '''
}

process normal_pindel{
  clusterOptions '-l p=4'
  memory "50 GB"
  queue "default.q"
  executor "sge"
  publishDir "${params.tumorName}/Pindel/", mode: 'copy'

  input:
  file pindel_beds_twice
  file pindel_n_bam from n_recal_bam    //from process baseRecalibrator_tumor
  file pindel_n_bam_bai from n_recal_bam_bai   //from process baseRecalibrator_tumor

  output:
  file "${params.normalName}.*.pindel.indel.vcf"  into normal_indel_31_vcfs

  shell:
  '''
  now=$(date +%Y%m%d)
  echo -e "!{pindel_n_bam}\t250\t!{params.normalName}" > !{params.normalName}.pindel.config
  !{params.pindel} -f !{params.ref} -i !{params.normalName}.pindel.config -x 1 -w 0.1 -m 6 -T 10 --include !{pindel_beds_twice} -o !{params.normalName}.!{pindel_beds_twice}.pindel
  !{params.pindel2vcf} -r !{params.ref} -R GRCh37 -P !{params.normalName}.!{pindel_beds_twice}.pindel -d ${now} -v !{params.normalName}.!{pindel_beds_twice}.pindel.indel.vcf
  '''
}

process pindel_merge_vcf{
  //merge_split_vcf && merge tumor and normal
  clusterOptions '-l p=4'
  memory "50 GB"
  queue "default.q"
  executor "sge"
  publishDir "${params.tumorName}/Pindel/", mode: 'copy'

  input:
  file "*" from tumor_indel_31_vcfs.collect()
  file "*" from normal_indel_31_vcfs.collect()

  output:
  file "${params.tumorName}.pindel.vcf2vcf.vcf"
  file "${params.tumorName}.pindel.final.vcf" into pindel_merge_vcf     //--------------> pindel Vcf <---------------
  file "${params.tumorName}.pindel.sort.vcf" 

  shell:
  """
  ${params.python} /amoydx/USER/wujianming/project/MC3/RUN/Merge_Pindel_vcf.py ${params.tumorName} ${params.tumorName}.pindel.vcf ${params.tumorName}
  ${params.python} /amoydx/USER/wujianming/project/MC3/RUN/Merge_Pindel_vcf.py ${params.normalName} ${params.normalName}.pindel.vcf ${params.normalName}
  sleep 10s
  ${params.bgzip} -c ${params.tumorName}.pindel.vcf > ${params.tumorName}.pindel.vcf.gz
  ${params.bgzip} -c ${params.normalName}.pindel.vcf > ${params.normalName}.pindel.vcf.gz
  ${params.bcftools} index ${params.tumorName}.pindel.vcf.gz
  ${params.bcftools} index ${params.normalName}.pindel.vcf.gz
  ${params.bcftools} merge -o ${params.tumorName}.merge.indel.vcf ${params.tumorName}.pindel.vcf.gz ${params.normalName}.pindel.vcf.gz
  cat ${params.tumorName}.merge.indel.vcf | ${params.vcf_sort} -c > ${params.tumorName}.pindel.sort.vcf
  ${params.perl} /amoydx/USER/wujianming/software/mskcc-vcf2maf-5453f80/vcf2vcf.pl --input-vcf ${params.tumorName}.pindel.sort.vcf --output-vcf ${params.tumorName}.pindel.vcf2vcf.vcf --new-tumor-id PRIMARY --new-normal-id NORMAL  --ref-fasta ${params.ref} --vcf-tumor-id ${params.tumorName} --vcf-normal-id ${params.normalName}
  ${params.python} /amoydx/USER/wujianming/config/MC3/bin/MC3_Pindel_DP_Freq_Filter.py -i ${params.tumorName}.pindel.vcf2vcf.vcf -md 25 -mad 3 -mf 0.05 -o ${params.tumorName}.pindel.final.vcf
  """
}

process run_radia{
  //must be run as each chrom
  //env python2
  clusterOptions '-l p=2'
  memory "20 GB"
  queue "default.q"
  executor "sge"
  beforeScript "rm -rf ${params.tumorName}/radia  && mkdir -p ${params.tumorName}/radia && source activate py2"
  publishDir "${params.tumorName}/radia/", mode: 'copy'

  input:
  val chrom_nums
  file radia_t_bam from t_recal_bam    //from process baseRecalibrator_tumor
  file radia_t_bam_bai from t_recal_bam_bai   //from process baseRecalibrator_tumor
  file radia_n_bam from n_recal_bam    //from process baseRecalibrator_normal
  file radia_n_bam_bai from n_recal_bam_bai   //from process baseRecalibrator_normal

  output:
  file "${params.tumorName}_*.vcf" into radia_25_vcf

  shell:
  """
  ${params.python2} /amoydx/USER/wujianming/project/MC3/bin/radia/radia-1.1.5/scripts/radia.py ${params.tumorName} ${chrom_nums} -n ${radia_n_bam} -t ${radia_t_bam} -f ${params.ref} -i hg19 -o ${params.tumorName}_chr${chrom_nums}.vcf
  """
}

process merge_radia{
  //must be run as each chrom
  //env python2
  clusterOptions '-l p=2'
  memory "20 GB"
  queue "default.q"
  executor "sge"
  publishDir "${params.tumorName}/radia/", mode: 'copy'

  input:
  file "*" from radia_25_vcf.collect()

  output:
  file "${params.tumorName}.radia.vcf"
  file "${params.tumorName}.radia.vcf2vcf.vcf"
  file "${params.tumorName}.radia.final.vcf" into radia_merge_vcf    //--------------> radia Vcf <---------------
  file "${params.tumorName}.radia.sort.vcf" 

  shell:
  """
  ${params.python2} /amoydx/USER/wujianming/project/MC3/bin/radia/radia-1.1.5/scripts/mergeChroms.py ${params.tumorName} ./ ./ -o ${params.tumorName}.radia.vcf
  cat ${params.tumorName}.radia.vcf | ${params.vcf_sort} -c > ${params.tumorName}.radia.sort.vcf
  ${params.perl} /amoydx/USER/wujianming/software/mskcc-vcf2maf-5453f80/vcf2vcf.pl --input-vcf ${params.tumorName}.radia.sort.vcf --output-vcf ${params.tumorName}.radia.vcf2vcf.vcf --new-tumor-id PRIMARY --new-normal-id NORMAL  --ref-fasta ${params.ref} --vcf-tumor-id DNA_TUMOR --vcf-normal-id DNA_NORMAL
  ${params.python} /amoydx/USER/wujianming/config/MC3/bin/MC3_radia_DP_Freq_Filter.py -i ${params.tumorName}.radia.vcf2vcf.vcf -md 25 -mad 3 -mf 0.05 -o ${params.tumorName}.radia.final.vcf
  """
}

process vcf2vcf_merge{
  clusterOptions '-l p=2'
  memory "10 GB"
  queue "default.q"
  executor "sge"
  beforeScript "rm -rf ${params.tumorName}/MAF  && mkdir -p ${params.tumorName}/MAF"
  publishDir "${params.tumorName}/MAF/", mode: 'copy'

  input:
  file vcf2vcf_indelocator from indelocator_vcf
  file vcf2vcf_pindel from pindel_merge_vcf
  file vcf2vcf_MuSE from muse_vcf
  file vcf2vcf_mutect from mutect_filter_Vcf
  file vcf2vcf_radia from radia_merge_vcf
  file vcf2vcf_somaticSniper from somaticSniper_Vcf
  file vcf2vcf_varscan_snp from varscan_snp_vcf
  file vcf2vcf_varscan_indel from varscan_indel_vcf

  output:
  file "${params.tumorName}.vcf2vcf.merge.vcf" into vcf2vcf_merge_file

  shell:
  """
  ${params.python2} /amoydx/USER/wujianming/software/vcf-tools/merge_vcfs.py  --input ${vcf2vcf_indelocator} ${vcf2vcf_MuSE} ${vcf2vcf_mutect} ${vcf2vcf_radia} ${vcf2vcf_somaticSniper} ${vcf2vcf_varscan_indel} ${vcf2vcf_varscan_snp} ${vcf2vcf_pindel}  --output ${params.tumorName}.vcf2vcf.merge.vcf --keys INDELOCATOR MUSE MUTECT RADIA SOMATICSNIPER VARSCAN VARSCAN PINDEL
  """
}

process vcf2maf{
  clusterOptions '-l p=2'
  memory "10 GB"
  queue "default.q"
  executor "sge"
  beforeScript "rm -rf ${params.tumorName}/MAF  && mkdir -p ${params.tumorName}/MAF"
  publishDir "${params.tumorName}/MAF/", mode: 'copy'

  input:
  file vcf2maf_vcf from vcf2vcf_merge_file

  output:
  file "${params.tumorName}.maf"
  file "${params.tumorName}.filter.maf"
  file "*vep*"

  shell:
  '''
  !{params.perl} /amoydx/USER/wujianming/software/mskcc-vcf2maf-5453f80/vcf2maf.pl --input-vcf !{vcf2maf_vcf} --output-maf !{params.tumorName}.maf --tumor-id !{params.tumorName} --normal-id !{params.normalName} --vep-path ${VEP_PATH} --vep-data ${VEP_DATA} --ref-fasta !{params.ref} --filter-vcf ${VEP_DATA}/ExAC_nonTCGA.r0.3.1.sites.vep.vcf.gz  --cache-version 97 --retain-info CENTERS
  ${params.python} /amoydx/USER/wujianming/config/MC3/bin/MC3.CENTER.Filter.py -i ${params.tumorName}.maf -o ${params.tumorName}.filter.maf
  '''
}
