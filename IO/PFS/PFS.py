#!/usr/bin/env python
# coding: utf-8

# In[2]:


from sklearn.linear_model import LogisticRegression
from lifelines.statistics import logrank_test
from sklearn.metrics import roc_auc_score
from lifelines import KaplanMeierFitter
from itertools import combinations
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import os
get_ipython().run_line_magic('matplotlib', 'inline')
pd.options.mode.chained_assignment = None


# In[27]:


from lifelines import KaplanMeierFitter
kmf = KaplanMeierFitter()


# In[4]:


T = data["duration"]
E = data["observed"]

kmf.fit(T, event_observed=E)


# In[7]:


kmf.survival_function_.plot()
plt.title('Survival function of political regimes');


# In[14]:


from lifelines import KaplanMeierFitter
kmf = KaplanMeierFitter()
dat = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS/PFS_TP53.csv",sep=',')
dat.head()


# In[12]:


T = dat["Month"]
E = dat["Status"]
kmf.fit(T, event_observed=E)
kmf.survival_function_.plot()
plt.title('Survival function of political regimes');


# In[55]:


ax = plt.subplot(111)
dat = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS/PFS_TP53.csv",sep=',')
T = dat["Month"]
E = dat["Status"]
dem = (dat["Type"] == "TP53")
kmf.fit(T[dem], event_observed=E[dem], label="TP53")
kmf.plot(ax=ax,ci_show=False)
kmf.fit(T[~dem], event_observed=E[~dem], label="Non-TP53")
kmf.plot(ax=ax,ci_show=False)
plt.ylim(0, 1)
results = logrank_test(T[dem], T[~dem], event_observed_A=E[dem], event_observed_B=E[~dem])
pvalue = results.p_value
pvalue = round(pvalue,4)
plt.text(30,0.7,"P: "+str(pvalue))


# # Only HRR (BARD1、BLM、BRCA1、BRCA2、RAD51C)

# In[70]:


ax = plt.subplot(111)
dat = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS/PFS_HRR.csv",sep=',')
T = dat["Month"]
E = dat["Status"]
dem = (dat["Type"] == "HRR-Mut")
kmf.fit(T[dem], event_observed=E[dem], label="HRR-Mut")
kmf.plot(ax=ax,ci_show=False)
kmf.fit(T[~dem], event_observed=E[~dem], label="Non-HRR-Mut")
kmf.plot(ax=ax,ci_show=False)
plt.ylim(0, 1)
results = logrank_test(T[dem], T[~dem], event_observed_A=E[dem], event_observed_B=E[~dem])
pvalue = results.p_value
pvalue = round(pvalue,4)
plt.text(30,0.7,"P: "+str(pvalue))
plt.title("HRR")


# # HRR+MSH6+PMS2+PAPPA2

# In[69]:


ax2 = plt.subplot(111)
dat2 = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS/PFS_HRR-PAPPA2.csv",sep=',')
T2 = dat2["Month"]
E2 = dat2["Status"]
dem2 = (dat2["Type"] == "Mut-HRR-PAPPA2")
kmf.fit(T2[dem2], event_observed=E2[dem2], label="Mut-HRR-PAPPA2")
kmf.plot(ax=ax2,ci_show=False)
kmf.fit(T2[~dem2], event_observed=E2[~dem2], label="Non-Mut-HRR-PAPPA2")
kmf.plot(ax=ax2,ci_show=False)
plt.ylim(0, 1)
results2 = logrank_test(T2[dem2], T2[~dem2], event_observed_A=E2[dem2], event_observed_B=E2[~dem2])
pvalue2 = results2.p_value
pvalue2 = round(pvalue2,4)
plt.text(30,0.7,"P: "+str(pvalue2))
plt.title("HRR+PAPPA2")


# # HRR+KMT2

# In[68]:


ax3 = plt.subplot(111)
dat3 = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS/PFS_HRR-KMT2.csv",sep=',')
T3 = dat3["Month"]
E3 = dat3["Status"]
dem3 = (dat3["Type"] == "Mut-HRR-KMT2")
kmf.fit(T3[dem3], event_observed=E3[dem3], label="Mut-HRR-KMT2")
kmf.plot(ax=ax3,ci_show=False)
kmf.fit(T3[~dem3], event_observed=E3[~dem3], label="Non-Mut-HRR-KMT2")
kmf.plot(ax=ax3,ci_show=False)
plt.ylim(0, 1)
results3 = logrank_test(T3[dem3], T3[~dem3], event_observed_A=E3[dem3], event_observed_B=E3[~dem3])
pvalue3 = results3.p_value
pvalue3 = round(pvalue3,4)
plt.text(30,0.7,"P: "+str(pvalue3))
plt.title("HRR+KMT2")


# In[72]:


ax4 = plt.subplot(111)
dat4 = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS/PFS_HRR-KMT2-PAPPA2.csv",sep=',')
dem4 = (dat4["Type"] == "combine-HRR-KMT2-PAPPA2")
T4 = dat4["Month"]
E4 = dat4["Status"]
kmf.fit(T4[dem4], event_observed=E4[dem4], label="Mut-HRR-KMT2-PAPPA2")
kmf.plot(ax=ax4,ci_show=False)
kmf.fit(T4[~dem4], event_observed=E4[~dem4], label="Non-Mut-HRR-KMT2-PAPPA2")
kmf.plot(ax=ax4,ci_show=False)
plt.ylim(0, 1)
results4 = logrank_test(T4[dem4], T4[~dem4], event_observed_A=E4[dem4], event_observed_B=E4[~dem4])
pvalue4 = results4.p_value
pvalue4 = round(pvalue4,4)
plt.text(30,0.7,"P: "+str(pvalue4))
plt.title("HRR+KMT2+PAPPA2")


# # PFS-免疫超进展（HPD）

# In[73]:


ax5 = plt.subplot(111)
dat5 = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS/PFS_免疫超进展.csv",sep=',')
dem5 = (dat5["Type"] == "HPD-Mut")
T5 = dat5["Month"]
E5 = dat5["Status"]
kmf.fit(T5[dem5], event_observed=E5[dem5], label="HPD-Mut")
kmf.plot(ax=ax5,ci_show=False)
kmf.fit(T5[~dem5], event_observed=E5[~dem5], label="HPD-non-Mut")
kmf.plot(ax=ax5,ci_show=False)
plt.ylim(0, 1)
results5 = logrank_test(T5[dem5], T5[~dem5], event_observed_A=E5[dem5], event_observed_B=E5[~dem5])
pvalue5 = results5.p_value
pvalue5 = round(pvalue5,4)
plt.text(30,0.7,"P: "+str(pvalue5))
plt.title("HPD PFS")


# # PFS-55-only-tumor-IO

# In[33]:


#clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
#sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
pfs = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS.csv")
#merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')
#merge_data = merge_data[['患者姓名','最好疗效','Response','组织样本编号','RNA-seq','TMB和WES-DNA文库构建','配对白细胞文库构建']]
#merge_pfs_response = pd.merge(merge_data,pfs,left_on = '组织样本编号',right_on = 'Sample')

raw_data_55_noHot = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])
raw_data_55_Hot = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])


# In[34]:


#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["3'UTR","5'UTR","Intronic"]
PASS_list = ['PASS','PASS;HotSpot']
data_55_noHot = raw_data_55_noHot[~raw_data_55_noHot['Type'].isin(Filter_list)]
data_55_noHot = data_55_noHot[data_55_noHot['Tag'].isin(PASS_list)]
data_55_noHot = data_55_noHot[['Sample','Freq','Tag','Gene','Type']]
#Freq过滤5%
data_55_noHot = data_55_noHot[data_55_noHot['Freq'] >= 0.05]

data_55_Hot = raw_data_55_Hot[['Sample','Freq','Tag','Gene','Type']]
data_55_Hot = data_55_Hot[data_55_Hot['Tag'].isin(PASS_list)]
data_55_Hot = data_55_Hot[data_55_Hot['Freq'] >= 0.01]
merge_55_data = pd.concat([data_55_noHot,data_55_Hot])


# In[35]:


def run(Genelist):
    #Genelist = GeneName.split('-')
    GeneFilter = merge_55_data[merge_55_data['Gene'].isin(Genelist)]
    #GeneFilter = merge_55_data[merge_55_data['Gene'] == Gene]
    GeneFilterSample = GeneFilter.groupby('Sample',as_index=False).count()[['Sample','Freq']]
    GeneFilterSample['Type'] = 'Mut'
    MERGE = pd.merge(GeneFilterSample,pfs,on='Sample',how='outer')
    MERGE = MERGE.dropna(subset=["PFS", "Status"])
    MERGE = MERGE.fillna('noMut')
    MERGE =MERGE[['Sample','Type','PFS','Status']]
    MERGE['Status'] = MERGE['Status'].astype("int")
    MERGE['PFS'] = MERGE['PFS'].astype("float")
    dem5 = MERGE["Type"] == 'Mut'
    ax5 = plt.subplot(111)
    T5 = MERGE["PFS"]
    E5 = MERGE["Status"]
    kmf.fit(T5[dem5], event_observed=E5[dem5], label="Mut")
    kmf.plot(ax=ax5,ci_show=False)
    kmf.fit(T5[~dem5], event_observed=E5[~dem5], label="non-Mut")
    kmf.plot(ax=ax5,ci_show=False)
    plt.ylim(0, 1)
    results5 = logrank_test(T5[dem5], T5[~dem5], event_observed_A=E5[dem5], event_observed_B=E5[~dem5])
    pvalue5 = results5.p_value
    pvalue5 = round(pvalue5,4)
    plt.text(30,0.7,"P: "+str(pvalue5))
    plt.title("HRR+KMT2 PFS")


# In[36]:


run(['BARD1','BLM','BRCA1','BRCA2','KMT2B','KMT2C','KMT2D','RAD51C','PAPPA2'])


# In[39]:


run(['EGFR','ALK','TP53','BRAF'])


# In[211]:


def run_Pvalue(GeneName):
    Genelist = GeneName.split('-')
    GeneFilter = merge_55_data[merge_55_data['Gene'].isin(Genelist)]
    #GeneFilter = merge_55_data[merge_55_data['Gene'] == Gene]
    GeneFilterSample = GeneFilter.groupby('Sample',as_index=False).count()[['Sample','Freq']]
    GeneFilterSample['Type'] = 'Mut'
    MERGE = pd.merge(GeneFilterSample,pfs,on='Sample',how='outer')
    MERGE = MERGE.dropna(subset=["PFS", "Status"])
    MERGE = MERGE.fillna('noMut')
    MERGE =MERGE[['Sample','Type','PFS','Status']]
    MERGE['Status'] = MERGE['Status'].astype("int")
    MERGE['PFS'] = MERGE['PFS'].astype("float")
    dem5 = MERGE["Type"] == 'Mut'
    T5 = MERGE["PFS"]
    E5 = MERGE["Status"]
    results5 = logrank_test(T5[dem5], T5[~dem5], event_observed_A=E5[dem5], event_observed_B=E5[~dem5])
    pvalue5 = results5.p_value
    pvalue5 = round(pvalue5,4)
    print(GeneName+'\t'+str(pvalue5))


# In[203]:


run_Pvalue('MDM2-EGFR-DNMT3A')


# In[204]:


from itertools import combinations
geneList = ['MDM2','EGFR','ALK','PTEN','DNMT3A']
allList = ['-'.join(x) for i in range(len(geneList)) for x in combinations(geneList, i + 1)]
for combine in allList:
    run_Pvalue(combine)


# In[213]:


from itertools import combinations
geneList = ['BRAD1','BLM','BRCA1','BRCA2','RAD51C','MSH6','PMS2','PAPPA2']
allList = ['-'.join(x) for i in range(len(geneList)) for x in combinations(geneList, i + 1)]
for combine in allList:
    run_Pvalue(combine)


# In[217]:


run('BRAD1-BRCA1-MSH6')


# # HRR Germline 39 tumor-normal 

# In[24]:


#clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
#sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
pfs = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS.csv")
#merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')
#merge_data = merge_data[['患者姓名','最好疗效','Response','组织样本编号','RNA-seq','TMB和WES-DNA文库构建','配对白细胞文库构建']]
#merge_pfs_response = pd.merge(merge_data,pfs,left_on = '组织样本编号',right_on = 'Sample')

raw_data_55_noHot = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])
raw_data_55_Hot = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])
raw_data_55_germline = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelGermline',index_col = None,na_values= ['9999'])


# In[29]:


#Type过滤5'UTR， 3'UTR， Intronic
#HRRList = ['ATM','BARD1','BLM','BRCA1','BRCA2','BRIP1','CHEK2','FANCA','FANCC','FANCD2','FANCE','FANCF','FANCG','FANCI','FANCL','FANCM','MRE11A','NBN','PALB2','RAD50','RAD51','RAD51B','RAD51C','RAD51D','RAD54L']
#MMRList = ['MLH1','MSH2','MSH6','PMS1','PMS2','PAPPA2']
HRRList = ['BARD1','BLM','BRCA1','BRCA2']
MMRList = ['MSH6','PMS2','PAPPA2']
plusList = HRRList + MMRList
#Filter_list = ['nonSynonymous_Substitution']
Filter_list = ["3'UTR","5'UTR","Intronic"]
PASS_list = ['PASS','PASS;HotSpot']
data_55_noHot = raw_data_55_noHot[~raw_data_55_noHot['Type'].isin(Filter_list)]
data_55_noHot = data_55_noHot[data_55_noHot['Gene'].isin(plusList)]
data_55_noHot = data_55_noHot[data_55_noHot['Tag'].isin(PASS_list)]
data_55_noHot = data_55_noHot[['Sample','Freq','Tag','Gene','Type']]
#Freq过滤5%
data_55_noHot = data_55_noHot[data_55_noHot['Freq'] >= 0.05]

data_55_Hot = raw_data_55_Hot[['Sample','Freq','Tag','Gene','Type']]
data_55_Hot = data_55_Hot[data_55_Hot['Tag'].isin(PASS_list)]
data_55_Hot = data_55_Hot[data_55_Hot['Gene'].isin(plusList)]
data_55_Hot = data_55_Hot[data_55_Hot['Freq'] >= 0.01]

##再把HRR相关基因germline突变
data_55_HRR = raw_data_55_germline[raw_data_55_germline['Gene'].isin(HRRList)]
data_55_HRR = data_55_HRR[~data_55_HRR['Type'].isin(Filter_list)]
data_55_HRR = data_55_HRR[['Sample','Freq','Tag','Gene','Type']]
merge_55_data = pd.concat([data_55_noHot,data_55_Hot,data_55_HRR])

from lifelines import KaplanMeierFitter
kmf5 = KaplanMeierFitter()
GeneFilterSample = merge_55_data.groupby('Sample',as_index=False).count()[['Sample','Freq']]
GeneFilterSample['Type'] = 'Mut'
MERGE = pd.merge(GeneFilterSample,pfs,on='Sample',how='outer')
MERGE = MERGE.dropna(subset=["PFS", "Status"])
MERGE = MERGE.fillna('noMut')
MERGE =MERGE[['Sample','Type','PFS','Status']]
MERGE['Status'] = MERGE['Status'].astype("int")
MERGE['PFS'] = MERGE['PFS'].astype("float")
dem5 = MERGE["Type"] == 'Mut'
ax5 = plt.subplot(111)
T5 = MERGE["PFS"]
E5 = MERGE["Status"]
kmf5.fit(T5[dem5], event_observed=E5[dem5], label="Mut")
kmf5.plot(ax=ax5,ci_show=False)
kmf5.fit(T5[~dem5], event_observed=E5[~dem5], label="non-Mut")
kmf5.plot(ax=ax5,ci_show=False)
plt.ylim(0, 1)
results5 = logrank_test(T5[dem5], T5[~dem5], event_observed_A=E5[dem5], event_observed_B=E5[~dem5])
pvalue5 = results5.p_value
pvalue5 = round(pvalue5,4)
plt.text(30,0.7,"P: "+str(pvalue5))
plt.title("BARD1+BLM+BRCA1+BRCA2+MSH6+PMS2+PAPPA2 PFS\n Somatic+Germline")


# In[27]:


#Type过滤5'UTR， 3'UTR， Intronic
HRRList2 = ['ATM','BARD1','BLM','BRCA1','BRCA2','BRIP1','CHEK2','FANCA','FANCC','FANCD2','FANCE','FANCF','FANCG','FANCI','FANCL','FANCM','MRE11A','NBN','PALB2','RAD50','RAD51','RAD51B','RAD51C','RAD51D','RAD54L']
MMRList2 = ['MLH1','MSH2','MSH6','PMS1','PMS2','PAPPA2']
#HRRList = ['BARD1','BLM','BRCA1','BRCA2']
#MMRList = ['MSH6','PMS2','PAPPA2']
plusList2 = HRRList2 + MMRList2
#Filter_list = ['nonSynonymous_Substitution']
Filter_list = ["3'UTR","5'UTR","Intronic"]
PASS_list = ['PASS','PASS;HotSpot']
data_55_noHot2 = raw_data_55_noHot[~raw_data_55_noHot['Type'].isin(Filter_list)]
data_55_noHot2 = data_55_noHot2[data_55_noHot2['Gene'].isin(plusList2)]
data_55_noHot2 = data_55_noHot2[data_55_noHot2['Tag'].isin(PASS_list)]
data_55_noHot2 = data_55_noHot2[['Sample','Freq','Tag','Gene','Type']]
#Freq过滤5%
data_55_noHot2 = data_55_noHot2[data_55_noHot2['Freq'] >= 0.05]

data_55_Hot2 = raw_data_55_Hot[['Sample','Freq','Tag','Gene','Type']]
data_55_Hot2 = data_55_Hot2[data_55_Hot2['Tag'].isin(PASS_list)]
data_55_Hot2 = data_55_Hot2[data_55_Hot2['Gene'].isin(plusList)]
data_55_Hot2 = data_55_Hot2[data_55_Hot2['Freq'] >= 0.01]

##再把HRR相关基因germline突变
data_55_HRR2 = raw_data_55_germline[raw_data_55_germline['Gene'].isin(HRRList2)]
data_55_HRR2 = data_55_HRR2[~data_55_HRR2['Type'].isin(Filter_list)]
data_55_HRR2 = data_55_HRR2[['Sample','Freq','Tag','Gene','Type']]
merge_55_data2 = pd.concat([data_55_noHot2,data_55_Hot2,data_55_HRR2])

from lifelines import KaplanMeierFitter
kmf = KaplanMeierFitter()
GeneFilterSample2 = merge_55_data2.groupby('Sample',as_index=False).count()[['Sample','Freq']]
GeneFilterSample2['Type'] = 'Mut'
MERGE2 = pd.merge(GeneFilterSample2,pfs,on='Sample',how='outer')
MERGE2 = MERGE2.dropna(subset=["PFS", "Status"])
MERGE2 = MERGE2.fillna('noMut')
MERGE2 = MERGE2[['Sample','Type','PFS','Status']]
MERGE2['Status'] = MERGE2['Status'].astype("int")
MERGE2['PFS'] = MERGE2['PFS'].astype("float")
dem = MERGE2["Type"] == 'Mut'
ax = plt.subplot(111)
T = MERGE2["PFS"]
E = MERGE2["Status"]
kmf.fit(T[dem], event_observed=E[dem], label="Mut")
kmf.plot(ax=ax,ci_show=False)
kmf.fit(T[~dem], event_observed=E[~dem], label="non-Mut")
kmf.plot(ax=ax,ci_show=False)
plt.ylim(0, 1)
results = logrank_test(T[dem], T[~dem], event_observed_A=E[dem], event_observed_B=E[~dem])
pvalue = results.p_value
pvalue = round(pvalue,4)
plt.text(30,0.7,"P: "+str(pvalue))
plt.title("HRR+MMR+PAPPA2 PFS\n Somatic+Germline")

