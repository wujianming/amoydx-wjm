#!/usr/bin/env python
# coding: utf-8

# In[52]:


from itertools import combinations
from lifelines import KaplanMeierFitter
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from lifelines.statistics import logrank_test
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import os
get_ipython().run_line_magic('matplotlib', 'inline')
pd.options.mode.chained_assignment = None
os.chdir("/amoydx/USER/wujianming/config/PATHWAY_GENE")


# In[161]:


# NFKB = ['CHUK','FADD','IKBKB','IKBKG','IL1A','IL1R1','MAP3K1','MAP3K14','MAP3K7','MYD88','NFKB1','NFKBIA','RELA','RIPK1','TAB1','TNF','TNFAIP3','TNFRSF1A','TNFRSF1B','TRADD','TRAF6']
# P53 = ['ADGRB1', 'APAF1', 'ATM', 'ATR', 'BAX', 'BBC3', 'BID', 'CASP3', 'CASP8', 'CASP9', 'CCNB1', 'CCNB2', 'CCNB3', 'CCND1', 'CCND2', 'CCND3', 'CCNE1', 'CCNE2', 'CCNG1', 'CCNG2', 'CD82', 'CDK1', 'CDK2', 'CDK4', 'CDK6', 'CDKN1A', 'CDKN2A', 'CHEK1', 'CHEK2', 'COP1', 'CYCS', 'DDB2', 'EI24', 'FAS', 'GADD45A', 'GADD45B', 'GADD45G', 'GTSE1', 'IGF1', 'IGFBP3', 'MDM2', 'MDM4', 'PERP', 'PIDD1', 'PMAIP1', 'PPM1D', 'PTEN', 'RCHY1', 'RPRM', 'RRM2', 'RRM2B', 'SERPINB5', 'SERPINE1', 'SESN1', 'SESN2', 'SESN3', 'SFN', 'SHISA5', 'SIAH1', 'STEAP3', 'THBS1', 'TNFRSF10B', 'TP53', 'TP53AIP1', 'TP53I3', 'TP73', 'TSC2', 'ZMAT3']
# BMP = ['ACVR1', 'ACVR1C', 'ACVR2A', 'ACVR2B', 'ACVRL1', 'AMH', 'AMHR2', 'BMP2', 'BMP4', 'BMP5', 'BMP6', 'BMP7', 'BMP8A', 'BMP8B', 'BMPR1A', 'BMPR1B', 'BMPR2', 'CDKN2B', 'CHRD', 'COMP', 'CREBBP', 'CUL1', 'DCN', 'E2F4', 'E2F5', 'EP300', 'FST', 'GDF5', 'GDF6', 'GDF7', 'ID1', 'ID2', 'ID3', 'ID4', 'IFNG', 'INHBA', 'INHBB', 'INHBC', 'INHBE', 'LEFTY1', 'LEFTY2', 'LTBP1', 'MAPK1', 'MAPK3', 'MYC', 'NODAL', 'NOG', 'PITX2', 'PPP2CA', 'PPP2CB', 'PPP2R1A', 'PPP2R1B', 'RBL1', 'RBL2', 'RBX1', 'RHOA', 'ROCK1', 'ROCK2', 'RPS6KB1', 'RPS6KB2', 'SKP1', 'SMAD1', 'SMAD2', 'SMAD3', 'SMAD4', 'SMAD5', 'SMAD6', 'SMAD7', 'SMAD9', 'SMURF1', 'SMURF2', 'SP1', 'TFDP1', 'TGFB1', 'TGFB2', 'TGFB3', 'TGFBR1', 'TGFBR2', 'THBS1', 'THBS2', 'THBS3', 'THBS4', 'TNF', 'ZFYVE16', 'ZFYVE9']
# JAK = ['AKT1', 'AKT2', 'AKT3', 'BCL2L1', 'CBL', 'CBLB', 'CBLC', 'CCND1', 'CCND2', 'CCND3', 'CISH', 'CLCF1', 'CNTF', 'CNTFR', 'CREBBP', 'CRLF2', 'CSF2', 'CSF2RA', 'CSF2RB', 'CSF3', 'CSF3R', 'CSH1', 'CTF1', 'EP300', 'EPO', 'EPOR', 'GH1', 'GH2', 'GHR', 'GRB2', 'IFNA1', 'IFNA10', 'IFNA13', 'IFNA14', 'IFNA16', 'IFNA17', 'IFNA2', 'IFNA21', 'IFNA4', 'IFNA5', 'IFNA6', 'IFNA7', 'IFNA8', 'IFNAR1', 'IFNAR2', 'IFNB1', 'IFNE', 'IFNG', 'IFNGR1', 'IFNGR2', 'IFNK', 'IFNL1', 'IFNL2', 'IFNL3', 'IFNLR1', 'IFNW1', 'IL10', 'IL10RA', 'IL10RB', 'IL11', 'IL11RA', 'IL12A', 'IL12B', 'IL12RB1', 'IL12RB2', 'IL13', 'IL13RA1', 'IL13RA2', 'IL15', 'IL15RA', 'IL19', 'IL2', 'IL20', 'IL20RA', 'IL20RB', 'IL21', 'IL21R', 'IL22', 'IL22RA1', 'IL22RA2', 'IL23A', 'IL23R', 'IL24', 'IL26', 'IL2RA', 'IL2RB', 'IL2RG', 'IL3', 'IL3RA', 'IL4', 'IL4R', 'IL5', 'IL5RA', 'IL6', 'IL6R', 'IL6ST', 'IL7', 'IL7R', 'IL9', 'IL9R', 'IRF9', 'JAK1', 'JAK2', 'JAK3', 'LEP', 'LEPR', 'LIF', 'LIFR', 'MPL', 'MYC', 'OSM', 'OSMR', 'PIAS1', 'PIAS2', 'PIAS3', 'PIAS4', 'PIK3CA', 'PIK3CB', 'PIK3CD', 'PIK3CG', 'PIK3R1', 'PIK3R2', 'PIK3R3', 'PIK3R5', 'PIM1', 'PRL', 'PRLR', 'PTPN11', 'PTPN6', 'SOCS1', 'SOCS2', 'SOCS3', 'SOCS4', 'SOCS5', 'SOCS7', 'SOS1', 'SOS2', 'SPRED1', 'SPRED2', 'SPRY1', 'SPRY2', 'SPRY3', 'SPRY4', 'STAM', 'STAM2', 'STAT1', 'STAT2', 'STAT3', 'STAT4', 'STAT5A', 'STAT5B', 'STAT6', 'TPO', 'TSLP', 'TYK2']
# MAPK = ['AKT1', 'AKT2', 'AKT3', 'ARRB1', 'ARRB2', 'ATF2', 'ATF4', 'BDNF', 'BRAF', 'CACNA1A', 'CACNA1B', 'CACNA1C', 'CACNA1D', 'CACNA1E', 'CACNA1F', 'CACNA1G', 'CACNA1H', 'CACNA1I', 'CACNA1S', 'CACNA2D1', 'CACNA2D2', 'CACNA2D3', 'CACNA2D4', 'CACNB1', 'CACNB2', 'CACNB3', 'CACNB4', 'CACNG1', 'CACNG2', 'CACNG3', 'CACNG4', 'CACNG5', 'CACNG6', 'CACNG7', 'CACNG8', 'CASP3', 'CD14', 'CDC25B', 'CDC42', 'CHP1', 'CHP2', 'CHUK', 'CRK', 'CRKL', 'DAXX', 'DDIT3', 'DUSP1', 'DUSP10', 'DUSP14', 'DUSP16', 'DUSP2', 'DUSP3', 'DUSP4', 'DUSP5', 'DUSP6', 'DUSP7', 'DUSP8', 'DUSP9', 'ECSIT', 'EGF', 'EGFR', 'ELK1', 'ELK4', 'FAS', 'FASLG', 'FGF1', 'FGF10', 'FGF11', 'FGF12', 'FGF13', 'FGF14', 'FGF16', 'FGF17', 'FGF18', 'FGF19', 'FGF2', 'FGF20', 'FGF21', 'FGF22', 'FGF23', 'FGF3', 'FGF4', 'FGF5', 'FGF6', 'FGF7', 'FGF8', 'FGF9', 'FGFR1', 'FGFR2', 'FGFR3', 'FGFR4', 'FLNA', 'FLNB', 'FLNC', 'FOS', 'GADD45A', 'GADD45B', 'GADD45G', 'GNA12', 'GNG12', 'GRB2', 'HRAS', 'HSPA1A', 'HSPA1B', 'HSPA1L', 'HSPA2', 'HSPA6', 'HSPA8', 'HSPB1', 'IKBKB', 'IKBKG', 'IL1A', 'IL1B', 'IL1R1', 'IL1R2', 'JMJD7-PLA2G4B', 'JUN', 'JUND', 'KRAS', 'LAMTOR3', 'MAP2K1', 'MAP2K2', 'MAP2K3', 'MAP2K4', 'MAP2K5', 'MAP2K6', 'MAP2K7', 'MAP3K1', 'MAP3K11', 'MAP3K12', 'MAP3K13', 'MAP3K14', 'MAP3K2', 'MAP3K20', 'MAP3K3', 'MAP3K4', 'MAP3K5', 'MAP3K6', 'MAP3K7', 'MAP3K8', 'MAP4K1', 'MAP4K2', 'MAP4K3', 'MAP4K4', 'MAPK1', 'MAPK10', 'MAPK11', 'MAPK12', 'MAPK13', 'MAPK14', 'MAPK3', 'MAPK7', 'MAPK8', 'MAPK8IP1', 'MAPK8IP2', 'MAPK8IP3', 'MAPK9', 'MAPKAPK2', 'MAPKAPK3', 'MAPKAPK5', 'MAPT', 'MAX', 'MECOM', 'MEF2C', 'MKNK1', 'MKNK2', 'MOS', 'MRAS', 'MYC', 'NF1', 'NFATC2', 'NFATC4', 'NFKB1', 'NFKB2', 'NGF', 'NLK', 'NR4A1', 'NRAS', 'NTF3', 'NTF4', 'NTRK1', 'NTRK2', 'PAK1', 'PAK2', 'PDGFA', 'PDGFB', 'PDGFRA', 'PDGFRB', 'PLA2G10', 'PLA2G12A', 'PLA2G12B', 'PLA2G1B', 'PLA2G2A', 'PLA2G2C', 'PLA2G2D', 'PLA2G2E', 'PLA2G2F', 'PLA2G3', 'PLA2G4A', 'PLA2G4B', 'PLA2G4E', 'PLA2G5', 'PLA2G6', 'PPM1A', 'PPM1B', 'PPP3CA', 'PPP3CB', 'PPP3CC', 'PPP3R1', 'PPP3R2', 'PPP5C', 'PRKACA', 'PRKACB', 'PRKACG', 'PRKCA', 'PRKCB', 'PRKCG', 'PRKX', 'PTPN5', 'PTPN7', 'PTPRR', 'RAC1', 'RAC2', 'RAC3', 'RAF1', 'RAP1A', 'RAP1B', 'RAPGEF2', 'RASA1', 'RASA2', 'RASGRF1', 'RASGRF2', 'RASGRP1', 'RASGRP2', 'RASGRP3', 'RASGRP4', 'RELA', 'RELB', 'RPS6KA1', 'RPS6KA2', 'RPS6KA3', 'RPS6KA4', 'RPS6KA5', 'RPS6KA6', 'RRAS', 'RRAS2', 'SOS1', 'SOS2', 'SRF', 'STK3', 'STK4', 'STMN1', 'TAB1', 'TAB2', 'TAOK1', 'TAOK2', 'TAOK3', 'TGFB1', 'TGFB2', 'TGFB3', 'TGFBR1', 'TGFBR2', 'TNF', 'TNFRSF1A', 'TP53', 'TRAF2', 'TRAF6']
# MOTR = ['ACACA', 'ACTR2', 'ACTR3', 'ADCY2', 'AKT1', 'AKT1S1', 'AP2M1', 'ARF1', 'ARHGDIA', 'ARPC3', 'ATF1', 'CAB39', 'CAB39L', 'CALR', 'CAMK4', 'CDK1', 'CDK2', 'CDK4', 'CDKN1A', 'CDKN1B', 'CFL1', 'CLTC', 'CSNK2B', 'CXCR4', 'DAPP1', 'DDIT3', 'DUSP3', 'E2F1', 'ECSIT', 'EGFR', 'EIF4E', 'FASLG', 'FGF17', 'FGF22', 'FGF6', 'GNA14', 'GNGT1', 'GRB2', 'GRK2', 'GSK3B', 'HRAS', 'HSP90B1', 'IL2RG', 'IL4', 'IRAK4', 'ITPR2', 'LCK', 'MAP2K3', 'MAP2K6', 'MAP3K7', 'MAPK1', 'MAPK10', 'MAPK8', 'MAPK9', 'MAPKAP1', 'MKNK1', 'MKNK2', 'MYD88', 'NCK1', 'NFKBIB', 'NGF', 'NOD1', 'PAK4', 'PDK1', 'PFN1', 'PIK3R3', 'PIKFYVE', 'PIN1', 'PITX2', 'PLA2G12A', 'PLCB1', 'PLCG1', 'PPP1CA', 'PPP2R1B', 'PRKAA2', 'PRKAG1', 'PRKAR2A', 'PRKCB', 'PTEN', 'PTPN11', 'RAC1', 'RAF1', 'RALB', 'RIPK1', 'RIT1', 'RPS6KA1', 'RPS6KA3', 'RPTOR', 'SFN', 'SLA', 'SLC2A1', 'SMAD2', 'SQSTM1', 'STAT2', 'TBK1', 'THEM4', 'TIAM1', 'TNFRSF1A', 'TRAF2', 'TRIB3', 'TSC2', 'UBE2D3', 'UBE2N', 'VAV3', 'YWHAB']
# WNT = ['APC', 'APC2', 'AXIN1', 'AXIN2', 'BTRC', 'CACYBP', 'CAMK2A', 'CAMK2B', 'CAMK2D', 'CAMK2G', 'CCND1', 'CCND2', 'CCND3', 'CER1', 'CHD8', 'CHP1', 'CHP2', 'CREBBP', 'CSNK1A1', 'CSNK1A1L', 'CSNK1E', 'CSNK2A1', 'CSNK2A2', 'CSNK2B', 'CTBP1', 'CTBP2', 'CTNNB1', 'CTNNBIP1', 'CUL1', 'CXXC4', 'DAAM1', 'DAAM2', 'DKK1', 'DKK2', 'DKK4', 'DVL1', 'DVL2', 'DVL3', 'EP300', 'FBXW11', 'FOSL1', 'FRAT1', 'FRAT2', 'FZD1', 'FZD10', 'FZD2', 'FZD3', 'FZD4', 'FZD5', 'FZD6', 'FZD7', 'FZD8', 'FZD9', 'GSK3B', 'JUN', 'LEF1', 'LRP5', 'LRP6', 'MAP3K7', 'MAPK10', 'MAPK8', 'MAPK9', 'MMP7', 'MYC', 'NFAT5', 'NFATC1', 'NFATC2', 'NFATC3', 'NFATC4', 'NKD1', 'NKD2', 'NLK', 'PLCB1', 'PLCB2', 'PLCB3', 'PLCB4', 'PORCN', 'PPARD', 'PPP2CA', 'PPP2CB', 'PPP2R1A', 'PPP2R1B', 'PPP2R5A', 'PPP2R5B', 'PPP2R5C', 'PPP2R5D', 'PPP2R5E', 'PPP3CA', 'PPP3CB', 'PPP3CC', 'PPP3R1', 'PPP3R2', 'PRICKLE1', 'PRICKLE2', 'PRKACA', 'PRKACB', 'PRKACG', 'PRKCA', 'PRKCB', 'PRKCG', 'PRKX', 'PSEN1', 'RAC1', 'RAC2', 'RAC3', 'RBX1', 'RHOA', 'ROCK1', 'ROCK2', 'RUVBL1', 'SENP2', 'SFRP1', 'SFRP2', 'SFRP4', 'SFRP5', 'SIAH1', 'SKP1', 'SMAD2', 'SMAD3', 'SMAD4', 'SOX17', 'TBL1X', 'TBL1XR1', 'TBL1Y', 'TCF7', 'TCF7L1', 'TCF7L2', 'TP53', 'VANGL1', 'VANGL2', 'WIF1', 'WNT1', 'WNT10A', 'WNT10B', 'WNT11', 'WNT16', 'WNT2', 'WNT2B', 'WNT3', 'WNT3A', 'WNT4', 'WNT5A', 'WNT5B', 'WNT6', 'WNT7A', 'WNT7B', 'WNT8A', 'WNT8B', 'WNT9A', 'WNT9B']
# HRR = ['ATM','BARD1','BLM','BRCA1','BRCA2','BRIP1','CHEK2','FANCA','FANCC','FANCD2','FANCE','FANCF','FANCG','FANCI','FANCL','FANCM','MRE11A','NBN','PALB2','RAD50','RAD51','RAD51B','RAD51C','RAD51D','RAD54L']
# MMR = ['EXO1', 'LIG1', 'MLH1', 'MLH3', 'MSH2', 'MSH3', 'MSH6', 'PCNA', 'PMS2', 'POLD1', 'POLD2', 'POLD3', 'POLD4', 'RFC1', 'RFC2', 'RFC3', 'RFC4', 'RFC5', 'RPA1', 'RPA2', 'RPA3', 'RPA4', 'SSBP1']

# NFKB = ['MAP3K14', 'TNFAIP3', 'MYD88', 'MAP3K1', 'NFKBIA']
# P53 = ['ATR', 'CDKN1A', 'CHEK2', 'FAS', 'MDM2', 'CCNE1', 'CDKN2A', 'PTEN', 'IGF1', 'CDK6', 'CASP8', 'TSC2', 'CCND3', 'CDK4', 'TP53', 'CCND2', 'ATM', 'MDM4', 'CHEK1', 'CCND1']
# BMP = ['INHBA', 'BMPR1A', 'TGFBR2', 'CREBBP', 'MYC', 'RHOA', 'SMAD3', 'RPS6KB1', 'EP300', 'PPP2R1A', 'MAPK1', 'RPS6KB2', 'SMAD4', 'CDKN2B', 'SMAD2', 'MAPK3']
# JAK = ['MPL', 'STAT3', 'PIK3CB', 'SOCS1', 'PTPN11', 'CBL', 'AKT3', 'BCL2L1', 'PIK3CD', 'CSF3R', 'PIK3CG', 'MYC', 'PIM1', 'PIK3CA', 'CCND3', 'STAT4', 'AKT2', 'CCND1', 'IL7R', 'PIK3R1', 'JAK2', 'CRLF2', 'EP300', 'CCND2', 'JAK1', 'AKT1', 'JAK3', 'CREBBP', 'PIK3R2']
# MAPK = ['MAP3K14', 'FGF14', 'RASA1', 'FGFR3', 'FGFR4', 'MAP2K2', 'FGF19', 'PDGFRA', 'FGF10', 'MAP2K1', 'TP53', 'PAK1', 'MAP3K13', 'PRKACA', 'NF1', 'RAF1', 'NTRK1', 'MAX', 'NRAS', 'AKT3', 'CRKL', 'MYC', 'FGFR2', 'FGF4', 'TAOK1', 'TGFBR2', 'AKT2', 'MAP2K4', 'EGFR', 'HRAS', 'NTRK2', 'FGFR1', 'RAC1', 'JUN', 'PDGFRB', 'BRAF', 'FGF6', 'FGF3', 'MAPK3', 'AKT1', 'KRAS', 'FAS', 'DAXX', 'MAPK1', 'FGF23', 'FGF7', 'MAP3K1']
# MOTR = ['PTEN', 'RPTOR', 'RIT1', 'PTPN11', 'RAF1', 'CALR', 'CDKN1B', 'MYD88', 'SMAD2', 'EGFR', 'GSK3B', 'CDKN1A', 'HRAS', 'LCK', 'PDK1', 'RAC1', 'CDK4', 'FGF6', 'AKT1', 'TSC2', 'MAPK1']
# WNT = ['AXIN1', 'TP53', 'TCF7L2', 'SMAD4', 'PRKACA', 'PPP2R1A', 'MYC', 'SMAD3', 'CCND3', 'APC', 'SMAD2', 'CCND1', 'GSK3B', 'AXIN2', 'CTNNB1', 'RHOA', 'EP300', 'RAC1', 'JUN', 'CCND2', 'TCF7L1', 'SOX17', 'CREBBP']
# HRR =['BRCA2', 'RAD54L', 'RAD51B', 'RAD51', 'NBN', 'RAD50', 'POLD1', 'RAD51C', 'RAD51D', 'BLM', 'RAD52']
# MMR = ['MSH2', 'MSH6', 'MSH3', 'POLD1', 'MLH3', 'PMS2', 'MLH1']

P53 = ['TP53', 'PTEN']
JAK = [ 'PIK3CA','JAK1']
MAPK = [ 'TP53', 'EGFR', 'KRAS']
MOTR = ['CALR', 'EGFR', 'PTEN']
WNT = ['TP53']
HRR =['BRCA2', 'BLM']
MMR = ['MSH6']


# In[97]:


raw_clinical_data1 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/PMID_29657128_lung/mmc2-clinical.xlsx','Table S2',index_col = None,na_values= ['9999'],skiprows =[0,1])
mut_data1 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/PMID_29657128_lung/mmc3-mutations.xlsx','Variant list_IpiNivo',index_col = None,na_values= ['9999'],skiprows =[0,1])


# In[162]:


clinical_data1 = raw_clinical_data1[['Patient ID','PFS','Status','Response','Nonsynonymous_Mut','Predicted neoantigen burden']]
clinical_data1['Response_Status'] = clinical_data1['Response'].apply(lambda x: 0 if x == 'PD' else 1)
clinical_data1['mutation_Nor'] =  (clinical_data1['Nonsynonymous_Mut'] - min(clinical_data1['Nonsynonymous_Mut'])) / (max(clinical_data1['Nonsynonymous_Mut']) - min(clinical_data1['Nonsynonymous_Mut']))
#neoantigen
clinical_data1['Neo_Nor'] =  (clinical_data1['Predicted neoantigen burden'] - min(clinical_data1['Predicted neoantigen burden'])) / (max(clinical_data1['Predicted neoantigen burden']) - min(clinical_data1['Predicted neoantigen burden']))
clinical_data1.sort_values(['Patient ID'],inplace=True)
sampleList1 = clinical_data1['Patient ID'].tolist()
f = open("pathway.result.txt",'w')
f.write('Patient ID'+'\t'+'NFKB'+'\t'+'P53'+'\t'+'BMP'+'\t'+'JAK'+'\t'+'MAPK'+'\t'+'MOTR'+'\t'+'WNT'+'\t'+'HRR'+'\t'+'MMR'+'\n')
for each_sample in sampleList1:
    mutationGeneList = mut_data1[mut_data1['Patient ID'] == each_sample]['gene_name'].tolist()
    NFKB_res = list(set(mutationGeneList).intersection(NFKB))
    if len(NFKB_res) > 0:
        NFKB_label = 1
    else:
        NFKB_label = 0
    P53_res = list(set(mutationGeneList).intersection(P53))
    if len(P53_res) > 0:
        P53_label = 1
    else:
        P53_label = 0
    BMP_res = list(set(mutationGeneList).intersection(BMP))
    if len(BMP_res) > 0:
        BMP_label = 1
    else:
        BMP_label = 0
    JAK_res = list(set(mutationGeneList).intersection(JAK))
    if len(JAK_res) > 0:
        JAK_label = 1
    else:
        JAK_label = 0
    MAPK_res = list(set(mutationGeneList).intersection(MAPK))
    if len(MAPK_res) > 0:
        MAPK_label = 1
    else:
        MAPK_label = 0
    MOTR_res = list(set(mutationGeneList).intersection(MOTR))
    if len(MOTR_res) > 0:
        MOTR_label = 1
    else:
        MOTR_label = 0
    WNT_res = list(set(mutationGeneList).intersection(WNT))
    if len(WNT_res) > 0:
        WNT_label = 1
    else:
        WNT_label = 0
    HRR_res = list(set(mutationGeneList).intersection(HRR))
    if len(HRR_res) > 0:
        HRR_label = 1
    else:
        HRR_label = 0
    MMR_res = list(set(mutationGeneList).intersection(MMR))
    if len(MMR_res) > 0:
        MMR_label = 1
    else:
        MMR_label = 0
    f.write(str(each_sample)+'\t'+str(NFKB_label)+'\t'+str(P53_label)+'\t'+str(BMP_label)+'\t'+str(JAK_label)+'\t'+str(MAPK_label)+'\t'+str(MOTR_label)+'\t'+str(WNT_label)+'\t'+str(HRR_label)+'\t'+str(MMR_label)+'\n')
f.close()
pathway_data = pd.read_table('pathway.result.txt',sep='\t')
merge_dataSet1 = pd.merge(clinical_data1,pathway_data,on = "Patient ID")
merge_dataSet1 = merge_dataSet1[['Patient ID','Response','Response_Status','mutation_Nor','Neo_Nor','NFKB','P53','BMP','JAK','MAPK','MOTR','WNT','HRR','MMR']]


# In[163]:


clinical_patient2  = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/From_cBIoPortal/cBioPortal-LUAD-MSKCC-Science-2015/data_clinical_patient.txt",sep='\t',skiprows=[0,1,2,3])
clinical_patient2 = clinical_patient2[['PATIENT_ID','PFS_MONTHS','EVENT_TYPE','TREATMENT_BEST_RESPONSE','NONSYNONYMOUS_MUTATION_BURDEN','NEOANTIGEN_BURDEN']]
clinical_sample2 = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/From_cBIoPortal/cBioPortal-LUAD-MSKCC-Science-2015/data_clinical_sample.txt",sep='\t',skiprows=[0,1,2,3])
clinical_sample2 = clinical_sample2[['PATIENT_ID','SAMPLE_ID']]
clinical_merge2 =  pd.merge(clinical_patient2,clinical_sample2,on='PATIENT_ID')
mut_data2 = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/From_cBIoPortal/cBioPortal-LUAD-MSKCC-Science-2015/data_mutations_mskcc.txt",sep='\t')
filter_list2 = ['RNA','Intron',"5'UTR","3'UTR","3'Flank","5'Flank"]
mut_data2 = mut_data2[~mut_data2['Variant_Classification'].isin(filter_list2)]
clinical_merge2['Response_Status'] = clinical_merge2['TREATMENT_BEST_RESPONSE'].apply(lambda x: 0 if x == 'POD' else 1)
clinical_merge2['mutation_Nor'] =  (clinical_merge2['NONSYNONYMOUS_MUTATION_BURDEN'] - min(clinical_merge2['NONSYNONYMOUS_MUTATION_BURDEN'])) / (max(clinical_merge2['NONSYNONYMOUS_MUTATION_BURDEN']) - min(clinical_merge2['NONSYNONYMOUS_MUTATION_BURDEN']))
#neoantigen
clinical_merge2['Neo_Nor'] =  (clinical_merge2['NEOANTIGEN_BURDEN'] - min(clinical_merge2['NEOANTIGEN_BURDEN'])) / (max(clinical_merge2['NEOANTIGEN_BURDEN']) - min(clinical_merge2['NEOANTIGEN_BURDEN']))


# In[164]:


sampleList2 = clinical_merge2['SAMPLE_ID'].tolist()
f2 = open("pathway.data2.result.txt",'w')
f2.write('SAMPLE_ID'+'\t'+'NFKB'+'\t'+'P53'+'\t'+'BMP'+'\t'+'JAK'+'\t'+'MAPK'+'\t'+'MOTR'+'\t'+'WNT'+'\t'+'HRR'+'\t'+'MMR'+'\n')
for each_sample in sampleList2:
    mutationGeneList = mut_data2[mut_data2['Tumor_Sample_Barcode'] == each_sample]['Hugo_Symbol'].tolist()
    NFKB_res = list(set(mutationGeneList).intersection(NFKB))
    if len(NFKB_res) > 0:
        NFKB_label = 1
    else:
        NFKB_label = 0
    P53_res = list(set(mutationGeneList).intersection(P53))
    if len(P53_res) > 0:
        P53_label = 1
    else:
        P53_label = 0
    BMP_res = list(set(mutationGeneList).intersection(BMP))
    if len(BMP_res) > 0:
        BMP_label = 1
    else:
        BMP_label = 0
    JAK_res = list(set(mutationGeneList).intersection(JAK))
    if len(JAK_res) > 0:
        JAK_label = 1
    else:
        JAK_label = 0
    MAPK_res = list(set(mutationGeneList).intersection(MAPK))
    if len(MAPK_res) > 0:
        MAPK_label = 1
    else:
        MAPK_label = 0
    MOTR_res = list(set(mutationGeneList).intersection(MOTR))
    if len(MOTR_res) > 0:
        MOTR_label = 1
    else:
        MOTR_label = 0
    WNT_res = list(set(mutationGeneList).intersection(WNT))
    if len(WNT_res) > 0:
        WNT_label = 1
    else:
        WNT_label = 0
    HRR_res = list(set(mutationGeneList).intersection(HRR))
    if len(HRR_res) > 0:
        HRR_label = 1
    else:
        HRR_label = 0
    MMR_res = list(set(mutationGeneList).intersection(MMR))
    if len(MMR_res) > 0:
        MMR_label = 1
    else:
        MMR_label = 0
    f2.write(str(each_sample)+'\t'+str(NFKB_label)+'\t'+str(P53_label)+'\t'+str(BMP_label)+'\t'+str(JAK_label)+'\t'+str(MAPK_label)+'\t'+str(MOTR_label)+'\t'+str(WNT_label)+'\t'+str(HRR_label)+'\t'+str(MMR_label)+'\n')
f2.close()


# In[165]:


pathway_data2 = pd.read_table('pathway.data2.result.txt',sep='\t')
merge_dataSet2 = pd.merge(pathway_data2,clinical_merge2,on='SAMPLE_ID')
merge_dataSet2 = merge_dataSet2[['SAMPLE_ID','TREATMENT_BEST_RESPONSE','Response_Status','mutation_Nor','Neo_Nor','NFKB','P53','BMP','JAK','MAPK','MOTR','WNT','HRR','MMR']]
merge_dataSet2.columns = ['Patient ID','Response','Response_Status','mutation_Nor','Neo_Nor','NFKB','P53','BMP','JAK','MAPK','MOTR','WNT','HRR','MMR']


# In[166]:


Two_Data_set = pd.concat([merge_dataSet1,merge_dataSet2])


# In[173]:


#feaName = 'mutation_Nor+Neo_Nor+BARD1+BLM+MSH6+CALR+BRCA2+JAK1+STK11+PTEN+SMARCA4+ARID2+PBRM1+EGFR+DNMT3A+PAPPA2+KRAS+TP53'
#feaList = feaName.split('+')
feaList = ['mutation_Nor','Neo_Nor','NFKB','P53','BMP','JAK','MAPK','MOTR','WNT','HRR','MMR']
#feaList = ['mutation_Nor','Neo_Nor','WNT','HRR','MMR']
X = np.array(Two_Data_set[feaList])
y = np.array(Two_Data_set['Response_Status'])
#构建训练集和测试集

X_train,X_test, y_train, y_test = train_test_split(X, y, random_state=31,test_size=0.2)
clf = LogisticRegression(solver='liblinear',class_weight='balanced').fit(X_train,y_train)
pre_test = clf.predict(X_test)
auc_score = roc_auc_score(y_test, pre_test)
print(str(auc_score))
# print(clf.coef_[0])
# print(clf.intercept_)
# from sklearn.model_selection import GridSearchCV
# from sklearn import svm
# parameters = {'solver':['liblinear']}
# svc = LogisticRegression()
# clf = GridSearchCV(svc, parameters, cv=5)
# clf.fit(X_train,y_train)
# pre_test = clf.predict_proba(X_test)
# roc_auc_score(y_true=y_test, y_score=pre_test[:,1])
##score 0.775

##RandomForest
# from sklearn.ensemble import RandomForestClassifier
# parameters = {'n_estimators':[4,5,6],'max_depth':[3,4,5]}
# svc = RandomForestClassifier()
# clf = GridSearchCV(svc, parameters, cv=5)
# clf.fit(X_train,y_train)
# pre_test = clf.predict_proba(X_test)
# print(roc_auc_score(y_true=y_test, y_score=pre_test[:,1]))


# # 39例tumor-normal作为测试集

# In[108]:


raw_TMB_sheet_39 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])
raw_TMB_sheet2_39 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])
raw_data_39_germline = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelGermline',index_col = None,na_values= ['9999'])


# In[168]:


#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["3'UTR","5'UTR","Intronic","FlankingRegion5"]
tag_filter = ['PASS']
TMB_sheet_39 = raw_TMB_sheet_39[~raw_TMB_sheet_39['Type'].isin(Filter_list)]
TMB_sheet_39 = TMB_sheet_39[TMB_sheet_39['Tag'].isin(tag_filter)]
#Freq过滤5%
TMB_sheet_39 = TMB_sheet_39[TMB_sheet_39['Freq'] >= 0.05]
TMB_sheet_39 = TMB_sheet_39[['Sample','Type','Tag','Gene']]

Tag_hot_filter = ['PASS;HotSpot']
TMB_sheet2_39 = raw_TMB_sheet2_39[raw_TMB_sheet2_39['Tag'].isin(Tag_hot_filter)]
TMB_sheet2_39 = TMB_sheet2_39[~TMB_sheet2_39['Type'].isin(Filter_list)]
TMB_sheet2_39 = TMB_sheet2_39[TMB_sheet2_39['Freq'] >= 0.01]
TMB_sheet2_39 = TMB_sheet2_39[['Sample','Type','Tag','Gene']]
data_39_germline = raw_data_39_germline[['Sample','Type','Tag','Gene']]
data_39_germline = data_39_germline[data_39_germline['Gene'].isin(HRR)]
TMB_merge2 = pd.concat([TMB_sheet_39,TMB_sheet2_39,data_39_germline])
#TMB_merge2 = pd.concat([TMB_sheet_39,TMB_sheet2_39])
#Mutations
Mutations_39 = TMB_merge2.groupby('Sample',as_index=False).count()[['Sample','Type']]
Mutations_39.columns = ['Sample','Mutations']
nomut_data = [{'Sample':'190713-25T','Mutations':0},
              {'Sample':'190713-33T','Mutations':0},
              {'Sample':'20141904H01LF1','Mutations':0},
              {'Sample':'20173679H01TF1','Mutations':0}]
df = pd.DataFrame.from_dict(nomut_data)
Mutations_39_plus4 = pd.concat([Mutations_39,df])

Neo = pd.read_csv('/amoydx/USER/wujianming/project/Jupyter/IO1-2/Neoantigens/Neoanti.tumor.normal.csv',sep=',')
Mut_Neo = pd.merge(Mutations_39_plus4,Neo,left_on='Sample',right_on='SampleName')

Mut_Neo = Mut_Neo[['Sample','Mutations','Type','Response2','SNV_5']]
Mut_Neo.columns = ['Sample','Mutations','Type','Response','Neo']
Mut_Neo['mutation_Nor'] =  (Mut_Neo['Mutations'] - min(Mut_Neo['Mutations'])) / (max(Mut_Neo['Mutations']) - min(Mut_Neo['Mutations']))
Mut_Neo['Neo_Nor'] =  (Mut_Neo['Neo'] - min(Mut_Neo['Neo'])) / (max(Mut_Neo['Neo']) - min(Mut_Neo['Neo']))


# In[169]:


sampleList_test = Mut_Neo['Sample'].tolist()
f2 = open("pathway.39TuNo.result.txt",'w')
f2.write('Sample'+'\t'+'NFKB'+'\t'+'P53'+'\t'+'BMP'+'\t'+'JAK'+'\t'+'MAPK'+'\t'+'MOTR'+'\t'+'WNT'+'\t'+'HRR'+'\t'+'MMR'+'\n')
for each_sample in sampleList_test:
    mutationGeneList = TMB_merge2[TMB_merge2['Sample'] == each_sample]['Gene'].tolist()
    NFKB_res = list(set(mutationGeneList).intersection(NFKB))
    if len(NFKB_res) > 0:
        NFKB_label = 1
    else:
        NFKB_label = 0
    P53_res = list(set(mutationGeneList).intersection(P53))
    if len(P53_res) > 0:
        P53_label = 1
    else:
        P53_label = 0
    BMP_res = list(set(mutationGeneList).intersection(BMP))
    if len(BMP_res) > 0:
        BMP_label = 1
    else:
        BMP_label = 0
    JAK_res = list(set(mutationGeneList).intersection(JAK))
    if len(JAK_res) > 0:
        JAK_label = 1
    else:
        JAK_label = 0
    MAPK_res = list(set(mutationGeneList).intersection(MAPK))
    if len(MAPK_res) > 0:
        MAPK_label = 1
    else:
        MAPK_label = 0
    MOTR_res = list(set(mutationGeneList).intersection(MOTR))
    if len(MOTR_res) > 0:
        MOTR_label = 1
    else:
        MOTR_label = 0
    WNT_res = list(set(mutationGeneList).intersection(WNT))
    if len(WNT_res) > 0:
        WNT_label = 1
    else:
        WNT_label = 0
    HRR_res = list(set(mutationGeneList).intersection(HRR))
    if len(HRR_res) > 0:
        HRR_label = 1
    else:
        HRR_label = 0
    MMR_res = list(set(mutationGeneList).intersection(MMR))
    if len(MMR_res) > 0:
        MMR_label = 1
    else:
        MMR_label = 0
    f2.write(str(each_sample)+'\t'+str(NFKB_label)+'\t'+str(P53_label)+'\t'+str(BMP_label)+'\t'+str(JAK_label)+'\t'+str(MAPK_label)+'\t'+str(MOTR_label)+'\t'+str(WNT_label)+'\t'+str(HRR_label)+'\t'+str(MMR_label)+'\n')
f2.close()
pathway_test = pd.read_table('pathway.39TuNo.result.txt',sep='\t')
merge_dataSet_test = pd.merge(pathway_test,Mut_Neo,on='Sample')


# In[170]:


feaList2 = ['mutation_Nor','Neo_Nor','NFKB','P53','BMP','JAK','MAPK','MOTR','WNT','HRR','MMR']
#feaList2 = ['mutation_Nor','Neo_Nor','WNT','HRR','MMR']
testing_X = np.array(merge_dataSet_test[feaList2])
testing_Y = np.array(merge_dataSet_test['Response'])
pre_test2 = clf.predict_proba(testing_X)
print(roc_auc_score(testing_Y, pre_test2[:,1]))


# In[57]:


merge_dataSet_test.head(1)

