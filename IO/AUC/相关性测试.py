#!/usr/bin/env python
# coding: utf-8

# # 39例paired TMB计算方式为同义突变+非HotSpot

# In[5]:


from sklearn.metrics import roc_auc_score
import pandas as pd
import xlrd
import os


# In[19]:



clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')

TMB_sheet = pd.read_excel('20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])

#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["3'UTR","5'UTR","Intronic"]
TMB_sheet = TMB_sheet[~TMB_sheet['Type'].isin(Filter_list)]
#Freq过滤5%
TMB_sheet = TMB_sheet[TMB_sheet['Freq'] >= 0.05]



TMB_count = TMB_sheet.groupby('Sample',as_index=None).count()
TMB_count['TMB_value_1.46'] = TMB_count['Library'] / 1.46
TMB_count['TMB_value_1.16'] = TMB_count['Library'] / 1.16
TMB_value = TMB_count[['Sample','TMB_value_1.46','TMB_value_1.16']]
sampleList = []
with open('sampleList','r') as FA:
    for line in FA:
        line = line.strip()
        if (line in TMB_value['Sample'].values) == False:
            append_data = pd.DataFrame({'Sample':line,'TMB_value_1.46':0,'TMB_value_1.16':0},index=[1])
            TMB_value=TMB_value.append(append_data,ignore_index=True)
mergeData2 = pd.merge(merge_data,TMB_value,left_on = '厦维编号',right_on = 'Sample')
cutPoint = 10
mergeData2['TMB_Type_1.16'] = mergeData2['TMB_value_1.16'].apply(lambda x: 'TMB High' if x >= 10 else 'TMB Low')
mergeData2['TMB_Type_1.46'] = mergeData2['TMB_value_1.46'].apply(lambda x: 'TMB High' if x >= 10 else 'TMB Low')



Result = mergeData2[['患者姓名','最好疗效','Response','厦维编号','TMB_value_1.16','TMB_Type_1.16','TMB_value_1.46','TMB_Type_1.46']]

#Result.groupby(['Response','TMB Type'],as_index=False).count()
#mergeData2.to_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/Batch1.TMB.39.value.txt",sep='\t',index=False)

from sklearn.metrics import roc_auc_score
y_true = mergeData2['Response']
y_scores = mergeData2['TMB_value_1.46']
roc_auc_score(y_true, y_scores)    

# import matplotlib.pyplot as plt
# import seaborn as sns
# %matplotlib inline
# #sns.boxplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'])
# ax = sns.boxplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'])
# ax = sns.swarmplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'], color=".25")


# # 39例paired TMB计算方式为排除同义突变+ 包括hotSpot

# In[244]:


import pandas as pd
import xlrd
import os

clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')

TMB_sheet = pd.read_excel('20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])

#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["3'UTR","5'UTR","Intronic","Synonymous_Substitution"]
TMB_sheet = TMB_sheet[~TMB_sheet['Type'].isin(Filter_list)]
#Freq过滤5%
TMB_sheet = TMB_sheet[TMB_sheet['Freq'] >= 0.05]
TMB_sheet = TMB_sheet[['Sample','Type','Tag','Gene']]

Tag_filter = ['Germline','Polymorphism']
TMB_sheet2 = pd.read_excel('20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])
TMB_sheet2 = TMB_sheet2[~TMB_sheet2['Tag'].isin(Tag_filter)]
TMB_sheet2 = TMB_sheet2[TMB_sheet2['Freq'] >= 0.01]
TMB_sheet2 = TMB_sheet2[['Sample','Type','Tag','Gene']]
TMB_merge = pd.concat([TMB_sheet,TMB_sheet2])

TMB_count = TMB_merge.groupby('Sample',as_index=None).count()
TMB_count['TMB_value_1.46'] = TMB_count['Gene'] / 1.46
TMB_count['TMB_value_1.16'] = TMB_count['Gene'] / 1.16
TMB_value = TMB_count[['Sample','TMB_value_1.46','TMB_value_1.16']]
sampleList = []
with open('sampleList','r') as FA:
    for line in FA:
        line = line.strip()
        if (line in TMB_value['Sample'].values) == False:
            append_data = pd.DataFrame({'Sample':line,'TMB_value_1.46':0,'TMB_value_1.16':0},index=[1])
            TMB_value=TMB_value.append(append_data,ignore_index=True)
mergeData2 = pd.merge(merge_data,TMB_value,left_on = '厦维编号',right_on = 'Sample')
cutPoint = 10
mergeData2['TMB_Type_1.16'] = mergeData2['TMB_value_1.16'].apply(lambda x: 'TMB High' if x >= 10 else 'TMB Low')
mergeData2['TMB_Type_1.46'] = mergeData2['TMB_value_1.46'].apply(lambda x: 'TMB High' if x >= 10 else 'TMB Low')



Result = mergeData2[['患者姓名','最好疗效','Response','厦维编号','TMB_value_1.16','TMB_Type_1.16','TMB_value_1.46','TMB_Type_1.46']]

#Result.groupby(['Response','TMB Type'],as_index=False).count()
mergeData2.to_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/Batch1.TMB.value.39.old.compute.txt",sep='\t',index=False)

from sklearn.metrics import roc_auc_score
y_true = mergeData2['Response']
y_scores = mergeData2['TMB_value_1.46']
roc_auc_score(y_true, y_scores)    


# In[245]:


import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
#sns.boxplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'])
ax = sns.boxplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'])
ax = sns.swarmplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'], color=".25")


# # 55例only tumor TMB计算方式为同义突变+非HotSpot

# In[6]:


import pandas as pd
import xlrd
import os

clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')

TMB_sheet = pd.read_excel('20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])

#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["3'UTR","5'UTR","Intronic",'FlankingRegion5']
tag_filter = ['PASS']
TMB_sheet = TMB_sheet[~TMB_sheet['Type'].isin(Filter_list)]
#Freq过滤5%
TMB_sheet = TMB_sheet[TMB_sheet['Freq'] >= 0.05]
TMB_sheet = TMB_sheet[TMB_sheet['Tag'].isin(tag_filter)]


TMB_count = TMB_sheet.groupby('Sample',as_index=None).count()
TMB_count['TMB_value_1.46'] = TMB_count['Freq'] / 1.46
TMB_count['TMB_value_1.16'] = TMB_count['Freq'] / 1.16
TMB_value = TMB_count[['Sample','TMB_value_1.46','TMB_value_1.16']]
sampleList = []
with open('sampleList','r') as FA:
    for line in FA:
        line = line.strip()
        if (line in TMB_value['Sample'].values) == False:
            append_data = pd.DataFrame({'Sample':line,'TMB_value_1.46':0,'TMB_value_1.16':0},index=[1])
            TMB_value=TMB_value.append(append_data,ignore_index=True)
mergeData2 = pd.merge(merge_data,TMB_value,left_on = '厦维编号',right_on = 'Sample')
cutPoint = 10
mergeData2['TMB_Type_1.16'] = mergeData2['TMB_value_1.16'].apply(lambda x: 'TMB High' if x >= 10 else 'TMB Low')
mergeData2['TMB_Type_1.46'] = mergeData2['TMB_value_1.46'].apply(lambda x: 'TMB High' if x >= 10 else 'TMB Low')



Result = mergeData2[['患者姓名','最好疗效','Response','厦维编号','TMB_value_1.16','TMB_Type_1.16','TMB_value_1.46','TMB_Type_1.46']]

#Result.groupby(['Response','TMB Type'],as_index=False).count()
mergeData2.to_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/Batch1.TMB.value.55.txt",sep='\t',index=False)

from sklearn.metrics import roc_auc_score
y_true = mergeData2['Response']
y_scores = mergeData2['TMB_value_1.46']
roc_auc_score(y_true, y_scores)    

# import matplotlib.pyplot as plt
# import seaborn as sns
# %matplotlib inline
# #sns.boxplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'])
# ax = sns.boxplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'])
# ax = sns.swarmplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'], color=".25")


# #  55例only tumor TMB计算方式为排除同义突变+ 包括hotSpot

# In[249]:


import pandas as pd
import xlrd
import os

clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')

TMB_sheet = pd.read_excel('20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])

#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["3'UTR","5'UTR","Intronic","Synonymous_Substitution",'FlankingRegion5']
tag_filter = ['PASS']
TMB_sheet = TMB_sheet[~TMB_sheet['Type'].isin(Filter_list)]
TMB_sheet = TMB_sheet[TMB_sheet['Tag'].isin(tag_filter)]
#Freq过滤5%
TMB_sheet = TMB_sheet[TMB_sheet['Freq'] >= 0.05]
TMB_sheet = TMB_sheet[['Sample','Type','Tag','Gene']]

Tag_filter = ['Germline','Polymorphism']
TMB_sheet2 = pd.read_excel('20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])
TMB_sheet2 = TMB_sheet2[~TMB_sheet2['Tag'].isin(Tag_filter)]
TMB_sheet2 = TMB_sheet2[TMB_sheet2['Freq'] >= 0.01]
TMB_sheet2 = TMB_sheet2[['Sample','Type','Tag','Gene']]
TMB_merge = pd.concat([TMB_sheet,TMB_sheet2])

TMB_count = TMB_merge.groupby('Sample',as_index=None).count()
TMB_count['TMB_value_1.46'] = TMB_count['Gene'] / 1.46
TMB_count['TMB_value_1.16'] = TMB_count['Gene'] / 1.16
TMB_value = TMB_count[['Sample','TMB_value_1.46','TMB_value_1.16']]
sampleList = []
with open('sampleList','r') as FA:
    for line in FA:
        line = line.strip()
        if (line in TMB_value['Sample'].values) == False:
            append_data = pd.DataFrame({'Sample':line,'TMB_value_1.46':0,'TMB_value_1.16':0},index=[1])
            TMB_value=TMB_value.append(append_data,ignore_index=True)
mergeData2 = pd.merge(merge_data,TMB_value,left_on = '厦维编号',right_on = 'Sample')
cutPoint = 10
mergeData2['TMB_Type_1.16'] = mergeData2['TMB_value_1.16'].apply(lambda x: 'TMB High' if x >= 10 else 'TMB Low')
mergeData2['TMB_Type_1.46'] = mergeData2['TMB_value_1.46'].apply(lambda x: 'TMB High' if x >= 10 else 'TMB Low')



Result = mergeData2[['患者姓名','最好疗效','Response','厦维编号','TMB_value_1.16','TMB_Type_1.16','TMB_value_1.46','TMB_Type_1.46']]

#Result.groupby(['Response','TMB Type'],as_index=False).count()
mergeData2.to_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/Batch1.TMB.value.55.old.compute.txt",sep='\t',index=False)

from sklearn.metrics import roc_auc_score
y_true = mergeData2['Response']
y_scores = mergeData2['TMB_value_1.46']
roc_auc_score(y_true, y_scores)    


# In[250]:


import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
#sns.boxplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'])
ax = sns.boxplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'])
ax = sns.swarmplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'], color=".25")


# # Mutations IO

# In[269]:


import xlrd
f = open('Samples.Protein.txt','w')
f.write("Sample"+'\t'+"Gene"+'\t'+"Type"+'\t'+'HGVSp'+'\n')
book = xlrd.open_workbook('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx')
sheet = book.sheet_by_name('SNVIndelSomatic')
for each in range(sheet.nrows):
    rows = sheet.row_values(each)
    if 'Sample' in rows[0]:
        continue
    #print(rows[0]+'\t'+rows[12]+'\t'+rows[13]+'\t'+rows[14])
    if float(rows[10]) < 0.05 or "Intronic" in rows[13] or "UTR" in rows[13]:
        continue
    P = rows[14].split(':')[-1]
    f.write(rows[0]+'\t'+rows[12]+'\t'+rows[13]+'\t'+rows[12]+' '+P+'\n')
sheet2 = book.sheet_by_name('SNVIndelHotSpot')
for each in range(sheet2.nrows):
    lines = sheet2.row_values(each)
    if 'Sample' in lines[0]:
        continue
    #print(rows[0]+'\t'+rows[12]+'\t'+rows[13]+'\t'+rows[14])
    if float(lines[10]) < 0.05 or "Intronic" in lines[13] or "UTR" in lines[13] or 'Germline' in lines[11]:
        continue
    P2 = lines[14].split(':')[-1]
    f.write(lines[0]+'\t'+lines[12]+'\t'+lines[13]+'\t'+lines[12]+' '+P2+'\n')
f.close()


# In[270]:


mutInfo = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/IO1-2/Samples.Protein.txt",sep='\t')


# In[271]:


PRSR_Group = Result[(Result['最好疗效'] == 'PR')|(Result['最好疗效'] == 'SR')|(Result['最好疗效'] == 'SD')]['厦维编号'].tolist()
PRSR_data = mutInfo[mutInfo['Sample'].isin(PRSR_Group)]
PD_Group = Result[Result['最好疗效'] == 'PD']['厦维编号'].tolist()
PD_data = mutInfo[mutInfo['Sample'].isin(PD_Group)]


# In[272]:


PD_data_gene_count = PD_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
PD_data_gene_count.columns = ['Gene','PD']
PRSR_data_gene_count = PRSR_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
PRSR_data_gene_count.columns = ['Gene','PR-SR-SD']
mergeData = pd.merge(PD_data_gene_count,PRSR_data_gene_count,on='Gene',how='outer')
mergeData = mergeData.fillna(0)
mergeData = mergeData[(mergeData['PR-SR-SD'] - mergeData['PD'] >= 2)|(mergeData['PR-SR-SD'] - mergeData['PD'] <= -2)]
mergeData.to_csv('IO.Gene.Count.39.txt',sep='\t',index=False)

PD_data_HGV_count = PD_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
PD_data_HGV_count.columns = ['HGVSp','PD']
PRSR_data_HGV_count = PRSR_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
PRSR_data_HGV_count.columns = ['HGVSp','PR-SR-SD']
mergeData2 = pd.merge(PD_data_HGV_count,PRSR_data_HGV_count,on='HGVSp',how='outer')
mergeData2 = mergeData2.fillna(0)
mergeData2 = mergeData2[(mergeData2['PR-SR-SD'] - mergeData2['PD'] >= 2)|(mergeData2['PR-SR-SD'] - mergeData2['PD'] <= -2)]
mergeData2.to_csv('IO.HGVSp.39.Count.txt',sep='\t',index=False)


# # Driver Gene

# In[111]:


driverList = []
with open("/amoydx/USER/wujianming/project/Jupyter/IO1-2/driverGeneList",'r') as FA:
    for line in FA:
        line = line.strip()
        driverList.append(line)
PRSR_Group = Result[(Result['最好疗效'] == 'PR')|(Result['最好疗效'] == 'SR')]['厦维编号'].tolist()
PRSR_data = mutInfo[mutInfo['Sample'].isin(PRSR_Group)]
PRSR_data = PRSR_data[PRSR_data['Gene'].isin(driverList)]
SD_Group = Result[Result['最好疗效'] == 'SD']['厦维编号'].tolist()
SD_data = mutInfo[mutInfo['Sample'].isin(SD_Group)]
SD_data = SD_data[SD_data['Gene'].isin(driverList)]
PD_Group = Result[Result['最好疗效'] == 'PD']['厦维编号'].tolist()
PD_data = mutInfo[mutInfo['Sample'].isin(PD_Group)]
PD_data = PD_data[PD_data['Gene'].isin(driverList)]

SD_data_gene_count = SD_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
SD_data_gene_count.columns = ['Gene','SD']
PD_data_gene_count = PD_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
PD_data_gene_count.columns = ['Gene','PD']
PRSR_data_gene_count = PRSR_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
PRSR_data_gene_count.columns = ['Gene','PRSR']
mergeData = pd.merge(PD_data_gene_count,SD_data_gene_count,on='Gene',how='outer')
mergeData = pd.merge(mergeData,PRSR_data_gene_count,on='Gene',how='outer')
mergeData = mergeData.fillna(0)
mergeData.to_csv('Driver.IO.Gene.Count.txt',sep='\t',index=False)

SD_data_HGV_count = SD_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
SD_data_HGV_count.columns = ['HGVSp','SD']
PD_data_HGV_count = PD_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
PD_data_HGV_count.columns = ['HGVSp','PD']
PRSR_data_HGV_count = PRSR_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
PRSR_data_HGV_count.columns = ['HGVSp','PRSR']
mergeData = pd.merge(PD_data_HGV_count,SD_data_HGV_count,on='HGVSp',how='outer')
mergeData = pd.merge(mergeData,PRSR_data_HGV_count,on='HGVSp',how='outer')
mergeData = mergeData.fillna(0)
mergeData.to_csv('Driver.IO.HGVSp.Count.txt',sep='\t',index=False)


# # suppressor Gene

# In[15]:


import xlrd
clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')
TMB_sheet = pd.read_excel('20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])
f = open('Samples.suppressor.cds.txt','w')
f.write("Sample"+'\t'+"Gene"+'\t'+'Tag'+'\t'+"Type"+'\t'+'CDS'+'\n')
book = xlrd.open_workbook('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx')
sheet = book.sheet_by_name('SNVIndelSomatic')
for each in range(sheet.nrows):
    rows = sheet.row_values(each)
    if 'Sample' in rows[0]:
        continue
    if float(rows[10]) < 0.05:
        continue
    CDS = rows[15].split(':')[-2]
    f.write(rows[0]+'\t'+rows[13]+'\t'+rows[11]+'\t'+rows[14]+'\t'+rows[13]+' '+CDS+'\n')
sheet2 = book.sheet_by_name('SNVIndelHotSpot')
for each in range(sheet2.nrows):
    lines = sheet2.row_values(each)
    if 'Sample' in lines[0]:
        continue
    if float(lines[10]) < 0.01:
        continue
    P2 = lines[14].split(':')[-2]
    f.write(lines[0]+'\t'+lines[12]+'\t'+lines[13]+'\t'+lines[12]+' '+P2+'\n')
f.close()

mutInfo = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/IO1-2/Samples.suppressor.cds.txt",sep='\t')
suppressorList = []
with open("/amoydx/USER/wujianming/project/Jupyter/IO1-2/suppressorGeneList",'r') as FA:
    for line in FA:
        line = line.strip()
        suppressorList.append(line)

PRSR_Group = merge_data[(merge_data['最好疗效'] == 'PR')|(merge_data['最好疗效'] == 'SR')|(merge_data['最好疗效'] == 'SD')]['厦维编号'].tolist()
PRSR_data = mutInfo[mutInfo['Sample'].isin(PRSR_Group)]
PRSR_data = PRSR_data.drop_duplicates(subset=['Sample','Gene','Type','CDS'],keep='first')
PD_Group = merge_data[merge_data['最好疗效'] == 'PD']['厦维编号'].tolist()
PD_data = mutInfo[mutInfo['Sample'].isin(PD_Group)]
PD_data = PD_data.drop_duplicates(subset=['Sample','Gene','Type','CDS'],keep='first')


PD_data_gene_count = PD_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
PD_data_gene_count.columns = ['Gene','PD']
PRSR_data_gene_count = PRSR_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
PRSR_data_gene_count.columns = ['Gene','PRSRSD']
mergeData = pd.merge(PD_data_gene_count,PRSR_data_gene_count,on='Gene',how='outer')
mergeData = mergeData.fillna(0)
mergeData.to_csv('Suppressor.IO.Gene.SNP.Count.txt',sep='\t',index=False)

PD_data_HGV_count = PD_data.groupby('CDS',as_index=None).count()[['CDS','Type']]
PD_data_HGV_count.columns = ['CDS','PD']
PRSR_data_HGV_count = PRSR_data.groupby('CDS',as_index=None).count()[['CDS','Type']]
PRSR_data_HGV_count.columns = ['CDS','PRSRSD']
mergeData = pd.merge(PD_data_HGV_count,PRSR_data_HGV_count,on='CDS',how='outer')
mergeData = mergeData.fillna(0)
mergeData = mergeData[(mergeData['PRSRSD'] - mergeData['PD'] >=4)|(mergeData['PRSRSD'] - mergeData['PD'] <= -4)]
mergeData.to_csv('Suppressor.IO.CDS.SNP.Count.txt',sep='\t',index=False)




# # Only nonSynonymous_Substitution 

# In[4]:


import xlrd
clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')

book = xlrd.open_workbook('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx')
somatic_sheet = pd.read_excel(book,'SNVIndelSomatic',index_col = None,na_values= ['9999'])
somatic_filter_data = somatic_sheet[(somatic_sheet['Freq'] >= 0.05) & (somatic_sheet['Tag'] == 'PASS') & (somatic_sheet['Type'] == 'nonSynonymous_Substitution')]
somatic_non_mut = somatic_filter_data.groupby('Sample',as_index=None).count()[['Sample','Gene']]
somatic_non_mut.columns = ['Sample','nonSyn_mut']

hotspot_sheet = pd.read_excel(book,'SNVIndelHotSpot',index_col = None,na_values= ['9999'])
hotspot_filter_data = hotspot_sheet[(hotspot_sheet['Freq'] >= 0.01) & (hotspot_sheet['Tag'] == 'PASS;HotSpot') & (hotspot_sheet['Type'] == 'nonSynonymous_Substitution')]
hotspot_non_mut = hotspot_filter_data.groupby('Sample',as_index=None).count()[['Sample','Gene']]
hotspot_non_mut.columns = ['Sample','nonSyn_mut']
merge_mut = pd.concat([somatic_non_mut,hotspot_non_mut])
merge_mut = merge_mut.groupby('Sample',as_index=False).sum()
data = pd.merge(merge_data,merge_mut,left_on = '厦维编号',right_on = 'Sample')
data = data[['Sample','最好疗效','Response','nonSyn_mut']]
data.to_csv("55_Only_nonSyn.csv",sep=',',index=False)
from sklearn.metrics import roc_auc_score
y_true = data['Response']
y_scores = data['nonSyn_mut']
roc_auc_score(y_true, y_scores)


# # OncoGene

# In[113]:


oncoList = []
with open("/amoydx/USER/wujianming/project/Jupyter/IO1-2/oncogeneList",'r') as FA:
    for line in FA:
        line = line.strip()
        oncoList.append(line)
PRSR_Group = Result[(Result['最好疗效'] == 'PR')|(Result['最好疗效'] == 'SR')]['厦维编号'].tolist()
PRSR_data = mutInfo[mutInfo['Sample'].isin(PRSR_Group)]
PRSR_data = PRSR_data[PRSR_data['Gene'].isin(oncoList)]
SD_Group = Result[Result['最好疗效'] == 'SD']['厦维编号'].tolist()
SD_data = mutInfo[mutInfo['Sample'].isin(SD_Group)]
SD_data = SD_data[SD_data['Gene'].isin(oncoList)]
PD_Group = Result[Result['最好疗效'] == 'PD']['厦维编号'].tolist()
PD_data = mutInfo[mutInfo['Sample'].isin(PD_Group)]
PD_data = PD_data[PD_data['Gene'].isin(oncoList)]

SD_data_gene_count = SD_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
SD_data_gene_count.columns = ['Gene','SD']
PD_data_gene_count = PD_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
PD_data_gene_count.columns = ['Gene','PD']
PRSR_data_gene_count = PRSR_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
PRSR_data_gene_count.columns = ['Gene','PRSR']
mergeData = pd.merge(PD_data_gene_count,SD_data_gene_count,on='Gene',how='outer')
mergeData = pd.merge(mergeData,PRSR_data_gene_count,on='Gene',how='outer')
mergeData = mergeData.fillna(0)
mergeData.to_csv('Onco.IO.Gene.Count.txt',sep='\t',index=False)

SD_data_HGV_count = SD_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
SD_data_HGV_count.columns = ['HGVSp','SD']
PD_data_HGV_count = PD_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
PD_data_HGV_count.columns = ['HGVSp','PD']
PRSR_data_HGV_count = PRSR_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
PRSR_data_HGV_count.columns = ['HGVSp','PRSR']
mergeData = pd.merge(PD_data_HGV_count,SD_data_HGV_count,on='HGVSp',how='outer')
mergeData = pd.merge(mergeData,PRSR_data_HGV_count,on='HGVSp',how='outer')
mergeData = mergeData.fillna(0)
mergeData.to_csv('Onco.IO.HGVSp.Count.txt',sep='\t',index=False)


# # 55 only tumor

# In[179]:


clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')
TMB_sheet = pd.read_excel('20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])


# In[180]:


Type_Filter_list = ["3'UTR","5'UTR","Intronic","FlankingRegion5"]
Tag_Filter_list = ['PASS']
TMB_sheet_Filter = TMB_sheet[~TMB_sheet['Type'].isin(Type_Filter_list)]
#Freq过滤5%
TMB_sheet_Filter = TMB_sheet_Filter[TMB_sheet_Filter['Freq'] >= 0.05]
TMB_sheet_Filter = TMB_sheet_Filter[TMB_sheet_Filter['Tag'].isin(Tag_Filter_list)]


# In[181]:


TMB_count2 = TMB_sheet_Filter.groupby('Sample',as_index=None).count()
TMB_count2['TMB_value_1.46'] = TMB_count2['Library'] / 1.46
TMB_count2['TMB_value_1.16'] = TMB_count2['Library'] / 1.16
TMB_value2 = TMB_count2[['Sample','TMB_value_1.46','TMB_value_1.16']]
sampleList = []
with open('sampleList','r') as FA:
    for line in FA:
        line = line.strip()
        if (line in TMB_value2['Sample'].values) == False:
            append_data = pd.DataFrame({'Sample':line,'TMB_value_1.46':0,'TMB_value_1.16':0},index=[1])
            TMB_value2=TMB_value2.append(append_data,ignore_index=True)
merge = pd.merge(merge_data,TMB_value2,left_on = '厦维编号',right_on = 'Sample')
cutPoint = 10
merge['TMB_Type_1.16'] = merge['TMB_value_1.16'].apply(lambda x: 'TMB High' if x >= cutPoint else 'TMB Low')
merge['TMB_Type_1.46'] = merge['TMB_value_1.46'].apply(lambda x: 'TMB High' if x >= cutPoint else 'TMB Low')


# In[182]:


from sklearn.metrics import roc_auc_score
y_true = merge['Response']
y_scores = merge['TMB_value_1.16']
roc_auc_score(y_true, y_scores)  


# In[276]:


import xlrd
f = open('Samples.55.Protein.txt','w')
f.write("Sample"+'\t'+"Gene"+'\t'+"Type"+'\t'+'HGVSp'+'\n')
book = xlrd.open_workbook('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx')
sheet = book.sheet_by_name('SNVIndelSomatic')
for each in range(sheet.nrows):
    rows = sheet.row_values(each)
    if 'Sample' in rows[0]:
        continue
    #print(rows[0]+'\t'+rows[12]+'\t'+rows[13]+'\t'+rows[14])
    if float(rows[10]) < 0.05 or "Intronic" in rows[14] or "UTR" in rows[14] or "PASS" not in rows[11]:
        continue
    P = rows[15].split(':')[-1]
    f.write(rows[0]+'\t'+rows[13]+'\t'+rows[14]+'\t'+rows[13]+' '+P+'\n')
sheet2 = book.sheet_by_name('SNVIndelHotSpot')
for each in range(sheet2.nrows):
    lines = sheet2.row_values(each)
    if 'Sample' in lines[0]:
        continue
    #print(rows[0]+'\t'+rows[12]+'\t'+rows[13]+'\t'+rows[14])
    if float(lines[10]) < 0.05 or "Intronic" in lines[13] or "UTR" in lines[13] or 'Germline' in lines[11] or 'Polymorphism' in lines[11]:
        continue
    
    P2 = lines[14].split(':')[-1]
    f.write(lines[0]+'\t'+lines[12]+'\t'+lines[13]+'\t'+lines[12]+' '+P2+'\n')
f.close()

mutInfo = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/IO1-2/Samples.55.Protein.txt",sep='\t')
Result = merge[['患者姓名','最好疗效','Response','厦维编号','TMB_value_1.16','TMB_Type_1.16','TMB_value_1.46','TMB_Type_1.46']]
PRSR_Group = Result[(Result['最好疗效'] == 'PR')|(Result['最好疗效'] == 'SR')|(Result['最好疗效'] == 'SD')]['厦维编号'].tolist()
PRSR_data = mutInfo[mutInfo['Sample'].isin(PRSR_Group)]
PD_Group = Result[Result['最好疗效'] == 'PD']['厦维编号'].tolist()
PD_data = mutInfo[mutInfo['Sample'].isin(PD_Group)]


# In[277]:


PD_data_gene_count = PD_data.groupby('Gene',as_in,dex=None).count()[['Gene','Type']]
PD_data_gene_count.columns = ['Gene','PD']
PRSR_data_gene_count = PRSR_data.groupby('Gene',as_index=None).count()[['Gene','Type']]
PRSR_data_gene_count.columns = ['Gene','PR-SR-SD']
mergeData = pd.merge(PD_data_gene_count,PRSR_data_gene_count,on='Gene',how='outer')
mergeData = mergeData.fillna(0)
mergeData = mergeData[(mergeData['PR-SR-SD'] - mergeData['PD'] >= 3)|(mergeData['PR-SR-SD'] - mergeData['PD'] <= -3)]
mergeData.to_csv('/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO.55.Gene.Count.txt',sep='\t',index=False)

PD_data_HGV_count = PD_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
PD_data_HGV_count.columns = ['HGVSp','PD']
PRSR_data_HGV_count = PRSR_data.groupby('HGVSp',as_index=None).count()[['HGVSp','Type']]
PRSR_data_HGV_count.columns = ['HGVSp','PR-SR-SD']
mergeData2 = pd.merge(PD_data_HGV_count,PRSR_data_HGV_count,on='HGVSp',how='outer')
mergeData2 = mergeData2.fillna(0)
mergeData2 = mergeData2[(mergeData2['PR-SR-SD'] - mergeData2['PD'] >= 2)|(mergeData2['PR-SR-SD'] - mergeData2['PD'] <= -2)]
mergeData2.to_csv('/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO.55.HGVSp.Count.txt',sep='\t',index=False)


# # Only noSyn

# In[10]:


import pandas as pd
import xlrd
import os

clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')

TMB_sheet = pd.read_excel('20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])

#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["nonSynonymous_Substitution","FrameShift_Insertion","Nonsense_Mutation","FrameShift_Deletion","nonFrameShift_Deletion"]
TMB_sheet = TMB_sheet[TMB_sheet['Type'].isin(Filter_list)]
#Freq过滤5%
TMB_sheet = TMB_sheet[TMB_sheet['Freq'] >= 0.05]
TMB_sheet = TMB_sheet[['Sample','Type','Tag','Gene']]

#Tag_filter = ['Germline','Polymorphism']
TMB_sheet2 = pd.read_excel('20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])
#TMB_sheet2 = TMB_sheet2[~TMB_sheet2['Tag'].isin(Tag_filter)]
TMB_sheet2 = TMB_sheet2[TMB_sheet2['Freq'] >= 0.01]
TMB_sheet2 = TMB_sheet2[TMB_sheet2['Type'].isin(Filter_list)]
TMB_sheet2 = TMB_sheet2[['Sample','Type','Tag','Gene']]
TMB_merge = pd.concat([TMB_sheet,TMB_sheet2])

TMB_count = TMB_merge.groupby('Sample',as_index=None).count()
TMB_count['nonSyn_mutation'] = TMB_count['Gene']

TMB_value = TMB_count[['Sample','nonSyn_mutation']]
sampleList = []
with open('sampleList','r') as FA:
    for line in FA:
        line = line.strip()
        if (line in TMB_value['Sample'].values) == False:
            append_data = pd.DataFrame({'Sample':line,'nonSyn_mutation':0},index=[1])
            TMB_value=TMB_value.append(append_data,ignore_index=True)
mergeData2 = pd.merge(merge_data,TMB_value,left_on = '厦维编号',right_on = 'Sample')

Result = mergeData2[['患者姓名','最好疗效','Response','厦维编号','nonSyn_mutation']] x

#Result.groupby(['Response','TMB Type'],as_index=False).count()
#mergeData2.to_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/Batch1.TMB.value.39.old.compute.txt",sep='\t',index=False)

from sklearn.metrics import roc_auc_score
y_true = mergeData2['Response']
y_scores = mergeData2['nonSyn_mutation']
roc_auc_score(y_true, y_scores)    


# # 55例only tumor ---- only non-syn

# In[8]:


import pandas as pd
import xlrd
import os

clinical = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/All_Clinical_Info.csv")
sample = pd.read_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/IO12.barcode.name.csv")
merge_data = pd.merge(clinical,sample,left_on = '患者姓名',right_on = '姓名')

TMB_sheet = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])

#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["nonSynonymous_Substitution","nonFrameShift_Deletion","Nonsense_Mutation","FrameShift_Insertion","nonFrameShift_Insertion","FrameShift_Deletion","FrameShift_Substitution"]
TMB_sheet = TMB_sheet[TMB_sheet['Type'].isin(Filter_list)]
#Freq过滤5%
TMB_sheet = TMB_sheet[TMB_sheet['Freq'] >= 0.05]
TMB_sheet = TMB_sheet[['Sample','Type','Tag','Gene']]

Tag_filter = ['PASS']
TMB_sheet2 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])
TMB_sheet2 = TMB_sheet2[TMB_sheet2['Tag'].isin(Tag_filter)]
TMB_sheet2 = TMB_sheet2[TMB_sheet2['Freq'] >= 0.01]
TMB_sheet2 = TMB_sheet2[TMB_sheet2['Type'].isin(Filter_list)]
TMB_sheet2 = TMB_sheet2[['Sample','Type','Tag','Gene']]
TMB_merge = pd.concat([TMB_sheet,TMB_sheet2])

TMB_count = TMB_merge.groupby('Sample',as_index=None).count()
TMB_count['nonSyn_mutation'] = TMB_count['Gene']

TMB_value = TMB_count[['Sample','nonSyn_mutation']]
sampleList = []
with open('sampleList','r') as FA:
    for line in FA:
        line = line.strip()
        if (line in TMB_value['Sample'].values) == False:
            append_data = pd.DataFrame({'Sample':line,'nonSyn_mutation':0},index=[1])
            TMB_value=TMB_value.append(append_data,ignore_index=True)
mergeData2 = pd.merge(merge_data,TMB_value,left_on = '厦维编号',right_on = 'Sample')

Result = mergeData2[['患者姓名','最好疗效','Response','厦维编号','nonSyn_mutation']]

#Result.groupby(['Response','TMB Type'],as_index=False).count()
#mergeData2.to_csv("/amoydx/USER/wujianming/project/Jupyter/IO1-2/Batch1.TMB.value.39.old.compute.txt",sep='\t',index=False)

from sklearn.metrics import roc_auc_score
y_true = mergeData2['Response']
y_scores = mergeData2['nonSyn_mutation']
roc_auc_score(y_true, y_scores)    


# In[9]:


import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
#sns.boxplot(x=mergeData2['Response'],y=mergeData2['TMB_value_1.16'])
ax = sns.boxplot(x=mergeData2['Response'],y=mergeData2['nonSyn_mutation'])
ax = sns.swarmplot(x=mergeData2['Response'],y=mergeData2['nonSyn_mutation'], color=".25")

