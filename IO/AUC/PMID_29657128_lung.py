#!/usr/bin/env python
# coding: utf-8

# In[1]:


from itertools import combinations
from lifelines import KaplanMeierFitter
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from lifelines.statistics import logrank_test
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import os
get_ipython().run_line_magic('matplotlib', 'inline')
pd.options.mode.chained_assignment = None
os.chdir("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/PMID_29657128_lung")


# In[2]:


clinical_data = pd.read_excel('mmc2-clinical.xlsx','Table S2',index_col = None,na_values= ['9999'],skiprows =[0,1])
mut_data = pd.read_excel('mmc3-mutations.xlsx','Variant list_IpiNivo',index_col = None,na_values= ['9999'],skiprows =[0,1])


# In[3]:


def run(GeneName):
    #GeneList = ['BARD1','BLM','BRCA1','BRCA2','RAD51C','MSH6','PMS2','PAPPA2']
    GeneList = GeneName.split('_')
    GeneFilter = mut_data[mut_data['gene_name'].isin(GeneList)]
    GeneFilterSample = GeneFilter.groupby('Patient ID',as_index=False).count()[['Patient ID','gene_name']]
    GeneFilterSample['Type'] = 'Mut'
    MERGE = pd.merge(GeneFilterSample,clinical_data,on='Patient ID',how='outer')
    MERGE = MERGE[['Patient ID','Type','Status','PFS']]
    MERGE = MERGE.fillna('noMut')
    dem5 = MERGE["Type"] == 'Mut'
    ax5 = plt.subplot(111)
    T5 = MERGE["PFS"]
    E5 = MERGE["Status"]
    kmf = KaplanMeierFitter()
    kmf.fit(T5[dem5], event_observed=E5[dem5], label="Mut")
    kmf.plot(ax=ax5,ci_show=False)
    kmf.fit(T5[~dem5], event_observed=E5[~dem5], label="non-Mut")
    kmf.plot(ax=ax5,ci_show=False)
    plt.ylim(0, 1)
    results5 = logrank_test(T5[dem5], T5[~dem5], event_observed_A=E5[dem5], event_observed_B=E5[~dem5])
    pvalue5 = results5.p_value
    pvalue5 = round(pvalue5,4)
    plt.text(30,0.7,"P: "+str(pvalue5))
    plt.title("HRR+MMR+PAPPA2 PFS\n PMID:29657128(75 Patients)")
    #plt.title("BARD1+BLM+BRCA1+BRCA2+MSH6+PMS2+PAPPA2 PFS\n PMID:29657128(75 Patients)")


# In[25]:


def run_Pvalue(GeneName):
    GeneList = GeneName.split('_')
    #GeneList = ['BARD1','BLM','BRCA1','BRCA2','RAD51C','MSH6','PMS2','PAPPA2']
    GeneFilter = mut_data[mut_data['gene_name'].isin(GeneList)]
    GeneFilterSample = GeneFilter.groupby('Patient ID',as_index=False).count()[['Patient ID','gene_name']]
    GeneFilterSample['Type'] = 'Mut'
    MERGE = pd.merge(GeneFilterSample,clinical_data,on='Patient ID',how='outer')
    MERGE = MERGE[['Patient ID','Type','Status','PFS']]
    MERGE = MERGE.fillna('noMut')
    dem5 = MERGE["Type"] == 'Mut'
    T5 = MERGE["PFS"]
    E5 = MERGE["Status"]
    results5 = logrank_test(T5[dem5], T5[~dem5], event_observed_A=E5[dem5], event_observed_B=E5[~dem5])
    pvalue5 = results5.p_value
    pvalue5 = round(pvalue5,4)
    print(GeneName+'\t'+str(pvalue5))


# In[26]:


geneList = ['BARD1','BLM','BRCA1','BRCA2','RAD51C','MSH6','PMS2','PAPPA2']
allList = ['_'.join(x) for i in range(len(geneList)) for x in combinations(geneList, i + 1)]
for combine in allList:
    run_Pvalue(combine)


# In[35]:


run('BARD1_BLM_BRCA1_MSH6_PAPPA2')


# In[576]:


run('BARD1_BLM_BRCA1_BRCA2_MSH6_PMS2_PAPPA2')


# In[37]:


run("BARD1_BRCA1_MSH6_PAPPA2")


# In[579]:


run("ATM_BARD1_BLM_BRCA1_BRCA2_BRIP1_CHEK2_FANCA_FANCC_FANCD2_FANCE_FANCF_FANCG_FANCI_FANCL_FANCM_MRE11A_NBN_PALB2_RAD50_RAD51_RAD51B_RAD51C_RAD51D_RAD54L_MLH1_MSH2_MSH6_PMS1_PMS2_PAPPA2")


# In[ ]:





# # ROC/AUC

# In[64]:


clinical_data['Response_Status'] = clinical_data['Response'].apply(lambda x: 0 if x == 'PD' or x == 'NE' or x == 'SD' else 1)
datas = clinical_data[['Nonsynonymous_Mut','Response_Status','Response']]


# In[65]:


from sklearn.metrics import roc_auc_score
y_true = datas['Response_Status']
y_scores = datas['Nonsynonymous_Mut']
roc_auc_score(y_true, y_scores) 


# # overlap 448gene 

# In[137]:


Filter_data = mut_data[mut_data['nonsynonymous'] == True][['chr','start','ref','alt','gene_name','Patient ID']]
Filter_data = Filter_data.fillna('.')
Filter_data['end'] = Filter_data['start']
Filter_data = Filter_data[['chr','start','end','ref','alt','gene_name','Patient ID']]
Filter_data.to_csv('lung_wes_mutations.txt',sep='\t',index=False,header=False)
#os.system("bedtools intersect -a lung_wes_mutations.txt -b ADXREG.TMB448V2.CDS20.GRCh37_v1.0.1.bed -v > lung_448_Panel_mutations.txt
")


# In[138]:


overlap_448_mutations = pd.read_table("lung_448_Panel_mutations.txt",sep='\t',names=['chr','start','end','ref','alt','gene_name','Patient ID'])


# In[144]:


group_data  = overlap_448_mutations.groupby('Patient ID',as_index=None).count()[['Patient ID','chr']]
group_data.columns=['Patient ID','nonsynonymous_448']
respon_data = clinical_data[['Patient ID','Response_Status','Response']]
merge_data =  pd.merge(group_data,respon_data,on='Patient ID')
merge_data['new_res'] = merge_data['Response'].apply(lambda x: 'PD-SD' if x == 'PD' or x == 'NE' or x == 'SD' else 'CR-PR')
y_true = merge_data['Response_Status']
y_scores = merge_data['nonsynonymous_448']
ax = sns.boxplot(x=merge_data['new_res'],y=merge_data['nonsynonymous_448'])
ax = sns.swarmplot(x=merge_data['new_res'],y=merge_data['nonsynonymous_448'], color=".25")
roc_auc_score(y_true, y_scores) 


# # 组合预测

# In[156]:


clinical_data = pd.read_excel('mmc2-clinical.xlsx','Table S2',index_col = None,na_values= ['9999'],skiprows =[0,1])
mut_data = pd.read_excel('mmc3-mutations.xlsx','Variant list_IpiNivo',index_col = None,na_values= ['9999'],skiprows =[0,1])


# In[184]:


clinical_data = clinical_data[['Patient ID','PFS','Status','Response','Nonsynonymous_Mut','Predicted neoantigen burden']]
clinical_data.sort_values(['Patient ID'],inplace=True)

Indel_number =  mut_data[mut_data['nonsynonymous_indel'] ==  True]
Indel_number_group = Indel_number.groupby('Patient ID',as_index=False).count()[['Patient ID','nonsynonymous_indel']] 
  
geneList = ['TP53','BARD1','BLM','MSH6','PMS2','PAPPA2','CALR','BRCA1','BRCA2','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']
sampleList = clinical_data['Patient ID'].tolist()
sample = pd.Series(sampleList)
sampleDict = {}
for each_sample in sampleList:
    sampleDict[each_sample] = mut_data[mut_data['Patient ID'] == each_sample]['gene_name'].tolist()
for gene in geneList:
    clinical_data[gene] = sample.apply(lambda x: 1 if gene in sampleDict[x] else 0 )
##再对Nonsynonymous_Mut,Predicted neoantigen burden,进行min-max归一化(x-min)/(max-min)

clinical_data['Response_Status'] = clinical_data['Response'].apply(lambda x: 0 if x == 'PD' else 1)
clinical_data['mutation_Nor'] =  (clinical_data['Nonsynonymous_Mut'] - min(clinical_data['Nonsynonymous_Mut'])) / (max(clinical_data['Nonsynonymous_Mut']) - min(clinical_data['Nonsynonymous_Mut']))
#neoantigen
clinical_data['Neo_Nor'] =  (clinical_data['Predicted neoantigen burden'] - min(clinical_data['Predicted neoantigen burden'])) / (max(clinical_data['Predicted neoantigen burden']) - min(clinical_data['Predicted neoantigen burden']))
Feature_data = clinical_data[['TP53','Patient ID','Response','Response_Status','mutation_Nor','Neo_Nor','BARD1','BLM','MSH6','BRCA1','BRCA2','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']]


feaName = 'mutation_Nor+Neo_Nor+BARD1+BLM+MSH6+CALR+BRCA1+BRCA2+JAK1+STK11+PTEN+SMARCA4+ARID2+PBRM1+EGFR+DNMT3A+PAPPA2+KRAS+TP53'
feaList = feaName.split('+')
X = np.array(Feature_data[feaList])
y = np.array(Feature_data['Response_Status'])
#构建训练集和测试集
#X_train,X_test, y_train, y_test = train_test_split(X, y, random_state=6,test_size=0.4)
# clf = LogisticRegression(solver='liblinear',class_weight='balanced').fit(X_train,y_train)
# pre_test = clf.predict(X_test)
# auc_score = roc_auc_score(y_test, pre_test)
# print(feaName+'\t'+str(auc_score))
# print(clf.coef_[0])
# print(clf.intercept_)

from sklearn.model_selection import LeaveOneOut
loo = LeaveOneOut()
#loo.get_n_splits(X)
for train_index, test_index in loo.split(X):
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]
    clf = LogisticRegression(solver='liblinear',class_weight='balanced').fit(X_train,y_train)
    pre_test = clf.predict(X_test)
    print(clf.score)


# In[169]:





# In[135]:


Feature_data['corr'] = 0.17362478 * Feature_data['mutation_Nor']+0.54598512*Feature_data['Neo_Nor']+0.71539614*Feature_data['BARD1']+-1.09893283*Feature_data['BLM']+(0.97524828*Feature_data['BRCA1'])+0.5613922*Feature_data['BRCA2']+(0.11311031*Feature_data['MSH6'])+(-0.323751*Feature_data['CALR'])+0.73334886*Feature_data['JAK1']+-0.86096115*Feature_data['STK11']+(0.60602235*Feature_data['PTEN'])+-0.27334375*Feature_data['SMARCA4']+0.54904736*Feature_data['ARID2']+(0.81202684*Feature_data['PBRM1'])+(-0.17070927*Feature_data['EGFR'])+(0.09797011*Feature_data['DNMT3A']+(1.09066881*Feature_data['PAPPA2'])+(0.09832759*Feature_data['KRAS'])-0.05771383)
Feature_data[['Response_Status','corr']]
Feature_data['corr2'] = Feature_data['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Feature_data['Response_Status'], Feature_data['corr2'])


# # 将两个public数据集合并

# In[ ]:


clinical_data1 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/PMID_29657128_lung/mmc2-clinical.xlsx','Table S2',index_col = None,na_values= ['9999'],skiprows =[0,1])
mut_data1 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/PMID_29657128_lung/mmc3-mutations.xlsx','Variant list_IpiNivo',index_col = None,na_values= ['9999'],skiprows =[0,1])


# In[608]:


clinical_data1 = clinical_data1[['Patient ID','PFS','Status','Response','Nonsynonymous_Mut','Predicted neoantigen burden']]
clinical_data1.sort_values(['Patient ID'],inplace=True)

geneList = ['PIK3CA','TP53','BARD1','BLM','MSH6','PMS2','PAPPA2','CALR','BRCA1','BRCA2','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']
sampleList1 = clinical_data1['Patient ID'].tolist()
sample = pd.Series(sampleList1)
sampleDict1 = {}
for each_sample in sampleList:
    sampleDict1[each_sample] = mut_data1[mut_data1['Patient ID'] == each_sample]['gene_name'].tolist()
for gene in geneList:
    clinical_data1[gene] = sample.apply(lambda x: 1 if gene in sampleDict1[x] else 0 )
##再对Nonsynonymous_Mut,Predicted neoantigen burden,进行min-max归一化(x-min)/(max-min)

clinical_data1['Response_Status'] = clinical_data1['Response'].apply(lambda x: 0 if x == 'PD' else 1)
clinical_data1['mutation_Nor'] =  (clinical_data1['Nonsynonymous_Mut'] - min(clinical_data1['Nonsynonymous_Mut'])) / (max(clinical_data1['Nonsynonymous_Mut']) - min(clinical_data1['Nonsynonymous_Mut']))
#neoantigen
clinical_data1['Neo_Nor'] =  (clinical_data1['Predicted neoantigen burden'] - min(clinical_data1['Predicted neoantigen burden'])) / (max(clinical_data1['Predicted neoantigen burden']) - min(clinical_data1['Predicted neoantigen burden']))
Feature_data1 = clinical_data1[['TP53','Patient ID','Response','Response_Status','mutation_Nor','Neo_Nor','PIK3CA','BARD1','BLM','MSH6','BRCA1','BRCA2','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']]


# In[609]:


clinical_patient2  = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/From_cBIoPortal/cBioPortal-LUAD-MSKCC-Science-2015/data_clinical_patient.txt",sep='\t',skiprows=[0,1,2,3])
clinical_patient2 = clinical_patient2[['PATIENT_ID','PFS_MONTHS','EVENT_TYPE','TREATMENT_BEST_RESPONSE','NONSYNONYMOUS_MUTATION_BURDEN','NEOANTIGEN_BURDEN']]
clinical_sample2 = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/From_cBIoPortal/cBioPortal-LUAD-MSKCC-Science-2015/data_clinical_sample.txt",sep='\t',skiprows=[0,1,2,3])
clinical_sample2 = clinical_sample2[['PATIENT_ID','SAMPLE_ID']]
clinical_merge2 =  pd.merge(clinical_patient2,clinical_sample2,on='PATIENT_ID')
mut_data2 = pd.read_table("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/From_cBIoPortal/cBioPortal-LUAD-MSKCC-Science-2015/data_mutations_mskcc.txt",sep='\t')
filter_list2 = ['RNA','Intron',"5'UTR","3'UTR","3'Flank","5'Flank"]
mut_data2 = mut_data2[~mut_data2['Variant_Classification'].isin(filter_list2)]
geneList2 = ['PIK3CA','TP53','BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']
sampleList2 = clinical_merge2['SAMPLE_ID'].tolist()
sample2 = pd.Series(sampleList2)
sampleDict2 = {}
for each_sample in sampleList2:
    sampleDict2[each_sample] = mut_data2[mut_data2['Tumor_Sample_Barcode'] == each_sample]['Hugo_Symbol'].tolist()
for gene in geneList2:
    clinical_merge2[gene] = sample2.apply(lambda x: 1 if gene in sampleDict2[x] else 0 )
clinical_merge2['Response_Status'] = clinical_merge2['TREATMENT_BEST_RESPONSE'].apply(lambda x: 0 if x == 'POD' else 1)
clinical_merge2['mutation_Nor'] =  (clinical_merge2['NONSYNONYMOUS_MUTATION_BURDEN'] - min(clinical_merge2['NONSYNONYMOUS_MUTATION_BURDEN'])) / (max(clinical_merge2['NONSYNONYMOUS_MUTATION_BURDEN']) - min(clinical_merge2['NONSYNONYMOUS_MUTATION_BURDEN']))
#neoantigen
clinical_merge2['Neo_Nor'] =  (clinical_merge2['NEOANTIGEN_BURDEN'] - min(clinical_merge2['NEOANTIGEN_BURDEN'])) / (max(clinical_merge2['NEOANTIGEN_BURDEN']) - min(clinical_merge2['NEOANTIGEN_BURDEN']))
Feature_data2 = clinical_merge2[['SAMPLE_ID','TREATMENT_BEST_RESPONSE','Response_Status','mutation_Nor','Neo_Nor','PIK3CA','TP53','BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']]
Feature_data2.columns = ['Patient ID','Response','Response_Status','mutation_Nor','Neo_Nor','PIK3CA','TP53','BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']
Feature_data3 = Feature_data2[['Patient ID','Response','Response_Status','mutation_Nor','Neo_Nor','PIK3CA','TP53','BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']]


# In[610]:


Two_Data_set = pd.concat([Feature_data1,Feature_data3])


# In[624]:


#feaName = 'mutation_Nor+Neo_Nor+BARD1+BLM+MSH6+CALR+BRCA2+JAK1+STK11+PTEN+SMARCA4+ARID2+PBRM1+EGFR+DNMT3A+PAPPA2+KRAS+TP53'
#feaList = feaName.split('+')
feaList = ['mutation_Nor','Neo_Nor','PIK3CA','BARD1','BLM','MSH6','CALR','BRCA1','BRCA2','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','DNMT3A','PAPPA2','KRAS','TP53']
X = np.array(Two_Data_set[feaList])
y = np.array(Two_Data_set['Response_Status'])
#构建训练集和测试集
X_train,X_test, y_train, y_test = train_test_split(X, y, random_state=8,test_size=0.2)
# clf = LogisticRegression(solver='liblinear',class_weight='balanced').fit(X_train,y_train)
# pre_test = clf.predict(X_test)
# auc_score = roc_auc_score(y_test, pre_test)
# print(feaName+'\t'+str(auc_score))
# print(clf.coef_[0])
# print(clf.intercept_)

# from sklearn.model_selection import LeaveOneOut
# loo = LeaveOneOut()
# #loo.get_n_splits(X)
# for train_index, test_index in loo.split(X):
#     X_train, X_test = X[train_index], X[test_index]
#     y_train, y_test = y[train_index], y[test_index]
#     clf = LogisticRegression(solver='liblinear',class_weight='balanced').fit(X_train,y_train)
#     pre_test = clf.predict(X_test)
#     print(clf.score)

from sklearn.model_selection import GridSearchCV
from sklearn import svm
parameters = {'solver':['liblinear']}
svc = LogisticRegression()
clf = GridSearchCV(svc, parameters, cv=5)
clf.fit(X_train,y_train)
pre_test = clf.predict_proba(X_test)
roc_auc_score(y_true=y_test, y_score=pre_test[:,1])
##score 0.775

##RandomForest
# from sklearn.ensemble import RandomForestClassifier
# parameters = {'n_estimators':[4,5,6],'max_depth':[3,4,5]}
# svc = RandomForestClassifier()
# clf = GridSearchCV(svc, parameters, cv=5)
# clf.fit(X_train,y_train)
# pre_test = clf.predict_proba(X_test)
# print(roc_auc_score(y_true=y_test, y_score=pre_test[:,1]))


# In[319]:


pre_test


# # 39例tumor-normal作为Testing数据集

# In[626]:


TMB_sheet_39 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])
TMB_sheet2_39 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])
#raw_data_39_germline = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelGermline',index_col = None,na_values= ['9999'])


# In[627]:


#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["3'UTR","5'UTR","Intronic","FlankingRegion5"]
tag_filter = ['PASS']
TMB_sheet_39 = TMB_sheet_39[~TMB_sheet_39['Type'].isin(Filter_list)]
TMB_sheet_39 = TMB_sheet_39[TMB_sheet_39['Tag'].isin(tag_filter)]
#Freq过滤5%
TMB_sheet_39 = TMB_sheet_39[TMB_sheet_39['Freq'] >= 0.05]
TMB_sheet_39 = TMB_sheet_39[['Sample','Type','Tag','Gene']]

Tag_hot_filter = ['PASS;HotSpot']
TMB_sheet2_39 = TMB_sheet2_39[TMB_sheet2_39['Tag'].isin(Tag_hot_filter)]
TMB_sheet2_39 = TMB_sheet2_39[~TMB_sheet2_39['Type'].isin(Filter_list)]
TMB_sheet2_39 = TMB_sheet2_39[TMB_sheet2_39['Freq'] >= 0.01]
TMB_sheet2_39 = TMB_sheet2_39[['Sample','Type','Tag','Gene']]
#data_39_germline = raw_data_55_germline[['Sample','Type','Tag','Gene']]
TMB_merge2 = pd.concat([TMB_sheet_39,TMB_sheet2_39])
#Mutations
Mutations_39 = TMB_merge2.groupby('Sample',as_index=False).count()[['Sample','Type']]
Mutations_39.columns = ['Sample','Mutations']
nomut_data = [{'Sample':'190713-25T','Mutations':0},
              {'Sample':'190713-33T','Mutations':0},
              {'Sample':'20141904H01LF1','Mutations':0},
              {'Sample':'20173679H01TF1','Mutations':0}]
df = pd.DataFrame.from_dict(nomut_data)
Mutations_39_plus4 = pd.concat([Mutations_39,df])

Neo = pd.read_csv('/amoydx/USER/wujianming/project/Jupyter/IO1-2/Neoantigens/Neoanti.tumor.normal.csv',sep=',')
Mut_Neo = pd.merge(Mutations_39_plus4,Neo,left_on='Sample',right_on='SampleName')

Mut_Neo = Mut_Neo[['Sample','Mutations','Type','Response2','SNV_5']]
Mut_Neo.columns = ['Sample','Mutations','Type','Response','Neo']


# In[628]:


sampleDict2 = {}
sampleList2 = Mut_Neo['Sample'].tolist()
geneList = ['PIK3CA','TP53','BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']
samples2 = pd.Series(sampleList2)
for each_sample in sampleList2:
    sampleDict2[each_sample] = TMB_merge2[TMB_merge2['Sample'] == each_sample]['Gene'].tolist()
for gene in geneList:
    Mut_Neo[gene] = samples2.apply(lambda x: 1 if gene in sampleDict2[x] else 0 )

Mut_Neo['mutation_Nor'] =  (Mut_Neo['Mutations'] - min(Mut_Neo['Mutations'])) / (max(Mut_Neo['Mutations']) - min(Mut_Neo['Mutations']))
Mut_Neo['Neo_Nor'] =  (Mut_Neo['Neo'] - min(Mut_Neo['Neo'])) / (max(Mut_Neo['Neo']) - min(Mut_Neo['Neo']))


# In[629]:


testing_X = np.array(Mut_Neo[feaList])
testing_Y = np.array(Mut_Neo['Response'])
pre_test2 = clf.predict_proba(testing_X)
print(roc_auc_score(testing_Y, pre_test2[:,1]))


# In[459]:


######梯度测试一下
#for i in range(1,100):
X_train,X_test, y_train, y_test = train_test_split(X, y,test_size=0.2)
from sklearn.model_selection import GridSearchCV
from sklearn import svm
parameters = {'solver':['liblinear'],'class_weight':['balanced']}
svc = LogisticRegression()
clf = GridSearchCV(svc, parameters, cv=5)
clf.fit(X_train,y_train)
pre_test = clf.predict_proba(X_test)
test = roc_auc_score(y_true=y_test, y_score=pre_test[:,1])
print(clf.best_params_)
testing_X = np.array(Mut_Neo[feaList])
testing_Y = np.array(Mut_Neo['Response'])
pre_test2 = clf.predict_proba(testing_X)
wu_39 = roc_auc_score(testing_Y, pre_test2[:,1])
print(str(test)+'\t'+str(wu_39))

