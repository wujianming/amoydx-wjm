#!/usr/bin/env python
# coding: utf-8

# In[10]:


from sklearn.linear_model import LogisticRegression
from lifelines.statistics import logrank_test
from sklearn.metrics import roc_auc_score
from lifelines import KaplanMeierFitter
from itertools import combinations
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import os
get_ipython().run_line_magic('matplotlib', 'inline')
pd.options.mode.chained_assignment = None
os.chdir("/amoydx/USER/wujianming/project/Jupyter/IO1-2/PFS_Other_Database/ourData")


# In[2]:


clinical_data = pd.read_csv("IO.TMB.Feature.55.csv",sep='\t')
TMB_sheet = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])
    
#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["3'UTR","5'UTR","Intronic","FlankingRegion5"]
tag_filter = ['PASS']
TMB_sheet = TMB_sheet[~TMB_sheet['Type'].isin(Filter_list)]
TMB_sheet = TMB_sheet[TMB_sheet['Tag'].isin(tag_filter)]
#Freq过滤5%
TMB_sheet = TMB_sheet[TMB_sheet['Freq'] >= 0.05]
TMB_sheet = TMB_sheet[['Sample','Type','Tag','Gene']]

Tag_hot_filter = ['PASS;HotSpot']
TMB_sheet2 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190828_ADXTMB448V2-tMut_v0.1.0_55LXSample.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])
TMB_sheet2 = TMB_sheet2[TMB_sheet2['Tag'].isin(Tag_hot_filter)]
TMB_sheet2 = TMB_sheet2[~TMB_sheet2['Type'].isin(Filter_list)]
TMB_sheet2 = TMB_sheet2[TMB_sheet2['Freq'] >= 0.01]
TMB_sheet2 = TMB_sheet2[['Sample','Type','Tag','Gene']]
TMB_merge = pd.concat([TMB_sheet,TMB_sheet2])


# In[19]:


sampleDict = {}
sampleList = clinical_data['厦维编号'].tolist()
geneList = ['BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']
samples = pd.Series(sampleList)
for each_sample in sampleList:
    sampleDict[each_sample] = TMB_merge[TMB_merge['Sample'] == each_sample]['Gene'].tolist()
for gene in geneList:
    clinical_data[gene] = samples.apply(lambda x: 1 if gene in sampleDict[x] else 0 )


# In[21]:


#MERGE = clinical_data[['Sample','最好疗效','Response','mutations','Germline_Somatic_Indel_mut_number','Somatic_Indel_mut_number','Neo_RNAdata_DNAvcfSNV_5','Only_nonSyn_mut','BARD1','BLM','BRCA1','BRCA2','RAD51C','MSH6','PMS2','PAPPA2','TP53']]
MERGE = clinical_data[['Sample','最好疗效','Response','mutations','Germline_Somatic_Indel_mut_number','Somatic_Indel_mut_number','Neo_RNAdata_DNAvcfSNV_5','Only_nonSyn_mut','BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']]
MERGE.dropna(subset=['Neo_RNAdata_DNAvcfSNV_5'],inplace=True)


# In[23]:


MERGE['mutation_Nor'] =  (MERGE['mutations'] - min(MERGE['mutations'])) / (max(MERGE['mutations']) - min(MERGE['mutations']))
#Germline+Somatic indel number
MERGE['GS_indel_Nor'] =  (MERGE['Germline_Somatic_Indel_mut_number'] - min(MERGE['Germline_Somatic_Indel_mut_number'])) / (max(MERGE['Germline_Somatic_Indel_mut_number']) - min(MERGE['Germline_Somatic_Indel_mut_number']))
#Somatic Indel number
MERGE['S_indel_Nor'] =  (MERGE['Somatic_Indel_mut_number'] - min(MERGE['Somatic_Indel_mut_number'])) / (max(MERGE['Somatic_Indel_mut_number']) - min(MERGE['Somatic_Indel_mut_number']))
#neoantigen
MERGE['Neo_Nor'] =  (MERGE['Neo_RNAdata_DNAvcfSNV_5'] - min(MERGE['Neo_RNAdata_DNAvcfSNV_5'])) / (max(MERGE['Neo_RNAdata_DNAvcfSNV_5']) - min(MERGE['Neo_RNAdata_DNAvcfSNV_5']))
#only nonSyn mut
MERGE['only_nonSyn_Nor'] =  (MERGE['Only_nonSyn_mut'] - min(MERGE['Only_nonSyn_mut'])) / (max(MERGE['Only_nonSyn_mut']) - min(MERGE['Only_nonSyn_mut']))
#Feature_data = MERGE[['Sample','最好疗效','Response','mutation_Nor','GS_indel_Nor','S_indel_Nor','Neo_Nor','only_nonSyn_Nor','BARD1','BLM','BRCA1','BRCA2','RAD51C','MSH6','PMS2','PAPPA2','TP53']]
Feature_data = MERGE[['Sample','最好疗效','Response','mutation_Nor','GS_indel_Nor','S_indel_Nor','Neo_Nor','only_nonSyn_Nor','BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']]
Feature_data['Response_2'] = Feature_data['最好疗效'].apply(lambda x: 0 if x == 'PD' or x == 'SD' else 1)


# In[409]:


from sklearn.model_selection import train_test_split  
#f = open('test.result.txt','w')
def run(feaName):
    feaList = feaName.split('+')
    #X = Feature_data[['BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','TP53']]
    X = Feature_data[feaList]
    y = Feature_data['Response']
    #构建训练集和测试集
    X_train,X_test, y_train, y_test = train_test_split(X, y, random_state=6,test_size=0.1)
    clf = LogisticRegression(solver='liblinear',class_weight='balanced').fit(X_train,y_train)
    pre_test = clf.predict(X_train)
    auc_score = roc_auc_score(y_train, pre_test)
    #f.write(feaName+'\t'+str(auc_score)+'\n')
    print(feaName+'\t'+str(auc_score))
    print(clf.coef_[0])
    print(clf.intercept_)


# In[15]:


# geneList = ['Response','mutation_Nor','GS_indel_Nor','S_indel_Nor','Neo_Nor','only_nonSyn_Nor','BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','PIK3CA']
# allList = ['+'.join(x) for i in range(len(geneList)) for x in combinations(geneList, i + 1)]
# for combine in allList:
#     run(combine)


# In[410]:


run('mutation_Nor+Neo_Nor+BARD1+BLM+BRCA1+BRCA2+MSH6+CALR+JAK1+STK11+PTEN+SMARCA4+ARID2+PBRM1+EGFR+DNMT3A+PAPPA2+KRAS')
#run('mutation_Nor+Neo_Nor+BARD1+BLM+BRCA1+MSH6+CALR+JAK1+STK11+SMARCA4+ARID2+PBRM1+EGFR+DNMT3A')


# In[419]:


#Feature_data['corr'] = -0.24601023 * Feature_data['mutation_Nor']+0.13324629*Feature_data['Neo_Nor']+0.37192396*Feature_data['BARD1']+0.522691*Feature_data['BLM']+(-0.50910116*Feature_data['BRCA1'])+0.19394394*Feature_data['BRCA2']+(-0.45938078*Feature_data['MSH6'])+(-0.60362655*Feature_data['CALR'])+0*Feature_data['JAK1']+0.6395658*Feature_data['STK11']+(-0.1790578*Feature_data['PTEN'])+0*Feature_data['SMARCA4']+0.43*Feature_data['ARID2']+(-0.14720544*Feature_data['PBRM1'])+(-0.4*Feature_data['EGFR'])+(-1.11)*Feature_data['DNMT3A']+(-0.41277043*Feature_data['PAPPA2'])+(0.20549203*Feature_data['KRAS'])+0.14905652
Feature_data['corr'] = 0.00847979 * Feature_data['mutation_Nor']+0.41274661*Feature_data['Neo_Nor']+0.38723267*Feature_data['BARD1']+-0.76574367*Feature_data['BLM']+(0.75322509*Feature_data['BRCA1'])+0.51243095*Feature_data['BRCA2']+(0*Feature_data['MSH6'])+(-0.63467014*Feature_data['CALR'])+0.72782231*Feature_data['JAK1']+-1.32523022*Feature_data['STK11']+(0.56421216*Feature_data['PTEN'])+-0.30249331*Feature_data['SMARCA4']+0.4738086*Feature_data['ARID2']+(0.37665355*Feature_data['PBRM1'])+(-0.1873022*Feature_data['EGFR'])+(-0.11981789*Feature_data['DNMT3A'])+(1.02997194*Feature_data['PAPPA2'])+(0.07539963*Feature_data['KRAS'])+0.23915592
Feature_data['corr2'] = Feature_data['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Feature_data['Response'], Feature_data['corr2'])


# In[422]:


#Feature_data['corr'] = -0.24601023 * Feature_data['mutation_Nor']+0.13324629*Feature_data['Neo_Nor']+0.37192396*Feature_data['BARD1']+0.522691*Feature_data['BLM']+(-0.50910116*Feature_data['BRCA1'])+0.19394394*Feature_data['BRCA2']+(-0.45938078*Feature_data['MSH6'])+(-0.60362655*Feature_data['CALR'])+0*Feature_data['JAK1']+0.6395658*Feature_data['STK11']+(-0.1790578*Feature_data['PTEN'])+0*Feature_data['SMARCA4']+0.43*Feature_data['ARID2']+(-0.14720544*Feature_data['PBRM1'])+(-0.4*Feature_data['EGFR'])+(-1.11)*Feature_data['DNMT3A']+(-0.41277043*Feature_data['PAPPA2'])+(0.20549203*Feature_data['KRAS'])+0.14905652
Feature_data['corr'] = 0.03533979 * Feature_data['mutation_Nor']+0.42930534*Feature_data['Neo_Nor']+0.63790297*Feature_data['BARD1']+-0.68081553*Feature_data['BLM']+(0.74332172*Feature_data['BRCA1'])+0.52981923*Feature_data['BRCA2']+(0*Feature_data['MSH6'])+(-0.48678383*Feature_data['CALR'])+0.72558418*Feature_data['JAK1']+-1.16915109*Feature_data['STK11']+(0.56585197*Feature_data['PTEN'])+-0.0189411*Feature_data['SMARCA4']+0.52841762*Feature_data['ARID2']+(0.83665604*Feature_data['PBRM1'])+(-0.05915863*Feature_data['EGFR'])+(-0.09776049*Feature_data['DNMT3A']+(1.0528755*Feature_data['PAPPA2'])+(-0.08891686*Feature_data['KRAS'])+0.0688328)
Feature_data['corr2'] = Feature_data['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Feature_data['Response'], Feature_data['corr2'])


# In[424]:


Feature_data['corr'] = 0.17362478 * Feature_data['mutation_Nor']+0.54598512*Feature_data['Neo_Nor']+0.71539614*Feature_data['BARD1']+-1.09893283*Feature_data['BLM']+(0.97524828*Feature_data['BRCA1'])+0.5613922*Feature_data['BRCA2']+(0.11311031*Feature_data['MSH6'])+(-0.323751*Feature_data['CALR'])+0.73334886*Feature_data['JAK1']+-0.86096115*Feature_data['STK11']+(0.60602235*Feature_data['PTEN'])+-0.27334375*Feature_data['SMARCA4']+0.54904736*Feature_data['ARID2']+(0.81202684*Feature_data['PBRM1'])+(-0.17070927*Feature_data['EGFR'])+(0.09797011*Feature_data['DNMT3A']+(1.09066881*Feature_data['PAPPA2'])+(0.09832759*Feature_data['KRAS'])-0.05771383)
Feature_data['corr2'] = Feature_data['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Feature_data['Response'], Feature_data['corr2'])


# # 39例tumor-normal样本（only-tumor存在假阳性突变）
# 

# In[451]:


TMB_sheet_39 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelSomatic',index_col = None,na_values= ['9999'])
#Type过滤5'UTR， 3'UTR， Intronic
Filter_list = ["3'UTR","5'UTR","Intronic","FlankingRegion5"]
tag_filter = ['PASS']
TMB_sheet_39 = TMB_sheet_39[~TMB_sheet_39['Type'].isin(Filter_list)]
TMB_sheet_39 = TMB_sheet_39[TMB_sheet_39['Tag'].isin(tag_filter)]
#Freq过滤5%
TMB_sheet_39 = TMB_sheet_39[TMB_sheet_39['Freq'] >= 0.05]
TMB_sheet_39 = TMB_sheet_39[['Sample','Type','Tag','Gene']]

Tag_hot_filter = ['PASS;HotSpot']
TMB_sheet2_39 = pd.read_excel('/amoydx/USER/wujianming/project/Jupyter/IO1-2/20190902_1409_ADXTMB448V2-ptMut_v0.1.4.xlsx','SNVIndelHotSpot',index_col = None,na_values= ['9999'])
TMB_sheet2_39 = TMB_sheet2_39[TMB_sheet2_39['Tag'].isin(Tag_hot_filter)]
TMB_sheet2_39 = TMB_sheet2_39[~TMB_sheet2_39['Type'].isin(Filter_list)]
TMB_sheet2_39 = TMB_sheet2_39[TMB_sheet2_39['Freq'] >= 0.01]
TMB_sheet2_39 = TMB_sheet2_39[['Sample','Type','Tag','Gene']]
TMB_merge2 = pd.concat([TMB_sheet_39,TMB_sheet2_39])
#Mutations
Mutations_39 = TMB_merge2.groupby('Sample',as_index=False).count()[['Sample','Type']]
Mutations_39.columns = ['Sample','Mutations']
nomut_data = [{'Sample':'190713-25T','Mutations':0},
              {'Sample':'190713-33T','Mutations':0},
              {'Sample':'20141904H01LF1','Mutations':0},
              {'Sample':'20173679H01TF1','Mutations':0}]
df = pd.DataFrame.from_dict(nomut_data)
Mutations_39_plus4 = pd.concat([Mutations_39,df])

Neo = pd.read_csv('/amoydx/USER/wujianming/project/Jupyter/IO1-2/Neoantigens/Neoanti.tumor.normal.csv',sep=',')
Mut_Neo = pd.merge(Mutations_39_plus4,Neo,left_on='Sample',right_on='SampleName')

Mut_Neo = Mut_Neo[['Sample','Mutations','Type','Response2','SNV_5']]
Mut_Neo.columns = ['Sample','Mutations','Type','Response','Neo']

sampleDict2 = {}
sampleList2 = Mut_Neo['Sample'].tolist()
geneList = ['TP53','BARD1','BLM','BRCA1','BRCA2','MSH6','PMS2','PAPPA2','CALR','JAK1','STK11','PTEN','SMARCA4','ARID2','PBRM1','EGFR','KRAS','DNMT3A']
samples2 = pd.Series(sampleList2)
for each_sample in sampleList2:
    sampleDict2[each_sample] = TMB_merge2[TMB_merge2['Sample'] == each_sample]['Gene'].tolist()
for gene in geneList:
    Mut_Neo[gene] = samples2.apply(lambda x: 1 if gene in sampleDict2[x] else 0 )

Mut_Neo['mutation_Nor'] =  (Mut_Neo['Mutations'] - min(Mut_Neo['Mutations'])) / (max(Mut_Neo['Mutations']) - min(Mut_Neo['Mutations']))
Mut_Neo['Neo_Nor'] =  (Mut_Neo['Neo'] - min(Mut_Neo['Neo'])) / (max(Mut_Neo['Neo']) - min(Mut_Neo['Neo']))


# In[443]:


##把测试集中不是非小细胞肺癌的样本去掉
noNSCLC_list = []


# In[429]:


feaName = 'mutation_Nor+Neo_Nor+BARD1+BLM+BRCA1+BRCA2+MSH6+CALR+JAK1+STK11+PTEN+SMARCA4+ARID2+PBRM1+EGFR+DNMT3A+PAPPA2+KRAS+TP53'
feaList = feaName.split('+')
X = Mut_Neo[feaList]
y = Mut_Neo['Response']
#构建训练集和测试集
X_train,X_test, y_train, y_test = train_test_split(X, y, random_state=6,test_size=0.3)
clf = LogisticRegression(solver='liblinear',class_weight='balanced').fit(X_train,y_train)
pre_test = clf.predict(X_test)
auc_score = roc_auc_score(y_test, pre_test)
#f.write(feaName+'\t'+str(auc_score)+'\n')
print(feaName+'\t'+str(auc_score))
print(clf.coef_[0])
print(clf.intercept_)


# In[407]:


Mut_Neo['corr'] = -0.14882343 * Mut_Neo['mutation_Nor']+0.83219386*Mut_Neo['Neo_Nor']+0.3356241*Mut_Neo['BARD1']+0*Mut_Neo['BLM']+(-0.44987617*Mut_Neo['BRCA1'])+0*Mut_Neo['BRCA2']+(-0.52526372*Mut_Neo['MSH6'])+(0*Mut_Neo['CALR'])+0*Mut_Neo['JAK1']+0*Mut_Neo['STK11']+(0*Mut_Neo['PTEN'])+0.3*Mut_Neo['SMARCA4']+0*Mut_Neo['ARID2']+(0*Mut_Neo['PBRM1'])+(0.34*Mut_Neo['EGFR'])+(-0.22*Mut_Neo['DNMT3A']+(-0.35*Mut_Neo['PAPPA2'])+(0.3761*Mut_Neo['KRAS'])+0.28380331)
#Feature_data['corr'] = -0.23486225 * Feature_data['mutation_Nor']+0.14985326*Feature_data['Neo_Nor']+0.28910715*Feature_data['BARD1']+(0.48639829*Feature_data['BLM'])+(-0.48758446*Feature_data['BRCA1'])+( -0.44934406*Feature_data['MSH6'])+(-0.51932894*Feature_data['CALR'])+0*Feature_data['JAK1']+0.61658467*Feature_data['STK11']+(-0.06075735*Feature_data['SMARCA4'])+0.4441461*Feature_data['ARID2']+(-0.44934406)*Feature_data['PBRM1']+(-0.17883752*Feature_data['EGFR'])+(-1.1018994*Feature_data['DNMT3A'] + )
#Feature_data[['corr','Response']]
Mut_Neo['corr2'] = Mut_Neo['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Mut_Neo['Response'], Mut_Neo['corr2'])


# In[421]:


#Mut_Neo['corr'] = -0.18369286 * Mut_Neo['mutation_Nor']+0.40881753*Mut_Neo['Neo_Nor']+0.34226404*Mut_Neo['BARD1']+0*Mut_Neo['BLM']+(-0.45537028*Mut_Neo['BRCA1'])+0*Mut_Neo['BRCA2']+(-0.5454788*Mut_Neo['MSH6'])+(0*Mut_Neo['CALR'])+0*Mut_Neo['JAK1']+0*Mut_Neo['STK11']+(0*Mut_Neo['PTEN'])+0.31575687*Mut_Neo['SMARCA4']+0*Mut_Neo['ARID2']+(0*Mut_Neo['PBRM1'])+(0.35210531*Mut_Neo['EGFR'])+(-0.23093551*Mut_Neo['DNMT3A']+(-0.50223892*Mut_Neo['PAPPA2'])+(0.3761*Mut_Neo['KRAS'])-0.05347751)
Mut_Neo['corr'] = 0.00847979 * Mut_Neo['mutation_Nor']+0.41274661*Mut_Neo['Neo_Nor']+0.38723267*Mut_Neo['BARD1']+-0.76574367*Mut_Neo['BLM']+(0.75322509*Mut_Neo['BRCA1'])+0.51243095*Mut_Neo['BRCA2']+(0*Mut_Neo['MSH6'])+(-0.63467014*Mut_Neo['CALR'])+0.72782231*Mut_Neo['JAK1']+-1.32523022*Mut_Neo['STK11']+(0.56421216*Mut_Neo['PTEN'])+-0.30249331*Mut_Neo['SMARCA4']+0.4738086*Mut_Neo['ARID2']+(0.37665355*Mut_Neo['PBRM1'])+(-0.1873022*Mut_Neo['EGFR'])+(-0.11981789*Mut_Neo['DNMT3A'])+(1.02997194*Mut_Neo['PAPPA2'])+(0.07539963*Mut_Neo['KRAS'])+0.23915592
#Feature_data[['corr','Response']]
Mut_Neo['corr2'] = Mut_Neo['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Mut_Neo['Response'], Mut_Neo['corr2'])


# In[423]:


#Mut_Neo['corr'] = -0.18369286 * Mut_Neo['mutation_Nor']+0.40881753*Mut_Neo['Neo_Nor']+0.34226404*Mut_Neo['BARD1']+0*Mut_Neo['BLM']+(-0.45537028*Mut_Neo['BRCA1'])+0*Mut_Neo['BRCA2']+(-0.5454788*Mut_Neo['MSH6'])+(0*Mut_Neo['CALR'])+0*Mut_Neo['JAK1']+0*Mut_Neo['STK11']+(0*Mut_Neo['PTEN'])+0.31575687*Mut_Neo['SMARCA4']+0*Mut_Neo['ARID2']+(0*Mut_Neo['PBRM1'])+(0.35210531*Mut_Neo['EGFR'])+(-0.23093551*Mut_Neo['DNMT3A']+(-0.50223892*Mut_Neo['PAPPA2'])+(0.3761*Mut_Neo['KRAS'])-0.05347751)
Mut_Neo['corr'] = 0.03533979 * Mut_Neo['mutation_Nor']+0.42930534*Mut_Neo['Neo_Nor']+0.63790297*Mut_Neo['BARD1']+-0.68081553*Mut_Neo['BLM']+(0.74332172*Mut_Neo['BRCA1'])+0.52981923*Mut_Neo['BRCA2']+(0*Mut_Neo['MSH6'])+(-0.48678383*Mut_Neo['CALR'])+0.72558418*Mut_Neo['JAK1']+-1.16915109*Mut_Neo['STK11']+(0.56585197*Mut_Neo['PTEN'])+-0.0189411*Mut_Neo['SMARCA4']+0.52841762*Mut_Neo['ARID2']+(0.83665604*Mut_Neo['PBRM1'])+(-0.05915863*Mut_Neo['EGFR'])+(-0.09776049*Mut_Neo['DNMT3A']+(1.0528755*Mut_Neo['PAPPA2'])+(-0.08891686*Mut_Neo['KRAS'])+0.0688328)
#Feature_data[['corr','Response']]
Mut_Neo['corr2'] = Mut_Neo['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Mut_Neo['Response'], Mut_Neo['corr2'])


# In[425]:


Mut_Neo['corr'] = 0.17362478 * Mut_Neo['mutation_Nor']+0.54598512*Mut_Neo['Neo_Nor']+0.71539614*Mut_Neo['BARD1']+-1.09893283*Mut_Neo['BLM']+(0.97524828*Mut_Neo['BRCA1'])+0.5613922*Mut_Neo['BRCA2']+(0.11311031*Mut_Neo['MSH6'])+(-0.323751*Mut_Neo['CALR'])+0.73334886*Mut_Neo['JAK1']+-0.86096115*Mut_Neo['STK11']+(0.60602235*Mut_Neo['PTEN'])+-0.27334375*Mut_Neo['SMARCA4']+0.54904736*Mut_Neo['ARID2']+(0.81202684*Mut_Neo['PBRM1'])+(-0.17070927*Mut_Neo['EGFR'])+(0.09797011*Mut_Neo['DNMT3A']+(1.09066881*Mut_Neo['PAPPA2'])+(0.09832759*Mut_Neo['KRAS'])-0.05771383)
#Feature_data[['corr','Response']]
Mut_Neo['corr2'] = Mut_Neo['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Mut_Neo['Response'], Mut_Neo['corr2'])


# # 添加TP53

# In[433]:


Mut_Neo['corr'] = -0.13070683 * Mut_Neo['mutation_Nor']+-0.60302276*Mut_Neo['Neo_Nor']+0.76487705*Mut_Neo['BARD1']+0.39801678*Mut_Neo['BLM']+(-0.81515257*Mut_Neo['BRCA1'])+0.69559065*Mut_Neo['BRCA2']+(0.0912783*Mut_Neo['MSH6'])+(1.0554914*Mut_Neo['CALR'])+0.64553927*Mut_Neo['JAK1']+0.37734101*Mut_Neo['STK11']+(0.55140441*Mut_Neo['PTEN'])+0.51302663*Mut_Neo['SMARCA4']+-0.47293503*Mut_Neo['ARID2']+(-0.52869456*Mut_Neo['PBRM1'])+(-0.19513775*Mut_Neo['EGFR'])+(0.53144183*Mut_Neo['DNMT3A']+(1.21383114*Mut_Neo['PAPPA2'])+(1.87839651*Mut_Neo['KRAS'])+(-0.15820358*Mut_Neo['TP53'])-0.23498507)
Mut_Neo['corr2'] = Mut_Neo['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Mut_Neo['Response'], Mut_Neo['corr2'])


# # 添加TP53，去除BRCA1

# In[460]:


Mut_Neo['corr'] = -0.07923974 * Mut_Neo['mutation_Nor']-0.32110714*Mut_Neo['Neo_Nor']+0.62536597*Mut_Neo['BARD1']+0.16694654*Mut_Neo['BLM']+(0*Mut_Neo['MSH6'])+(0.68193894*Mut_Neo['CALR'])+(1.09041284*Mut_Neo['BRCA2'])+0.3568774*Mut_Neo['JAK1']+0.29711458*Mut_Neo['STK11']+(0.4387655*Mut_Neo['PTEN'])+0.28603775*Mut_Neo['SMARCA4']-0.51807425*Mut_Neo['ARID2']+(-0.36331218*Mut_Neo['PBRM1'])+(-0.21533162*Mut_Neo['EGFR'])+(0.33385312*Mut_Neo['DNMT3A']+(1.04298682*Mut_Neo['PAPPA2'])+(1.67302379*Mut_Neo['KRAS'])+(-0.15664253*Mut_Neo['TP53'])-0.25187684)
Mut_Neo['corr2'] = Mut_Neo['corr'].apply(lambda x: 0 if x < 0.5 else 1)
roc_auc_score(Mut_Neo['Response'], Mut_Neo['corr2'])

